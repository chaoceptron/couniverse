/*
 *  Copyright (c) 2001 Sun Microsystems, Inc.  All rights
 *  reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  1. Redistributions of source code must retain the above copyright
 *  notice, this list of conditions and the following disclaimer.
 *
 *  2. Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in
 *  the documentation and/or other materials provided with the
 *  distribution.
 *
 *  3. The end-user documentation included with the redistribution,
 *  if any, must include the following acknowledgment:
 *  "This product includes software developed by the
 *  Sun Microsystems, Inc. for Project JXTA."
 *  Alternately, this acknowledgment may appear in the software itself,
 *  if and wherever such third-party acknowledgments normally appear.
 *
 *  4. The names "Sun", "Sun Microsystems, Inc.", "JXTA" and "Project JXTA" must
 *  not be used to endorse or promote products derived from this
 *  software without prior written permission. For written
 *  permission, please contact Project JXTA at http://www.jxta.org.
 *
 *  5. Products derived from this software may not be called "JXTA",
 *  nor may "JXTA" appear in their name, without prior written
 *  permission of Sun.
 *
 *  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED.  IN NO EVENT SHALL SUN MICROSYSTEMS OR
 *  ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
 *  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 *  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 *  SUCH DAMAGE.
 *  ========================================================
 *
 *  This software consists of voluntary contributions made by many
 *  individuals on behalf of Project JXTA.  For more
 *  information on Project JXTA, please see
 *  <http://www.jxta.org/>.
 *
 *  This license is based on the BSD license adopted by the Apache Foundation.
 *
 *  $Id: Rendezvous.java,v 1.1 2007/07/31 13:37:41 xliska Exp $
 */

package rendezvous;

import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.BufferedReader;
import java.util.Enumeration;

import net.jxta.membership.MembershipService;
import net.jxta.credential.Credential;
import net.jxta.credential.AuthenticationCredential;
import net.jxta.peergroup.PeerGroup;
import net.jxta.peergroup.PeerGroupFactory;
import net.jxta.exception.PeerGroupException;
import net.jxta.rendezvous.RendezVousService;
import net.jxta.rendezvous.RendezvousListener;
import net.jxta.rendezvous.RendezvousEvent;
import net.jxta.discovery.DiscoveryService;
import net.jxta.protocol.PeerAdvertisement;
import net.jxta.platform.Application;
import net.jxta.id.ID;
import net.jxta.id.IDFactory;
import net.jxta.peer.PeerID;

import net.jxta.impl.membership.pse.StringAuthenticator;

/**
 *  This is a very simple application, which can be used as an app to run a
 *  rendezvous.
 */
public class Rendezvous implements RendezvousListener {
    
    PeerGroup netPeerGroup = null;
    DiscoveryService discovery = null;
    RendezVousService rendezvous = null;
    
    boolean shutdown = false;
    
    public static final String CONFIG_FILE = ".rdvrc";
    
    // The main thread will keep running and keep holding a reference
    // to JXTA until the quit command comes. When the quit command is
    // received, *this* application will clear all references to JXTA
    // and terminate. This does not necessarily mean that JXTA will
    // shutdown; there's no shutdown API in JXTA. Another application
    // may still have a reference to JXTA.
    // If JXTA was not help running by anything else than
    // this object, then, it will shutdown.
    // Note that this object is sharing ist reference to the netPeerGroup
    // with the main application, if there's one. So either that application
    // or this object can alone put an end to JXTA.
    
    // NOTE: For security reasons, there is not yet a mechanism to send
    // the quit command.
    
    public static void main(String args[]) {
        
        Thread.currentThread().setName( Rendezvous.class.getName() + ".main()" );
        
        Rendezvous rend = new Rendezvous( );
        rend.waitForQuit();
        rend.releaseJxta();
    }
    
    public Rendezvous( ) {
        
        try {
            // create, and Start the default jxta NetPeerGroup
            System.out.println("Starting JXTA ....");
            netPeerGroup = PeerGroupFactory.newNetPeerGroup();
            discovery = netPeerGroup.getDiscoveryService();
            rendezvous = netPeerGroup.getRendezVousService();
            
            MembershipService membership = netPeerGroup.getMembershipService();
            
            Credential cred = membership.getDefaultCredential();
            
            try {
                if( null == cred ) {
                    AuthenticationCredential authCred = new AuthenticationCredential( netPeerGroup, "StringAuthentication", null );
                    
                    StringAuthenticator auth = null;
                    try {
                        auth = (StringAuthenticator) membership.apply( authCred );
                    } catch( Exception failed ) {
                        ;
                    }
                    
                    if( null != auth ) {
                        auth.setAuth1_KeyStorePassword( System.getProperty( "net.jxta.tls.password", "").toCharArray() );
                        auth.setAuth2Identity( netPeerGroup.getPeerID() );
                        auth.setAuth3_IdentityPassword( System.getProperty( "net.jxta.tls.password", "").toCharArray() );
                        
                        if( auth.isReadyForJoin() ) {
                            membership.join( auth );
                            System.out.println("Authenticated");
                        }
                    }
                }
            } catch( Exception failed ) {
                System.err.println("Authentication failed");
            }
            
            rendezvous.addListener(this);
            System.out.println("Registered for rendezvous events");
            
            String initialApp = getConfig();
            if (initialApp != null) {
                startInitApp(initialApp, netPeerGroup);
            }
            System.out.println("JXTA started ....");
        } catch (PeerGroupException e) {
            // could not instantiate the group, print the stack and exit
            System.err.println("fatal error : group creation failure");
            e.printStackTrace(System.err);
            System.exit(1);
        }
    }
    
    /**
     * {@inheritDoc}
     **/
    public void rendezvousEvent(RendezvousEvent event) {
        
        System.out.println( "{" + idToName(event.getPeer()) + "} " + event.toString() );
        
        printStats();
    }
    
    private void printStats() {
        int count= rendezvous.getConnectedPeerIDs().size();
        
        System.out.println( "\t" + count  + " Clients Connected, ThreadCount : "+Thread.activeCount());
    }
    
    // No quit command can be received yet. Therefore, we just wait forever.
    protected synchronized void waitForQuit() {
        while ( !shutdown ) {
            try {
                wait();
            } catch (InterruptedException ie) {
                Thread.interrupted();
            }
        }
    }
    
    protected void releaseJxta() {
        rendezvous.removeListener(this);
        System.out.println("Unreferencing net peer group");
        netPeerGroup.unref();
    }
    
    private static void startInitApp(String appClassName, PeerGroup group) {
        Application app = null;
        
        try {
            // find the class
            Class appClass = Class.forName(appClassName);
            app = (Application) appClass.newInstance();
            app.init(group, null, null);
            app.startApp(new String[0]);
            System.out.println(appClassName + " is started");
        } catch (Throwable e) {
            System.err.println("Cannot start : " + appClassName );
            e.printStackTrace( System.err );
            return;
        }
    }
    
    private static String getConfig() {
        
        try {
            InputStreamReader in = new InputStreamReader(new FileInputStream(CONFIG_FILE));
            BufferedReader reader = new BufferedReader(in);
            return reader.readLine();
            
        } catch (Exception ez1) {
            return null;
        }
    }
    
    // given a peerid string, retrun the peername
    protected String idToName(String id) {
        
        Enumeration res;
        
        try {
            res = discovery.getLocalAdvertisements(discovery.PEER, "PID", id);
            if ((null == res) || (!res.hasMoreElements()))
                return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        
        while (res.hasMoreElements()) {
            PeerAdvertisement peer=(PeerAdvertisement)res.nextElement();
            return peer.getName();
        }
        
        return "";
    }
}

