package utils;

/**
 * TODO: Description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 30.8.2007
 * Time: 17:09:01
 */
public interface ShimGenerator {

    public Shim generateShim();
    
}
