package Monitoring;

/**
 * Created by IntelliJ IDEA.
 * User: xsuchom1
 * Date: Nov 12, 2008
 * Time: 6:36:37 PM
 * To change this template use File | Settings | File Templates.
 */
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import MediaAppFactory.MediaApplication;
import Monitoring.MediaApplicationMonitorListener;
import NetworkRepresentation.*;
import org.apache.log4j.Logger;
import utils.ApplicationProxyJNI;
import utils.TimeUtils;
import utils.ApplicationProxy;

public class MediaApplicationMonitor {
    static Logger logger = Logger.getLogger(MediaApplicationMonitor.class);

    private ConcurrentHashMap<ApplicationProxyJNI, MonitoringResults> applicationToResultsMap; //map appications - it's results
    private ConcurrentHashMap<Thread, ApplicationProxyJNI> threadToApplicationMap;
    private ConcurrentHashMap<ApplicationProxyJNI, CopyOnWriteArraySet<MediaApplicationMonitorListener>> applicationToListenersMap;

    private AtomicBoolean startMonitoring = new AtomicBoolean(false);
    private AtomicBoolean stopMonitoring = new AtomicBoolean(false);
    private ThreadPoolExecutor threadPoolExecutor;
    private int threadPoolSize = 10;


    static {
         System.loadLibrary("Monitoring_MediaApplicationMonitor");
    }

    /**
     * Class for storing last monitoring results of one application
     */
    private class MonitoringResults {
        private long lastAllocatedMemory;
        private double lastCPUUsage;
        private boolean lastRunningState;

        public long getLastAllocatedMemory() {
            return lastAllocatedMemory;
        }

        public synchronized void setLastAllocatedMemory(long lastAllocatedMemory) {
            this.lastAllocatedMemory = lastAllocatedMemory;
        }

        public synchronized double getLastCPUUsage() {
            return lastCPUUsage;
        }

        public synchronized void setLastCPUUsage(double lastCPUUsage) {
            this.lastCPUUsage = lastCPUUsage;
        }

        public synchronized boolean isLastRunningState() {
            return lastRunningState;
        }

        public synchronized void setLastRunningState(boolean lastRunningState) {
            this.lastRunningState = lastRunningState;
        }
    }

     /**
     * Constructor
     */
    public MediaApplicationMonitor() {
        applicationToResultsMap = new ConcurrentHashMap<ApplicationProxyJNI, MonitoringResults>();
        applicationToListenersMap = new ConcurrentHashMap<ApplicationProxyJNI, CopyOnWriteArraySet<MediaApplicationMonitorListener>>();
        threadPoolExecutor = new ThreadPoolExecutor(threadPoolSize, 2 * threadPoolSize, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        threadToApplicationMap = new ConcurrentHashMap<Thread, ApplicationProxyJNI>(); 

    }

    /**
     * returns amount of allocated memory in bytes
     * @param pid
     * @return allocated memory in bytes
     */
    private synchronized native long getAllocatedMemory(int pid);

    /**
     * gets CPU load
     * @param pid
     * @return current cpu percentual usage of application
     */
    private synchronized native double getCPUUsage(int pid);



    /**
     * Must be called in order to get monitoring information immediately, otherwise it's waiting call
     */
    public void startMonitoring() {
        logger.debug("starting monitoring");

        this.startMonitoring.set(true);
        synchronized (startMonitoring) {
            startMonitoring.notifyAll();
        }
    }

    /**
     * Stops monitoring, join threads
     */
    public void stopMonitoring() {
        logger.debug("Stopping monitoring");
        this.stopMonitoring.set(true);

        for (Thread th: threadToApplicationMap.keySet()) {
            try {
                th.join();
            }
            catch (InterruptedException ex){
                logger.error("Unable to join thread, thread interrupted.");
            }
        }
        threadPoolExecutor.shutdown();
    }

    /**
     * Gets allocated physical memory of Media Application
     * @param medApp ApplicationProxyJNI
     * @return allocated physical memory
     */
    public long getAllocatedMemory(ApplicationProxyJNI medApp) {
        assert medApp != null;
        if (startMonitoring.get()) {
            return applicationToResultsMap.get(medApp).getLastAllocatedMemory();
        }
        long result = getAllocatedMemory(medApp.getPid());
        if (result == -1) {
             throw new RuntimeException("Error in getAllocated memory native function");
        }
        else {
            return result;
        }
    }

    /**
     *gets current CPU usage of Media Application
     * @param medApp
     * @return CPU usage of Media Applications in percents
     */
    public double getCPUUsage(ApplicationProxyJNI medApp) {
        assert medApp != null;
        if (startMonitoring.get()) {
            return applicationToResultsMap.get(medApp).getLastCPUUsage();
        }
        double result = getCPUUsage(medApp.getPid());
            if (result == -1) {
                throw new RuntimeException("Error in getCPUUsage native function");
            }
            else {
                return result;
            }
    }

    /**
     * Registers application for monitoring
     * @param app ApplicationProxyJNI to register in monitor
     */
   public void registerApplication(ApplicationProxyJNI app) {
        assert app!= null;
        assert !applicationToResultsMap.containsKey(app) : "Application is already registered!";

        applicationToResultsMap.put(app, new MonitoringResults());
        logger.debug("Application registered.");


        //creates monitoring thread 
        Thread applicationMonitorThread = new Thread() {
            public void run() {
                // monitoring loop
                while (!stopMonitoring.get() && threadToApplicationMap.containsKey(this) ) {
                    // wait until we are supposed to start
                    synchronized (startMonitoring) {
                        if (!startMonitoring.get()) {
                            //intentionaly empty Catch block
                            try{
                                startMonitoring.wait();
                            } catch (InterruptedException e) {
                            }
                        }
                    }

                    // sleep for random time up to 200ms so that not all the threads start synchronously
                    TimeUtils.sleepFor(Math.round(200 * Math.random()));
                    ApplicationProxyJNI app = threadToApplicationMap.get(this);
                    if (app != null) {
                          MonitoringResults results = applicationToResultsMap.get(app);
                          results.setLastCPUUsage(getCPUUsage(app.getPid()));
                          results.setLastAllocatedMemory(getAllocatedMemory(app.getPid()));

                          boolean lastState = results.isLastRunningState();
                          try {
                            results.setLastRunningState(app.checkRunning());
                           }
                          catch (ApplicationProxy.ApplicationProxyException ex) {
                              ex.printStackTrace();
                          }
                         //check registered listeners
                          if (lastState != results.isLastRunningState()) { //state changed
                              logger.debug("Changed state of application Listener");
                              if (applicationToListenersMap.containsKey(app)) {
                                  for(MediaApplicationMonitorListener lis: applicationToListenersMap.get(app)) {
                                     if (lastState) {
                                         lis.onMediaApplicationDown(app);
                                     }
                                     else{
                                         lis.onMediaApplicationStart(app);
                                     }
                                  }
                              }
                          }
                    }
                }
            }
        };
        threadToApplicationMap.put(applicationMonitorThread, app);
        threadPoolExecutor.execute(applicationMonitorThread);
   }

    /**
     * Deletes application from monitoring applications
     * @param app ApplicationProxyJNI to remove
     */
   public synchronized void removeApplication(ApplicationProxyJNI app) {
        assert app!= null;

               
            for (Thread th: threadToApplicationMap.keySet()) {
                if(th != null) {
                    if (threadToApplicationMap.get(th).equals(app)) {
                        th.interrupt();
                        threadPoolExecutor.remove(th);
                        threadToApplicationMap.remove(th,app);
                    }
                }
            }


        applicationToResultsMap.remove(app);
        if (applicationToListenersMap.containsKey(app)) {
            applicationToListenersMap.remove(app);
        }
        logger.debug("Application was deleted.");
    }

    /**
     * Registers given listener to given monitored application 
     * @param app ApplicationProxyJNI = application, to whitch the listener is registered
     * @param lis MediaApplicationMonitorListener to register
     */
    public void registerMediaApplicationMonitorListener(ApplicationProxyJNI app, MediaApplicationMonitorListener lis) {
        assert app != null;
        assert lis != null;

        if (!this.applicationToListenersMap.containsKey(app)) {
            this.applicationToListenersMap.put(app, new CopyOnWriteArraySet<MediaApplicationMonitorListener>());    
        }
        this.applicationToListenersMap.get(app).add(lis);

    }

    public String toString() {
        return "MediaApplication monitor monitors " + applicationToResultsMap.size() + " Applications";
    }

    
}
