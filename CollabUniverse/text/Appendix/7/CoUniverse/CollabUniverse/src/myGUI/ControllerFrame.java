package myGUI;

import AppControllers.LocalController;
import AppControllers.Controller;
import utils.ApplicationProxy;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

/**
 * GUI for LocalController. Main feature is to display STDOUT and STDERR of applications being run.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 10.9.2007
 * Time: 9:25:19
 */
public class ControllerFrame extends JFrame {
    static Logger logger = Logger.getLogger("myGUI");

    JTabbedPane appTabbedPane;
    ArrayList<JPanel> appPanes;
    ConcurrentHashMap<Controller, JPanel> appPanesIndex;
    ConcurrentHashMap<Controller, JTextPane> textPaneIndex;
    ConcurrentHashMap<ApplicationProxy, LocalController> applicationProxyToLocalController;

    public ControllerFrame() {
        appTabbedPane = new JTabbedPane();
        appPanes = new ArrayList<JPanel>();
        appPanesIndex = new ConcurrentHashMap<Controller, JPanel>();
        textPaneIndex = new ConcurrentHashMap<Controller, JTextPane>();
        applicationProxyToLocalController = new ConcurrentHashMap<ApplicationProxy, LocalController>();
        this.setTitle("Local AppControllers");
        this.getContentPane().add(appTabbedPane);
        this.pack();
        this.setSize(this.getPreferredSize());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });
    }

    public void addController(Controller controller, ApplicationProxy applicationProxy) {
        JPanel panel = createAppProxyPanel(controller);
        appPanes.add(panel);
        appPanesIndex.put(controller, panel);
        appTabbedPane.addTab(controller.getApplication().toString(), panel);
        if (controller instanceof LocalController) {
            assert applicationProxy != null;
            applicationProxyToLocalController.put(applicationProxy, (LocalController) controller);
        }
        this.pack();
        appTabbedPane.invalidate();
    }

    public void removeController(Controller controller) {
        JPanel panel = appPanesIndex.get(controller);
        int tabIndex = -1;
        for (int i = 0; i < appTabbedPane.getTabCount(); i++) {
            if (appTabbedPane.getComponentAt(i) == panel) {
                tabIndex = i;
            }
        }
        assert tabIndex != -1;
        appTabbedPane.removeTabAt(tabIndex);
        appPanesIndex.remove(controller, panel);
        int nApplicationProxies = 0;
        for (ApplicationProxy applicationProxy : applicationProxyToLocalController.keySet()) {
            if (applicationProxyToLocalController.get(applicationProxy) == controller) {
                nApplicationProxies++;
                applicationProxyToLocalController.remove(applicationProxy, controller);
            }
        }
        assert nApplicationProxies == 1;
        textPaneIndex.remove(controller);
        appPanes.remove(panel);
        this.pack();
        appTabbedPane.invalidate();
    }

    public void writeStdOut(ApplicationProxy proxy, String text) {
        writeStdOut(applicationProxyToLocalController.get(proxy), text);
    }

    public void writeStdOut(LocalController controller, String text) {
        this.writeText(controller, text, "normal");
    }

    public void writeStdErr(ApplicationProxy proxy, String text) {
        writeStdErr(applicationProxyToLocalController.get(proxy), text);
    }

    public void writeStdErr(LocalController controller, String text) {
        this.writeText(controller, text, "italic");
    }

    public void writeCmd(ApplicationProxy proxy, String text) {
        writeCmd(applicationProxyToLocalController.get(proxy), text);
    }

    public void writeCmd(Controller controller, String text) {
        this.writeText(controller, text, "bold");
    }

    private void onExit() {
        this.setVisible(false);
        dispose();
    }

    private void writeText(Controller controller, String text, String style) {
        JTextPane textPane = textPaneIndex.get(controller);
        StyledDocument d = textPane.getStyledDocument();
        try {
            d.insertString(d.getLength(), text + "\n", d.getStyle(style));
        } catch (BadLocationException e) {
            if (ControllerFrame.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            ControllerFrame.logger.error("ControllerFrame: failed to append text!");
        }
    }


    private JPanel createAppProxyPanel(Controller localController) {

        JTextPane textPane = new JTextPane();
        textPane.setPreferredSize(new Dimension(800, 600));
        textPane.setEditable(false);
        textPane.setDocument(new DefaultStyledDocument());
        StyledDocument d = textPane.getStyledDocument();

        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
        Style regular = d.addStyle("regular", def);
        StyleConstants.setFontFamily(def, "Monospaced");
        Style s;
        s = d.addStyle("italic", regular);
        StyleConstants.setItalic(s, true);
        s = d.addStyle("bold", regular);
        StyleConstants.setBold(s, true);
        StyleConstants.setForeground(s, Color.BLUE);

        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.getViewport().add(textPane);

        JPanel panel = new JPanel();
        panel.add(jScrollPane);

        textPaneIndex.put(localController, textPane);

        return panel;
    }
}
