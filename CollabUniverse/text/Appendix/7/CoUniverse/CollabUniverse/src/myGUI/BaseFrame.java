package myGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;


/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Mar 18, 2008
 * Time: 2:17:24 PM
 * To change this template use File | Settings | File Templates.
 */
@Deprecated
public class BaseFrame extends JFrame {
    //main panel
    // progress panel
    // dialog log
    //maybe menu



    public BaseFrame(String Title) {
        super(Title);
        this.setLayout(new GridBagLayout());
        init();


        this.addWindowListener(
            new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    onClose();
                }
            }
        );
    }





    private void init() {


    }



    private void onClose() {
        this.setVisible(false);
        this.dispose();
    }


    protected static void createAndShowGUI() {
        //BaseFrame baseFrame = new BaseFrame(Constants.titleOfBaseFrame);
        //do nothing on close. Make sure windowClosing event is caught
        //baseFrame.setLocationRelativeTo(null);
        //baseFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //baseFrame.setVisible(true);
    }



}
