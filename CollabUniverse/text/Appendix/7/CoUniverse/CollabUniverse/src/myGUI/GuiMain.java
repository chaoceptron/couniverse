package myGUI;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Mar 18, 2008
 * Time: 6:23:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class GuiMain {
          //possible extension for JXMapVisualizer
        // result screen could span multiple physical devices
        static boolean isMultipleScreen;
        static Rectangle virtualBounds = new Rectangle();


    static {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: ClassNotFoundException");
        } catch (InstantiationException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: InstantiationException");
        } catch (IllegalAccessException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: IllegalAccessException");
        } catch (UnsupportedLookAndFeelException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: UnsupportedLookAndFeelException");
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
    }



    public static void main(String[] args) {

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] screenDevices = ge.getScreenDevices();

        for (int j = 0; j < screenDevices.length; j++) {
            GraphicsDevice gd = screenDevices[j];
            GraphicsConfiguration[] gc = gd.getConfigurations();

            for (int i = 0; i < gc.length; i++) {
                if(gc[i].getBounds().getX() != 0.0d || gc[i].getBounds().getY() != 0.0d ) isMultipleScreen = true;
                virtualBounds = virtualBounds.union(gc[i].getBounds());
            }
        }



        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //BaseFrame.createAndShowGUI();
            }
        });
    }


}
