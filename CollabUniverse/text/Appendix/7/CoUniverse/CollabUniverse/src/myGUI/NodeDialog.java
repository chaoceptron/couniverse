package myGUI;

import MediaAppFactory.MediaApplication;
import NetworkRepresentation.EndpointNetworkNode;
import NetworkRepresentation.EndpointNodeInterface;
import NetworkRepresentation.NetworkSite;
import java.awt.event.*;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;
import utils.TimeUtils;

public class NodeDialog extends JDialog {
    static Logger logger = Logger.getLogger("myGUI");

    private JPanel contentPane;
    private JButton saveButton;
    private JButton exitButton;
    private JButton reloadButton;
    private JLabel fileLabel;
    private JTextField nodeNameField;
    private JTextField peerIDField;
    private JTextField siteNameField;
    private JCheckBox localNodeCheckBox;
    private JList interfacesList;
    private JButton addInterfaceButton;
    private JButton editInterfaceButton;
    private JButton removeInterfaceButton;
    private JList applicationsList;
    private JButton addApplicationButton;
    private JButton editApplicationButton;
    private JButton removeApplicationButton;
    private JTextField gpsLatitudeField;
    private JTextField gpsLongitudeField;
    private JButton regenUUIDButton;
    private JCheckBox proxyNodeCheckBox;
    private JTextField proxyString;

    private static DefaultListModel applicationsListModel = new DefaultListModel();
    private static DefaultListModel interfacesListModel = new DefaultListModel();

    private boolean createConfigFile;
    private File configFile;
    EndpointNetworkNode node = null;
    final static String IDprefix = "urn:jxta:uuid-";

    static {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            NodeDialog.logger.error("NetworkNode Configurator: ClassNotFoundException");
        } catch (InstantiationException e) {
            NodeDialog.logger.error("NetworkNode Configurator: InstantiationException");
        } catch (IllegalAccessException e) {
            NodeDialog.logger.error("NetworkNode Configurator: IllegalAccessException");
        } catch (UnsupportedLookAndFeelException e) {
            NodeDialog.logger.error("NetworkNode Configurator: UnsupportedLookAndFeelException");
        }
    }

    public NodeDialog(File configFile) {
        assert configFile != null;

        this.configFile = configFile;
        this.createConfigFile = false;
        if (!this.configFile.exists() || this.configFile.length() == 0L) {
            NodeDialog.logger.info("NetworkNode Configurator: Specified config file doesn't exist! Continuing with a new file...");
            this.createConfigFile = true;
        } else if (!this.configFile.isFile()) {
            NodeDialog.logger.error("NetworkNode Configurator: Specified config file is not an ordinary file!");
            System.exit(1);
        } else if (!this.configFile.canRead() || !this.configFile.canWrite()) {
            NodeDialog.logger.error("NetworkNode Configurator: Can't read or write specified config file!");
            System.exit(1);
        }
        load();  // this also creates a new file if needed

        setContentPane(contentPane);
        setModal(false);
        getRootPane().setDefaultButton(saveButton);

        this.applicationsList.setModel(applicationsListModel);
        this.interfacesList.setModel(interfacesListModel);
        fileLabel.setText(configFile.getAbsolutePath());

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        nodeNameField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                syncNodeToGUI();
            }
        });
        siteNameField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                syncNodeToGUI();
            }
        });
        gpsLatitudeField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                syncNodeToGUI();
            }
        });
        gpsLongitudeField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                syncNodeToGUI();
            }
        });
        localNodeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                syncNodeToGUI();
            }
        });
        proxyNodeCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                syncNodeToGUI();
            }
        });
        nodeNameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                syncNodeToGUI();
            }
        });
        siteNameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                syncNodeToGUI();
            }
        });
        gpsLatitudeField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                syncNodeToGUI();
            }
        });
        gpsLongitudeField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                syncNodeToGUI();
            }
        });
        proxyString.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                syncNodeToGUI();
            }
        });


        interfacesList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                boolean enabled = (!interfacesListModel.isEmpty() && interfacesList.getSelectedIndex() != -1);
                editInterfaceButton.setEnabled(enabled);
                removeInterfaceButton.setEnabled(enabled);
            }
        });
        addInterfaceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InterfaceDialog intDialog = new InterfaceDialog();
                intDialog.setSize(intDialog.getPreferredSize());
                intDialog.setVisible(true);
                EndpointNodeInterface ni = intDialog.getNodeInterface();
                if (ni != null) {
                    // this should be only after OK button is pressed
                    ni.setParentNode(node);
                    node.addInterface(ni);
                    syncGUItoNode();
                }
            }
        });
        editInterfaceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                InterfaceDialog intDialog = new InterfaceDialog();
                intDialog.setValues((EndpointNodeInterface) interfacesListModel.get(interfacesList.getSelectedIndex()));
                intDialog.setSize(intDialog.getPreferredSize());
                intDialog.setVisible(true);
                syncGUItoNode();
            }
        });
        removeInterfaceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                node.removeInterface((EndpointNodeInterface) interfacesListModel.get(interfacesList.getSelectedIndex()));
                interfacesListModel.remove(interfacesList.getSelectedIndex());
                syncGUItoNode();
            }
        });

        applicationsList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                boolean enabled = (!applicationsListModel.isEmpty() && applicationsList.getSelectedIndex() != -1);
                editApplicationButton.setEnabled(enabled);
                removeApplicationButton.setEnabled(enabled);
            }
        });
        addApplicationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ApplicationDialog appDialog = new ApplicationDialog();
                appDialog.setSize(appDialog.getPreferredSize());
                appDialog.setVisible(true);
                if (appDialog.getApplication() != null) {
                    // this should be only after OK button is pressed
                    node.addApplication(appDialog.getApplication());
                    syncGUItoNode();
                }
            }
        });
        editApplicationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ApplicationDialog appDialog = new ApplicationDialog();
                appDialog.setValues((MediaApplication) applicationsListModel.get(applicationsList.getSelectedIndex()));
                appDialog.setSize(appDialog.getPreferredSize());
                appDialog.setVisible(true);
                syncGUItoNode();
            }
        });
        removeApplicationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                node.removeApplication((MediaApplication) applicationsListModel.get(applicationsList.getSelectedIndex()));
                applicationsListModel.remove(applicationsList.getSelectedIndex());
                syncGUItoNode();
            }
        });

        reloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onReload();
            }
        });

        regenUUIDButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onRegenerateUUID();
            }
        });

        this.pack();

        syncGUItoNode();
    }

    private void onRegenerateUUID() {
        this.node.setUuid(UUID.randomUUID().toString());
        for (MediaApplication mediaApplication : this.node.getNodeApplications()) {
            mediaApplication.setUuid(UUID.randomUUID().toString());
        }
        for (EndpointNodeInterface nodeInterface : this.node.getNodeInterfaces()) {
            nodeInterface.setUuid(UUID.randomUUID().toString());
        }
    }


    private void onSave() {
        save();
    }

    private void onExit() {
        this.setVisible(false);
        dispose();
    }

    private void onReload() {
        load();
        syncGUItoNode();
    }

    public boolean isCreateConfigFile() {
        return createConfigFile;
    }

    public void setCreateConfigFile(boolean createConfigFile) {
        this.createConfigFile = createConfigFile;
    }

    private void load() {
        if (!createConfigFile) {
            // load the NetworkNode configuration from the file
            try {
                XMLDecoder dec = new XMLDecoder(
                        new BufferedInputStream(
                                new FileInputStream(configFile)
                        )
                );
                node = (EndpointNetworkNode) dec.readObject();
                dec.close();
            } catch (FileNotFoundException e) {
                NodeDialog.logger.error("NetworkNode Configurator: Failed to read the file! This is a bug in the code!");
                System.exit(1);
            } catch (NoSuchElementException e) {
                NodeDialog.logger.error("NetworkNode Configurator: Failed to decode the file!");
            }
        } else {
            // initialize a new empty NetworkNode
            node = new EndpointNetworkNode("", "", null, true, false,
                    new NetworkSite(""),
                    new CopyOnWriteArraySet<MediaApplication>(),
                    new CopyOnWriteArraySet<EndpointNodeInterface>()
            );
        }
    }

    private void save() {
        BufferedOutputStream f;
        try {
            f = new BufferedOutputStream(new FileOutputStream(configFile));
            XMLEncoder enc = new XMLEncoder(f);
            enc.writeObject(node);
            enc.flush();
            enc.close();
        } catch (FileNotFoundException e) {
            NodeDialog.logger.error("NetworkNode Configurator: File not found!");
        }
        createConfigFile = false;
    }

    /**
     * Synchronizes node object accroding to GUI
     */
    private void syncNodeToGUI() {
        node.setNodeName(nodeNameField.getText());
        node.setNodePeerID(IDprefix + peerIDField.getText());
        node.setLocalNode(localNodeCheckBox.isSelected());
        node.getNodeSite().setSiteName(siteNameField.getText());
        try {
            node.setGeoLatitude(Double.parseDouble(gpsLatitudeField.getText()));
        }
        catch (NumberFormatException e) {
            node.setGeoLatitude(0.0);
        }
        try {
            node.setGeoLongitude(Double.parseDouble(gpsLongitudeField.getText()));
        }
        catch (NumberFormatException e) {
            node.setGeoLongitude(0.0);
        }

        node.setProxyNode(proxyNodeCheckBox.isSelected());
        node.setProxyString(proxyString.getText());

        node.getNodeApplications().clear();
        if (!applicationsListModel.isEmpty()) {
            Object[] mas = applicationsListModel.toArray();
            for (Object ma : mas) {
                node.addApplication((MediaApplication) ma);
            }
        }

        node.getNodeInterfaces().clear();
        if (!interfacesListModel.isEmpty()) {
            Object[] ints = interfacesListModel.toArray();
            for (Object anInt : ints) {
                node.addInterface((EndpointNodeInterface) anInt);
            }
        }
    }

    /**
     * Synchronizes GUI according to state of node object
     */
    private void syncGUItoNode() {
        nodeNameField.setText(node.getNodeName());
        peerIDField.setText(node.getNodePeerID());
        if (node.getNodePeerID().length() > IDprefix.length()) {
            peerIDField.setText(node.getNodePeerID().substring(IDprefix.length()));
        } else {
            peerIDField.setText("");
        }
        localNodeCheckBox.setSelected(node.isLocalNode());
        proxyNodeCheckBox.setSelected(node.isProxyNode());
        siteNameField.setText(node.getNodeSite().getSiteName());
        gpsLatitudeField.setText(Double.toString(node.getGeoLatitude()));
        gpsLongitudeField.setText(Double.toString(node.getGeoLongitude()));

        if (!node.isProxyNode()) {
            proxyString.setEnabled(false);
        }
        else {
            proxyString.setEnabled(true);
            proxyString.setText(node.getProxyString());
        }

        applicationsListModel.clear();
        for (MediaApplication m : node.getNodeApplications()) {
            applicationsListModel.addElement(m);
        }
        applicationsList.setSelectedIndex(-1);

        interfacesListModel.clear();
        for (EndpointNodeInterface i : node.getNodeInterfaces()) {
            interfacesListModel.addElement(i);
        }
        interfacesList.setSelectedIndex(-1);
    }


    public static void main(String[] args) {
        if (args.length < 1) {
            NodeDialog.logger.error("NetworkNode Configurator: File name to operate upon has to be given! Bailing out...");
            System.exit(1);
        }

        NodeDialog dialog = new NodeDialog(new File(args[0]));
        dialog.setVisible(true);
        while (dialog.isVisible()) {
            TimeUtils.sleepFor(200);
        }
        System.exit(0);
    }
}
