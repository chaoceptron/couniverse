package myGUI;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationProxy;
import MediaApplications.*;
import NetworkRepresentation.NetworkSite;

import javax.swing.*;
import java.awt.event.*;

public class ApplicationDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox appType;
    private JTextField appPath;
    private JTextField appCmdOpts;
    private JTextField sourceSiteField;
    private JTextField stopCmdField;
    private JTextField startCmdField;

    private static final String UltraGrid1500ProducerLabel = "UltraGrid 1.5 Gbps producer";
    private static final String UltraGrid1500ConsumerLabel = "UltraGrid 1.5 Gbps consumer";
    private static final String UltraGrid750ProducerLabel = "UltraGrid 750 Mbps producer";
    private static final String UltraGrid750ConsumerLabel = "UltraGrid 750 Mbps consumer";
    private static final String UltraGridDXTProducerLabel = "UltraGrid DXT producer";
    private static final String UltraGridDXTConsumerLabel = "UltraGrid DXT consumer";
    private static final String HDVMPEG2ProducerLabel = "HDV MPEG-2 TS producer";
    private static final String HDVMPEG2ConsumerLabel = "HDV MPEG-2 TS consumer";
    private static final String RATLabel = "RAT";
    private static final String VICLabel = "VIC";
    private static final String RumHDLabel = "Rum HD";
    private static final String RumLabel = "Rum";
    private static final String PolycomProducerLabel = "Polycom Producer";
    private static final String PolycomConsumerLabel = "Polycom Consumer";

    MediaApplication mediaApplication = null;

    public ApplicationDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        appType.addItem(UltraGrid1500ProducerLabel);
        appType.addItem(UltraGrid1500ConsumerLabel);
        appType.addItem(UltraGrid750ProducerLabel);
        appType.addItem(UltraGrid750ConsumerLabel);
        appType.addItem(UltraGridDXTProducerLabel);
        appType.addItem(UltraGridDXTConsumerLabel);
        appType.addItem(HDVMPEG2ProducerLabel);
        appType.addItem(HDVMPEG2ConsumerLabel);
        appType.addItem(VICLabel);
        appType.addItem(RATLabel);
        appType.addItem(RumHDLabel);
        appType.addItem(RumLabel);
        appType.addItem(PolycomProducerLabel);
        appType.addItem(PolycomConsumerLabel);
        appType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setEditability();
            }
        });
        // also do this on dialog startup
        setEditability();

    }

    private void setEditability() {
        if (UltraGrid1500ConsumerLabel.equals(appType.getSelectedItem())
                || UltraGrid750ConsumerLabel.equals(appType.getSelectedItem())
                || UltraGridDXTConsumerLabel.equals(appType.getSelectedItem())
                || HDVMPEG2ConsumerLabel.equals(appType.getSelectedItem())
                || VICLabel.equals(appType.getSelectedItem())
                || RATLabel.equals(appType.getSelectedItem())
                || PolycomConsumerLabel.equals(appType.getSelectedItem())) {
            sourceSiteField.setEditable(true);
        } else {
            sourceSiteField.setEditable(false);
        }
        if (PolycomProducerLabel.equals(appType.getSelectedItem())
               || PolycomConsumerLabel.equals(appType.getSelectedItem()) ) {
            appPath.setEditable(false);
            appCmdOpts.setEditable(false);
            startCmdField.setEditable(true);
            stopCmdField.setEditable(true);
        }
        else {
            appPath.setEditable(true);
            appCmdOpts.setEditable(true);
            startCmdField.setEditable(false);
            stopCmdField.setEditable(false);
        }
    }

    private void onOK() {
        if (mediaApplication == null) {
            // this only happens when creating a new application
            if (UltraGrid1500ProducerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGrid1500Producer();
            } else if (UltraGrid1500ConsumerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGrid1500Consumer();
            } else if (UltraGrid750ProducerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGrid750Producer();
            } else if (UltraGrid750ConsumerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGrid750Consumer();
            } else if (UltraGridDXTProducerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGridDXTProducer();
            } else if (UltraGridDXTConsumerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new UltraGridDXTConsumer();
            } else if (HDVMPEG2ProducerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new HDVMPEG2Producer();
            } else if (HDVMPEG2ConsumerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new HDVMPEG2Consumer();
            } else if (VICLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new VIC();
            } else if (RATLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new RAT();
            } else if (RumHDLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new RumHD();
            } else if (RumLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new Rum();
            } else if (PolycomProducerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new PolycomProducer();
            } else if (PolycomConsumerLabel.equals(appType.getSelectedItem())) {
                mediaApplication = new PolycomConsumer();
            } else {
                assert false : "Application listed in the code is not handled!";
            }
        }

        mediaApplication.setupApplication(appPath.getText(), appCmdOpts.getText());
        if (sourceSiteField.isEditable()) {
            if ("".equals(sourceSiteField.getText())) {
                ((MediaApplicationConsumer) mediaApplication).setSourceSite(null);
            } else {
                ((MediaApplicationConsumer) mediaApplication).setSourceSite(new NetworkSite(sourceSiteField.getText()));
            }
        }
        if (startCmdField.isEditable()) {
            ((MediaApplicationProxy) mediaApplication).setStartCommand(startCmdField.getText());
        }
        if (stopCmdField.isEditable()) {
            ((MediaApplicationProxy) mediaApplication).setStopCommand(stopCmdField.getText());
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public MediaApplication getApplication() {
        return mediaApplication;
    }

    /**
     * setValues is only called in case of editing an existnig MediaApplication; it doesn't support
     * changing application type, as it would have to recreate the object
     * <p/>
     *
     * @param ma MediaApplication to edit
     */
    public void setValues(MediaApplication ma) {
        this.mediaApplication = ma;

        if (ma instanceof UltraGrid1500Producer) {
            appType.setSelectedItem(UltraGrid1500ProducerLabel);
        } else if (ma instanceof UltraGrid1500Consumer) {
            appType.setSelectedItem(UltraGrid1500ConsumerLabel);
        } else if (ma instanceof UltraGrid750Producer) {
            appType.setSelectedItem(UltraGrid750ProducerLabel);
        } else if (ma instanceof UltraGrid750Consumer) {
            appType.setSelectedItem(UltraGrid750ConsumerLabel);
        } else if (ma instanceof UltraGridDXTProducer) {
            appType.setSelectedItem(UltraGridDXTProducerLabel);
        } else if (ma instanceof UltraGridDXTConsumer) {
            appType.setSelectedItem(UltraGridDXTConsumerLabel);
        } else if (ma instanceof HDVMPEG2Producer) {
            appType.setSelectedItem(HDVMPEG2ProducerLabel);
        } else if (ma instanceof HDVMPEG2Consumer) {
            appType.setSelectedItem(HDVMPEG2ConsumerLabel);
        } else if (ma instanceof VIC) {
            appType.setSelectedItem(VICLabel);
        } else if (ma instanceof RAT) {
            appType.setSelectedItem(RATLabel);
        } else if (ma instanceof RumHD) {
            appType.setSelectedItem(RumHDLabel);
        } else if (ma instanceof Rum) {
            appType.setSelectedItem(RumLabel);
        } else if (ma instanceof PolycomProducer) {
            appType.setSelectedItem(PolycomProducerLabel);
        } else if (ma instanceof PolycomConsumer) {
            appType.setSelectedItem(PolycomConsumerLabel);            
        } else {
            assert false : "Application listed in the code is not handled!";
        }

        appType.setEnabled(false); //disallow changing application type
        appPath.setText(ma.getApplicationPath());
        appCmdOpts.setText(ma.getApplicationCmdOptions());

        if (ma instanceof MediaApplicationConsumer) {
            if (((MediaApplicationConsumer) ma).getSourceSite() == null) {
                sourceSiteField.setText("");
            } else {
                sourceSiteField.setText(((MediaApplicationConsumer) ma).getSourceSite().getSiteName());
            }
            sourceSiteField.setEditable(true);
        }
        if (ma instanceof MediaApplicationProxy) {
            startCmdField.setText(((MediaApplicationProxy) ma).getStartCommand());
            startCmdField.setEditable(true);
            stopCmdField.setText(((MediaApplicationProxy) ma).getStopCommand());
            stopCmdField.setEditable(true);
        }
    }
}
