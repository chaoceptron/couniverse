package MediaApplications;

import MediaAppFactory.*;
import NetworkRepresentation.NetworkSite;
import MediaApplications.Streams.PolycomStream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import nu.xom.Element;

/**
 * Polycom device Consumer
 * <p/>
 * User: Milos Liska (xliska@fi.muni.cz)
 * Date: 7.4.2009
 * Time: 15:52:45
 */
public class PolycomConsumer extends MediaApplication implements MediaApplicationProxy, MediaApplicationVideo, MediaApplicationAudio, MediaApplicationConsumer, CoNMLInterface  {
    private MediaApplication source;
    private NetworkSite sourceSite;
    private String startCommand;
    private String stopCommand;

    public PolycomConsumer() {
        super("Polycom Client Consumer", PolycomStream.getMyStreams());
    }
    public PolycomConsumer(String applicationName) {
        super(applicationName, PolycomStream.getMyStreams());
    }

    @Override
    public void setSource(MediaApplication source) {
        this.source = source;
    }

    @Override
    public MediaApplication getSource() {
        return this.source;
    }

    @Override
    public void setSourceSite(NetworkSite sourceSite) {
        this.sourceSite = sourceSite;
    }

    @Override
    public NetworkSite getSourceSite() {
        return this.sourceSite;
    }

    @Override
    public String getStartCommand() {
        return startCommand;
    }

    @Override
    public void setStartCommand(String startCommand) {
        this.startCommand = startCommand;
    }

    @Override
    public void setStopCommand(String command) {
        this.stopCommand = command;
    }

    @Override
    public String getStopCommand() {
        return this.stopCommand;
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:PolycomConsumer", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode);  
        
        if(this.source != null){
            Element mediaAppReferenceElem = new Element("cou:source", CoNMLInterface.CONML_NS);                
            mediaAppReferenceElem.appendChild(this.source.getUuid()); 
            baseElementNode.appendChild(mediaAppReferenceElem);
        }
        
        if(this.sourceSite != null){
            this.sourceSite.encodeToCoNML(result, new CoNMLEncodeParameters(baseElementNode));            
        }
        
        if(this.startCommand != null){
            Element startCommandElem = new Element("cou:startCommand", CoNMLInterface.CONML_NS);             
            startCommandElem.appendChild(this.startCommand); 
            baseElementNode.appendChild(startCommandElem);
        }
        
        if(this.stopCommand != null){
            Element stopCommandElem = new Element("cou:stopCommand", CoNMLInterface.CONML_NS);        
            stopCommandElem.appendChild(this.stopCommand); 
            baseElementNode.appendChild(stopCommandElem);
        }
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element sourceElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("source", CoNMLInterface.CONML_NS);
        if(sourceElem != null){
            MediaApplication decoded = (MediaApplication) result.getDecodedApps().get(sourceElem.getValue());//the presence of source application is assumed!!!
            this.source = decoded;
        }
       
        Element networkSiteElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("networkSite", CoNMLInterface.CONML_NS); 
        if(networkSiteElem != null){
            this.sourceSite = new NetworkSite();
            this.sourceSite.decodeFromCoNML(result, new CoNMLDecodeParameters(networkSiteElem));
        }

        Element startCommandElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("startCommand", CoNMLInterface.CONML_NS);         
        if(startCommandElem != null){
            this.startCommand = startCommandElem.getValue();
        }

        Element stopCommandElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("stopCommand", CoNMLInterface.CONML_NS);        
        if(stopCommandElem != null){
            this.stopCommand = stopCommandElem.getValue();
        }
    }
}
