package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * UltraGrid 1080i 1.5 Gbps stream description
 * <p/> 
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 12:19:04
 */
public final class UltraGrid1500Stream {

    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("UltraGrid 1500 stream", 1500000000L, 1500000000L, 80, 6000000000L, 8.0f));
            }
        };
    }

}
