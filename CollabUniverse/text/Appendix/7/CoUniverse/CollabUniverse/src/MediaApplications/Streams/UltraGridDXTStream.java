package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * UltraGrid 1080i DXT compressed 250 Mbps stream description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 12:19:04
 */
public final class UltraGridDXTStream {

    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("UltraGrid DXT stream", 250000000L, 250000000L, 80, 500000000L, 7.5f));
            }
        };
    }

}
