package MediaApplications;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationDistributor;
import MediaAppFactory.MediaStream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import java.util.HashSet;
import nu.xom.Element;

/**
 * Optimized version of RUM (UDP Packet Reflector) for uncompressed HD video distribution
 * <p/> 
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:35:56
 */
public class RumHD extends MediaApplication implements MediaApplicationDistributor, CoNMLInterface {

    public RumHD() {
        super("HD UDP Packet Reflector", new HashSet<MediaStream>());
    }

    public RumHD(String applicationName) {
        super(applicationName, new HashSet<MediaStream>());
    }
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:RumHD", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode); 
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
    }
}
