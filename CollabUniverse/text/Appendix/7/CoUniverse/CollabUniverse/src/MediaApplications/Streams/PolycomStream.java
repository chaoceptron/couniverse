package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * Polycom stream description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 7.4.2009
 * Time: 15:54:29
 */
public class PolycomStream {
        public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("Polycom stream", 128000L, 6000000L, 100, 6000000L, 5.0f));
            }
        };
    }
}
