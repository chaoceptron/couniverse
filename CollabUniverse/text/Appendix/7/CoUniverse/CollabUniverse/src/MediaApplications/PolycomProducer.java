package MediaApplications;

import MediaAppFactory.*;
import NetworkRepresentation.EndpointNetworkNode;
import MediaApplications.Streams.PolycomStream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import nu.xom.Element;

/**
 * Polycom device producer
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 7.4.2009
 * Time: 15:52:45
 * To change this template use File | Settings | File Templates.
 */
public class PolycomProducer extends MediaApplication implements MediaApplicationProxy, MediaApplicationVideo, MediaApplicationAudio, MediaApplicationProducer, CoNMLInterface {
    private String targetIP;
    private EndpointNetworkNode targetNetworkNode;
    private String startCommand;
    private String stopCommand;

    public PolycomProducer() {
        super("Polycom Client Producer", PolycomStream.getMyStreams());
    }
    public PolycomProducer(String applicationName) {
        super(applicationName, PolycomStream.getMyStreams());
    }

    @Override
    public void setTargetIP(String targetIP) {
        this.targetIP = targetIP;
    }

    @Override
    public String getTargetIP() {
        return this.targetIP;
    }

    public EndpointNetworkNode getTargetNetworkNode() {
        return targetNetworkNode;
    }

    public void setTargetNetworkNode(EndpointNetworkNode targetNetworkNode) {
        this.targetNetworkNode = targetNetworkNode;
    }

    @Override
    public String getStartCommand() {
        return startCommand;
    }

    @Override
    public void setStartCommand(String startCommand) {
        this.startCommand = startCommand;
    }

    @Override
    public void setStopCommand(String command) {
        this.stopCommand = command;
    }

    @Override
    public String getStopCommand() {
        return this.stopCommand;
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:PolycomProducer");
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode);
        
        if(this.targetIP != null){
            Element targetIPElem = new Element("cou:targetIP", CoNMLInterface.CONML_NS);                
            targetIPElem.appendChild(this.targetIP); 
            baseElementNode.appendChild(targetIPElem);
        }
        
        if(this.targetNetworkNode != null){
            Element targetNodeElem = new Element("cou:target", CoNMLInterface.CONML_NS);                
            targetNodeElem.appendChild(this.targetNetworkNode.getUuid()); 
            baseElementNode.appendChild(targetNodeElem);
        }
        
        if(this.startCommand != null){
            Element startCommandElem = new Element("cou:startCommand", CoNMLInterface.CONML_NS);             
            startCommandElem.appendChild(this.startCommand); 
            baseElementNode.appendChild(startCommandElem);
        }
        
        if(this.stopCommand != null){
            Element stopCommandElem = new Element("cou:stopCommand", CoNMLInterface.CONML_NS);        
            stopCommandElem.appendChild(this.stopCommand); 
            baseElementNode.appendChild(stopCommandElem);
        }
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element targetIPElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("targetIP", CoNMLInterface.CONML_NS);             
        if(targetIPElem != null){
            this.targetIP = targetIPElem.getValue();
        }
        
        Element targetElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("target", CoNMLInterface.CONML_NS);
        if(targetElem != null){
            EndpointNetworkNode decoded = (EndpointNetworkNode) result.getDecodedNodes().get(targetElem.getValue());//the presence of target node is assumed!!! (dummy uninitialized instance will do)
            this.targetNetworkNode = decoded;
        }

        Element startCommandElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("startCommand", CoNMLInterface.CONML_NS);
        if(startCommandElem != null){
            this.startCommand = startCommandElem.getValue();
        }

        Element stopCommandElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("stopCommand", CoNMLInterface.CONML_NS);           
        if(stopCommandElem != null){
            this.stopCommand = stopCommandElem.getValue();
        }
    }
}
