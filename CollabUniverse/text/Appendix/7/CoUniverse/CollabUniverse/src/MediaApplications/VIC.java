package MediaApplications;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationVideo;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationProducer;
import MediaApplications.Streams.VICStream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import NetworkRepresentation.EndpointNetworkNode;
import NetworkRepresentation.NetworkSite;
import nu.xom.Element;

/**
 * VIC client
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:35:56
 */
public class VIC extends MediaApplication implements MediaApplicationVideo, MediaApplicationConsumer, MediaApplicationProducer, CoNMLInterface {
    private MediaApplication source;
    private EndpointNetworkNode targetNetworkNode;
    private String targetIP;
    private NetworkSite sourceSite;


    public VIC() {
        super("VIC Video Client", VICStream.getMyStreams());
    }

    public VIC(String applicationName) {
        super(applicationName, VICStream.getMyStreams());
    }

    @Override
    public void setSource(MediaApplication target) {
        this.source = target;
    }

    @Override
    public MediaApplication getSource() {
        return this.source;
    }

    @Override
    public void setTargetIP(String targetIP) {
        this.targetIP = targetIP;
    }

    @Override
    public String getTargetIP() {
        return this.targetIP;
    }

    public void setTargetNetworkNode(EndpointNetworkNode networkNode) {
        this.targetNetworkNode = networkNode;
    }

    public EndpointNetworkNode getTargetNetworkNode() {
        return this.targetNetworkNode;
    }

    @Override
    public NetworkSite getSourceSite() {
        return sourceSite;
    }

    @Override
    public void setSourceSite(NetworkSite sourceSite) {
        this.sourceSite = sourceSite;
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:VIC", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode);        
        
        if(this.source != null){
            Element mediaAppReferenceElem = new Element("cou:source", CoNMLInterface.CONML_NS);                
            mediaAppReferenceElem.appendChild(this.source.getUuid()); 
            baseElementNode.appendChild(mediaAppReferenceElem);
        }
        
        if(this.targetNetworkNode != null){
            Element targetNodeElem = new Element("cou:target", CoNMLInterface.CONML_NS);                
            targetNodeElem.appendChild(this.targetNetworkNode.getUuid()); 
            baseElementNode.appendChild(targetNodeElem);
        }
        
        if(this.sourceSite != null){
            this.sourceSite.encodeToCoNML(result, new CoNMLEncodeParameters(baseElementNode));            
        }
        
        if(this.targetIP != null){
            Element targetIPElem = new Element("cou:targetIP", CoNMLInterface.CONML_NS);                
            targetIPElem.appendChild(this.targetIP); 
            baseElementNode.appendChild(targetIPElem);
        }
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element sourceElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("source", CoNMLInterface.CONML_NS);
        if(sourceElem != null){
            MediaApplication decoded = (MediaApplication) result.getDecodedApps().get(sourceElem.getValue());//the presence of source application is assumed!!! (dummy uninitialized instance will do)
            this.source = decoded;
        }
        
        Element targetElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("target", CoNMLInterface.CONML_NS);
        if(targetElem != null){
            EndpointNetworkNode decoded = (EndpointNetworkNode) result.getDecodedNodes().get(targetElem.getValue());//the presence of target node is assumed!!! (dummy uninitialized instance will do)
            this.targetNetworkNode = decoded;
        }
        
        Element networkSiteElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("networkSite", CoNMLInterface.CONML_NS); 
        if(networkSiteElem != null){
            this.sourceSite = new NetworkSite();
            this.sourceSite.decodeFromCoNML(result, new CoNMLDecodeParameters(networkSiteElem));
        }

        Element targetIPElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("targetIP", CoNMLInterface.CONML_NS);
        if(targetIPElem != null){
            this.targetIP = targetIPElem.getValue();
        } 
    }
}
