package MediaApplications;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationVideo;
import MediaApplications.Streams.UltraGridDXTStream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import NetworkRepresentation.NetworkSite;
import nu.xom.Element;

/**
 * UltraGrid 1080i DXT compressed 250 Mbps consumer
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:35:56
 */
public class UltraGridDXTConsumer extends MediaApplication implements MediaApplicationVideo, MediaApplicationConsumer, CoNMLInterface {
    private MediaApplication source;
    private NetworkSite sourceSite;


    public UltraGridDXTConsumer() {
        super("UltraGrid DXT compressed HD video consumer, 250Mbps", UltraGridDXTStream.getMyStreams());
    }

    public UltraGridDXTConsumer(String applicationName) {
        super(applicationName, UltraGridDXTStream.getMyStreams());
    }

    @Override
    public void setSource(MediaApplication target) {
        this.source = target;
    }

    @Override
    public MediaApplication getSource() {
        return this.source;
    }

    @Override
    public NetworkSite getSourceSite() {
        return sourceSite;
    }

    @Override
    public void setSourceSite(NetworkSite sourceSite) {
        this.sourceSite = sourceSite;
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:UltraGridDXTConsumer", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode);        
        
        if(this.source != null){
            Element mediaAppReferenceElem = new Element("cou:source", CoNMLInterface.CONML_NS);                
            mediaAppReferenceElem.appendChild(this.source.getUuid()); 
            baseElementNode.appendChild(mediaAppReferenceElem);
        }
        
        if(this.sourceSite != null){
            this.sourceSite.encodeToCoNML(result, new CoNMLEncodeParameters(baseElementNode));            
        }
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element sourceElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("source", CoNMLInterface.CONML_NS);
        if(sourceElem != null){
            MediaApplication decoded = (MediaApplication) result.getDecodedApps().get(sourceElem.getValue());//the presence of source application is assumed!!!
            this.source = decoded;
        }
        
        Element networkSiteElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("networkSite", CoNMLInterface.CONML_NS); 
        if(networkSiteElem != null){
            this.sourceSite = new NetworkSite();
            this.sourceSite.decodeFromCoNML(result, new CoNMLDecodeParameters(networkSiteElem));
        }
    }
}
