package MediaAppFactory;

/**
 * Interface extending MediaApplication for media producers
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:16:26
 */
public interface MediaApplicationProducer {

    public void setTargetIP(String targetIP);
    
    public String getTargetIP();

}
