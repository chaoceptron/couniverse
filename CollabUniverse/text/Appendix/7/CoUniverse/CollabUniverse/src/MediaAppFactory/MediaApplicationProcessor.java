package MediaAppFactory;

/**
 * Interface extending MediaApplication for media processors (e.g. Active Elements with modules)
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:20:32
 */
public interface MediaApplicationProcessor {
}
