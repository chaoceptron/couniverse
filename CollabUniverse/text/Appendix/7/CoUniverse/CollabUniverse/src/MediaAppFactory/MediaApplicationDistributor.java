package MediaAppFactory;

/**
 * Interface extending MediaApplication for data distributors (e.g., reflectors)
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:16:58
 */
public interface MediaApplicationDistributor {
}
