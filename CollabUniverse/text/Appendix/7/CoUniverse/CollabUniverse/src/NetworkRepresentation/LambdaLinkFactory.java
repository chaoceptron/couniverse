package NetworkRepresentation;

import net.es.oscars.client.Client;
import net.es.oscars.wsdlTypes.*;
import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;
import utils.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: xliska
 * Date: 15.1.2009
 * Time: 15:15:59
 */
public class LambdaLinkFactory {
    static Logger logger = Logger.getLogger("NetworkRepresentation");

    private Client oscarsClient;

    public LambdaLinkFactory(LambdaLink lambdaLink) {
        String url = lambdaLink.getLambdaLinkIDC();
        // Axis2 repo must be in $CWD or in $CLASSPATH
        String repo = "repo";

        this.oscarsClient = new Client();

        try {
            this.oscarsClient.setUp(true, url, repo);
        } catch (AxisFault e) {
            e.printStackTrace();
        }
    }

    public void allocate(LambdaLink lambdaLink) {
        ResCreateContent request = new ResCreateContent();
        PathInfo pathInfo = new PathInfo();
        Layer2Info layer2Info = new Layer2Info();

        /* Set request parameters */
        layer2Info.setSrcEndpoint(lambdaLink.getFromLambdaLinkEndPoint().getLambdaLinkEndpoint());
        layer2Info.setDestEndpoint(lambdaLink.getToLambdaLinkEndPoint().getLambdaLinkEndpoint());

        VlanTag srcVtag = new VlanTag();
        srcVtag.setString(lambdaLink.getFromLambdaLinkEndPoint().getLambdaLinkEndpointVlan());
        srcVtag.setTagged(lambdaLink.getFromLambdaLinkEndPoint().isLambdaLinkEndpointTagged());
        layer2Info.setSrcVtag(srcVtag);

        VlanTag destVtag = new VlanTag();
        destVtag.setString(lambdaLink.getToLambdaLinkEndPoint().getLambdaLinkEndpointVlan());
        destVtag.setTagged(lambdaLink.getToLambdaLinkEndPoint().isLambdaLinkEndpointTagged());
        layer2Info.setDestVtag(destVtag);

        pathInfo.setPathSetupMode("timer-automatic");
        request.setStartTime(System.currentTimeMillis() / 1000);
        request.setEndTime(System.currentTimeMillis() / 1000 + 60 * 60);
        // TODO: is this typcasting correct?
        request.setBandwidth((int) lambdaLink.bandwidth);
        request.setDescription("CoUniverse lambda reservation.");
        pathInfo.setLayer2Info(layer2Info);
        request.setPathInfo(pathInfo);

        LambdaLinkFactory.logger.info("Allocating lambda link: " + lambdaLink);
        try {
            /* Alocate the lambda */
            CreateReply response = this.oscarsClient.createReservation(request);

            lambdaLink.lambdaReservationId = response.getGlobalReservationId();
            /* Print repsponse information */
            LambdaLinkFactory.logger.info("  GRI: " + lambdaLink.lambdaReservationId);
            LambdaLinkFactory.logger.info("  Status: " + response.getStatus());

            // TODO I2 winter joint techs hack
            while (!(lambdaLink.status.equals("ACTIVE") || lambdaLink.status.equals("FINISHED"))) {
                this.query(lambdaLink);
                TimeUtils.sleepFor(5000);
            }
            lambdaLink.isAllocated = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        assert (lambdaLink.lambdaReservationId != null);

        LambdaLinkFactory.logger.info("Lambda link " + lambdaLink + " allocated with GRI: " + lambdaLink.lambdaReservationId);

        this.oscarsClient.cleanUp();
    }

    public void deallocate(LambdaLink lambdaLink) {
        if (!lambdaLink.status.equals("FINISHED")) {
            /* Send teardownPathContent request and print response */
            String cancelResponse;
            LambdaLinkFactory.logger.info("Deallocating lambda link: " + lambdaLink);
            if (lambdaLink.getLambdaReservationId() != null) {
                try {
                    GlobalReservationId gri = new GlobalReservationId();
                    gri.setGri(lambdaLink.lambdaReservationId);
                    cancelResponse = this.oscarsClient.cancelReservation(gri);
                    LambdaLinkFactory.logger.info("  GRI: " + lambdaLink.lambdaReservationId);
                    LambdaLinkFactory.logger.info("  Status: " + cancelResponse);

                    lambdaLink.isAllocated = false;
                    this.oscarsClient.cleanUp();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                LambdaLinkFactory.logger.debug("Lambda link was not allocated yet.");
            }
        } else {
            LambdaLinkFactory.logger.warn("Cannot cancel lambda link " + lambdaLink + " reservation with status FINISHED");
        }
    }

    public void modify(LambdaLink lambdaLink) {
        LambdaLinkFactory.logger.info("Modyfying lambda link: " + lambdaLink);

        ModifyResReply modifyResponse;
        if (lambdaLink.getLambdaReservationId() != null) {
            try {
                GlobalReservationId gri = new GlobalReservationId();
                gri.setGri(lambdaLink.lambdaReservationId);

                ModifyResContent request = new ModifyResContent();

                request.setGlobalReservationId(lambdaLink.lambdaReservationId);
                request.setBandwidth((int) lambdaLink.bandwidth);
                request.setDescription("CoUniverse lambda reservation.");
                // TODO set the times
                request.setStartTime(System.currentTimeMillis() / 1000);
                request.setEndTime(System.currentTimeMillis() / 1000 + 60 * 60);

                modifyResponse = this.oscarsClient.modifyReservation(request);

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                date.setTime(modifyResponse.getReservation().getStartTime() * 1000L);
                String startTime = df.format(date);

                date.setTime(modifyResponse.getReservation().getEndTime() * 1000L);
                String endTime = df.format(date);

                LambdaLinkFactory.logger.info("  GRI: " + lambdaLink.lambdaReservationId);
                LambdaLinkFactory.logger.info("  Status: " + modifyResponse.getReservation().getStatus());
                LambdaLinkFactory.logger.info("  New startTime: " + startTime);
                LambdaLinkFactory.logger.info("  New endTime: " + endTime);

                this.oscarsClient.cleanUp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LambdaLinkFactory.logger.debug("Lambda link was not allocated yet.");
        }
    }

    public void query(LambdaLink lambdaLink) {
        /* Send refreshPathContent request and print response */
        ResDetails queryResponse;
        String lambdaStatus = "UNKNOWN";

        // TODO move query logs to debug
        LambdaLinkFactory.logger.info("Querying lambda link: " + lambdaLink);
        if (lambdaLink.getLambdaReservationId() != null) {
            try {
                GlobalReservationId gri = new GlobalReservationId();
                gri.setGri(lambdaLink.lambdaReservationId);
                queryResponse = this.oscarsClient.queryReservation(gri);
                lambdaStatus = queryResponse.getStatus();
                LambdaLinkFactory.logger.info("GRI: " + queryResponse.getGlobalReservationId());
                LambdaLinkFactory.logger.info("Status: " + lambdaStatus);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LambdaLinkFactory.logger.debug("Lambda link was not allocated yet.");
        }

        this.oscarsClient.cleanUp();

        lambdaLink.status = lambdaStatus;
    }

    public void queryAndModify(LambdaLink lambdaLink) {
        ResDetails queryResponse;
        String lambdaStatus = "UNKNOWN";

        if (lambdaLink.getLambdaReservationId() != null) {
            try {
                GlobalReservationId gri = new GlobalReservationId();
                gri.setGri(lambdaLink.lambdaReservationId);
                queryResponse = this.oscarsClient.queryReservation(gri);
                lambdaStatus = queryResponse.getStatus();
                LambdaLinkFactory.logger.info("GRI: " + queryResponse.getGlobalReservationId());
                LambdaLinkFactory.logger.info("Status: " + lambdaStatus);

                long currentEndTime = queryResponse.getEndTime();
                long now = System.currentTimeMillis() / 1000;

                if (now > currentEndTime) {
                    LambdaLinkFactory.logger.warn("Cannot modify lambda link " + lambdaLink + " in state FINISHED.");    
                } else if (currentEndTime - now < 5 * 60) {
                    this.modify(lambdaLink);
                }

                this.oscarsClient.cleanUp();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            LambdaLinkFactory.logger.debug("Lambda link was not allocated yet.");
        }

        lambdaLink.status = lambdaStatus;

    }

}
