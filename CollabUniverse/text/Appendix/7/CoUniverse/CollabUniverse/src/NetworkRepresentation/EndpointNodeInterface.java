package NetworkRepresentation;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Nodes;
import nu.xom.XPathContext;

/**
 * Description of particular interface of an endpoint network node.
 * Node interfaces are connected to a particular SubNetworks. 
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class EndpointNodeInterface extends GeneralNodeInterface implements Serializable, CoNMLInterface {
    //private String nodeInterfaceName;
    private String ipAddress;
    private String netMask;
    private double bandwidth;
    private String subnet;
    private EndpointNetworkNode parentNode;    

    public EndpointNodeInterface() {
        super("lo");
        this.ipAddress = "127.0.0.1";
        this.netMask = "255.0.0.0";
        this.bandwidth = 100;
        this.subnet = "";
        this.parentNode = null;         
    }

    public EndpointNodeInterface(String hostInterface, String ipAddress, String netMask, long bandwidth, String subnet, EndpointNetworkNode parentNode) {
        super();
        super.setNodeInterfaceName(hostInterface);
        this.ipAddress = ipAddress;
        this.netMask = netMask;
        this.bandwidth = bandwidth;
        this.subnet = subnet;
        this.parentNode = parentNode;        
    }

    /**
     * Getter for node interface IP address
     * <p>
     *
     * @return String IP of the interface
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Setter for the node interface  IP address
     * <p>
     *
     * @param ipAddress String
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * Getter for the node interface netmask
     * <p>
     *
     * @return String interface network mask
     */
    public String getNetMask() {
        return netMask;
    }

    /**
     * Setter for the interface netmask
     * <p>
     *
     * @param netMask String
     */
    public void setNetMask(String netMask) {
        this.netMask = netMask;
    }

    /**
     * Getter for the interface bandwidth
     * <p>
     *
     * @return double bandwidth
     */
    public double getBandwidth() {
        return bandwidth;
    }

    /**
     * Setter for the interface bandwidth
     * <p>
     *
     * @param bandwidth double
     */
    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    /**
     * Setter for interface subnet
     * <p>
     *
     * @param subnet SubNetwork
     */
    public void setSubNetwork(String subnet) {
        this.subnet = subnet;
    }

    /**
     * Getter for node interface name
     * <p>
     *
     * @return String node interface name
     */
    /*public String getNodeInterfaceName() {
        return nodeInterfaceName;
    }*/

    /**
     * Setter for node interface name
     * <p>
     *
     * @param nodeInterfaceName String
     */
    /*public void setNodeInterfaceName(String nodeInterfaceName) {
        this.nodeInterfaceName = nodeInterfaceName;
    }*/

    /**
     * Getter for subnet which is interface connected to
     * <p>
     * @return SubNetwork
     */
    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public EndpointNetworkNode getParentNode() {
        return parentNode;
    }

    public void setParentNode(EndpointNetworkNode parentNode) {
        this.parentNode = parentNode;
    }

    public Set<GeneralNetworkLink> getOutputLinks() {
        return new HashSet<GeneralNetworkLink>(parentNode.getNetworkTopologyGraph().outgoingEdgesOf(parentNode));
    }
    
    public Set<GeneralNetworkLink> getInputLinks() {
        return new HashSet<GeneralNetworkLink>(parentNode.getNetworkTopologyGraph().incomingEdgesOf(parentNode));
    }

    @Override
    public String toString() {
        return "" + super.getNodeInterfaceName() + "/" + this.ipAddress + "/" + this.netMask;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {          
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);
        if(extNodes == null){            
            return null;
        }    
        if(extNodes.getMainNode() == null  || extNodes.getSubclassExtensionNode() == null){
            return new CoNMLExtensionNodes(extNodes.getMainNode(), extNodes.getSubclassExtensionNode(), extNodes.getReferenceNode());
        }
        
        //writing anyOther elements
        Element extensionElem;
        {
            extensionElem = new Element("cou:EndpointNodeInterface", CoNMLInterface.CONML_NS);
            ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);

            if(this.ipAddress != null){
                Element ipAddressElem = new Element("cou:ipAddress", CoNMLInterface.CONML_NS);
                ipAddressElem.appendChild(this.ipAddress); 
                extensionElem.appendChild(ipAddressElem);
            }

            if(this.netMask != null){
                Element netMaskElem = new Element("cou:netMask", CoNMLInterface.CONML_NS);
                netMaskElem.appendChild(this.netMask); 
                extensionElem.appendChild(netMaskElem);
            }
            
            Element bandwidthElem = new Element("cou:bandwidth", CoNMLInterface.CONML_NS);
            bandwidthElem.appendChild(Double.toString(this.bandwidth)); 
            extensionElem.appendChild(bandwidthElem);

            if(this.subnet != null){
                Element subnetElem = new Element("cou:subnet", CoNMLInterface.CONML_NS);
                subnetElem.appendChild(this.subnet); 
                extensionElem.appendChild(subnetElem);        
            }
        }

        //writing related links
        Set<GeneralNetworkLink> links = null;
        Element relationElem = null; //element node of interface currently being written, which describes a relation to its links 
        if(parameters.getInputsRelation() != null){
            relationElem = (Element) parameters.getInputsRelation();
            links = this.getInputLinks();                
        }else if(parameters.getOutputsRelation() != null){
            relationElem = (Element) parameters.getOutputsRelation();
            links = this.getOutputLinks();
        }
        if(links == null){
            links = new HashSet<GeneralNetworkLink>();
        }

        for(GeneralNetworkLink link: links){
            link.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, relationElem));
        }
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), extensionElem, extNodes.getReferenceNode());
    }    
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS);
        
        Element ipAddrElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("ipAddress", CoNMLInterface.CONML_NS); 
        if(ipAddrElem != null){
            this.ipAddress = ipAddrElem.getValue();
        }

        Element netMaskElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("netMask", CoNMLInterface.CONML_NS);
        if(netMaskElem != null){
            this.netMask = netMaskElem.getValue();
        }

        Element bandwidthElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("bandwidth", CoNMLInterface.CONML_NS);
        if(bandwidthElem != null){
            if(bandwidthElem.getValue().isEmpty() == false){
                this.bandwidth = new Double(bandwidthElem.getValue());
            }
        }

        Element subnetElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("subnet", CoNMLInterface.CONML_NS);
        if(subnetElem != null){
            this.subnet= subnetElem.getValue();
        }
            
        //decoding outcoming links  
        Elements links = null;
        Nodes linkRelationNodes = parameters.getCurrentNode().getParent().getParent().query("nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#isSink']", context);
        Element linkRelationElem = null;
        if(linkRelationNodes.size() > 0){
            linkRelationElem = (Element) linkRelationNodes.get(0);
        }
        if(linkRelationElem != null){
            links = linkRelationElem.getChildElements("Link", CoNMLInterface.NML_NS);
        }
        if(links != null){
            for(int i = 0; i < links.size(); i++){
                GeneralNetworkLink link = (GeneralNetworkLink) result.getDecodedLinks().get(((Element) links.get(i)).getAttributeValue("id"));
                if(link != null){
                    link.setFromNode(this.parentNode);
                    if(link instanceof PhysicalNetworkLink){
                        ((PhysicalNetworkLink) link).setNodeInterface(this);
                    }
                    if(link instanceof LogicalNetworkLink){
                        ((LogicalNetworkLink) link).setFromInterface(this);
                    }
                }
            }
        }

        //decoding incoming links                
        linkRelationNodes = parameters.getCurrentNode().getParent().getParent().query("nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#isSource']", context);
        if(linkRelationNodes.size() > 0){
            linkRelationElem = (Element) linkRelationNodes.get(0);
        }        
        if(linkRelationElem != null){
            links = linkRelationElem.getChildElements("Link", CoNMLInterface.NML_NS);
        }
        if(links != null){
            for(int i = 0; i < links.size(); i++){
                GeneralNetworkLink link = (GeneralNetworkLink) result.getDecodedLinks().get(((Element) links.get(i)).getAttributeValue("id"));
                if(link != null){
                    link.setToNode(this.parentNode);
                    if(link instanceof PhysicalNetworkLink){
                        ((PhysicalNetworkLink) link).setNodeInterface(this);
                    }
                    if(link instanceof LogicalNetworkLink){
                        ((LogicalNetworkLink) link).setToInterface(this);
                    }
                }
            }
        }   
    }
}
