package NetworkRepresentation;

import java.io.Serializable;
import nu.xom.Element;

/**
 * Representation of a subnetwork with unknown internal topology as a network node. The UnknownNetworkNode class supports serializing the instance into a XML to be sent over JXTA pipe.
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class UnknownNetworkNode extends GeneralNetworkNode implements Serializable, CoNMLInterface {    

    /**
     * This is an empty JavaBean constructor in order to support XMLEncoder and XMLDecoder
     */
    public UnknownNetworkNode() {
        super();
        
        this.nodeType = GeneralNetworkNode.NODE_TYPE_UNKNOWN_NETWORK;        
    }

    /**
     * UnknownNetworkNode constructor.
     * <p/>
     *
     * @param nodeName         name of the node
     */
    public UnknownNetworkNode(String nodeName) {
        super(nodeName);
        
        this.nodeType = GeneralNetworkNode.NODE_TYPE_UNKNOWN_NETWORK;        
    }

    @Override
    public String toString() {
        return "" + this.getNodeName();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }         
        
        //anyOther element
        Element extensionElem = new Element("cou:UnknownNetworkNode", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);
        
        //writing one dummy interface/port
        DummyInterface dummyPort = new DummyInterface();
        
        CoNMLEncodeParameters inboundPortParameters = new CoNMLEncodeParameters(parameters.getParentNode());
        inboundPortParameters.setInputsRelation(parameters.getInputsRelation());
        dummyPort.encodeToCoNML(result, inboundPortParameters);
            
        CoNMLEncodeParameters outboundPortParameters = new CoNMLEncodeParameters(parameters.getParentNode());
        outboundPortParameters.setOutputsRelation(parameters.getOutputsRelation());
        dummyPort.encodeToCoNML(result, outboundPortParameters);
        
        //writing links
        for(GeneralNetworkLink link: this.getInputLinks()){
            link.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, inboundPortParameters.getInputsRelation()));
        }
        for(GeneralNetworkLink link: this.getOutputLinks()){
            link.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, outboundPortParameters.getOutputsRelation()));
        }
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), extensionElem, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNML resultoutput parameter cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }   
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
    }
    
    public void update(UnknownNetworkNode newerNode){
        super.update(newerNode);
    }
}
