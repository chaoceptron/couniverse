/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package NetworkRepresentation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Node;
import nu.xom.ParsingException;

/**
 *
 * @author Matus
 */
public class CoNMLDecodeParameters {//This class allows an easy extensibility of parameters for CoNML. For example, another parameters may be added for objects which are being decoded from multiple separate dom nodes.
    private Node currentNode;
    private int decodeMode;
    
    public CoNMLDecodeParameters(String xmlString) {
        ByteArrayInputStream in = new ByteArrayInputStream(xmlString.getBytes());
        Builder builder = new Builder();
        try {
            Document doc = builder.build(in);
            this.currentNode = doc.getRootElement();
        } catch (ParsingException ex) {
            Logger.getLogger(CoNMLEncodeResult.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CoNMLEncodeResult.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.decodeMode = 0;
    }
    
    public CoNMLDecodeParameters(Document doc) {
        if(doc == null){
            throw new IllegalArgumentException("Document cannot be null.");
        }
        this.currentNode = doc.getRootElement();
        if(currentNode == null){
            throw new NullPointerException("Document doesn't have a root node.");
        }
        this.decodeMode = 0;
    }
    
    public CoNMLDecodeParameters(Node currentNode, int decodeMode) {
        if(currentNode == null){
            throw new IllegalArgumentException("Node cannot be null.");
        }        
        this.currentNode = currentNode;
        this.decodeMode = decodeMode;
    }
    
    public CoNMLDecodeParameters(Node currentNode) {
        this(currentNode, 0);
    }

    public Node getCurrentNode() {        
        return currentNode;
    }

    public void setCurrentNode(Node currentNode) {
        if(currentNode == null){
            throw new IllegalArgumentException("Node cannot be null.");
        }
        this.currentNode = currentNode;
    }

    public int getDecodeMode() {
        return decodeMode;
    }

    public void setDecodeMode(int decodeMode) {
        this.decodeMode = decodeMode;
    }
    
    //TODO read file, byteBuffer, ...
}
