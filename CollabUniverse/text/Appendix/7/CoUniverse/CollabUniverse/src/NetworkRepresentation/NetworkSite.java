package NetworkRepresentation;

import java.io.Serializable;
import nu.xom.Element;

/**
 * Representation of a site. Allows for aggregation of NetworkNodes per site.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 2.8.2007
 * Time: 11:10:35
 */
public class NetworkSite implements Serializable, CoNMLInterface {
    private String siteName;

    /**
     * Empty site constructor
     */
    public NetworkSite() {
        this.siteName = "";
    }

    /**
     * Site constructor.
     * <p/>
     *
     * @param siteName String representation of site name
     */
    public NetworkSite(String siteName) {
        assert siteName != null;
        this.siteName = siteName;
    }


    /**
     * Returns site name
     * <p/>
     *
     * @return site name
     */
    public String getSiteName() {
        return siteName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkSite that = (NetworkSite) o;

        return siteName.equals(that.siteName);

    }

    @Override
    public int hashCode() {
        return siteName.hashCode();
    }

    // JavaBean enforced setters

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        Element baseElementNode = new Element("cou:networkSite", CoNMLInterface.CONML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().appendChild(baseElementNode);
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElementNode);
        }
        
        if(this.siteName != null){
            baseElementNode.appendChild(this.siteName);
        }         
        
        return new CoNMLExtensionNodes(baseElementNode, null, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        this.siteName = parameters.getCurrentNode().getValue();
    }
}
