/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

/**
 * 
 * @author Matus
 */
public interface CoNMLInterface {  
    public static final String NML_NS = "http://schemas.ogf.org/nml/2013/05/base#";
    public static final String CONML_NS = "https://www.sitola.cz/CoUniverse/2013/12/NetworkRepresentation";
    
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters);
    
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters); 
}
