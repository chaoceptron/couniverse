package NetworkRepresentation;

import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import java.util.HashMap;

/**
 * Class for visualizing network topology
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 21.8.2007
 * Time: 13:41:53
 */
public class TopologyVisualizer extends JFrame {
    static Logger logger = Logger.getLogger("NetworkRepresentation");

    DirectedWeightedMultigraph<NetworkNode, NetworkLink> networkTopologyGraph;
    static JGraphModelAdapter<NetworkNode, NetworkLink> m_jgAdapter;
    JGraph topoGraph;
    JScrollPane scrollPane;

    private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    private static final Dimension DEFAULT_SIZE = new Dimension(1000, 800);

    public TopologyVisualizer(DirectedWeightedMultigraph<NetworkNode, NetworkLink> g) throws HeadlessException {
        super();

        this.networkTopologyGraph = g;
        m_jgAdapter = new JGraphModelAdapter<NetworkNode, NetworkLink>(g);
        topoGraph = new JGraph(m_jgAdapter);
        // topoGraph.setPreferredSize(DEFAULT_SIZE);
        // MaximumSize has to be twice that wide - why???
        // topoGraph.setMaximumSize(new Dimension((int) DEFAULT_SIZE.getWidth() * 2, (int) DEFAULT_SIZE.getHeight()));
        // topoGraph.setSize(DEFAULT_SIZE);
        // topoGraph.setBackground(DEFAULT_BG_COLOR);

        scrollPane = new JScrollPane(topoGraph);
        this.setTitle("Network Topology");
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                TopologyVisualizer.logger.debug("Closing window on request.");
            }
        });
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);
        this.pack();
        this.setSize(DEFAULT_SIZE);
    }

    public void updateTopologyVisualisation(DirectedWeightedMultigraph<NetworkNode, NetworkLink> g) {

        // This should return all cells existing in the graph model
        Object[] graphCells = topoGraph.getRoots();
        // Clear existing network topology visualisation
        topoGraph.getModel().remove(graphCells);

        int nVertices = g.vertexSet().size();
        double angleIncrement = 360.0 / nVertices;
        double angle = 0.0;
        double diameter = 300.0;
        for (NetworkNode v : g.vertexSet()) {
            positionVertexAt(v,
                    (int) Math.round(diameter * Math.sin(Math.toRadians(angle))),
                    (int) Math.round(diameter * Math.cos(Math.toRadians(angle))));
            angle += angleIncrement;
        }
    }

    private void positionVertexAt(Object vertex, int x, int y) {
        DefaultGraphCell cell = m_jgAdapter.getVertexCell(vertex);
        Map attr = cell.getAttributes();
        Rectangle2D b = GraphConstants.getBounds(attr);

        GraphConstants.setBounds(attr, new Rectangle((int) (DEFAULT_SIZE.getWidth() / 2) - 100 + x, (int) (DEFAULT_SIZE.getHeight() / 2) - 50 + y, (int) Math.round(b.getWidth()), (int) Math.round(b.getHeight())));

        Map<DefaultGraphCell, Map> cellAttr = new HashMap<DefaultGraphCell, Map>();
        //noinspection unchecked
        cellAttr.put(cell, attr);
        m_jgAdapter.edit(cellAttr, null, null, null);
    }
    
}
