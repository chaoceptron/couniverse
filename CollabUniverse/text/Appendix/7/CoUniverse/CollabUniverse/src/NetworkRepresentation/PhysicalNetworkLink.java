package NetworkRepresentation;

import java.io.Serializable;
import java.util.HashSet;
import nu.xom.Attribute;
import nu.xom.Element;

/**
 * Representation of known physical link between GeneralNetworkNodes
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class PhysicalNetworkLink extends GeneralNetworkLink implements Serializable {

    private String linkName = "";
    private boolean fullDuplex = false;//was set to true
    private LambdaLink lambda = null;
    private HashSet<LogicalNetworkLink> traversingLogicalLinks = new HashSet<LogicalNetworkLink>();
    private EndpointNodeInterface nodeInterface = null; // since physical link can not connect two endpoint nodes, it can also have only one interface associated
    private PhysicalNetworkLink backLink = null;

    public PhysicalNetworkLink() {
        super();

    }

    public PhysicalNetworkLink(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex) {
        this(linkName, capacity, fromNode, toNode, fullDuplex, null, null);
    }
    
    public PhysicalNetworkLink(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex, EndpointNodeInterface iface) {
        this(linkName, capacity, fromNode, toNode, fullDuplex, iface, null);
    }
        
    public PhysicalNetworkLink(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex, EndpointNodeInterface iface, PhysicalNetworkLink backLink) {
        super(capacity, fromNode, toNode);

        assert !(fromNode.getClass() == EndpointNetworkNode.class && toNode.getClass() == EndpointNetworkNode.class) : "Physical link connecting two endpoint nodes!";
        this.linkName = linkName;
        this.fullDuplex = fullDuplex;
        this.nodeInterface = iface;
        this.backLink = backLink;
    }
    
    public void setParameters(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex) {
        this.setParameters(linkName, capacity, fromNode, toNode, fullDuplex, null, null);
    }
    
    public void setParameters(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex, EndpointNodeInterface nodeInterface) {
        this.setParameters(linkName, capacity, fromNode, toNode, fullDuplex, nodeInterface, null);
    }

    public void setParameters(String linkName, double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode, boolean fullDuplex, EndpointNodeInterface iface, PhysicalNetworkLink backLink) {
        assert !(fromNode.getClass() == EndpointNetworkNode.class && toNode.getClass() == EndpointNetworkNode.class) : "Physical link connecting two endpoint nodes!";
        super.setParameters(capacity, fromNode, toNode);
        this.linkName = linkName;
        this.fullDuplex = fullDuplex;
        this.nodeInterface = iface;
        this.backLink = backLink;
    }

    @Override
    public String toString() {
        return "" + this.linkName + ": " + this.getFromNode() + "  --->  " + this.getToNode();
    }

    boolean isLambdaBased() {
        return this.lambda != null;
    }

    boolean isLinkAllocated() {
        // is there are no lambdas associated, the link is understood to be allocated permanently
        if (this.lambda != null && !this.lambda.isAllocated) {
            return false;
        }
        return true;
    }

    void associateLambda(LambdaLink lambdaLink) {
        this.lambda = lambdaLink;
    }

    boolean isFullDuplex() {
        return fullDuplex;
    }

    void setFullDuplex(boolean fullDuplex) {
        this.fullDuplex = fullDuplex;
    }

    LambdaLink getLambda() {
        return lambda;
    }

    void setLambda(LambdaLink lambda) {
        this.lambda = lambda;
    }

    EndpointNodeInterface getEndpointNodeInterface() {
        return nodeInterface;
    }

    public PhysicalNetworkLink getBackLink() {
        return backLink;
    }

    public void setBackLink(PhysicalNetworkLink backLink) {
        this.backLink = backLink;
    }
    
    void addTraversingLogicalLink(LogicalNetworkLink link) {
        this.traversingLogicalLinks.add(link);
    }
    
    void removeTraversingLogicalLink(LogicalNetworkLink link) {
        this.traversingLogicalLinks.remove(link);
    }
    
    void clearTraversingLogicalLinks() {
        this.traversingLogicalLinks.clear();
    }

    public HashSet<LogicalNetworkLink> getTraversingLogicalLinks() {
        return traversingLogicalLinks;
    }

    public void setTraversingLogicalLinks(HashSet<LogicalNetworkLink> traversingLogicalLinks) {
        this.traversingLogicalLinks = traversingLogicalLinks;
    }
    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }  

    public EndpointNodeInterface getNodeInterface() {
        return nodeInterface;
    }

    public void setNodeInterface(EndpointNodeInterface nodeInterface) {
        this.nodeInterface = nodeInterface;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return super.equals(obj);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){            
            return null;
        }    
        if(extNodes.getMainNode() == null  || extNodes.getSubclassExtensionNode() == null){
            return new CoNMLExtensionNodes(extNodes.getMainNode(), extNodes.getSubclassExtensionNode(), extNodes.getReferenceNode());
        }
        
        if(this.linkName != null){//name element
            Element nameElem = new Element("nml:name", CoNMLInterface.NML_NS);
            nameElem.appendChild(this.linkName);
            ((Element) extNodes.getMainNode()).insertChild(nameElem, 0);
        }
        
        //anyOther element
        Element extensionElem = new Element("cou:PhysicalNetworkLink", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);
        
        Element fullDuplexElem = new Element("cou:fullDuplex", CoNMLInterface.CONML_NS);
        fullDuplexElem.appendChild(String.valueOf(this.fullDuplex)); 
        extensionElem.appendChild(fullDuplexElem);
        
        //bidirectional link
        if(this.backLink != null){
            //getting id of bidirectional link
            String id;
            if(this.uuid.compareTo(this.backLink.uuid) < 0){
                id = this.uuid;
            }else{
                id = this.backLink.uuid;
            }   
            id = id + "-bidirectional";
            
            //writing bidirectional link to root/parent node, unless it's already there            
            if(result.getUuidSet() != null && result.getUuidSet().add(id)){                
                Element bidirectionalLinkElem = new Element("nml:BidirectionalLink", CoNMLInterface.NML_NS);
                if(parameters.getParentNode() == null){
                    result.getDoc().getRootElement().appendChild(bidirectionalLinkElem);
                }else{
                    ((Element) parameters.getParentNode()).appendChild(bidirectionalLinkElem);
                }                
                bidirectionalLinkElem.addAttribute(new Attribute("id", id, Attribute.Type.ID));                
                
                Element thisLinkReference = new Element("nml:Link", CoNMLInterface.NML_NS);            
                thisLinkReference.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));
                bidirectionalLinkElem.appendChild(thisLinkReference);

                Element backLinkReference = new Element("nml:Link", CoNMLInterface.NML_NS);
                backLinkReference.addAttribute(new Attribute("id", this.backLink.uuid, Attribute.Type.ID));
                bidirectionalLinkElem.appendChild(backLinkReference);
            }                
        }
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), extensionElem, extNodes.getReferenceNode());
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element linkNode = (Element) parameters.getCurrentNode().getParent().getParent();
        
        Element nameElem = linkNode.getFirstChildElement("name", CoNMLInterface.NML_NS); 
        if(nameElem != null){
            this.linkName = nameElem.getValue();
        }
        
        Element fullDuplexElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("fullDuplex", CoNMLInterface.CONML_NS); 
        if(fullDuplexElem != null){
            this.fullDuplex = Boolean.valueOf(fullDuplexElem.getValue());
        }
    }
}
