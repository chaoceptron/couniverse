package NetworkRepresentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Nodes;
import nu.xom.XPathContext;

/**
 * Representation of logical end-to-end links between EndpointNetworkNodes
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class LogicalNetworkLink extends GeneralNetworkLink implements Serializable, CoNMLInterface {

    private double latency = 0;
    private EndpointNodeInterface fromInterface = null;
    private EndpointNodeInterface toInterface = null;
    private HashSet<PhysicalNetworkLink> physicalLinksOnThePath = new HashSet<PhysicalNetworkLink>();

    public LogicalNetworkLink() {
        super();
    }

    public void setParameters(double capacity, double latency, EndpointNetworkNode fromNode, EndpointNodeInterface fromInterface, EndpointNetworkNode toNode, EndpointNodeInterface toInterface) {
        super.setParameters(capacity, fromNode, toNode);
        this.latency = latency;
        this.fromInterface = fromInterface;
        this.toInterface = toInterface;
    }

    public double getLatency() {
        return latency;
    }

    public void setLatency(double latency) {
        this.latency = latency;
    }

    @Override
    public EndpointNetworkNode getFromNode() {
        return (EndpointNetworkNode) fromNode;
    }

    public void setFromNode(EndpointNetworkNode fromNode) {
        this.fromNode = fromNode;
    }

    public EndpointNodeInterface getFromInterface() {
        return fromInterface;
    }

    public void setFromInterface(EndpointNodeInterface fromInterface) {
        this.fromInterface = fromInterface;
    }

    @Override
    public EndpointNetworkNode getToNode() {
        return (EndpointNetworkNode) toNode;
    }

    public void setToNode(EndpointNetworkNode toNode) {
        this.toNode = toNode;
    }

    public EndpointNodeInterface getToInterface() {
        return toInterface;
    }

    public void setToInterface(EndpointNodeInterface toInterface) {
        this.toInterface = toInterface;
    }

    @Override
    public String toString() {
        return "" + this.getFromInterface() + "  --->  " + this.getToInterface();
    }

    boolean isLinkAllocated() {
        // is there are no lambdas along the path, the link is understood to be allocated permanently
        if (this.physicalLinksOnThePath.isEmpty()) {
            return true;
        }
        for (PhysicalNetworkLink physicalLink : physicalLinksOnThePath) {
            if (!physicalLink.isLinkAllocated()) {
                return false;
            }
        }
        return true;
    }

    void addPhysicalLink(PhysicalNetworkLink link) {
        assert link != null;
        this.physicalLinksOnThePath.add(link);
    }
    
    void removePhysicalLink(PhysicalNetworkLink link) {
        assert link != null;
        this.physicalLinksOnThePath.remove(link);
    }

    HashSet<PhysicalNetworkLink> getPhysicalLinksOnThePath() {
        return physicalLinksOnThePath;
    }

    void setPhysicalLinksOnThePath(HashSet<PhysicalNetworkLink> physicalLinks) {
        assert physicalLinks != null;
        this.physicalLinksOnThePath = physicalLinks;
    }

    public ArrayList<LambdaLink> getAssociatedLambdas() {
        ArrayList<LambdaLink> lambdas = new ArrayList<LambdaLink>();
        for (PhysicalNetworkLink physical : physicalLinksOnThePath) {
            if (physical.getLambda() != null) {
                lambdas.add(physical.getLambda());
            }
        }
        return lambdas;
    }
    
    void removeAllPhysicalLinks() {
        this.physicalLinksOnThePath.clear();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){            
            return null;
        }    
        if(extNodes.getMainNode() == null  || extNodes.getSubclassExtensionNode() == null){
            return new CoNMLExtensionNodes(extNodes.getMainNode(), extNodes.getSubclassExtensionNode(), extNodes.getReferenceNode());
        }  
        
        Element extensionElem = new Element("cou:LogicalNetworkLink", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);
        
        {
            Element latencyElem = new Element("cou:latency", CoNMLInterface.CONML_NS);
            latencyElem.appendChild(Double.toString(this.latency)); 
            extensionElem.appendChild(latencyElem); 
        }
        
        //writing isSerialCompoundLink relation
        if(this.physicalLinksOnThePath.isEmpty() == false){
            Element relationElem = new Element("nml:Relation", CoNMLInterface.NML_NS);
            ((Element) extNodes.getMainNode()).appendChild(relationElem);

            relationElem.addAttribute(new Attribute("type", "http://schemas.ogf.org/nml/2013/05/base#isSerialCompoundLink"));     

            GeneralNetworkLink currentLink = null;
            HashSet<PhysicalNetworkLink> physicalLinks = new HashSet<PhysicalNetworkLink>();
            physicalLinks.addAll(this.physicalLinksOnThePath);

            for(PhysicalNetworkLink link: physicalLinks){
                if(link.fromNode.equals(this.fromNode)){
                    currentLink = link;
                    physicalLinks.remove(link);
                    break;
                }                
            }
            if(currentLink != null){
                CoNMLExtensionNodes thisLinksExtNodes = currentLink.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, relationElem));
                
                while(!currentLink.toNode.equals(this.toNode)){
                    for(PhysicalNetworkLink link: physicalLinks){
                        if(link.fromNode.equals(currentLink.toNode)){//searching for next
                            Element hasNextRelation = new Element("nml:Relation", CoNMLInterface.NML_NS);  
                            ((Element) thisLinksExtNodes.getReferenceNode()).appendChild(hasNextRelation);
                            
                            hasNextRelation.addAttribute(new Attribute("type", "http://schemas.ogf.org/nml/2013/05/base#next"));  

                            link.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, hasNextRelation));
                            thisLinksExtNodes = link.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode(), null, relationElem));
                            
                            currentLink = link;
                            physicalLinks.remove(link); 
                            break;
                        }                
                    }
                }
            }
        }
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), extensionElem, extNodes.getReferenceNode());
    }    
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS); 
        
        Element latencyElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("latency", CoNMLInterface.CONML_NS); 
        if(latencyElem != null){
            try{
                this.latency = Double.valueOf(latencyElem.getValue());
            }catch(NumberFormatException ex){}
        }

        //decoding traversing physical links
        Nodes relationNodes = parameters.getCurrentNode().getParent().getParent().query("nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#isSerialCompoundLink']", context);
        if(relationNodes.size() > 0){
            Elements links = ((Element) relationNodes.get(0)).getChildElements("Link");
            for(int i = 0; i < links.size(); i++){
                String linkUuid = ((Element) links.get(i)).getAttributeValue("id");
                PhysicalNetworkLink pLink = (PhysicalNetworkLink) result.getDecodedLinks().get(linkUuid);
                if(pLink == null){
                    pLink = new PhysicalNetworkLink();
                    pLink.setUuid(linkUuid);
                    result.getDecodedLinks().put(linkUuid, pLink);
                }
                this.getPhysicalLinksOnThePath().add(pLink);
                pLink.getTraversingLogicalLinks().add(this);
            }
        }      
    }
}
