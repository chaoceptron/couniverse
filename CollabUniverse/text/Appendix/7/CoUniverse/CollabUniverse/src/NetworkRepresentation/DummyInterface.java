/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.XPathContext;

/**
 * It's main purpose is for CoNML interface implementations of
 * encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters),
 * which are used by nodes with incoming/outcoming links, but have no
 * network interfaces with which they could associate them.
 * 
 * @author Matus
 */
public class DummyInterface extends GeneralNodeInterface implements CoNMLInterface {
    
    public DummyInterface() {
        super("dummy_interface");         
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){            
            return null;
        }    
        if(extNodes.getMainNode() == null  || extNodes.getSubclassExtensionNode() == null){
            return new CoNMLExtensionNodes(extNodes.getMainNode(), extNodes.getSubclassExtensionNode(), extNodes.getReferenceNode());
        }
        
        Element extensionElem = new Element("cou:DummyInterface", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), (Node) extensionElem, extNodes.getReferenceNode());
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS);
        
        //decoding outcoming links 
        Elements links = null;
        Nodes linkRelationNodes = parameters.getCurrentNode().getParent().getParent().query("nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#isSink']", context);
        Element linkRelationElem = null;
        if(linkRelationNodes.size() > 0){
            linkRelationElem = (Element) linkRelationNodes.get(0);
        }
        if(linkRelationElem != null){
            links = linkRelationElem.getChildElements("Link", CoNMLInterface.NML_NS);
        }
        if(links != null){
            for(int i = 0; i < links.size(); i++){
                GeneralNetworkLink link = (GeneralNetworkLink) result.getDecodedLinks().get(((Element) links.get(i)).getAttributeValue("id"));
                if(link != null){
                    Nodes refNodes = parameters.getCurrentNode().getParent().getParent().getParent().query(".//nml:Node/nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#hasOutboundPort']/nml:Port[@id='" + this.uuid + "']", context);
                    if(refNodes.size() > 0){
                        link.setFromNode((GeneralNetworkNode) result.getDecodedNodes().get(((Element) refNodes.get(0).getParent().getParent()).getAttributeValue("id")));
                    }
                }
            }
        }

        //decoding incoming links                
        linkRelationNodes = parameters.getCurrentNode().getParent().getParent().query("nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#isSource']", context);
        if(linkRelationNodes.size() > 0){
            linkRelationElem = (Element) linkRelationNodes.get(0);
        }        
        if(linkRelationElem != null){
            links = linkRelationElem.getChildElements("Link", CoNMLInterface.NML_NS);
        }
        if(links != null){
            for(int i = 0; i < links.size(); i++){
                GeneralNetworkLink link = (GeneralNetworkLink) result.getDecodedLinks().get(((Element) links.get(i)).getAttributeValue("id"));
                if(link != null){
                    Nodes refNodes = parameters.getCurrentNode().getParent().getParent().getParent().query(".//nml:Node/nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#hasInboundPort']/nml:Port[@id='" + this.uuid + "']", context);
                    if(refNodes.size() > 0){
                        link.setToNode((GeneralNetworkNode) result.getDecodedNodes().get(((Element) refNodes.get(0).getParent().getParent()).getAttributeValue("id")));
                    }
                }
            }
        }
    }
}
