/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import nu.xom.Node;

/**
 * Holds parameters for CoNML interface implementations of encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters).
 * 
 * @author Matus
 */
public class CoNMLEncodeParameters {
    private Node parentNode;
    private Node inputsRelation;
    private Node outputsRelation;
    
    public CoNMLEncodeParameters() {
    }

    public CoNMLEncodeParameters(Node parentNode) {
        this.parentNode = parentNode;              
    }
    
    public CoNMLEncodeParameters(Node parentNode, Node inputsRelation, Node outputsRelation) {
        this.parentNode = parentNode;
        this.inputsRelation = inputsRelation;
        this.outputsRelation = outputsRelation;        
    }

    /**
     * 
     * @return modifiable Node
     */
    public Node getParentNode() {
        return parentNode;
    }

    public void setParentNode(Node parentNode) {
        this.parentNode = parentNode;
    }

    /**
     * 
     * @return modifiable Node
     */
    public Node getInputsRelation() {
        return inputsRelation;
    }

    public void setInputsRelation(Node inputsRelation) {
        this.inputsRelation = inputsRelation;
    }

    /**
     * 
     * @return modifiable node 
     */
    public Node getOutputsRelation() {
        return outputsRelation;
    }

    public void setOutputsRelation(Node outputsRelation) {
        this.outputsRelation = outputsRelation;
    }
}
