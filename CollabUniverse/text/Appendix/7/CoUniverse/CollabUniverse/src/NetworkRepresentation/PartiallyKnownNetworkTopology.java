package NetworkRepresentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CopyOnWriteArraySet;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.XPathContext;
import org.apache.log4j.Logger;
import org.jgrapht.graph.DirectedWeightedMultigraph;

/**
 * Representation of partially knowh network topology
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class PartiallyKnownNetworkTopology implements Cloneable, Serializable, CoNMLInterface {
    static Logger logger = Logger.getLogger("NetworkRepresentation");

    /**
     * This is a synchronized class to work with change status.
     */
    private class ChangedStatus {
        private NetworkTopologyStatus status;

        public ChangedStatus() {
            status = new NetworkTopologyStatus();
            status.setChanged(false);
            status.setChangedStamp(System.currentTimeMillis());
            status.setChangesCount(0);
        }

        synchronized NetworkTopologyStatus getChanged() {
            NetworkTopologyStatus s = new NetworkTopologyStatus();
            s.setChanged(status.isChanged());
            s.setChangedStamp(status.getChangedStamp());
            s.setChangesCount(status.getChangesCount());
            return s;
        }

        synchronized void makeChange() {
            status.setChanged(true);
            status.setChangedStamp(System.currentTimeMillis());
            status.setChangesCount(status.getChangesCount() + 1);
        }

        synchronized void invalidateChanges() {
            status.setChanged(false);
            status.setChangedStamp(System.currentTimeMillis());
            status.setChangesCount(0);
        }
    }

    private DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> networkTopologyGraph;
    private HashSet<EndpointNetworkNode> endpointNodes;
    private HashSet<UnknownNetworkNode> unknownNetworkNodes;
    private HashSet<PhysicalNetworkNode> physicalNodes;
    private HashSet<PhysicalNetworkLink> physicalLinks;
    private HashSet<LogicalNetworkLink> logicalLinks;
    private HashMap<String, EndpointSubNetwork> subNets;
    private ChangedStatus changed;
    private String topologyName;
    private static final double initLinkLatency = 150;
    private HashSet<String> subTopologies;

    /**
     * This is an empty JavaBean constructor in order to support XMLEncoder and XMLDecoder
     */
    public PartiallyKnownNetworkTopology() {
        this.networkTopologyGraph = new DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink>(GeneralNetworkLink.class);
        this.endpointNodes = new HashSet<EndpointNetworkNode>();
        this.unknownNetworkNodes = new HashSet<UnknownNetworkNode>();
        this.physicalNodes = new HashSet<PhysicalNetworkNode>();
        this.physicalLinks = new HashSet<PhysicalNetworkLink>();
        this.logicalLinks = new HashSet<LogicalNetworkLink>();
        this.subNets = new HashMap<String, EndpointSubNetwork>();
        this.changed = new ChangedStatus();
        this.topologyName = new String();
        this.subTopologies = new HashSet<String>();
    }

    /*
     * TODO: correspondence between physical and logical links
     */
    /**
     * Adds a Network node to the network topology.
     * <p/>
     * Each peer in the network sould create resective EndpointNetworkNode instance and add it to the NetworkTopology
     * <p/>
     *
     * @param node Network node to add
     */
    synchronized public void addNetworkNode(EndpointNetworkNode node) {
        assert this.networkTopologyGraph != null : "Network topology hasn't been properly initialized!";//TODO maybe throw unchecked exception instead?
        assert node != null : "Attempted to add null node to the network";

        if (!this.networkTopologyGraph.containsVertex(node)) {
            // Update list of available subnets
            for (EndpointNodeInterface ni : node.getNodeInterfaces()) {
                if (!subNets.containsKey(ni.getSubnet())) {
                    subNets.put(ni.getSubnet(), new EndpointSubNetwork(ni.getSubnet(), new CopyOnWriteArraySet<EndpointNodeInterface>()));
                }
                EndpointSubNetwork subNetwork = subNets.get(ni.getSubnet());
                subNetwork.addSubNetworkNodeInterface(ni);
            }

            this.networkTopologyGraph.addVertex(node);
            this.endpointNodes.add(node);            
            node.setNetworkTopologyGraph(networkTopologyGraph);

            // Create the private subnetwork topology for added node
            for (EndpointNodeInterface localInterface : node.getNodeInterfaces()) {
                for (EndpointNodeInterface toIface : subNets.get(localInterface.getSubnet()).getSubNetworkNodeInterfaces()) {
                    EndpointNetworkNode toNode = toIface.getParentNode();
                    if (node == toNode) {
                        continue;
                    }
                    double capacity = (localInterface.getBandwidth() < toIface.getBandwidth()) ? localInterface.getBandwidth() : toIface.getBandwidth();

//                    // Check for potential LambdaLinks
//                    if ((localInterface.getLambdaLinkEndpoint() != null) && (toIface.getLambdaLinkEndpoint() != null)) {
//                        // TODO: is this check really necessary now when we use LambdaLinkEndPoint object?
//                        if ((!localInterface.getLambdaLinkEndpoint().getLambdaLinkEndpoint().equals("")) && (!toIface.getLambdaLinkEndpoint().getLambdaLinkEndpoint().equals(""))) {
//                            // TODO: How to handle the capacity for a lambda? This is obviously wrong!!!
//                            lambda = new LambdaLink(localInterface.getLambdaLinkEndpoint(), toIface.getLambdaLinkEndpoint(), capacity);
//                        }
//                    }
                    LogicalNetworkLink forthLink = new LogicalNetworkLink();                           
                    forthLink.setParameters(capacity, initLinkLatency, node, localInterface, toNode, toIface);
                    logicalLinks.add(forthLink);
                    if (!this.networkTopologyGraph.addEdge(node, toNode, forthLink)) {
                        throw new RuntimeException("Failed to add the edge");
                    }
//                    if (lambda != null) {
//                        forthLink.associateLambda(lambda);
//                        lambda.addNetworkLink(forthLink);
//                    }
                    LogicalNetworkLink backLink = new LogicalNetworkLink();
                    backLink.setParameters(capacity, initLinkLatency, toNode, toIface, node, localInterface);
                    logicalLinks.add(backLink);
                    if (!this.networkTopologyGraph.addEdge(toNode, node, backLink)) {
                        throw new RuntimeException("Failed to add the edge");
                    }
//                    if (lambda != null) {
//                        backLink.associateLambda(lambda);
//                        lambda.addNetworkLink(backLink);
//                    }
                }
            }

            changed.makeChange();
        } else {
            PartiallyKnownNetworkTopology.logger.warn("Network node " + node.getNodeName() + " is already in the network topology.");
        }
    }

    /**
     * Adds a physical network node to the network topology.
     * <p/>
     * Some peers in the network might be informed about some part of physical network topology. For the known network nodes, (routers, switches), these peers should create respective instances of the PhysicalNetworkNode and add it to the network topology.
     * <p/>
     * @param node Physical network node to add
     */
    synchronized public void addNetworkNode(PhysicalNetworkNode node) {
        assert this.networkTopologyGraph != null : "Network topology hasn't been properly initialized!";//TODO maybe throw unchecked exception instead?
        assert node != null : "Attempted to add null node to the network";

        if (this.networkTopologyGraph.containsVertex(node)) {//TODO maybe look at return value of this.physicalNodes.add(node) instead?
            PartiallyKnownNetworkTopology.logger.warn("Network node " + node.getNodeName() + " is already in the network topology.");
            return;
        }

        this.networkTopologyGraph.addVertex(node);
        this.physicalNodes.add(node);
        node.setNetworkTopologyGraph(networkTopologyGraph);
        changed.makeChange();
    }

    /**
     * Adds a network node representing an unknown topology subnetwork to the network topology.
     * <p/>
     * Peers aware of subnetworks with unknown internal topology should create respective instances of the UnknownNetworkNode and add it to the network topology.
     * <p/>
     * @param node Physical network node to add
     */
    synchronized public void addNetworkNode(UnknownNetworkNode node) {
        assert this.networkTopologyGraph != null : "Network topology hasn't been properly initialized!";//TODO maybe throw unchecked exception instead?
        assert node != null : "Attempted to add null node to the network";

        if (this.networkTopologyGraph.containsVertex(node)) {//TODO maybe look at return value of this.unknownNetworkNodes.add(node) instead?
            PartiallyKnownNetworkTopology.logger.warn("Network node " + node.getNodeName() + " is already in the network topology.");
            return;
        }

        this.networkTopologyGraph.addVertex(node);
        this.unknownNetworkNodes.add(node);
        node.setNetworkTopologyGraph(networkTopologyGraph);
        changed.makeChange();
    }

    /**
     * Removes an endpoint network node from the network topology.
     * <p/>
     *
     * @param node NetworkNode to remove
     */
    synchronized public void removeNetworkNode(EndpointNetworkNode node) {
        if (this.networkTopologyGraph != null) {
            if (node != null) {
                if (this.networkTopologyGraph.containsVertex(node)) {
                    // Update list of available subnets
                    for (EndpointNodeInterface ni : node.getNodeInterfaces()) {
                        EndpointSubNetwork subNetwork = subNets.get(ni.getSubnet());
                        subNetwork.removeSubNetworkNodeInterface(ni);
                    }

                    // Do not forget to remove all edges comming from the node
                    // Only logical links are allowed between two endpoints
                    for (EndpointNetworkNode otherNode : this.endpointNodes) {
                        Set<GeneralNetworkLink> removedForthLinks = this.networkTopologyGraph.removeAllEdges(node, otherNode);
                        for (GeneralNetworkLink link : removedForthLinks) {
                            removeLinkAssociations((LogicalNetworkLink) link);
                            this.logicalLinks.remove((LogicalNetworkLink) link);
                        }
                        Set<GeneralNetworkLink> removedBackLinks = this.networkTopologyGraph.removeAllEdges(node, otherNode);
                        for (GeneralNetworkLink link : removedBackLinks) {
                            removeLinkAssociations((LogicalNetworkLink) link);
                            this.logicalLinks.remove((LogicalNetworkLink) link);
                        }
                    }

                    // While only physical links are allowed among any other couple of nodes
                    for (UnknownNetworkNode otherNode : this.unknownNetworkNodes) {
                        GeneralNetworkLink removedForthLink = this.networkTopologyGraph.removeEdge(node, otherNode);
                        if (removedForthLink != null) {
                            removeLinkAssociations((PhysicalNetworkLink) removedForthLink);
                            this.physicalLinks.remove((PhysicalNetworkLink) removedForthLink);
                        }
                        GeneralNetworkLink removedBackLink = this.networkTopologyGraph.removeEdge(node, otherNode);
                        if (removedBackLink != null) {
                            removeLinkAssociations((PhysicalNetworkLink) removedBackLink);
                            this.physicalLinks.remove((PhysicalNetworkLink) removedForthLink);
                        }
                    }
                    for (PhysicalNetworkNode otherNode : this.physicalNodes) {
                        GeneralNetworkLink removedForthLink = this.networkTopologyGraph.removeEdge(node, otherNode);
                        if (removedForthLink != null) {
                            removeLinkAssociations((PhysicalNetworkLink) removedForthLink);
                            this.physicalLinks.remove((PhysicalNetworkLink) removedForthLink);
                        }
                        GeneralNetworkLink removedBackLink = this.networkTopologyGraph.removeEdge(node, otherNode);
                        if (removedBackLink != null) {
                            removeLinkAssociations((PhysicalNetworkLink) removedBackLink);
                            this.physicalLinks.remove((PhysicalNetworkLink) removedForthLink);
                        }
                    }
                    
                    // And finaly remove the node vertex
                    this.networkTopologyGraph.removeVertex(node);
                    this.endpointNodes.remove(node);

                    changed.makeChange();
                } else {
                    PartiallyKnownNetworkTopology.logger.warn("No such network node in network topology.");
                }
            }
        }
    }

    synchronized public void removeNetworkNode(PhysicalNetworkNode node) {
        if (this.networkTopologyGraph == null || node == null) {
            return;
        }
        
        if (this.networkTopologyGraph.containsVertex(node)) {
            // Remove all edges to another nodes
            for (PhysicalNetworkLink link : this.physicalLinks) {
                if (link.fromNode.equals(node)) {
                    this.networkTopologyGraph.removeEdge(node, link.toNode);
                    this.physicalLinks.remove(link);
                }
                if (link.toNode.equals(node)) {
                    this.networkTopologyGraph.removeEdge(link.fromNode, node);
                    this.physicalLinks.remove(link);
                            
                }
            }
            
            // Remove the vertex
            this.networkTopologyGraph.removeVertex(node);
            this.physicalNodes.remove(node);
                    
            changed.makeChange();
        } else {
            PartiallyKnownNetworkTopology.logger.warn("No such network node in network topology.");
        }
    }
    
    synchronized public void removeNetworkNode(UnknownNetworkNode node) {
        if (this.networkTopologyGraph == null || node == null) {
            return;
        }
        
        if (this.networkTopologyGraph.containsVertex(node)) {
            // Remove all edges to another nodes
            for (PhysicalNetworkLink link : this.physicalLinks) {
                if (link.fromNode.equals(node)) {
                    this.networkTopologyGraph.removeEdge(node, link.toNode);
                    this.physicalLinks.remove(link);
                }
                if (link.toNode.equals(node)) {
                    this.networkTopologyGraph.removeEdge(link.fromNode, node);
                    this.physicalLinks.remove(link);
                            
                }
            }
            
            // Remove the vertex
            this.networkTopologyGraph.removeVertex(node);
            this.unknownNetworkNodes.remove(node);
                    
            changed.makeChange();
        } else {
            PartiallyKnownNetworkTopology.logger.warn("No such network node in network topology.");
        }
    }

    synchronized public void addPhysicalLink(PhysicalNetworkLink link) {
        assert this.networkTopologyGraph != null : "Network topology hasn't been properly initialized!";
        assert link != null : "Attempted to add null link to the network!";
        
        if (this.networkTopologyGraph.containsEdge(link)) {
            PartiallyKnownNetworkTopology.logger.warn("Network edge " + link.getLinkName() + " is already in the network topology.");
            return;
        }
        
        this.networkTopologyGraph.addEdge(link.fromNode, link.toNode, link);
        this.physicalLinks.add(link);
        
        changed.makeChange();
    }

    synchronized public void removeLink(PhysicalNetworkLink link) {
        if (this.networkTopologyGraph == null || link == null) {
            return;
        }
        
        // remove associations with logical links
        this.removeLinkAssociations(link);
        
        //Remove from the structures
        this.networkTopologyGraph.removeEdge(link);
        this.physicalLinks.remove(link);
        
        changed.makeChange();
    }
    
    synchronized public void removeLink(LogicalNetworkLink link) {
        if (this.networkTopologyGraph == null || link == null) {
            return;
        }
        
        // remove associations with logical links
        this.removeLinkAssociations(link);
        
        //Remove from the structures
        this.networkTopologyGraph.removeEdge(link);
        this.logicalLinks.remove(link);
        
        changed.makeChange();
    }
    
    synchronized public void associateLinks(PhysicalNetworkLink physical, LogicalNetworkLink logical) {
        assert physical != null : "Passed physical link is null!";
        assert logical != null : "Passed logical link is null!";
        
        physical.addTraversingLogicalLink(logical);
        logical.addPhysicalLink(physical);
        
        changed.makeChange();
    }

    synchronized public void associateIntersiteLinks(NetworkSite site1, NetworkSite site2, String subnetName, ArrayList<PhysicalNetworkLink> physicalLinks) {
        for (EndpointNetworkNode site1node : this.getSiteEndpointNodes(site1)) {
            for (EndpointNetworkNode site2node : this.getSiteEndpointNodes(site2)) {
                // graph is directed, we need to repeat this twice
                this.associateInternodeLinks(site1node, site2node, subnetName, physicalLinks);
//                this.associateInternodeLinks(site2node, site1node, subnetName, physicalLinks);
            }
        }
    }
    
    synchronized public void associateInternodeLinks(EndpointNetworkNode node1, EndpointNetworkNode node2, String subnetName, ArrayList<PhysicalNetworkLink> physicalLinks) {
        LogicalNetworkLink logical;
        boolean linkFound = false;
        
        for (GeneralNetworkLink general : this.networkTopologyGraph.getAllEdges(node1, node2)) {
            // physical links between these nodes are not allowed
            assert general instanceof LogicalNetworkLink : "Suddenly a wild physical link appears!";
            logical = (LogicalNetworkLink) general;
            // in theory, both subnets should be equal
            if (logical.getFromInterface().getSubnet().equals(subnetName)
                    && logical.getToInterface().getSubnet().equals(subnetName)) {
                linkFound = true;
                for (PhysicalNetworkLink physical : physicalLinks) {
                    associateLinks(physical, logical);
                }
            }
        }
        
        if (!linkFound) {
            PartiallyKnownNetworkTopology.logger.warn("No link was found between " + node1 + " and " + node2 + " in subnet " + subnetName + ".");
        }
    }
    
    synchronized public void buildEndpointLinkAssociations(EndpointNetworkNode node, String nodeInterfaceName, GeneralNetworkNode networkNode) {
//        System.out.println("Run bELA: " + node + ", " + nodeInterfaceName + ", " + networkNode);
        EndpointNodeInterface iface = node.getNodeInterface(nodeInterfaceName);
        if (iface == null) {
            PartiallyKnownNetworkTopology.logger.warn("Interface " + nodeInterfaceName + " not found at node " + node + ".");
            return;
        }

        // Is is supposed that there is at most one bidirectional physical link connected to an interface
        ArrayList<PhysicalNetworkLink> nodePhysicalLinksIn = new ArrayList<PhysicalNetworkLink>(1);
        ArrayList<PhysicalNetworkLink> nodePhysicalLinksOut = new ArrayList<PhysicalNetworkLink>(1);
        ArrayList<LogicalNetworkLink> nodeLogicalLinksIn = new ArrayList<LogicalNetworkLink>(this.endpointNodes.size());
        ArrayList<LogicalNetworkLink> nodeLogicalLinksOut = new ArrayList<LogicalNetworkLink>(this.endpointNodes.size());
        
        for (GeneralNetworkLink general : this.networkTopologyGraph.edgesOf(node)) {
            if (general instanceof LogicalNetworkLink) {
                LogicalNetworkLink logical = (LogicalNetworkLink) general;
                if (logical.getFromInterface().getNodeInterfaceName().equals(nodeInterfaceName) &&
                    logical.getFromNode().equals(node)) {
                    nodeLogicalLinksOut.add(logical);
                } else if (logical.getToInterface().getNodeInterfaceName().equals(nodeInterfaceName) &&
                           logical.getToNode().equals(node))  {
                    nodeLogicalLinksIn.add((LogicalNetworkLink) general);
                }
            } else if (general instanceof PhysicalNetworkLink) {
                PhysicalNetworkLink physical = (PhysicalNetworkLink) general;
                if (physical.getEndpointNodeInterface() != null && physical.getEndpointNodeInterface().getNodeInterfaceName().equals(nodeInterfaceName)) {
                    if (node.equals(physical.getFromNode())) {
                        nodePhysicalLinksOut.add(physical);
                    } else if (node.equals(physical.getToNode())) {
                        nodePhysicalLinksIn.add(physical);
                    }
                }
            }
        }
        
        for (LogicalNetworkLink logical : nodeLogicalLinksIn) {
            for (PhysicalNetworkLink physical : nodePhysicalLinksIn) {
                associateLinks(physical, logical);
            }
        }
        for (LogicalNetworkLink logical : nodeLogicalLinksOut) {
            for (PhysicalNetworkLink physical : nodePhysicalLinksOut) {
                associateLinks(physical, logical);
            }
        }
        
        if (nodeLogicalLinksIn.isEmpty()) {
          if (!nodeInterfaceName.equals("1GE")) {
            PartiallyKnownNetworkTopology.logger.warn("No incoming logical links in node " + node + " on interface " + nodeInterfaceName);
          }
        }
        if (nodeLogicalLinksOut.isEmpty()) {
          if (!nodeInterfaceName.equals("1GE")) {
            PartiallyKnownNetworkTopology.logger.warn("No outgoing logical links in node " + node + " on interface " + nodeInterfaceName);
          }
        }
        if (nodePhysicalLinksIn.isEmpty()) {
            PartiallyKnownNetworkTopology.logger.warn("No incoming physical links in node " + node);
        }
        if (nodePhysicalLinksOut.isEmpty()) {
            PartiallyKnownNetworkTopology.logger.warn("No outgoing physical links in node " + node);
        }
    }
    
    synchronized public void buildEndpointLinkAssociationsMultinode(Collection<EndpointNetworkNode> nodes, String nodeInterfaceName, GeneralNetworkNode networkNode) {
        for (EndpointNetworkNode endpoint : nodes) {
            if (endpoint != null) {
                buildEndpointLinkAssociations(endpoint, nodeInterfaceName, networkNode);
            }
        }
    }
    
    public ArrayList<EndpointNetworkNode> getSiteEndpointNodes(NetworkSite site) {
        ArrayList<EndpointNetworkNode> siteNodes = new ArrayList<EndpointNetworkNode>();
        
        for (EndpointNetworkNode node : this.endpointNodes) {
            if (node.getNodeSite().equals(site)) {
                siteNodes.add(node);
            }
        }
        return siteNodes;
    }
    
    /**
     * Gets graph representation of the network topology
     * <p/>
     *
     * @return network topology graph
     */
    synchronized public DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> getNetworkTopologyGraph() {
        return networkTopologyGraph;
    }


    /**
     * Returns complete status of network topology, i.e. changed flag plus last changed timestamp.
     * This is worked upon and returned atomically.
     * <p/>
     *
     * @return status of network topology
     */
    synchronized public NetworkTopologyStatus getStatus() {
        return changed.getChanged();
    }

    /**
     * Returns snapshot of the network topology graph to work upon. It also resets changed status on
     * current NetworkTopology object.
     * <p/>
     *
     * @param networkTopology to make snapshot of
     * @return snapshot
     * @throws CloneNotSupportedException when networkTopology is not cloneable
     */
    static public PartiallyKnownNetworkTopology getCurrentSnapshot(PartiallyKnownNetworkTopology networkTopology) throws CloneNotSupportedException {
        synchronized (networkTopology) {
            PartiallyKnownNetworkTopology n = (PartiallyKnownNetworkTopology) networkTopology.clone();
            networkTopology.changed.invalidateChanges();
            return n;
        }
    }

    synchronized public void switchNetworkLinkUp(GeneralNetworkLink linkToBringUp) {
        assert linkToBringUp != null;
        GeneralNetworkLink linkToWorkOn = this.findEdge(linkToBringUp);
        assert linkToWorkOn != null;
        if (this.networkTopologyGraph.containsEdge(linkToWorkOn)) {
            if (!linkToWorkOn.isActive()) {
                linkToWorkOn.setActive(true);
                changed.makeChange();
            } else {
                PartiallyKnownNetworkTopology.logger.warn("" + linkToWorkOn + " was already up.");
            }
        } else {
            PartiallyKnownNetworkTopology.logger.error("Failed to find edge " + linkToWorkOn + " to set up.");
        }
    }

    synchronized public void switchNetworkLinkDown(GeneralNetworkLink linkToShutDown) {
        assert linkToShutDown != null;
        GeneralNetworkLink linkToWorkOn = this.findEdge(linkToShutDown);
        assert linkToWorkOn != null;
        if (this.networkTopologyGraph.containsEdge(linkToWorkOn)) {
            if (linkToWorkOn.isActive()) {
                linkToWorkOn.setActive(false);
                changed.makeChange();
            } else {
                PartiallyKnownNetworkTopology.logger.warn("" + linkToWorkOn + " was already down.");
            }
        } else {
            PartiallyKnownNetworkTopology.logger.error("Failed to find edge " + linkToWorkOn + " to set down.");
        }
    }

    public void setNetworkTopologyGraph(DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> networkTopologyGraph) {
        this.networkTopologyGraph = networkTopologyGraph;
    }


    public HashMap<String, EndpointSubNetwork> getSubNets() {
        return subNets;
    }

    public void setSubNets(HashMap<String, EndpointSubNetwork> subNets) {
        this.subNets = subNets;
    }

    public String getName() {
      return topologyName;
    }

    public void setName(String name) {
      this.topologyName = name;
    }

    public HashSet<PhysicalNetworkLink> getPhysicalLinks() {
        return physicalLinks;
    }

    public HashSet<PhysicalNetworkNode> getPhysicalNodes() {
        return physicalNodes;
    }

    public HashSet<UnknownNetworkNode> getUnknownNetworkNodes() {
        return unknownNetworkNodes;
    }

    public HashSet<EndpointNetworkNode> getEndpointNodes() {
        return endpointNodes;
    }

    public HashSet<LogicalNetworkLink> getLogicalLinks() {
        return logicalLinks;
    }

    private GeneralNetworkLink findEdge(GeneralNetworkLink matchLink) {
        for (GeneralNetworkLink graphLink : this.networkTopologyGraph.edgeSet()) {
            if (graphLink.equals(matchLink)) {
                return graphLink;
            }
        }
        return null;
    }

    private GeneralNetworkNode findNode(GeneralNetworkNode matchNode) {
        for (GeneralNetworkNode graphNode : this.networkTopologyGraph.vertexSet()) {
            if (graphNode.equals(matchNode)) {
                return graphNode;
            }
        }
        return null;
    }
    
    public void removeLinkAssociations(LogicalNetworkLink logical) {
        for (PhysicalNetworkLink physical : logical.getPhysicalLinksOnThePath()) {
            physical.removeTraversingLogicalLink(logical);
        }
        logical.removeAllPhysicalLinks();
    }
    
    public void removeLinkAssociations(PhysicalNetworkLink physical) {
        for (LogicalNetworkLink logical : physical.getTraversingLogicalLinks()) {
            logical.removePhysicalLink(physical);
        }
        physical.clearTraversingLogicalLinks();
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        Element baseElem = new Element("nml:Topology", CoNMLInterface.NML_NS);
        baseElem.addNamespaceDeclaration("nml", CoNMLInterface.NML_NS);
        baseElem.addNamespaceDeclaration("cou", CoNMLInterface.CONML_NS);
        if(parameters.getParentNode() == null){
            if(result.getDoc() == null){
                result.setDoc(new Document(baseElem));
            }else{
                result.getDoc().appendChild(baseElem);
            }
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElem);
        }
        
        //writing topology name
        if(this.topologyName != null){
            Element nameElem = new Element("nml:name", CoNMLInterface.NML_NS);
            nameElem.appendChild(this.topologyName);
            baseElem.appendChild(nameElem); 
        }
        
        //writing nodes
        for(EndpointNetworkNode n: this.endpointNodes){
            n.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode()));//each node needs its own parameters
        }  
        for(PhysicalNetworkNode n: this.physicalNodes){
            n.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode()));//each node needs its own parameters
        }
        for(UnknownNetworkNode n: this.unknownNetworkNodes){
            n.encodeToCoNML(result, new CoNMLEncodeParameters(parameters.getParentNode()));//each node needs its own parameters
        }
        
        //anyOther elements        
        this.changed.getChanged().encodeToCoNML(result, new CoNMLEncodeParameters(baseElem));//writing network topology status (synchronized)
        
        return new CoNMLExtensionNodes(baseElem, baseElem, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }    
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS);        
        
        Element nameElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("name", CoNMLInterface.NML_NS);
        if(nameElem != null){
            this.topologyName = nameElem.getValue();            
        } 

        //decoding physical links
        Nodes toParse = parameters.getCurrentNode().query(".//nml:Link/cou:GeneralNetworkLink/cou:PhysicalNetworkLink", context);
        for(int i = 0; i < toParse.size(); i++){
            PhysicalNetworkLink link = new PhysicalNetworkLink();
            link.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));
            this.physicalLinks.add(link);
        }

        //decoding physical backlinks
        toParse = parameters.getCurrentNode().query("nml:BidirectionalLink", context);            
        for(int i = 0; i < toParse.size(); i++){
            Nodes links = toParse.get(i).query("nml:Link", context);
            //Elements links = ((Element) toParse.get(i)).getChildElements("nml:Link", CoNML.NML_NS);//TODO check whether also works
            if(links.size() != 2){
                continue;
            }
            PhysicalNetworkLink pLink1 = (PhysicalNetworkLink) result.getDecodedLinks().get(((Element) links.get(0)).getAttributeValue("id"));                
            PhysicalNetworkLink pLink2 = (PhysicalNetworkLink) result.getDecodedLinks().get(((Element) links.get(1)).getAttributeValue("id"));
            if(pLink1 == null || pLink2 == null){
                continue;
            }
            pLink1.setFullDuplex(true);
            pLink2.setFullDuplex(true);
            pLink1.setBackLink(pLink2);
            pLink2.setBackLink(pLink1);
        }

        //decoding logical links
        toParse = parameters.getCurrentNode().query(".//nml:Link/cou:GeneralNetworkLink/cou:LogicalNetworkLink", context);  
        for(int i = 0; i < toParse.size(); i++){
            LogicalNetworkLink link = new LogicalNetworkLink();
            link.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));
            this.logicalLinks.add(link);                
        }

        //registering endpoint network nodes
        toParse = parameters.getCurrentNode().query("nml:Node/cou:GeneralNetworkNode/cou:EndpointNetworkNode", context);
        for(int i = 0; i < toParse.size(); i++){
            EndpointNetworkNode node = new EndpointNetworkNode();
            String id = ((Element) toParse.get(i).getParent().getParent()).getAttributeValue("id");
            if(id.isEmpty() == false){
                node.setUuid(id);
                result.getDecodedNodes().put(id, node);                   
            }
        }

        //decoding endpoint interfaces
        toParse = parameters.getCurrentNode().query("nml:BidirectionalPort", context);
        for(int i = 0; i < toParse.size(); i++){
            Nodes ports = toParse.get(i).query("nml:Port", context);
            if(ports.size() != 2){
                continue;
            }
            Node[] port = new Node[2];                
            for(int j = 0; j < port.length; j++){
                Nodes n = parameters.getCurrentNode().query(".//nml:Port[@id='" + ((Element) ports.get(j)).getAttributeValue("id") + "']/cou:GeneralNodeInterface/cou:EndpointNodeInterface", context);
                if(n.size() > 0){
                    port[j] = n.get(0);
                }
            }
            boolean ok = true;
            for(Node p : port){
                if(p == null){
                    ok = false;
                    break;
                }
            }
            if(ok){
                Nodes refElem = parameters.getCurrentNode().query(".//nml:Node/nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#hasOutboundPort']/nml:Port[@id='" + ((Element) ports.get(0)).getAttributeValue("id") + "']", context);
                if(refElem.size() == 0){
                    refElem = parameters.getCurrentNode().query(".//nml:Node/nml:Relation[@type='http://schemas.ogf.org/nml/2013/05/base#hasInboundPort']/nml:Port[@id='" + ((Element) ports.get(0)).getAttributeValue("id") + "']", context);
                }
                EndpointNetworkNode parentNode = null;
                if(refElem.size() > 0){
                    parentNode = (EndpointNetworkNode) result.getDecodedNodes().get(((Element) refElem.get(0).getParent().getParent()).getAttributeValue("id"));
                }

                EndpointNodeInterface iface = new EndpointNodeInterface(); 
                for(Node p : port){
                    iface.setParentNode(parentNode);
                    iface.decodeFromCoNML(result, new CoNMLDecodeParameters(p));
                }

                String id = ((Element) toParse.get(i)).getAttributeValue("id");
                if(id.isEmpty() == false){
                    iface.setUuid(id);
                }
                if(parentNode != null){
                    parentNode.getNodeInterfaces().add(iface);
                }
            }
        }

        //decoding unknown network nodes
        toParse = parameters.getCurrentNode().query("nml:Node/cou:GeneralNetworkNode/cou:UnknownNetworkNode", context);
        for(int i = 0; i < toParse.size(); i++){
            UnknownNetworkNode node = new UnknownNetworkNode();
            node.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));
            node.setParentTopologyName(this.topologyName);
            this.addNetworkNode(node);
        }

        //decoding physical network nodes
        toParse = parameters.getCurrentNode().query("nml:Node/cou:GeneralNetworkNode/cou:PhysicalNetworkNode", context);
        for(int i = 0; i < toParse.size(); i++){
            PhysicalNetworkNode node = new PhysicalNetworkNode();
            node.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));   
            node.setParentTopologyName(this.topologyName);
            this.addNetworkNode(node);
        }

        //decoding dummy interfaces
        toParse = parameters.getCurrentNode().query(".//nml:Port/cou:GeneralNodeInterface/cou:DummyInterface", context);
        for(int i = 0; i < toParse.size(); i++){
            DummyInterface port = new DummyInterface();
            port.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));         
        }

        //decoding endpoint network nodes
        toParse = parameters.getCurrentNode().query("nml:Node/cou:GeneralNetworkNode/cou:EndpointNetworkNode", context);
        for(int i = 0; i < toParse.size(); i++){
            EndpointNetworkNode node = (EndpointNetworkNode) result.getDecodedNodes().get(((Element) toParse.get(i).getParent().getParent()).getAttributeValue("id"));
            if(node != null){
                node.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i)));
                node.setParentTopologyName(this.topologyName);
                this.addNetworkNode(node);
            }
        }        
        
        //decoding media applications of endpoint nodes
        for(int i = 0; i < toParse.size(); i++){
            EndpointNetworkNode node = (EndpointNetworkNode) result.getDecodedNodes().get(((Element) toParse.get(i).getParent().getParent()).getAttributeValue("id"));
            if(node != null){
                node.decodeFromCoNML(result, new CoNMLDecodeParameters(toParse.get(i), 1));                
            }
        }
        
        //adding links to topology
        for(CoNMLInterface link: result.getDecodedLinks().values()){
            this.networkTopologyGraph.addEdge(((GeneralNetworkLink) link).getFromNode(), ((GeneralNetworkLink) link).getToNode(), (GeneralNetworkLink) link);
            changed.makeChange();
        }
    }
    
    /**
     * Makes place for more encode options.
     * @return XML string in specified encoding
     */
    public String encodeToXML(){
        CoNMLEncodeResult result = new CoNMLEncodeResult();
        encodeToCoNML(result, null);
        return result.toString();
    }
    
    /**
     * Makes place for more decode options.
     * @param xmlString input XML string
     */
    public void decodeFromXML(String xmlString){
        decodeFromCoNML(new CoNMLDecodeResult(), new CoNMLDecodeParameters(xmlString));
    }
    
    /**
     * Updates knowledge of this topology with given XML.
     * @param xmlString input XML string
     */
    public void updateFromXML(String xmlString){
        PartiallyKnownNetworkTopology newTopology = new PartiallyKnownNetworkTopology();
        newTopology.decodeFromCoNML(new CoNMLDecodeResult(), new CoNMLDecodeParameters(xmlString));        
        this.mergeWithTopology(newTopology);
    }
    
    /**
     * Acquires all nodes with their "parentTopologyName" attribute equal to given parameter.
     * @param subTopologyName Value, which is being compared to attribute "parentTopologyName" of all nodes in this topology.
     * @return Set of nodes with their "parentTopologyName" attribute equal to given parameter. (may be empty)
     */
    public HashSet<GeneralNetworkNode> getSubTopologyNodes(String subTopologyName){
        HashSet<GeneralNetworkNode> result = new HashSet<GeneralNetworkNode>();
        for(UnknownNetworkNode node: this.unknownNetworkNodes){
            if(node.getParentTopologyName().equals(subTopologyName)){
                result.add(node);
            }
        }
        for(PhysicalNetworkNode node: this.physicalNodes){
            if(node.getParentTopologyName().equals(subTopologyName)){
                result.add(node);
            }
        }
        for(EndpointNetworkNode node: this.endpointNodes){
            if(node.getParentTopologyName().equals(subTopologyName)){
                result.add(node);
            }
        }
        return result;
    }
    
    /**
     * Updates this topology with data from given topology.
     * @param topology input
     */
    public void mergeWithTopology(PartiallyKnownNetworkTopology topology){
        if(topology == null){
            throw new IllegalArgumentException("PartiallyKnownNetworkTopology cannot be null!");
        }
        
        HashSet<GeneralNetworkNode> nodesToUpdate = null;
        if(topology.topologyName != null && topology.topologyName.isEmpty() == false){
            this.subTopologies.add(topology.topologyName);
            nodesToUpdate = getSubTopologyNodes(topology.topologyName);
        }else{
            nodesToUpdate = new HashSet<GeneralNetworkNode>();
        }
        
        boolean addNew;
        
        //unknown nodes
        for(UnknownNetworkNode newNode: topology.getUnknownNetworkNodes()){
            addNew = true;
            if(newNode.getNodeName().isEmpty() == false){
                for(UnknownNetworkNode node: this.unknownNetworkNodes){
                    if(node.getNodeName().equals(newNode.getNodeName())){
                        node.update(newNode);
                        nodesToUpdate.remove(node);
                        addNew = false;
                        break;
                    }
                }
            }
            if(addNew){
                this.removeNetworkNode(newNode);                    
                this.addNetworkNode(newNode);
            }              
        }
        
        //physical nodes
        for(PhysicalNetworkNode newNode: topology.getPhysicalNodes()){
            addNew = true;
            if(newNode.getNodeName().isEmpty() == false){
                for(PhysicalNetworkNode node: this.physicalNodes){
                    if(node.getNodeName().equals(newNode.getNodeName())){
                        node.update(newNode);
                        nodesToUpdate.remove(node);
                        addNew = false;
                        break;
                    }
                }
            }
            if(addNew){
                this.removeNetworkNode(newNode);
                this.addNetworkNode(newNode);
            }
        }
        
        //endpoint nodes
        for(EndpointNetworkNode newNode: topology.getEndpointNodes()){            
            addNew = true;
            if(newNode.getNodeName().isEmpty() == false){
                for(EndpointNetworkNode node: this.endpointNodes){
                    if(node.getNodeName().equals(newNode.getNodeName())){
                        node.update(newNode);
                        nodesToUpdate.remove(node);
                        addNew = false;
                        break;
                    }
                }
            }
            if(addNew){
                this.removeNetworkNode(newNode);
                this.addNetworkNode(newNode);
            }
        }
        
        //removing non-updated nodes from the same topology
        for(GeneralNetworkNode node: nodesToUpdate){
            switch(node.nodeType){
                case 1:{
                    removeNetworkNode((EndpointNetworkNode) node);
                }break;
                case 2:{
                    removeNetworkNode((PhysicalNetworkNode) node);
                }break;
                case 3:{
                    removeNetworkNode((UnknownNetworkNode) node);
                }
            }
        }
        
        //adding physical links
        for(PhysicalNetworkLink newLink: topology.getPhysicalLinks()){
            this.removeLink(newLink);           
            this.physicalLinks.add(newLink);
            try
            {
                this.networkTopologyGraph.addEdge(newLink.getFromNode(), newLink.getToNode(), newLink);
            }catch(IllegalArgumentException e){/*nothing should happen*/}            
            this.changed.makeChange();
        }
        //adding logical links
        for(LogicalNetworkLink newLink: topology.getLogicalLinks()){
            this.removeLink(newLink);
            this.logicalLinks.add(newLink);
            try
            {
                this.networkTopologyGraph.addEdge(newLink.getFromNode(), newLink.getToNode(), newLink);
            }catch(IllegalArgumentException e){/*nothing should happen*/}       
            this.changed.makeChange();
        }        
        
        completeLogicalLinks();        
        
        //TODO parse timestamp, reset changes counter or set it to 1?
    }  
    
    /**
     * Finds all paths from one node to other using only physical links. Beware, this is a NP-hard problem!
     * @param startNode Node to start from.
     * @param targetNode Target node.
     * @return Paths with their bandwidth.
     */
    public HashMap<Stack<PhysicalNetworkLink>, Double> getAllPhysicalPaths(GeneralNetworkNode startNode, GeneralNetworkNode targetNode){
        if(startNode == null || targetNode == null){
            throw new IllegalArgumentException("Neither node can be null!");
        }
        if(this.networkTopologyGraph == null){
            throw new NullPointerException("networkTopologyGraph cannot be null!");
        }
        if(this.networkTopologyGraph.containsVertex(startNode) == false || this.networkTopologyGraph.containsVertex(targetNode) == false){
            throw new IllegalArgumentException("Both nodes must be in networkTopologyGraph!");
        }
        
        Stack<GeneralNetworkNode> nodesOnPath = new Stack<GeneralNetworkNode>();
        Stack<PhysicalNetworkLink> linksOnPath = new Stack<PhysicalNetworkLink>();
        Stack<PhysicalNetworkLink> found = new Stack<PhysicalNetworkLink>();
        
        for(GeneralNetworkLink link: this.networkTopologyGraph.outgoingEdgesOf(startNode)){
            if(link instanceof PhysicalNetworkLink){
                found.add((PhysicalNetworkLink) link);                
            }
        }
        
        HashMap<Stack<PhysicalNetworkLink>, Double> result = new HashMap<Stack<PhysicalNetworkLink>, Double>();
        
        while(found.empty() == false){
            PhysicalNetworkLink next = found.pop();            
            linksOnPath.push(next);
            nodesOnPath.push(next.getToNode());
            if(next.getToNode().equals(targetNode)){
                Stack<PhysicalNetworkLink> newPath = new Stack<PhysicalNetworkLink>();
                double pathCapacity = Double.MAX_VALUE;
                for(PhysicalNetworkLink link : linksOnPath) {
                    newPath.push((PhysicalNetworkLink) link);
                    if(link.getCapacity() < pathCapacity){
                        pathCapacity = link.getCapacity();
                    }
                }
                result.put(newPath, new Double(pathCapacity));
                
                linksOnPath.pop();
                nodesOnPath.pop();
                continue;
            }            
            boolean deadEnd = true;
            for(GeneralNetworkLink link : this.networkTopologyGraph.outgoingEdgesOf(next.getToNode())){
                if(link instanceof PhysicalNetworkLink){
                    if(linksOnPath.contains((PhysicalNetworkLink) link) == false){
                        if(nodesOnPath.contains(link.getToNode()) == false){
                            found.push((PhysicalNetworkLink) link);
                            deadEnd = false;
                        }
                    }
                }
            }
            if(deadEnd){
                linksOnPath.pop();
                nodesOnPath.pop();
            }
        }        
        return result;
    }
    
    /**
     * Finds all endpoint nodes in this topology, who share an endpoint node interface in the same subnetwork, and creates
     * logical links between them (unless it already exists) associated with the best possible path of physical links.
     */
    public void completeLogicalLinks() { /*TODO should we make this method synchronized?*/
        for(String subnetName: this.subNets.keySet()){
            for(EndpointNodeInterface iface1: this.subNets.get(subnetName).getSubNetworkNodeInterfaces()){
                for(EndpointNodeInterface iface2: this.subNets.get(subnetName).getSubNetworkNodeInterfaces()){
                    if(iface1.equals(iface2) || iface1.getParentNode() == null || iface2.getParentNode() == null ||iface1.getParentNode().equals(iface2.getParentNode())){
                        continue;
                    }
                    HashSet<GeneralNetworkLink> links = (HashSet) this.networkTopologyGraph.getAllEdges(iface1.getParentNode(), iface2.getParentNode());
                    boolean logical = false;
                    for(GeneralNetworkLink link: links){
                        if(link instanceof LogicalNetworkLink){
                            logical = true;
                            break;
                        }
                    }
                    if(logical){
                        continue;
                    }                    
                    
                    //creating link 1
                    HashMap<Stack<PhysicalNetworkLink>, Double> paths = getAllPhysicalPaths(iface1.getParentNode(), iface2.getParentNode());
                    if(paths == null){
                        continue;
                    }
                    Stack<PhysicalNetworkLink> bestPath = (Stack<PhysicalNetworkLink>) paths.values().toArray()[0];
                    for(Stack<PhysicalNetworkLink> path: paths.keySet()){
                        if((paths.get(path) > paths.get(bestPath)) || (paths.get(path) == paths.get(bestPath) && path.size() < bestPath.size())){
                            bestPath = path;
                        }
                    }
                    
                    LogicalNetworkLink newLink1 = new LogicalNetworkLink();
                    newLink1.setParameters(paths.get(bestPath), initLinkLatency, iface1.getParentNode(), iface1, iface2.getParentNode(), iface2);
                    
                    for(PhysicalNetworkLink pLink: bestPath){
                        newLink1.addPhysicalLink(pLink);
                    }
                    
                    //creating link 2
                    paths = getAllPhysicalPaths(iface2.getParentNode(), iface1.getParentNode());
                    if(paths == null){
                        continue;
                    }
                    bestPath = (Stack<PhysicalNetworkLink>) paths.values().toArray()[0];
                    for(Stack<PhysicalNetworkLink> path: paths.keySet()){
                        if((paths.get(path) > paths.get(bestPath)) || (paths.get(path) == paths.get(bestPath) && path.size() < bestPath.size())){
                            bestPath = path;
                        }
                    }
                    
                    LogicalNetworkLink newLink2 = new LogicalNetworkLink();
                    newLink2.setParameters(paths.get(bestPath), initLinkLatency, iface2.getParentNode(), iface2, iface1.getParentNode(), iface1);
                    
                    for(PhysicalNetworkLink pLink: bestPath){
                        newLink2.addPhysicalLink(pLink);
                    }
                }
            }       
        }
    }
}