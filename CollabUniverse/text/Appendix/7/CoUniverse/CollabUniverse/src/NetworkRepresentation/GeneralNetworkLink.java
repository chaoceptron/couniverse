package NetworkRepresentation;

import java.io.Serializable;
import java.util.UUID;
import nu.xom.Attribute;
import nu.xom.Element;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Representation of the link between NetworkNodes and their NodeInterfaces.
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class GeneralNetworkLink extends DefaultEdge implements Serializable, CoNMLInterface {

    private double weight = WeightedGraph.DEFAULT_EDGE_WEIGHT; // this is actually a capacity of the link
    protected GeneralNetworkNode fromNode = null;
    protected GeneralNetworkNode toNode = null;
    private boolean active = true;//TODO incorporate to link asociation search?
    private double linkShim = 0.0;
    private double shimRange = 1.0;    
    protected String uuid;

    public GeneralNetworkLink() {
        super();
        this.active = true;
        this.linkShim = (Math.random() - 0.5) * shimRange;
        //if(this.uuid == null) {//TODO why was this here?
            this.uuid = UUID.randomUUID().toString();
        //}
    }

    public GeneralNetworkLink(double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode) {
        super();
        this.weight = capacity;
        this.fromNode = fromNode;
        this.toNode = toNode;
        this.active = true;
        this.linkShim = (Math.random() - 0.5) * shimRange;
        this.uuid = UUID.randomUUID().toString();
    }
    
    public void setParameters(double capacity, GeneralNetworkNode fromNode, GeneralNetworkNode toNode) {
        this.weight = capacity;
        this.fromNode = fromNode;
        this.toNode = toNode;
    }

    public double getCapacity() {
        return weight;
    }

    public void setCapacity(double capacity) {
        this.weight = capacity;
    }

    public GeneralNetworkNode getFromNode() {
        return fromNode;
    }

    public void setFromNode(GeneralNetworkNode fromNode) {
        this.fromNode = fromNode;
    }

    public GeneralNetworkNode getToNode() {
        return toNode;
    }

    public void setToNode(GeneralNetworkNode toNode) {
        this.toNode = toNode;
    }

    public double getLinkShim() {
        return linkShim;
    }

    public void setLinkShim(double linkShim) {
        this.linkShim = linkShim;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "" + this.getFromNode() + "  --->  " + this.getToNode();
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getShimRange() {
        return shimRange;
    }

    public void setShimRange(double shimRange) {
        this.shimRange = shimRange;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeneralNetworkLink other = (GeneralNetworkLink) obj;
        if ((this.uuid == null) ? (other.uuid != null) : !this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }    
    
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        } 
        
        Element parentRelationElem = null; //element node of caller, which describes its relation to this link
        if(parameters.getOutputsRelation() != null){
            parentRelationElem = (Element) parameters.getOutputsRelation();
        }else if(parameters.getInputsRelation() != null){
            parentRelationElem = (Element) parameters.getInputsRelation();
        } 
          
        //writing link to callers relation element node
        Element linkReferenceElem = null;
        if(parentRelationElem != null){
            linkReferenceElem = new Element("nml:Link", CoNMLInterface.NML_NS);  
            linkReferenceElem.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));
            parentRelationElem.appendChild(linkReferenceElem);
        }
        
        //writing link to root/parent node, unless it's already there
        if(result.getUuidSet() != null && result.getUuidSet().add(this.uuid) == false){
            return new CoNMLExtensionNodes(null, null, linkReferenceElem);
        }
        
        Element baseElementNode = new Element("nml:Link", CoNMLInterface.NML_NS);        
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(baseElementNode);
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElementNode);
        }
        baseElementNode.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));           
        
        /*Element nameElem = new Element("nml:name", CoNML.NML_NS);
        baseElementNode.appendChild(nameElem);*/
        
        //anyOther element
        Element extensionElem = new Element("cou:GeneralNetworkLink", CoNMLInterface.CONML_NS);
        baseElementNode.appendChild(extensionElem);
        
        Element capacityElem = new Element("cou:weight", CoNMLInterface.CONML_NS);
        capacityElem.appendChild(Double.toString(this.weight)); 
        extensionElem.appendChild(capacityElem);
        
        Element activeElem = new Element("cou:active", CoNMLInterface.CONML_NS);
        activeElem.appendChild(String.valueOf(this.active));         
        extensionElem.appendChild(activeElem);
        
        return new CoNMLExtensionNodes(baseElementNode, extensionElem, linkReferenceElem);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        } 
        
        //reading id attribute and registering it to result
        String id = ((Element) parameters.getCurrentNode().getParent()).getAttributeValue("id");
        if(id != null){
            this.uuid = id;
            result.getDecodedLinks().put(id, this);                
        }

        Element weightElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("weight", CoNMLInterface.CONML_NS); 
        if(weightElem != null){
            try{
                this.weight = Double.valueOf(weightElem.getValue());
            }catch(NumberFormatException ex){}
        }
        
        Element activeElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("active", CoNMLInterface.CONML_NS); 
        if(activeElem != null){
            this.active = Boolean.valueOf(activeElem.getValue());
        }
    }
}
