package GUI;

import GUI.MapVisualization.MapVisualizerPanel;
import NetworkRepresentation.EndpointNetworkNode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import myGUI.ControllerFrame;

/**
 * @author martin
 */
public class BaseFrame extends JFrame {
    private ContentPanePanel contentPanePanel;
    private MyBasicConfigPanel initialSetting;
    private NodeSettingPanel nodeSetting;
    private InterfacesSettingPanel interfacesSetting;
    private ApplicationsSettingPanel applicationSetting;
    private WaitingPanel waitPanel;
    private boolean isAgc;

    private GuiUniversePeer universePeer;

    private EndpointNetworkNode networkNode;
    private ControllerFrame localControllerFrame;



    public BaseFrame(String title) {
        super(title);
        this.contentPanePanel = new ContentPanePanel();
        this.setContentPane(this.contentPanePanel);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });
        this.contentPanePanel.buttonPanel.nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                nextButtonClicked();
            }
        });
        this.contentPanePanel.buttonPanel.cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                cancelButtonClicked();
            }
        });
        this.contentPanePanel.buttonPanel.backButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                backButtonClicked();
            }
        });
        this.initialSetting = new MyBasicConfigPanel();
        this.nodeSetting = new NodeSettingPanel();
        this.interfacesSetting = new InterfacesSettingPanel(this);
        this.applicationSetting = new ApplicationsSettingPanel(this);
        createWelcomePanel();
    }


    private void onExit() {
        //1 stop local node applications

        //2 stop monitoring

       
        if(this.isAgc){
            new StopAGCTask().execute();
        }
        new LeaveUniverseTask().execute();
        this.dispose();
    }

    private void createWelcomePanel() {
        this.contentPanePanel.setMainPanel(new MyWelcomePanel());
    }


    private void nextButtonClicked() {
        int currentState = contentPanePanel.progressPanel.next();
        switch (currentState) {
            case 0:
                contentPanePanel.setMainPanel(this.initialSetting);
                this.contentPanePanel.buttonPanel.nextButton.setText(" NEXT > ");
                contentPanePanel.buttonPanel.backButton.setEnabled(false);
                break;

            case 1:
                contentPanePanel.buttonPanel.nextButton.setEnabled(false);
                contentPanePanel.buttonPanel.backButton.setEnabled(false);
                try {
                    GuiUniversePeer.initialize(initialSetting.isConfigLocalFile(), initialSetting.getConfigFilePath());
                    networkNode = GuiUniversePeer.loadNetworkNode();
                    nodeSetting.setComponentsFromConfig(networkNode);
                    contentPanePanel.setMainPanel(nodeSetting);
                    contentPanePanel.buttonPanel.nextButton.setEnabled(true);
                    contentPanePanel.buttonPanel.backButton.setEnabled(true);

                } catch (MalformedURLException mue) {
                    contentPanePanel.setMainPanel(this.initialSetting);
                    this.contentPanePanel.progressPanel.back();
                    JOptionPane.showMessageDialog(this, "The URL you have specified is malformed", "",JOptionPane.WARNING_MESSAGE);
                    contentPanePanel.buttonPanel.nextButton.setEnabled(true);

                } catch (FileNotFoundException e) {
                    contentPanePanel.setMainPanel(this.initialSetting);
                    this.contentPanePanel.progressPanel.back();
                    JOptionPane.showMessageDialog(this, "Unable to read the file you have specified", "",JOptionPane.WARNING_MESSAGE);
                    contentPanePanel.buttonPanel.nextButton.setEnabled(true);

                } catch (IOException ioe) {
                    contentPanePanel.setMainPanel(this.initialSetting);
                    this.contentPanePanel.progressPanel.back();
                    JOptionPane.showMessageDialog(this, "Unable to read the file you have specified", "",JOptionPane.WARNING_MESSAGE);
                    contentPanePanel.buttonPanel.nextButton.setEnabled(true);
                }
                break;

            case 2:
                nodeSetting.setConfigFromComponents(this.networkNode);
                interfacesSetting.setComponentsFromConfig(networkNode);
                contentPanePanel.setMainPanel(interfacesSetting);
                contentPanePanel.buttonPanel.nextButton.setEnabled(true);
                break;

            case 3:
                interfacesSetting.setConfigFromComponents(networkNode);
                applicationSetting.setComponentsFromConfig(networkNode);
                contentPanePanel.setMainPanel(this.applicationSetting);
                this.contentPanePanel.buttonPanel.nextButton.setEnabled(true);
                break;
            case 4:
                applicationSetting.setConfigFromComponents(networkNode);
                contentPanePanel.buttonPanel.nextButton.setEnabled(false);
                contentPanePanel.buttonPanel.backButton.setEnabled(false);

                waitPanel = new WaitingPanel("Connecting to universe, please wait...");
                contentPanePanel.setMainPanel(waitPanel);
                new JoinUniverseTask().execute();
                break;
        }

    }
    private void backButtonClicked() {
        int currentState = contentPanePanel.progressPanel.back();
        switch (currentState) {
            case 0:
                contentPanePanel.setMainPanel(initialSetting);
                contentPanePanel.buttonPanel.backButton.setEnabled(false);
                break;

            case 1:
                nodeSetting.setComponentsFromConfig(networkNode);
                contentPanePanel.setMainPanel(this.nodeSetting);
                contentPanePanel.buttonPanel.backButton.setEnabled(true);
                break;

            case 2:
                interfacesSetting.setComponentsFromConfig(networkNode);
                contentPanePanel.setMainPanel(this.interfacesSetting);
                break;

            case 3:
                interfacesSetting.setComponentsFromConfig(networkNode);
                contentPanePanel.setMainPanel(this.applicationSetting);
                contentPanePanel.buttonPanel.nextButton.setEnabled(true);
                break;

            case 4:
                contentPanePanel.buttonPanel.nextButton.setEnabled(false);
                break;
        }
    }
    private void cancelButtonClicked() {
        this.onExit();
    }


    private void runApplicationsAndVisualization() {
        System.err.println("run applications and visualization");
        //new RunApplications().execute();
        MapVisualizerPanel mapPanel = new MapVisualizerPanel(null);
        this.contentPanePanel.setMapPanel( mapPanel);
    }


   


    class JoinUniverseTask extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            universePeer = new GuiUniversePeer(networkNode);
            universePeer.joinUniverse(initialSetting.getInfraCheckBox().isSelected());

            //GuiController gc = new GuiController(BaseFrame.this)
            return null;
        }
        @Override
        public void done() {
            if(initialSetting.getAgcCheckBox().isSelected()){
                    new StartAGCTask().execute();
                isAgc = true;
            }else{
                isAgc = false;
            }
            waitPanel.setText("Saving configuration, please wait...");
            new SaveConfigTask().execute();
        }
    }

    class RunApplications extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            universePeer.runApplications(localControllerFrame = new ControllerFrame());
            localControllerFrame.setVisible(true);
            return null;
        }
        @Override
        public void done() {
        }
    }

    class SaveConfigTask extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            if(universePeer != null){
                universePeer.saveConfig();
            }
            return null;
        }
        @Override
        public void done(){
            runApplicationsAndVisualization();
        }
    }

    class LeaveUniverseTask extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            if(universePeer != null){
                universePeer.leaveUniverse();
            }
            return null;
        }
        @Override
        public void done() {}
    }

    class StartAGCTask extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            if(universePeer != null){
                universePeer.startAGC();
            }
            return null;
        }
        @Override
        public void done() {}
    }

    class StopAGCTask extends SwingWorker<Void, Void> {
        @Override
        public Void doInBackground() {
            if(universePeer != null){
                universePeer.stopAGC();
            }
            return null;
        }
        @Override
        public void done() {}
    }


}