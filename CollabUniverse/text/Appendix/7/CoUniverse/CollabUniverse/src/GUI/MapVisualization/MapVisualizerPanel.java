package GUI.MapVisualization;

import org.jdesktop.swingx.mapviewer.GeoPosition;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import GUI.MyButtonPanel;
import GUI.MyWelcomePanel;
import GUI.MyTitlePanel;
import NetworkRepresentation.SampleTopologies;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Apr 23, 2008
 * Time: 7:17:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class MapVisualizerPanel extends JPanel {
    private MyMapVisualizer map;
    private BottomPanel bottomPanel;

    public MapVisualizerPanel(GeoPosition startPosition) {
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weighty = 0.1;
        c.weightx = 1;
        c.fill = GridBagConstraints.BOTH;
        MyTitlePanel title = new MyTitlePanel(37);
        title.setPreferredSize(new Dimension(1100, 90));
        this.add(title, c);

        c.gridy = 1;
        c.weighty = 0.8;
        c.fill = GridBagConstraints.BOTH;
        this.map = new MyMapVisualizer(startPosition, SampleTopologies.getGLIFDemoTopology().getNetworkTopologyGraph());
        map.setPreferredSize(new Dimension(1100, 600));
        this.add(map, c);

        c.gridy = 2;
        c.weighty = 0.1;
        c.fill = GridBagConstraints.BOTH;
        this.bottomPanel = new BottomPanel(true);
        this.bottomPanel.setPreferredSize(new Dimension(1100, 75));
        this.add(this.bottomPanel, c);

        this.bottomPanel.googleButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                map.setFactory(MyMapVisualizer.TileProviderEnum.GoogleMaps);
                bottomPanel.googleButton.setSelected(true);
                bottomPanel.openStreetsButton.setSelected(false);
                bottomPanel.nasaButton.setSelected(false);
            }});
        this.bottomPanel.openStreetsButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                map.setFactory(MyMapVisualizer.TileProviderEnum.OpenStreetMaps);
                bottomPanel.googleButton.setSelected(false);
                bottomPanel.openStreetsButton.setSelected(true);
                bottomPanel.nasaButton.setSelected(false);
            }});
        this.bottomPanel.nasaButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                map.setFactory(MyMapVisualizer.TileProviderEnum.NasaMaps);
                bottomPanel.googleButton.setSelected(false);
                bottomPanel.openStreetsButton.setSelected(false);
                bottomPanel.nasaButton.setSelected(true);
            }});
        this.bottomPanel.googleButton.doClick();
    }


    public static void main(String[] args) {

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame();
                frame.add(new MapVisualizerPanel(null));
                //frame.setPreferredSize(new Dimension(1100, 900));
                //frame.setMinimumSize(new Dimension(1100, 900));
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
