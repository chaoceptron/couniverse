package GUI.MapVisualization;

import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.*;
import org.jgrapht.graph.DirectedWeightedMultigraph;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;

import NetworkRepresentation.NetworkNode;
import NetworkRepresentation.NetworkLink;
import utils.Shim;
import utils.ShimGenerator;
import utils.CircleShimGenerator;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Apr 22, 2008
 * Time: 4:37:21 PM
 * To change this template use File | Settings | File Templates.
 */

public class MyMapVisualizer extends JXMapViewer {
    public static enum TileProviderEnum {GoogleMaps, OpenStreetMaps, NasaMaps};
    private JSlider zoomSlider;
    private JButton zoomInButton;
    private JButton zoomOutButton;
    private TileFactory googleFactory;
    private TileFactory nasaFactory;
    private TileFactory openStreetFactory;
    private MyNodeWaipointInfoPanel selectedNodeWaipointInfoPanel;

    private GeoPosition startPosition;
    private TileFactory currentFactory = null;
    private final DirectedWeightedMultigraph<NetworkNode, NetworkLink> networkTopologyGraph;
    //for lucid overwiev is better to pack following atribute
    private WaypointRenderer nodeRenderer = new WaypointRenderer() {
            public boolean paintWaypoint(Graphics2D graphics2D, JXMapViewer jxMapViewer, Waypoint waypoint) {
                if (waypoint instanceof MyNodeWaypoint) {
                    NetworkNode node = ((MyNodeWaypoint) waypoint).getNetworkNode();
                    graphics2D = (Graphics2D) graphics2D.create();

                    graphics2D.translate(((MyNodeWaypoint) waypoint).getShimX(), ((MyNodeWaypoint) waypoint).getShimY());

                    // get size of node description
                    Font f = graphics2D.getFont();
                    FontRenderContext frc = graphics2D.getFontRenderContext();
                    Rectangle2D tRect = f.getStringBounds(node.getNodeName(), frc);
                    LineMetrics lMetrics = f.getLineMetrics(node.getNodeName(), frc);
                    int boxWidth = (int) Math.round(tRect.getWidth() + 10);
                    int boxHeight = (int) Math.round(tRect.getHeight() + 5);
                    ((MyNodeWaypoint) waypoint).setSize(boxWidth, boxHeight);

                    // draw the node
                    //graphics2D.setColor(new Color(255, 0, 0, 50));
                    graphics2D.setPaint(new Color(0, 0, 0, 150));
                    graphics2D.fillRoundRect((int) Math.round(-0.5 * boxWidth), (int) Math.round(-0.5 * boxHeight), boxWidth, boxHeight, 20, 20);
                    graphics2D.setColor(Color.white);
                    graphics2D.drawString(node.getNodeName(), Math.round(-0.5 * tRect.getWidth()), Math.round(-0.5 * tRect.getHeight() + lMetrics.getAscent()));


                    graphics2D.setColor(Color.black);
                    // draw the anchor to physical location
                    if (((MyNodeWaypoint) waypoint).getShimX() != 0 || ((MyNodeWaypoint) waypoint).getShimY() != 0) {
                        graphics2D.setStroke(new BasicStroke(1));
                        graphics2D.drawLine(0, 0, -((MyNodeWaypoint) waypoint).getShimX(), -((MyNodeWaypoint) waypoint).getShimY());
                        graphics2D.fillOval(-((MyNodeWaypoint) waypoint).getShimX() - 2, -((MyNodeWaypoint) waypoint).getShimY() - 2, 4, 4);
                    }

                    // restore the original position
                    graphics2D.translate(-((MyNodeWaypoint) waypoint).getShimX(), -((MyNodeWaypoint) waypoint).getShimY());
                } else if (waypoint instanceof MyStreamWaypoint) {
                    MyStreamWaypoint streamWaypoint = (MyStreamWaypoint) waypoint;
                    Point2D fromPoint = jxMapViewer.getTileFactory().geoToPixel(
                            streamWaypoint.getFromPosition(), jxMapViewer.getZoom());
                    fromPoint.setLocation(fromPoint.getX() + streamWaypoint.getFromShimX(), fromPoint.getY() + streamWaypoint.getFromShimY());
                    Point2D toPoint = jxMapViewer.getTileFactory().geoToPixel(
                            streamWaypoint.getToPosition(), jxMapViewer.getZoom());
                    toPoint.setLocation(toPoint.getX() + streamWaypoint.getToShimX(), toPoint.getY() + streamWaypoint.getToShimY());
                    graphics2D.translate(streamWaypoint.getFromShimX(), streamWaypoint.getFromShimY());

                    // draw the line
                    //MapVisualizer.logger.debug("Painting link from " + jxMapViewer.getTileFactory().geoToPixel(streamWaypoint.getFromPosition(), jxMapViewer.getZoom()) + " to " + jxMapViewer.getTileFactory().geoToPixel(
                            //streamWaypoint.getToPosition(), jxMapViewer.getZoom()));
                    //MapVisualizer.logger.debug("   i.e. with shim " + (int) fromPoint.getX() + " " + (int) fromPoint.getY() + " to " + (int) toPoint.getX() + " " + (int) toPoint.getY());
                    graphics2D.setColor(streamWaypoint.getColor());
                    graphics2D.setStroke(new BasicStroke(3));
                    // graphics2D.drawLine(0, 0, (int) (toPoint.getX() - fromPoint.getX()), (int) (toPoint.getY() - fromPoint.getY()));
                    int posShimX = (int) Math.round(((toPoint.getX() - fromPoint.getX()) / 2) + 0.5 * streamWaypoint.networkLink.getLinkShim() * toPoint.distance(fromPoint));
                    int posShimY = (int) Math.round(((toPoint.getY() - fromPoint.getY()) / 2) - 0.5 * streamWaypoint.networkLink.getLinkShim() * toPoint.distance(fromPoint));
                    QuadCurve2D link = new QuadCurve2D.Double(0, 0, posShimX, posShimY, (int) (toPoint.getX() - fromPoint.getX()), (int) (toPoint.getY() - fromPoint.getY()));
                    graphics2D.draw(link);
                    graphics2D.fillOval((int) (toPoint.getX() - fromPoint.getX()) - 5, (int) (toPoint.getY() - fromPoint.getY()) - 5, 10, 10);

                    /*
                    if (paintLinkLegend) {
                        // generate StreamLink description
                        // get size of node description
                        Font origFont = graphics2D.getFont();
                        Font smallFont = new Font(origFont.getFontName(), origFont.getStyle(), origFont.getSize() - 3);
                        graphics2D.setFont(smallFont);
                        Font f = graphics2D.getFont();
                        FontRenderContext frc = graphics2D.getFontRenderContext();
                        String slDescription = "" + streamWaypoint.getNetworkLink().fromInterface.getIpAddress() + " --> " + streamWaypoint.getNetworkLink().toInterface.getIpAddress() + ", " + Converters.bandwidthToString(streamWaypoint.getMediaApplication().getMediaMaxBandwidth());
                        Rectangle2D tRect = f.getStringBounds(slDescription, frc);
                        LineMetrics lMetrics = f.getLineMetrics(slDescription, frc);
                        int boxWidth = (int) Math.round(tRect.getWidth() + 10);
                        int boxHeight = (int) Math.round(tRect.getHeight() + 5);
                        // draw it
                        // TODO - it would be great to find some better (more precise) position, as the shim{X,Y} may be rather way off the link
                        final float posCoeff = 0.6f;
                        graphics2D.setColor(new Color(streamWaypoint.getColor().getRed(), streamWaypoint.getColor().getGreen(), streamWaypoint.getColor().getBlue(), 50));
                        graphics2D.fillRoundRect((int) Math.round(posCoeff * posShimX) + (int) Math.round(-0.5 * boxWidth), (int) Math.round(posCoeff * posShimY) + (int) Math.round(-0.5 * boxHeight), boxWidth, boxHeight, 20, 20);
                        graphics2D.setColor(new Color(0, 0, 0, 50));
                        graphics2D.drawString(slDescription, (int) Math.round(posCoeff * posShimX) + Math.round(-0.5 * tRect.getWidth()), (int) Math.round(posCoeff * posShimY) + Math.round(-0.5 * tRect.getHeight() + lMetrics.getAscent()));

                        // restore original font
                        graphics2D.setFont(origFont);
                    }
                    */

                    // restore the original position
                    graphics2D.translate(-streamWaypoint.getFromShimX(), -streamWaypoint.getFromShimY());
                }
                return false;
            }
        };
    private NetworkPainter painter;





    //should check whether position is not out of bounds
    //ideally it should not be possible to create GeoPosition out of bounds but it is
    // or maybe the values are (mod 360)
    public MyMapVisualizer(GeoPosition startPosition, DirectedWeightedMultigraph<NetworkNode, NetworkLink> g) {
        this.setLayout(new GridBagLayout());
        this.networkTopologyGraph = g;
        checkAndAdjustShims();
        googleFactory = new GoogleTileFactory();
        nasaFactory = new NasaTileFactory();
        openStreetFactory = new OpenStreetsTileFactory();
        ((DefaultTileFactory) googleFactory).setThreadPoolSize(4);
        ((DefaultTileFactory) nasaFactory).setThreadPoolSize(4);
        ((DefaultTileFactory) openStreetFactory).setThreadPoolSize(4);
        init();

        if(startPosition != null && startPosition.getLatitude() == 0L && startPosition.getLongitude() == 0L){
            this.startPosition = startPosition;
        }else{
            this.startPosition = new GeoPosition(50.086226, 14.423289);
        }
        this.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent event) {
                setInfoPanels(event.getPoint());
            }
        });
        this.addMouseWheelListener(new MouseWheelListener()
        {
            public void mouseWheelMoved(MouseWheelEvent event) {
                if(event.getWheelRotation() > 0){
                    MyMapVisualizer.this.zoomSlider.setValue(zoomSlider.getValue() +1);
                }else{
                    MyMapVisualizer.this.zoomSlider.setValue(zoomSlider.getValue() -1);
                } }});

        //this.setRestrictOutsidePanning(true);
        //this.setPaintBorderInsets(true);
        //this.setHorizontalWrapped(true);
        painter = new NetworkPainter();
        painter.setRenderer(nodeRenderer);
        this.setOverlayPainter(painter);

        for (NetworkNode networkNode : this.networkTopologyGraph.vertexSet()) {
            painter.getWaypoints().add(new MyNodeWaypoint(networkNode));
        }
    }
    public void setMyStreamWaypoints( List<MyStreamWaypoint> myStreamWaypoints){
        for(MyStreamWaypoint wp : myStreamWaypoints){
            this.painter.getWaypoints().add(wp);
        }
    }

    private void setInfoPanels(Point point) {
        System.err.println(point);
        for(Waypoint p : this.painter.getWaypoints()){
            if(p instanceof MyNodeWaypoint){
                //this.selectedNodeWaipointInfoPanel.setNodeWaipoint((MyNodeWaypoint) this.painter.getWaypoints().iterator().next());
                if(((MyNodeWaypoint) p).wasClickedOnMap((int)point.getX(),(int)point.getY())){
                    this.selectedNodeWaipointInfoPanel.setNodeWaipoint((MyNodeWaypoint)p);
                    this.repaint();
                    break;
                }
            }else{
                System.err.println("nalezen NonMynodeWaypoint");
            }

        }
    }



    private void init(){
        zoomInButton = new javax.swing.JButton();
        zoomOutButton = new javax.swing.JButton();
        zoomSlider = new javax.swing.JSlider();
        JPanel jPanel = new JPanel();
        jPanel.setOpaque(false);
        jPanel.setLayout(new java.awt.GridBagLayout());

        zoomInButton.setText("-");
        zoomInButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        zoomInButton.setMaximumSize(new java.awt.Dimension(20, 20));
        zoomInButton.setMinimumSize(new java.awt.Dimension(20, 20));
        zoomInButton.setOpaque(false);
        zoomInButton.setPreferredSize(new java.awt.Dimension(20, 20));
        zoomInButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MyMapVisualizer.this.zoomSlider.setValue(zoomSlider.getValue() +1);
            }});

        zoomOutButton.setText("+");
        zoomOutButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        zoomOutButton.setMaximumSize(new java.awt.Dimension(20, 20));
        zoomOutButton.setMinimumSize(new java.awt.Dimension(20, 20));
        zoomOutButton.setOpaque(false);
        zoomOutButton.setPreferredSize(new java.awt.Dimension(20, 20));
        zoomOutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MyMapVisualizer.this.zoomSlider.setValue(zoomSlider.getValue() -1);

        }});

        zoomSlider.setOpaque(false);
        zoomSlider.setForeground(Color.black);
        zoomSlider.setMajorTickSpacing(1);
        zoomSlider.setMinorTickSpacing(1);
        zoomSlider.setOrientation(javax.swing.JSlider.VERTICAL);
        zoomSlider.setPaintTicks(true);
        zoomSlider.setSnapToTicks(true);
        zoomSlider.setMinimumSize(new java.awt.Dimension(35, 100));
        zoomSlider.setPreferredSize(new java.awt.Dimension(35, 230));
        zoomSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                MyMapVisualizer.this.setZoom(zoomSlider.getValue());
        }});

        GridBagConstraints c = new java.awt.GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 1.0;
        c.weighty = 1.0;
        c.fill = java.awt.GridBagConstraints.VERTICAL;
        c.anchor = java.awt.GridBagConstraints.PAGE_START;
        jPanel.add(zoomSlider, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;        
        c.fill = java.awt.GridBagConstraints.NONE;
        c.anchor = java.awt.GridBagConstraints.FIRST_LINE_START;
        c.weighty = 1.0;
        jPanel.add(zoomOutButton, c);

        c.gridx = 1;
        c.gridy = 1;
        c.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        c.weightx = 1.0;
        c.weighty = 1.0;
        jPanel.add(zoomInButton, c);

        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1;
        c.weighty = 1;
        c.anchor = GridBagConstraints.LAST_LINE_START;
        c.insets = new Insets(5,5,5,5);
        jPanel.setPreferredSize(new Dimension(40, 250));
        this.add(jPanel, c);

        c.anchor = GridBagConstraints.FIRST_LINE_START;
        this.selectedNodeWaipointInfoPanel = new MyNodeWaipointInfoPanel();
        selectedNodeWaipointInfoPanel.setPreferredSize(new Dimension(200, 200));
        this.add(selectedNodeWaipointInfoPanel, c);

    }

    public void setFactory(TileProviderEnum factory){
        TileFactory newFactory = null;
        switch(factory){
            case GoogleMaps:
                newFactory = this.googleFactory;
                break;
            case NasaMaps:
                newFactory = this.nasaFactory;
                break;
            case OpenStreetMaps:
                newFactory = this.openStreetFactory;
                break;
            default:
                System.err.println("factory not found");
        }
        if(newFactory != null && 
                this.currentFactory != newFactory){
            this.currentFactory = newFactory;

            zoomSlider.setMaximum(this.currentFactory.getInfo().getMaximumZoomLevel());
            zoomSlider.setMinimum(this.currentFactory.getInfo().getMinimumZoomLevel());
            this.setTileFactory(this.currentFactory);
            this.zoomSlider.setValue(this.currentFactory.getInfo().getMaximumZoomLevel() -2);
            this.setAddressLocation(this.startPosition);
            this.repaint();
        }
    }




    class NetworkPainter extends WaypointPainter<JXMapViewer> {
        protected void doPaint(Graphics2D g, JXMapViewer map, int width, int height) {
            //figure out which waypoints are within this map viewport
            //so, get the bounds
            Rectangle viewportBounds = map.getViewportBounds();
            int zoom = map.getZoom();
            Dimension sizeInTiles = map.getTileFactory().getMapSize(zoom);
            int tileSize = map.getTileFactory().getTileSize(zoom);
            Dimension sizeInPixels = new Dimension(sizeInTiles.width * tileSize, sizeInTiles.height * tileSize);

            double vpx = viewportBounds.getX();
            // normalize the left edge of the viewport to be positive
            while (vpx < 0) {
                vpx += sizeInPixels.getWidth();
            }
            // normalize the left edge of the viewport to no wrap around the world
            while (vpx > sizeInPixels.getWidth()) {
                vpx -= sizeInPixels.getWidth();
            }

            // create two new viewports next to eachother
            Rectangle2D vp2 = new Rectangle2D.Double(vpx,
                    viewportBounds.getY(), viewportBounds.getWidth(), viewportBounds.getHeight());
            Rectangle2D vp3 = new Rectangle2D.Double(vpx - sizeInPixels.getWidth(),
                    viewportBounds.getY(), viewportBounds.getWidth(), viewportBounds.getHeight());

            //for each waypoint within these bounds
            for (Waypoint w : getWaypoints()) {
                Point2D point = map.getTileFactory().geoToPixel(w.getPosition(), map.getZoom());
                if (w instanceof MyNodeWaypoint && vp2.contains(point) ) {
                    int x = (int) (point.getX() - vp2.getX());
                    int y = (int) (point.getY() - vp2.getY());
                    g.translate(x, y);
                    ((MyNodeWaypoint)w).setMapOriginPosition(x,y);
                    paintWaypoint(w, map, g);
                    g.translate(-x, -y);
                }
                if (vp3.contains(point)) {
                    int x = (int) (point.getX() - vp3.getX());
                    int y = (int) (point.getY() - vp3.getY());
                    g.translate(x, y);
                    paintWaypoint(w, map, g);
                    g.translate(-x, -y);
                }
                if (w instanceof MyStreamWaypoint) {
                     //MyStreamWaypoints get painted independent of whether they are in viewports
                    {
                        int x = (int) (point.getX() - vp2.getX());
                        int y = (int) (point.getY() - vp2.getY());
                        g.translate(x, y);
                        paintWaypoint(w, map, g);
                        g.translate(-x, -y);
                    }
                }
            }
        }
    }

    private void checkAndAdjustShims() {
        ArrayList<NetworkNode> nodeList = new ArrayList<NetworkNode>(networkTopologyGraph.vertexSet());
        class Position {
            double geoLongitude;
            double geoLatitude;

            public Position(double geoLongitude, double geoLatitude) {
                this.geoLongitude = geoLongitude;
                this.geoLatitude = geoLatitude;
            }

            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Position position = (Position) o;

                if (Double.compare(position.geoLatitude, geoLatitude) != 0) return false;
                if (Double.compare(position.geoLongitude, geoLongitude) != 0) return false;

                return true;
            }

            public int hashCode() {
                int result;
                long temp;
                temp = geoLongitude != +0.0d ? Double.doubleToLongBits(geoLongitude) : 0L;
                result = (int) (temp ^ (temp >>> 32));
                temp = geoLatitude != +0.0d ? Double.doubleToLongBits(geoLatitude) : 0L;
                result = 31 * result + (int) (temp ^ (temp >>> 32));
                return result;
            }
        }
        HashMap<Position, ArrayList<NetworkNode>> identicalPositionList = new HashMap<Position, ArrayList<NetworkNode>>();
        for (int i = 0; i < nodeList.size(); i++) {
            Position p1 = new Position(nodeList.get(i).getGeoLongitude(), nodeList.get(i).getGeoLatitude());

            for (int j = 0; j < nodeList.size(); j++) {
                if (i != j) {
                    Position p2 = new Position(nodeList.get(j).getGeoLongitude(), nodeList.get(j).getGeoLatitude());
                    if (p1.equals(p2)) {
                        if (!identicalPositionList.containsKey(p1)) {
                            identicalPositionList.put(p1, new ArrayList<NetworkNode>());
                        }
                        ArrayList<NetworkNode> l = identicalPositionList.get(p1);
                        if (!l.contains(nodeList.get(i)) && !nodeList.get(i).getShim().isShimSet()) {
                            l.add(nodeList.get(i));
                        }
                        if (!l.contains(nodeList.get(j)) && !nodeList.get(j).getShim().isShimSet()) {
                            l.add(nodeList.get(j));
                        }
                    }
                }
            }
        }

        for (ArrayList<NetworkNode> networkNodes : identicalPositionList.values()) {
            final int shimCircleRadius = 40;
            ShimGenerator shimGenerator = new CircleShimGenerator(shimCircleRadius, networkNodes.size());
            for (NetworkNode networkNode : networkNodes) {
                networkNode.setShim(shimGenerator.generateShim());
            }
        }
    }
}
