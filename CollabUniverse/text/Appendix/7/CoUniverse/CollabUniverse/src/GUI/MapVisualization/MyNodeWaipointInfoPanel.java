package GUI.MapVisualization;

import NetworkRepresentation.NetworkNode;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: May 6, 2008
 * Time: 12:18:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyNodeWaipointInfoPanel extends JPanel {
    private MyNodeWaypoint nodeWaipoint = null;
    private boolean changed = false;


    public MyNodeWaipointInfoPanel() {
        this.setLayout(null);
    }

    public void paint(Graphics graphics) {
        if (this.changed && this.nodeWaipoint != null) {

            Graphics2D g = (Graphics2D) graphics;
            g.setPaint(new Color(0, 0, 0, 150));
            g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), 15, 15);
            //g.fillRoundRect(50, 10, 182 , 30, 10, 10);
            g.setPaint(Color.WHITE);
            g.setFont(new Font("Dialog", Font.BOLD, 14));
            //g.drawString(this.nodeWaipoint.getNetworkNode().getNodeName(), 13,23);
            NetworkNode node = this.nodeWaipoint.getNetworkNode();

            if(node.getNodeName().length() > 20){
                g.drawString(node.getNodeName().subSequence(0,21) +"...", 16,23);
            }else{
                g.drawString(node.getNodeName(), 16,23);
            }
            if(node.getNodePeerID().length() > 20){
                g.drawString(node.getNodePeerID().subSequence(0,21) +"...", 16,43);
            }else{
                g.drawString(node.getNodePeerID(), 16,43);
            }
            if(node.getNodeSite().getSiteName().length() > 20){
                g.drawString(node.getNodeSite().getSiteName().subSequence(0,21) +"...", 16,63);
            }else{
                g.drawString(node.getNodeSite().getSiteName(), 16,63);
            }


            g.drawString("(" +Double.toString(node.getGeoLatitude()) +", "
                    +Double.toString(node.getGeoLongitude())+ ")" ,13,183);



        }
    }

    public MyNodeWaypoint getNodeWaipoint() {
        return nodeWaipoint;
    }

    public void setNodeWaipoint(MyNodeWaypoint nodeWaipoint) {
        this.changed = true;
        this.nodeWaipoint = nodeWaipoint;
    }


}
