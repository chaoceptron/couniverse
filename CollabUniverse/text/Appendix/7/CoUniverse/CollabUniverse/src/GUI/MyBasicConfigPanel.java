package GUI;



import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.*;

import GUI.constants.GuiConstants;

/**
 *
 * @author martin
 */
public class MyBasicConfigPanel extends JPanel {

    private JFileChooser fc;
    private File configFile;
    private String previousFile;
    private JTextField path;
    private MyButton openButton;

    private MyCheckBox outDialogCheckBox;
    private MyCheckBox infraCheckBox;
    private MyCheckBox cDialogCheckBox;
    private MyCheckBox agcCheckBox;

    private MyRadioButton localFileRadioButton;
    private MyRadioButton urlRadioButton;

    public MyBasicConfigPanel() {
        super(new GridBagLayout(), false);        
        fc = new JFileChooser();
        init();
    }

    public MyCheckBox getAgcCheckBox() {
        return agcCheckBox;
    }

    public MyCheckBox getCDialogCheckBox() {
        return cDialogCheckBox;
    }

    public MyCheckBox getInfraCheckBox() {
        return infraCheckBox;
    }

    public MyCheckBox getOutDialogCheckBox() {
        return outDialogCheckBox;
    }

    public boolean isConfigLocalFile(){
        return this.localFileRadioButton.isSelected();
    }

    public String getConfigFilePath(){
        return this.path.getText();
    }

    private void init() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 0.2;
        
        c.anchor = GridBagConstraints.PAGE_START;
        c.insets = new Insets(15,0,0,0);

        add(new MyTextLabel(GuiConstants.progressPanelStates[0], 27, 22, 200, 30), c);

        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(0,0,0,0);

        c.weighty = 0.2;
        c.gridy = 1;
        c.gridwidth = 1;        
        c.weightx = 0.5;
        add(this.outDialogCheckBox = new MyCheckBox("OUT Dialog", true), c);

        c.gridx = 1;
        add(this.infraCheckBox = new MyCheckBox("Infra", true), c);

        c.gridx = 0;
        c.gridy = 2;
        add(this.cDialogCheckBox = new MyCheckBox("C dialog", true), c);

        c.gridx = 1;
        add(this.agcCheckBox = new MyCheckBox("AGC", true), c);

        c.gridx = 0;
        c.gridy = 3;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_END;
        this.localFileRadioButton = new MyRadioButton("Local file", true, 12);
        this.localFileRadioButton.setPreferredSize(new Dimension(90, 40));
        add(this.localFileRadioButton, c);

        this.urlRadioButton = new MyRadioButton("URL", false, 12);
        c.gridx = 1;
        c.insets = new Insets(5, 5, 5, 5);
        c.anchor = GridBagConstraints.LINE_START;
        this.urlRadioButton.setPreferredSize(new Dimension(110, 40));
        add(this.urlRadioButton, c);

        ButtonGroup group = new ButtonGroup();
        group.add(this.localFileRadioButton);
        group.add(this.urlRadioButton);
        
        path = new JTextField("/home/user/CollabUniverse/configFile.xml");
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 2;
        c.insets = new Insets(10, 30, 10, 0);
        c.anchor = GridBagConstraints.LAST_LINE_START;
        path.setPreferredSize(new Dimension(435, 25));        
        add(path, c);

        this.openButton = new MyButton("Open file...");
        c.gridx = 1;
        c.gridy = 3;
        c.insets = new Insets(0, 0, 7, 23);
        c.anchor = GridBagConstraints.LAST_LINE_END;
        c.fill = GridBagConstraints.NONE;
        add(openButton, c);

        openButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                openButtonClicked();}
        });

        localFileRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                localFileRadioButtonClicked();}
        });
        urlRadioButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                urlRadioButtonClicked();}
        });          
    }

    private void localFileRadioButtonClicked() {
        this.openButton.setEnabled(true);
        this.path.setText("/home/user/CollabUniverse/configFile.xml");
    }

    private void urlRadioButtonClicked() {
        this.openButton.setEnabled(false);
        this.path.setText("http://server.com/configFile.xml");
    }

    public void openButtonClicked() {
            int returnVal = fc.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File configFile = fc.getSelectedFile();
                path.setText(configFile.getAbsolutePath());
            }
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint backgroundColor = new GradientPaint(0, 0, new Color(0, 0, 0),
                getWidth() - 5, getHeight() - 5, new Color(196, 196, 255),
                true);
        g2.setPaint(backgroundColor);
        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 40, 40);

        paintChildren(g);
        g2.dispose();
    }
}


