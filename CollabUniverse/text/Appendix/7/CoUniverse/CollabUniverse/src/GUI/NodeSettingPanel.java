package GUI;

import GUI.constants.GuiConstants;
import NetworkRepresentation.EndpointNetworkNode;
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class NodeSettingPanel extends JPanel {
    private JTextField nodeNameField;
    public JTextField nodePeerIDField;
    private JTextField geoLatitudeField;
    private JTextField geoLongitudeField;
    private JTextField siteNameField;

    //private String nodeName;
    //private String nodePeerID;
    //private double geoLatitude;
    //private double geoLongitude;
    //private String siteName;

    private MyCheckBox isLocalNodeCheckBox;


    public NodeSettingPanel() {
        super(new GridBagLayout());
        init();
    }


    public void setComponentsFromConfig(EndpointNetworkNode node) {
        if (node != null) {

            if (node.getNodeName() != null) {
                this.nodeNameField.setText(node.getNodeName());
            }
            if (node.getNodePeerID() != null) {
                this.nodePeerIDField.setText(node.getNodePeerID());
            }
            if (node.getNodeSite() != null && node.getNodeSite().getSiteName() != null) {
                this.siteNameField.setText(node.getNodeSite().getSiteName());
            }
            this.geoLatitudeField.setText(Double.toString(node.getGeoLatitude()));
            this.geoLongitudeField.setText(Double.toString(node.getGeoLongitude()));

            this.isLocalNodeCheckBox.setSelected(node.isLocalNode());
        }

    }

    public void setConfigFromComponents(EndpointNetworkNode node) {
        if(node != null){
            node.setNodeName(this.nodeNameField.getText());
            node.setNodePeerID(this.nodePeerIDField.getText());
            if(node.getNodeSite() != null){
                node.getNodeSite().setSiteName(this.siteNameField.getText());
            }
            node.setGeoLatitude(Double.parseDouble(this.geoLatitudeField.getText()));
            node.setGeoLongitude(Double.parseDouble(this.geoLongitudeField.getText()));
            node.setLocalNode(this.isLocalNodeCheckBox.isSelected());
        }
    }


    private void init() {
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weighty = 0.2;

        c.anchor = GridBagConstraints.PAGE_START;
        c.insets = new Insets(15, 0, 0, 0);

        add(new MyTextLabel(GuiConstants.progressPanelStates[1], 27, 22, 200, 30), c);

        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(0, 0, 0, 0);

        c.weightx = 0.15;
        c.weighty = 0.13;
        c.gridwidth = 1;
        c.gridy++;
        c.insets = new Insets(5, 5, 5, 5);
        add(new MyTextLabel("Node Name:", 16, 17, 160, 23), c);

        c.gridy++;
        add(new MyTextLabel("Node Peer Id:", 16, 17, 160, 23), c);

        c.gridy++;
        add(new MyTextLabel("Geo Latitude:", 16, 17, 160, 23), c);

        c.gridy++;
        add(new MyTextLabel("Geo Longtitude:", 16, 17, 160, 23), c);

        c.gridy++;
        add(new MyTextLabel("Network Site:", 16, 17, 160, 23), c);

        c.weightx = 0.85;
        c.gridx = 1;
        c.fill = GridBagConstraints.HORIZONTAL;


        add(siteNameField = new JTextField(), c);


        c.gridy--;
        add(geoLongitudeField = new JTextField(), c);

        c.gridy--;
        add(geoLatitudeField = new JTextField(), c);

        c.gridy--;
        add(nodePeerIDField = new JTextField(), c);
        this.nodePeerIDField.setPreferredSize(new Dimension(120, 30));

        c.gridy--;
        add(nodeNameField = new JTextField(), c);


        c.weightx = 0.3;
        c.weighty = 0.15;
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 6;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.LINE_END;
        add(this.isLocalNodeCheckBox = new MyCheckBox("Local Node", true), c);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setPaint(Color.white);
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint backgroundColor = new GradientPaint(0, 0, new Color(0, 0, 0),
                getWidth() - 5, getHeight() - 5, new Color(196, 196, 255),
                true);
        g2.setPaint(backgroundColor);
        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 40, 40);

        paintChildren(g);
        g2.dispose();
    }
}
