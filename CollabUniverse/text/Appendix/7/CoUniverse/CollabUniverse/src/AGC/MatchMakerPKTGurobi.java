package AGC;

import MediaAppFactory.*;
import MediaApplications.RumHD;
import NetworkRepresentation.*;
import gurobi.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.*;
import org.jgrapht.alg.StrongConnectivityInspector;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.jgrapht.graph.EdgeReversedGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;
import utils.CombinationGenerator;

/* TODO: rewrite */
/**
 * MatchMakerUtils class is responsible for matching of available network
 * topology with available MediaApplications in the network. It uses
 * constranit-based approach for scheduling MediaStreams onto specific links.
 * <p/>
 * BEWARE OF OGRES! This class is not thread safe and even worse, it requires
 * that results are obtained while the network topology hasn't changed in
 * meantime! This will improve later as this is just a fast prototype.
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public final class MatchMakerPKTGurobi extends MatchMaker {

    private MatchMakerConfig matchMakerConfig;
    private final PartiallyKnownNetworkTopology networkTopology;
    private boolean matchFound = false;
    private DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink> g;
    private ArrayList<LogicalNetworkLink> activeLogicalEdges;
    private ArrayList<PhysicalNetworkLink> activePhysicalLinks;
    private ArrayList<MediaApplication> activeSrcApps;
    private HashMap<MediaApplication, ArrayList<MediaApplication>> activeConsumers;
    private int[][] resultStreamLinks;
    private ArrayList<MediaApplication> resultProducers;
    private ArrayList<LogicalNetworkLink> resultLogicalLinks;

    /**
     * MatchMakerUtils constructor
     * <p/>
     *
     * @param networkTopology input network topology, as constructed by
     * Application Group Controller
     * @param matchMakerConfig configuration of the MatchMaker
     */
    public MatchMakerPKTGurobi(PartiallyKnownNetworkTopology networkTopology, MatchMakerConfig matchMakerConfig) {
        PartiallyKnownNetworkTopology temporaryTopology;
        try {
            temporaryTopology = PartiallyKnownNetworkTopology.getCurrentSnapshot(networkTopology);
        } catch (CloneNotSupportedException e) {
            temporaryTopology = null;
        }
        this.networkTopology = temporaryTopology;
        this.resultStreamLinks = null;
        this.matchMakerConfig = matchMakerConfig;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean doMatch() {
        boolean timedOut = false;
        Executor exec = Executors.newSingleThreadExecutor();
        FutureTask task = new FutureTask(new Runnable() {
            @Override
            public void run() {
                constructGraph();
                doMatchInternal();
            }
        }, null);
        exec.execute(task);
        //noinspection EmptyCatchBlock
        try {
            if (this.matchMakerConfig.timeout == 0) {
                task.get();
            } else {
                task.get(this.matchMakerConfig.timeout, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
        } catch (ExecutionException e) {
        } catch (TimeoutException e) {
            timedOut = true;
        } finally {
            task.cancel(true);
        }
        if (timedOut) {
            this.matchFound = false;
        }
        return this.matchFound;
    }

    @Override
    public ArrayList<PlanElement> getPlan() {

        ArrayList<PlanElement> plan = new ArrayList<PlanElement>();

        // dependency list for each source application using topological sort
        MatchMaker.logger.debug("Implementing the master plan:");
        MatchMaker.logger.debug("");
        for (int j = 0; j < activeSrcApps.size(); j++) {
            // graph for configuring nodes
            DirectedWeightedSubgraph<EndpointNetworkNode, LogicalNetworkLink> applicationGraph =
                    new DirectedWeightedSubgraph<EndpointNetworkNode, LogicalNetworkLink>(g, g.vertexSet(), new HashSet<LogicalNetworkLink>());
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                if (resultStreamLinks[i][j] == 1) {
                    // we have to reverse orientation of the edge
                    applicationGraph.addEdge(g.getEdgeSource(activeLogicalEdges.get(i)), g.getEdgeTarget(activeLogicalEdges.get(i)), activeLogicalEdges.get(i));
                }
            }
            // remove nodes that are not connected (beware applicationGraph is read-only while being iterated upon - that's why
            // it's a two step process
            ArrayList<EndpointNetworkNode> nodesForRemoval = new ArrayList<EndpointNetworkNode>();
            nodesForRemoval.clear();
            for (EndpointNetworkNode networkNode : applicationGraph.vertexSet()) {
                if (applicationGraph.inDegreeOf(networkNode) < 1 && applicationGraph.outDegreeOf(networkNode) < 1) {
                    nodesForRemoval.add(networkNode);
                }
            }
            for (EndpointNetworkNode networkNode : nodesForRemoval) {
                applicationGraph.removeVertex(networkNode);
            }

            // graph for topological sort
            EdgeReversedGraph<EndpointNetworkNode, LogicalNetworkLink> toposortGraph = new EdgeReversedGraph<EndpointNetworkNode, LogicalNetworkLink>(applicationGraph);

            TopologicalOrderIterator<EndpointNetworkNode, LogicalNetworkLink> toposort = new TopologicalOrderIterator<EndpointNetworkNode, LogicalNetworkLink>(toposortGraph);
            while (toposort.hasNext()) {
                EndpointNetworkNode n = toposort.next();
                MediaApplication ma = null;
                if (activeSrcApps.get(j).getParentNode() == n) {
                    ma = activeSrcApps.get(j);
                }
                if (ma == null) {
                    // current node is not hosting the source media application, thus looking for an appropriate consumer
                    for (MediaApplication mediaApplication : n.getNodeApplications()) {
                        if (mediaApplication instanceof MediaApplicationConsumer) {
                            if (((MediaApplicationConsumer) mediaApplication).getSource() == activeSrcApps.get(j)) {
                                ma = mediaApplication;
                            }
                        }
                    }
                }
                if (ma == null) {
                    // there was no appropriate MediaApplicationConsumer on the node, thus looking for a reflector
                    for (MediaApplication mediaApplication : n.getNodeApplications()) {
                        if (mediaApplication instanceof MediaApplicationDistributor) {
                            ma = mediaApplication;
                        }
                    }
                }
                assert ma != null : "Failed to identify application on the node";
                MatchMaker.logger.debug("Application " + ma + " on node " + n + " will be configured for source application " + activeSrcApps.get(j));
                PlanElement p;
                if (!(ma instanceof MediaApplicationConsumer)) {
                    ArrayList<LogicalNetworkLink> nls = new ArrayList<LogicalNetworkLink>();
                    for (LogicalNetworkLink networkLink : applicationGraph.outgoingEdgesOf(n)) {
                        nls.add(networkLink);
                    }
                    assert nls.size() > 0 : "Unable to identify outgoing interfaces!";
                    p = new PlanElement(ma, nls);
                } else {
                    LogicalNetworkLink nl = null;
                    assert applicationGraph.outgoingEdgesOf(activeSrcApps.get(j).getParentNode()).size() == 1 : "There are more or less edges on the source than anticipated (= 1)!";
                    for (LogicalNetworkLink networkLink : applicationGraph.outgoingEdgesOf(activeSrcApps.get(j).getParentNode())) {
                        nl = networkLink;
                    }
                    assert nl != null : "Unable to identify a sending interface of the source!";
                    p = new PlanElement((MediaApplicationConsumer) ma, nl.getFromInterface());
                }
                plan.add(p);
                MatchMaker.logger.debug("   added plan: " + p);
            }
            MatchMaker.logger.debug("");

        }

        return plan;

    }

    @Override
    public MapVisualizer getMapVizualization() {
        MapVisualizer map;
        map = new MapVisualizer(g);

        for (int j = 0; j < resultProducers.size(); j++) {
            Color c = new Color((int) Math.round(Math.random() * 255.0), (int) Math.round(Math.random() * 180.0), (int) Math.round(Math.random() * 180.0), 255);

            for (int i = 0; i < resultLogicalLinks.size(); i++) {
                if (resultStreamLinks[i][j] == 1) {
                    map.addLink(resultProducers.get(j), resultLogicalLinks.get(i), c);
                }
            }
        }
        map.setPaintLinkLegend(true);
        return map;
    }

    // ----------------------------------------------------------------------------------------------
    // Internal part
    // ----------------------------------------------------------------------------------------------
    /**
     * Checks whether two media streams are identical.
     *
     * @param ms1 media stream to be compared
     * @param ms2 media stream to be compared
     * @return true iff identical
     */
    private boolean mediaStreamMatch(CopyOnWriteArraySet<MediaStream> ms1, CopyOnWriteArraySet<MediaStream> ms2) {
        for (MediaStream ms : ms1) {
            if (!ms2.contains(ms)) {
                return false;
            }
        }
        for (MediaStream ms : ms2) {
            if (!ms1.contains(ms)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Gets graph from the networkTopology
     */
    @SuppressWarnings({"unchecked"})
    private void constructGraph() {
        g = new DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink>(LogicalNetworkLink.class);
        for (EndpointNetworkNode node : networkTopology.getEndpointNodes()) {
            g.addVertex(node);
        }
        for (LogicalNetworkLink link : networkTopology.getLogicalLinks()) {
            g.addEdge(link.getFromNode(), link.getToNode(), link);
        }
    }

    private void doMatchInternal() {
        // auxilliary class for dynamic IntVar lists
        class GRBVarList extends ArrayList<GRBVar> {

            @Override
            public boolean add(GRBVar o) {
                return o != null && super.add(o);
            }

            @Override
            public GRBVar[] toArray() {
                GRBVar[] tmp = new GRBVar[this.size()];
                tmp = super.toArray(tmp);
                return tmp;
            }
        }

        // network mapping
        ArrayList<MediaApplication> mediaApplications = new ArrayList<MediaApplication>();
        ArrayList<EndpointNetworkNode> streamSources = new ArrayList<EndpointNetworkNode>();
        ArrayList<EndpointNetworkNode> streamTargets = new ArrayList<EndpointNetworkNode>();
        ArrayList<EndpointNetworkNode> streamReflectors = new ArrayList<EndpointNetworkNode>();
        activeSrcApps = new ArrayList<MediaApplication>();

        for (EndpointNetworkNode networkNode : g.vertexSet()) {
            for (MediaApplication mediaApplication : networkNode.getNodeApplications()) {
                mediaApplications.add(mediaApplication);
                if (mediaApplication instanceof MediaApplicationProducer) {
                    streamSources.add(mediaApplication.getParentNode());
                }
                if (mediaApplication instanceof MediaApplicationConsumer) {
                    streamTargets.add(mediaApplication.getParentNode());
                }
                if (mediaApplication instanceof MediaApplicationDistributor) {
                    streamReflectors.add(mediaApplication.getParentNode());
                }
            }
        }

        // in the following code, we analyze the set-up network and print out different properties
        MatchMaker.logger.info("--------- Network configuration ---------");
        MatchMaker.logger.info("");

        MatchMaker.logger.debug("We have the following data sources:");
        for (EndpointNetworkNode streamSource : streamSources) {
            MatchMaker.logger.debug("   " + streamSource);
        }
        MatchMaker.logger.debug("");

        MatchMaker.logger.debug("We have the following data sinks:");
        for (EndpointNetworkNode streamTarget : streamTargets) {
            MatchMaker.logger.debug("   " + streamTarget);
        }
        MatchMaker.logger.debug("");

        MatchMaker.logger.debug("We have the following possible reflectors:");
        for (EndpointNetworkNode streamReflector : streamReflectors) {
            MatchMaker.logger.debug("   " + streamReflector);
        }
        MatchMaker.logger.debug("");

        MatchMaker.logger.debug("Applications have the following dependencies:");
        activeConsumers = new HashMap<MediaApplication, ArrayList<MediaApplication>>();
        for (MediaApplication ma : mediaApplications) {
            if (!(ma instanceof MediaApplicationConsumer)) {
                continue;
            }
            MediaApplicationConsumer mtgt = (MediaApplicationConsumer) ma;
            if (mtgt.getSource() != null) {
                MediaApplication msrc = mtgt.getSource();
                MatchMaker.logger.debug("   " + mtgt + " (" + ma.getParentNode() + ") wants data from "
                        + msrc + " (" + msrc.getParentNode() + ")");
                if (!activeSrcApps.contains(msrc)) {
                    activeSrcApps.add(msrc);
                }
                ArrayList<MediaApplication> applist;
                if (!activeConsumers.containsKey(msrc)) {
                    applist = new ArrayList<MediaApplication>();
                } else {
                    applist = activeConsumers.get(msrc);
                }
                applist.add(ma);
                activeConsumers.put(msrc, applist);
            }
        }

        MatchMaker.logger.debug("");

        MatchMaker.logger.info("Consumer list w.r.t. producers:");
        for (MediaApplication mediaProducer : activeConsumers.keySet()) {
            MatchMaker.logger.info("   Producer: " + mediaProducer + " on node " + mediaProducer.getParentNode());
            for (MediaApplication mediaConsumer : activeConsumers.get(mediaProducer)) {
                MatchMaker.logger.info("      Consumer: " + mediaConsumer + " on node " + mediaConsumer.getParentNode());
            }
        }

        MatchMaker.logger.debug("");

        MatchMaker.logger.debug("Streams for which scheduling will be done:");
        for (MediaApplication msrc : activeSrcApps) {
            MatchMaker.logger.debug("   " + msrc + " (" + msrc.getParentNode() + ")");
        }

        MatchMaker.logger.info("");
        MatchMaker.logger.info("--------- End of network configuration ---------");
        MatchMaker.logger.info("");

        //-------------------------------------------------------------------
        // Solver part
        //-------------------------------------------------------------------

        long preparationStartTime, solvingStartTime, solvingStopTime;

        preparationStartTime = System.currentTimeMillis();

        //1- Create the Gurobi environment
        GRBEnv env;
        GRBModel model;
        try {
            env = new GRBEnv();
            model = new GRBModel(env);
        } catch (GRBException ex) {
            infoOnException(ex, "Gurobi environment or model initialization failed.");
            this.matchFound = false;
            return;
        }

        //2- Create the variables

        activeLogicalEdges = new ArrayList<LogicalNetworkLink>(g.edgeSet());
        activePhysicalLinks = new ArrayList<PhysicalNetworkLink>(networkTopology.getPhysicalLinks());

        // Elimination of the following type physically reduces number of variables, thus making the problem
        // less memory consuming.
        MatchMaker.logger.debug("Link elimination phase.");

        // eliminate edges that don't have sufficient capacity for any application
        long minStreamBandwidth = 0, maxStreamBandwidth = 0;
        for (MediaApplication app : activeSrcApps) {
            for (MediaStream mediaStream : app.getMediaStreams()) {
                if (minStreamBandwidth == 0) {
                    minStreamBandwidth = mediaStream.getBandwidth_min();
                } else {
                    if (minStreamBandwidth > mediaStream.getBandwidth_min()) {
                        minStreamBandwidth = mediaStream.getBandwidth_min();
                    }
                }
                if (maxStreamBandwidth < mediaStream.getBandwidth_max()) {
                    maxStreamBandwidth = mediaStream.getBandwidth_max();
                }
            }
        }
        double minCapacityMb = minStreamBandwidth / 1000000;
        ArrayList<LogicalNetworkLink> newActiveLogicalEdges = new ArrayList<LogicalNetworkLink>();
        ArrayList<PhysicalNetworkLink> newActivePhysicalLinks = new ArrayList<PhysicalNetworkLink>();
        // for the paper, we assume that the intra-site links are removed
        if (this.matchMakerConfig.getBooleanProperty("enableElimIntrasiteLinksLogical")) {
            MatchMaker.logger.warn("MatchMakerGurobi assumes it's run with elimination of intra-site links enabled!");
            // this construction makes us more flexible - e.g., MatchMakerTest runs without intra-site links elimination, so we can just comment it out
            //assert !this.matchMakerConfig.enableIntrasiteLinks; 
        }
        long eliminatedCapacity = 0, eliminatedActivity = 0, eliminatedIntraSite = 0, eliminatedMissingApplication = 0;
        for (LogicalNetworkLink activeEdge : activeLogicalEdges) {

            boolean eliminate = false;

            // elimination of edges based on capacity
            if (matchMakerConfig.getBooleanProperty("enableElimLinkCapacityLogical")) {
                if (activeEdge.getCapacity() < minCapacityMb) {
                    MatchMaker.logger.debug("removing edge: " + activeEdge + " because its capacity is " + activeEdge.getCapacity() + " (less than " + minCapacityMb + ")");
                    eliminatedCapacity++;
                    eliminate = true;
                }
            }

            // elimination of inactive edges
            if (!activeEdge.isActive()) {
                eliminatedActivity++;
                eliminate = true;
            }

            // eliminiation of edges based on intra-site links
            if (!this.matchMakerConfig.getBooleanProperty("enableElimIntrasiteLinksLogical")) {
                if (activeEdge.getFromNode().getNodeSite().equals(activeEdge.getToNode().getNodeSite())
                        && !activeEdge.getFromNode().hasDistributor() && !activeEdge.getToNode().hasDistributor()) {
                    MatchMaker.logger.debug("removing edge: " + activeEdge + " because it is intra-site link");
                    eliminatedIntraSite++;
                    eliminate = true;
                }
            }

            // elimination of edges to/from nodes where no consumer, producer or media distributor is running
            // these two conditions need to be enabled-disabled simultanously
            if (matchMakerConfig.getBooleanProperty("enableElimNoappLogical")) {
                boolean maySend = false, mayRecv = false;
                for (MediaApplication ma : activeEdge.getFromNode().getNodeApplications()) {
                    if (ma instanceof MediaApplicationProducer || ma instanceof MediaApplicationDistributor) {
                        maySend = true;
                        break;
                    }
                }
                for (MediaApplication ma : activeEdge.getToNode().getNodeApplications()) {
                    if (ma instanceof MediaApplicationDistributor || ma instanceof MediaApplicationConsumer) {
                        mayRecv = true;
                        break;
                    }
                }
                if (!(maySend && mayRecv)) {
                    MatchMaker.logger.debug("removing edge: " + activeEdge + " because it doesn't have appropriate applications on either end");
                    eliminatedMissingApplication++;
                    eliminate = true;
                }
            }

            if (eliminate) {
                networkTopology.removeLink(activeEdge);
            } else {
                newActiveLogicalEdges.add(activeEdge);
            }
        }
        activeLogicalEdges = newActiveLogicalEdges;

        long eliminatedCapacityPhysical = 0, eliminatedMissingApplicationPhysical = 0;
        double maxPhysicalCapacity = 0;
        for (PhysicalNetworkLink activeEdge : activePhysicalLinks) {
            boolean eliminate = false;
            // elimination of edges based on capacity
            // this is not needed anymore, since 
            if (matchMakerConfig.getBooleanProperty("enableElimLinkCapacityPhysical")) {
                if (activeEdge.getCapacity() < minCapacityMb) {
                    MatchMaker.logger.debug("removing edge: " + activeEdge + " because its capacity is " + activeEdge.getCapacity() + " (less than " + minCapacityMb + ")");
                    eliminatedCapacityPhysical++;
                    eliminate = true;
                }
            }

            // elimination of edges to/from nodes where no consumer, producer or media distributor is running
            // these two conditions need to be enabled-disabled simultanously
            if (matchMakerConfig.getBooleanProperty("enableElimNoappPhysical")) {
                boolean maySend = false, mayRecv = false;
                if (activeEdge.getFromNode() instanceof EndpointNetworkNode) {
                    for (MediaApplication ma : ((EndpointNetworkNode) activeEdge.getFromNode()).getNodeApplications()) {
                        if (ma instanceof MediaApplicationProducer || ma instanceof MediaApplicationDistributor) {
                            maySend = true;
                            break;
                        }
                    }
                } else {
                    maySend = true;
                }
                if (activeEdge.getToNode() instanceof EndpointNetworkNode) {
                    for (MediaApplication ma : ((EndpointNetworkNode) activeEdge.getToNode()).getNodeApplications()) {
                        if (ma instanceof MediaApplicationDistributor || ma instanceof MediaApplicationConsumer) {
                            mayRecv = true;
                            break;
                        }
                    }
                } else {
                    mayRecv = true;
                }
                if (!(maySend && mayRecv)) {
                    MatchMaker.logger.debug("removing edge: " + activeEdge + " because it doesn't have appropriate applications on either end");
                    eliminatedMissingApplicationPhysical++;
                    eliminate = true;
                }
            }

            // TODO: add configuration property for this
            // eliminate those physical links which nave none logical link assigned

            if (eliminate) {
                networkTopology.removeLink(activeEdge);
            } else {
                newActivePhysicalLinks.add(activeEdge);
                if (activeEdge.getCapacity() > maxPhysicalCapacity) {
                    maxPhysicalCapacity = activeEdge.getCapacity();
                }
            }
        }

        activePhysicalLinks = newActivePhysicalLinks;

        System.out.println("Logical edge elimination results: " + eliminatedCapacity + " for low capacity, " + eliminatedActivity + " for inactivity, "
                + eliminatedIntraSite + " as intrasite, " + eliminatedMissingApplication + " because of missing application.");
        System.out.println("Physical link elimination results: " + eliminatedCapacityPhysical + " for low capacity, " + eliminatedMissingApplicationPhysical + " because of missing application.");
        System.out.println("Left with " + activeLogicalEdges.size() + " logical edges and " + activePhysicalLinks.size() + " physical links.");

        MatchMaker.logger.debug("");
        MatchMaker.logger.debug("The following edges has been left in the optimization:");
        for (LogicalNetworkLink activeEdge : activeLogicalEdges) {
            MatchMaker.logger.debug("" + activeEdge + ", capacity " + activeEdge.getCapacity() + "Mbps, latency " + activeEdge.getLatency() + "ms.");
        }
        MatchMaker.logger.debug("");

        //3- Create variables to work with and post constraints

        // create streamlinks
        GRBVar[][] logicalStreamLinks = new GRBVar[activeLogicalEdges.size()][activeSrcApps.size()];
        GRBVar[][] physicalStreamLinks = new GRBVar[activePhysicalLinks.size()][activeSrcApps.size()];
        GRBVar[][] physicalLinkFlows = new GRBVar[activePhysicalLinks.size()][activeSrcApps.size()];
        boolean[][] logicalStreamLinksActive = new boolean[activeLogicalEdges.size()][activeSrcApps.size()];
        boolean[][] physicalStreamLinksActive = new boolean[activePhysicalLinks.size()][activeSrcApps.size()];
        int[] consumerNumbers = new int[activeSrcApps.size()];

        boolean flowsEnabled = matchMakerConfig.getBooleanProperty("enableFlows");
        if (flowsEnabled) {
            for (int j = 0; j < activeSrcApps.size(); j++) {
                consumerNumbers[j] = activeConsumers.get(activeSrcApps.get(j)).size();
            }
        }

        for (int j = 0; j < activeSrcApps.size(); j++) {
            long minBandwidth = Long.MAX_VALUE;
            for (MediaStream ms : activeSrcApps.get(j).getMediaStreams()) {
                if (ms.getBandwidth_min() < minBandwidth) {
                    minBandwidth = ms.getBandwidth_min();
                }
            }
            minBandwidth /= 1e6;
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                try {
                    logicalStreamLinks[i][j] = model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "logical[" + activeSrcApps.get(j) + "][" + activeLogicalEdges.get(i) + "]");
                } catch (GRBException ex) {
                    infoOnException(ex, "Adding variable to model failed.");
                    this.matchFound = false;
                    return;
                }
                logicalStreamLinksActive[i][j] = true;
            }
            for (int i = 0; i < activePhysicalLinks.size(); i++) {
                double upperBound = Math.floor(activePhysicalLinks.get(i).getCapacity() / minBandwidth);
                if (upperBound >= 0.0) {
                    try {
                        physicalStreamLinks[i][j] = model.addVar(0.0, upperBound, 0.0, GRB.INTEGER, "physical[" + activeSrcApps.get(j) + "][" + activePhysicalLinks.get(i) + "]");
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding variable to model failed.");
                        this.matchFound = false;
                        return;
                    }
                    physicalStreamLinksActive[i][j] = true;
                }
            }
            if (flowsEnabled) {
                for (int i = 0; i < activePhysicalLinks.size(); i++) {
                    double upperBound = Math.floor(activePhysicalLinks.get(i).getCapacity() / minBandwidth);
                    try {
                        physicalLinkFlows[i][j] = model.addVar(0.0, Math.min(upperBound, consumerNumbers[j]), 0.0, GRB.INTEGER, "flow[" + activeSrcApps.get(j) + "][" + activePhysicalLinks.get(i) + "]");
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding variable to model failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
        }
        try {
            model.update();
        } catch (GRBException ex) {
            infoOnException(ex, "Model update failed.");
            this.matchFound = false;
            return;
        }
        // for measurements purposes:
        System.out.println("Total of " + logicalStreamLinks.length * logicalStreamLinks[0].length + " logical streamlinks and " + physicalStreamLinks.length * physicalStreamLinks[0].length + " physical streamlinks created.");

        // eliminate those streamlinks that are not suitable for give application because of
        // 1) insufficient capacity
        // 2) there is no active listener for given application nor reflector on this link
        for (int j = 0; j < activeSrcApps.size(); j++) {
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                if (matchMakerConfig.getBooleanProperty("enableConstrSingleLogicalStreamLinkCapacity")) {
                    // bandwidth-based elimination
                    if (activeSrcApps.get(j).getMediaMaxBandwidth() > activeLogicalEdges.get(i).getCapacity()) {
                        MatchMaker.logger.debug("Link " + activeLogicalEdges.get(i) + " (" + activeLogicalEdges.get(i).getCapacity() + "Mbps) is not suitable for " + activeSrcApps.get(j).getApplicationName());
                        if (!setGRBVar(model, logicalStreamLinks[i][j], 0.0, null)) {
                            this.matchFound = false;
                            return;
                        }
                        logicalStreamLinksActive[i][j] = false;
                        continue;
                    }
                }

                // source/target/reflector-based elimination
                if (matchMakerConfig.getBooleanProperty("enableConstrWhoMayReceiveLogical")) {
                    boolean mayRecv = false;
                    for (MediaApplication mediaApplication : activeConsumers.get(activeSrcApps.get(j))) {
                        if (mediaApplication.getParentNode() == g.getEdgeTarget(activeLogicalEdges.get(i))) {
                            // this should be generally sufficient (TODO: why did I added the stream matching below?)
                            if (mediaStreamMatch(activeSrcApps.get(j).getMediaStreams(), mediaApplication.getMediaStreams())) {
                                mayRecv = true;
                            }
                        }
                    }
                    if (!mayRecv) {
                        // we may still receive if there is at least a reflector on the receiving side
                        for (MediaApplication mediaApplication : g.getEdgeTarget(activeLogicalEdges.get(i)).getNodeApplications()) {
                            // TODO not every reflector is a suitable one
                            if (mediaApplication instanceof MediaApplicationDistributor) {
                                mayRecv = true;
                            }
                        }
                    }
                    if (!mayRecv) {
                        MatchMaker.logger.debug("Disabling link " + activeLogicalEdges.get(i) + " for application " + activeSrcApps.get(j) + " (" + activeSrcApps.get(j).getParentNode() + ") because there is no suitable target.");
                        if (!setGRBVar(model, logicalStreamLinks[i][j], 0.0, null)) {
                            this.matchFound = false;
                            return;
                        }
                        logicalStreamLinksActive[i][j] = false;
                    }
                }
                if (matchMakerConfig.getBooleanProperty("enableConstrWhoMaySendLogical")) {
                    boolean maySend = false;
                    if (g.getEdgeSource(activeLogicalEdges.get(i)) == activeSrcApps.get(j).getParentNode()) {
                        // there is source application on sending side of the edge
                        maySend = true;
                    } else {
                        // we may still send if there is at least a reflector on the sending side
                        for (MediaApplication mediaApplication : g.getEdgeSource(activeLogicalEdges.get(i)).getNodeApplications()) {
                            // TODO not every reflector is a suitable one
                            if (mediaApplication instanceof MediaApplicationDistributor) {
                                maySend = true;
                            }
                        }
                    }
                    if (!maySend) {
                        MatchMaker.logger.debug("Disabling link " + activeLogicalEdges.get(i) + " for application " + activeSrcApps.get(j) + " (" + activeSrcApps.get(j).getParentNode() + ") because there is no suitable source.");
                        if (!setGRBVar(model, logicalStreamLinks[i][j], 0.0, null)) {
                            this.matchFound = false;
                            return;
                        }
                        logicalStreamLinksActive[i][j] = false;
                    }
                }
            }

            for (int i = 0; i < activePhysicalLinks.size(); i++) {
                // This is not needed anymore, since bounds are calculated when variables are added
                if (matchMakerConfig.getBooleanProperty("enableConstrSinglePhysicalStreamLinkCapacity")) {
                    // bandwidth-based elimination
                    if (activeSrcApps.get(j).getMediaMaxBandwidth() / 10e6 > activePhysicalLinks.get(i).getCapacity()) {
                        MatchMaker.logger.debug("Link " + activePhysicalLinks.get(i) + " (" + activePhysicalLinks.get(i).getCapacity() + "Mbps) is not suitable for " + activeSrcApps.get(j).getApplicationName());
                        if (!setGRBVar(model, physicalStreamLinks[i][j], 0.0, null) || (flowsEnabled && !setGRBVar(model, physicalLinkFlows[i][j], 0.0, null))) {
                            this.matchFound = false;
                            return;
                        }
                        physicalStreamLinksActive[i][j] = false;
                        continue;
                    }
                }

                // source/target/reflector-based elimination
                if (matchMakerConfig.getBooleanProperty("enableConstrWhoMayReceivePhysical")) {
                    if (activePhysicalLinks.get(i).getToNode() instanceof EndpointNetworkNode) {
                        boolean mayRecv = false;
                        for (MediaApplication mediaApplication : activeConsumers.get(activeSrcApps.get(j))) {
                            if (mediaApplication.getParentNode() == activePhysicalLinks.get(i).getToNode()) {
                                // this should be generally sufficient (TODO: why did I added the stream matching below?)
                                if (mediaStreamMatch(activeSrcApps.get(j).getMediaStreams(), mediaApplication.getMediaStreams())) {
                                    mayRecv = true;
                                }
                            }
                        }
                        if (!mayRecv) {
                            // we may still receive if there is at least a reflector on the receiving side
                            for (MediaApplication mediaApplication : ((EndpointNetworkNode) activePhysicalLinks.get(i).getToNode()).getNodeApplications()) {
                                // TODO not every reflector is a suitable one
                                if (mediaApplication instanceof MediaApplicationDistributor) {
                                    mayRecv = true;
                                }
                            }
                        }
                        if (!mayRecv) {
                            MatchMaker.logger.debug("Disabling link " + activePhysicalLinks.get(i) + " for application " + activeSrcApps.get(j) + " (" + activeSrcApps.get(j).getParentNode() + ") because there is no suitable target.");
                            if (!setGRBVar(model, physicalStreamLinks[i][j], 0.0, null)
                                    || (flowsEnabled && !setGRBVar(model, physicalLinkFlows[i][j], 0.0, null))) {
                                this.matchFound = false;
                                return;
                            }
                            physicalStreamLinksActive[i][j] = false;
                            continue;
                        }
                    }
                }

                if (matchMakerConfig.getBooleanProperty("enableConstrWhoMaySendPhysical")) {
                    if (activePhysicalLinks.get(i).getFromNode() instanceof EndpointNetworkNode) {
                        boolean maySend = false;
                        if (activePhysicalLinks.get(i).getFromNode() == activeSrcApps.get(j).getParentNode()) {
                            // there is source application on sending side of the edge
                            maySend = true;
                        } else {
                            // we may still send if there is at least a reflector on the sending side
                            for (MediaApplication mediaApplication : ((EndpointNetworkNode) activePhysicalLinks.get(i).getFromNode()).getNodeApplications()) {
                                // TODO not every reflector is a suitable one
                                if (mediaApplication instanceof MediaApplicationDistributor) {
                                    maySend = true;
                                }
                            }
                        }
                        if (!maySend) {
                            MatchMaker.logger.debug("Disabling link " + activePhysicalLinks.get(i) + " for application " + activeSrcApps.get(j) + " (" + activeSrcApps.get(j).getParentNode() + ") because there is no suitable source.");
                            if (!setGRBVar(model, physicalStreamLinks[i][j], 0.0, null)
                                    || (flowsEnabled && !setGRBVar(model, physicalLinkFlows[i][j], 0.0, null))) {
                                this.matchFound = false;
                                return;
                            }
                            physicalStreamLinksActive[i][j] = false;
                        }
                    }
                }

            }
        }

        // print out summary information on active streamlinks
        MatchMaker.logger.debug("");
//        MatchMaker.logger.debug("Total of " + getActiveSL(logicalStreamLinksActive) + " logical streamlinks remained in scheduling.");
//        MatchMaker.logger.debug("Total of " + getActiveSL(physicalStreamLinksActive) + " physical streamlinks remained in scheduling.");
        MatchMaker.logger.debug("");

        // sender constraints
        // 1) when more than on consumer is active, sender has to send through a reflector
        // 2) when there is only a single consumer, sender will be sending directly
        MatchMaker.logger.debug("Sender-based constraints...");
        for (int j = 0; j < activeSrcApps.size(); j++) {
            if (matchMakerConfig.getBooleanProperty("enableConstrForbidDirectLinksLogical")
                    || matchMakerConfig.getBooleanProperty("enableConstrProducerSingleOutlinkLogical")) {
                // if there is more than one consumer, data has to go through reflectors, i.e. we disable direct
                // sending to consumers
                GRBLinExpr sendingLogicalLinks = new GRBLinExpr();
                for (int i = 0; i < activeLogicalEdges.size(); i++) {
                    if (!logicalStreamLinksActive[i][j]) {
                        // ignore previously disabled streamlinks
                        continue;
                    }
                    if (g.getEdgeSource(activeLogicalEdges.get(i)) != activeSrcApps.get(j).getParentNode()) {
                        // we are only interested in edges from sender node
                        continue;
                    }
                    ArrayList<MediaApplication> consumers = activeConsumers.get(activeSrcApps.get(j));
                    if (consumers.size() > 1) { // we don't need to care for this source otherwise
                        if (matchMakerConfig.getBooleanProperty("enableConstrForbidDirectLinksLogical")) {
                            for (MediaApplication consumer : consumers) {
                                if (g.getEdgeTarget(activeLogicalEdges.get(i)) == consumer.getParentNode()) {
                                    MatchMaker.logger.debug("Disabling " + logicalStreamLinks[i][j]);
                                    if (!setGRBVar(model, logicalStreamLinks[i][j], 0.0, null)) {
                                        this.matchFound = false;
                                        return;
                                    }
                                    logicalStreamLinksActive[i][j] = false;
                                }
                            }
                        }
                        // current edge leads to a reflector
                        sendingLogicalLinks.addTerm(1.0, logicalStreamLinks[i][j]);
                    }
                }
                if (matchMakerConfig.getBooleanProperty("enableConstrProducerSingleOutlinkLogical")) {
                    if (sendingLogicalLinks.size() != 0) {
                        // sender has to send over exactly one of remaining streamlinks to some reflector
                        assert activeConsumers.get(activeSrcApps.get(j)).size() > 1;
                        MatchMaker.logger.debug("");
                        MatchMaker.logger.debug("One of these links has to be nonzero for " + activeSrcApps.get(j) + ": ");
                        MatchMaker.logger.debug("");
                        try {
                            model.addConstr(sendingLogicalLinks, GRB.EQUAL, 1.0, null);
                        } catch (GRBException ex) {
                            infoOnException(ex, "Adding constraint failed.");
                            this.matchFound = false;
                            return;
                        }
                    }
                }
            }

            int prodOut = 0;
            if (matchMakerConfig.getBooleanProperty("enableConstrProducerSingleOutlinkPhysical")) {
                GRBLinExpr sendingPhysicalLinks = new GRBLinExpr();
                GRBLinExpr sendingLinkFlows = new GRBLinExpr();
                for (int i = 0; i < activePhysicalLinks.size(); i++) {
                    if (!physicalStreamLinksActive[i][j]
                            || !(activePhysicalLinks.get(i).getFromNode() instanceof EndpointNetworkNode)
                            || ((EndpointNetworkNode) activePhysicalLinks.get(i).getFromNode() != activeSrcApps.get(j).getParentNode())) {
                        continue;
                    }
                    sendingPhysicalLinks.addTerm(1.0, physicalStreamLinks[i][j]);
                    if (flowsEnabled) {
                        sendingLinkFlows.addTerm(1.0, physicalLinkFlows[i][j]);
                    }
                }
                if (sendingPhysicalLinks.size() != 0) {
                    MatchMaker.logger.debug("");
                    MatchMaker.logger.debug("One of these links has to be nonzero for " + activeSrcApps.get(j) + ": ");
                    MatchMaker.logger.debug("");
                    try {
                        prodOut++;
                        model.addConstr(sendingPhysicalLinks, GRB.EQUAL, 1.0, null);
                        if (flowsEnabled) {
                            model.addConstr(sendingLinkFlows, GRB.EQUAL, consumerNumbers[j], null);
                        }
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
        }

        MatchMaker.logger.debug("done.");

        if (flowsEnabled) {
            // tie physical stream links and link flows together
            for (int i = 0; i < activePhysicalLinks.size(); i++) {
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    if (!physicalStreamLinksActive[i][j]) {
                        continue;
                    }
                    try {
                        model.addConstr(physicalStreamLinks[i][j], GRB.LESS_EQUAL, physicalLinkFlows[i][j], null);
                        GRBLinExpr lhs = new GRBLinExpr();
                        lhs.addTerm(consumerNumbers[j], physicalStreamLinks[i][j]);
                        model.addConstr(lhs, GRB.GREATER_EQUAL, physicalLinkFlows[i][j], null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
        }

        if (matchMakerConfig.getBooleanProperty("enableConstrPhysicalNodeSanityFlows")) {
            // flow conservation on internal physical nodes
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (PhysicalNetworkNode node : networkTopology.getPhysicalNodes()) {
                    GRBLinExpr physicalNodeOutputs = new GRBLinExpr();
                    GRBLinExpr physicalNodeInputs = new GRBLinExpr();
                    for (int i = 0; i < activePhysicalLinks.size(); i++) {
                        if (activePhysicalLinks.get(i).getFromNode() == node) {
                            physicalNodeOutputs.addTerm(1.0, physicalLinkFlows[i][j]);
                        } else if (activePhysicalLinks.get(i).getToNode() == node) {
                            physicalNodeInputs.addTerm(1.0, physicalLinkFlows[i][j]);
                        }
                    }
                    try {
                        model.addConstr(physicalNodeInputs, GRB.EQUAL, physicalNodeOutputs, null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }

            // flow conservation on internal unknown nodes
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (UnknownNetworkNode node : networkTopology.getUnknownNetworkNodes()) {
                    GRBLinExpr unknownNodeOutputs = new GRBLinExpr();
                    GRBLinExpr unknownNodeInputs = new GRBLinExpr();
                    for (int i = 0; i < activePhysicalLinks.size(); i++) {
                        if (activePhysicalLinks.get(i).getFromNode() == node) {
                            unknownNodeOutputs.addTerm(1.0, physicalLinkFlows[i][j]);
                        } else if (activePhysicalLinks.get(i).getToNode() == node) {
                            unknownNodeInputs.addTerm(1.0, physicalLinkFlows[i][j]);
                        }
                    }
                    try {
                        model.addConstr(unknownNodeInputs, GRB.EQUAL, unknownNodeOutputs, null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
        }

        if (matchMakerConfig.getBooleanProperty("enableConstrPhysicalNodeSanityStreamlinks")) {
            // flow conservation on internal physical nodes
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (PhysicalNetworkNode node : networkTopology.getPhysicalNodes()) {
                    GRBLinExpr physicalNodeOutputs = new GRBLinExpr();
                    GRBLinExpr physicalNodeInputs = new GRBLinExpr();
                    for (int i = 0; i < activePhysicalLinks.size(); i++) {
                        if (activePhysicalLinks.get(i).getFromNode() == node) {
                            physicalNodeOutputs.addTerm(1.0, physicalStreamLinks[i][j]);
                        } else if (activePhysicalLinks.get(i).getToNode() == node) {
                            physicalNodeInputs.addTerm(1.0, physicalStreamLinks[i][j]);
                        }
                    }
                    try {
                        model.addConstr(physicalNodeInputs, GRB.EQUAL, physicalNodeOutputs, null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }

            // flow conservation on internal unknown nodes
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (UnknownNetworkNode node : networkTopology.getUnknownNetworkNodes()) {
                    GRBLinExpr unknownNodeOutputs = new GRBLinExpr();
                    GRBLinExpr unknownNodeInputs = new GRBLinExpr();
                    for (int i = 0; i < activePhysicalLinks.size(); i++) {
                        if (activePhysicalLinks.get(i).getFromNode() == node) {
                            unknownNodeOutputs.addTerm(1.0, physicalStreamLinks[i][j]);
                        } else if (activePhysicalLinks.get(i).getToNode() == node) {
                            unknownNodeInputs.addTerm(1.0, physicalStreamLinks[i][j]);
                        }
                    }
                    try {
                        model.addConstr(unknownNodeInputs, GRB.EQUAL, unknownNodeOutputs, null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
        }

        // print out summary information on active streamlinks
//        MatchMaker.logger.debug("");
//        MatchMaker.logger.debug("Total of " + getActiveSL(streamLinksActive) + " streamlinks remained in scheduling.");
//        MatchMaker.logger.debug("");

        // receiver constraints
        // 1) data will be received excatly over one streamlink
        MatchMaker.logger.debug("Receiver-based constraints...");
        if (matchMakerConfig.getBooleanProperty("enableConstrConsumerSingleInlinkLogical")
                || matchMakerConfig.getBooleanProperty("enableConstrConsumerSingleInlinkPhysical")) {
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (MediaApplication consumer : activeConsumers.get(activeSrcApps.get(j))) {
                    if (matchMakerConfig.getBooleanProperty("enableConstrConsumerSingleInlinkLogical")) {
                        GRBLinExpr receivingLogicalLinks = new GRBLinExpr();
                        for (int i = 0; i < activeLogicalEdges.size(); i++) {
                            if (!logicalStreamLinksActive[i][j]) {
                                // ignore previously disabled streamlinks
                                // e.g. this eliminates direct links from sender in case there are multiple consumers
                                continue;
                            }
                            if (g.getEdgeTarget(activeLogicalEdges.get(i)) != consumer.getParentNode()) {
                                // we are only interested in edges to receiver node
                                continue;
                            }
                            receivingLogicalLinks.addTerm(1.0, logicalStreamLinks[i][j]);
                        }
                        MatchMaker.logger.debug("One of these links has to be nonzero for " + consumer + ": ");
                        try {
                            model.addConstr(receivingLogicalLinks, GRB.EQUAL, 1.0, null);
                        } catch (GRBException ex) {
                            infoOnException(ex, "Adding constraint failed.");
                            this.matchFound = false;
                            return;
                        }
                    }

                    if (matchMakerConfig.getBooleanProperty("enableConstrConsumerSingleInlinkPhysical")) {
                        GRBLinExpr receivingPhysicalLinks = new GRBLinExpr();
                        GRBLinExpr receivingLinkFlows = new GRBLinExpr();
                        for (int i = 0; i < activePhysicalLinks.size(); i++) {
                            if (!physicalStreamLinksActive[i][j]
                                    || !(activePhysicalLinks.get(i).getToNode() instanceof EndpointNetworkNode)
                                    || ((EndpointNetworkNode) activePhysicalLinks.get(i).getToNode() != consumer.getParentNode())) {
                                continue;
                            }

                            receivingPhysicalLinks.addTerm(1.0, physicalStreamLinks[i][j]);
                            if (flowsEnabled) {
                                receivingLinkFlows.addTerm(1.0, physicalLinkFlows[i][j]);
                            }
                        }
                        MatchMaker.logger.debug("One of these links has to be nonzero for " + consumer + ": ");
                        if (receivingPhysicalLinks.size() != 0) {
                            try {
                                model.addConstr(receivingPhysicalLinks, GRB.EQUAL, 1.0, null);
                                if (flowsEnabled) {
                                    model.addConstr(receivingLinkFlows, GRB.EQUAL, 1.0, null);
                                }
                            } catch (GRBException ex) {
                                infoOnException(ex, "Adding constraint failed.");
                                this.matchFound = false;
                                return;
                            }
                        }
                    }
                }
            }
        }
        MatchMaker.logger.debug("done.");

        // distribution tree constraint
        // 1) minimum number of active links (n_producers + 1 for multipoint distribution)
        // 2) maximum number of active links (n_producers + n_reflectors)
        MatchMaker.logger.debug("Distribution-tree-based constraints...");
//        { // scoping n_reflectors
//            int n_reflectors = 0;
//            for (EndpointNetworkNode node : g.vertexSet()) {
//                boolean isReflector = false;
//                for (MediaApplication app : node.getNodeApplications()) {
//                    if (app instanceof MediaApplicationDistributor) {
//                        isReflector = true;
//                    }
//                }
//                if (isReflector) {
//                    n_reflectors += 1;
//                }
//            }
//            if (matchMakerGurobiConfig.enable_constr_links_number) {
//                for (int j = 0; j < activeSrcApps.size(); j++) {
//                    GRBLinExpr sendingLinks = new GRBLinExpr();
//                    for (int i = 0; i < activeLogicalEdges.size(); i++) {
//                        if (streamLinksActive[i][j]) {
//                            sendingLinks.addTerm(1.0, streamLinks[i][j]);
//                        }
//                    }
//                    int min_value;
//                    int max_value;
//                    if (activeConsumers.get(activeSrcApps.get(j)).size() > 1) {
//                        min_value = activeConsumers.get(activeSrcApps.get(j)).size() + 1;
//                        max_value = activeConsumers.get(activeSrcApps.get(j)).size() + n_reflectors;
//                    } else {
//                        min_value = 1;
//                        max_value = n_reflectors + 1;
//                    }
//                    MatchMaker.logger.debug("");
//                    MatchMaker.logger.debug("Number of sending links for " + activeSrcApps.get(j) + " has to be in [" + min_value + ";" + max_value + "].");
//                    try {
//                        model.addConstr(sendingLinks, GRB.GREATER_EQUAL, (double) min_value, null);
//                        model.addConstr(sendingLinks, GRB.LESS_EQUAL, (double) max_value, null);
//                    } catch (GRBException ex) {
//                        infoOnException(ex, "Adding constraint failed.");
//                        this.matchFound = false;
//                        return;
//                    }
//                }
//            }
//        }

        // build list of RumHD-enabled nodes: needed in subsequent tests
        ArrayList<EndpointNetworkNode> rumHDlist = new ArrayList<EndpointNetworkNode>();
        for (EndpointNetworkNode networkNode : g.vertexSet()) {
            // if this node doesn't have any ingress edges, this constraint has no meaning
            if (g.inDegreeOf(networkNode) < 1) {
                continue;
            }
            boolean hasRumHD = false;
            for (MediaApplication mediaApplication : networkNode.getNodeApplications()) {
                if (mediaApplication instanceof RumHD) {
                    hasRumHD = true;
                }
            }
            // if this node doesn't have RumHD application, we can skip it
            if (!hasRumHD) {
                continue;
            }
            rumHDlist.add(networkNode);
        }

        // test that RumHD is not used for multiple data sources
        if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSingleInlinkLogical")) {
            // for all nodes with distributors
            for (EndpointNetworkNode networkNode : rumHDlist) {
                GRBLinExpr receivingLogicalLinks = new GRBLinExpr();
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    for (int i = 0; i < activeLogicalEdges.size(); i++) {
                        if (networkNode != activeLogicalEdges.get(i).getToNode()) {
                            continue;
                        }
                        receivingLogicalLinks.addTerm(1.0, logicalStreamLinks[i][j]);
                    }
                }

                try {
                    model.addConstr(receivingLogicalLinks, GRB.LESS_EQUAL, 1.0, null);
                } catch (GRBException ex) {
                    infoOnException(ex, "Adding constraint failed.");
                    this.matchFound = false;
                    return;
                }
            }
        }


        if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSingleInlinkPhysical")) {
            for (EndpointNetworkNode networkNode : rumHDlist) {
                GRBLinExpr receivingPhysicalLinks = new GRBLinExpr();
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    for (int i = 0; i < activePhysicalLinks.size(); i++) {
                        if (networkNode != activePhysicalLinks.get(i).getToNode()) {
                            continue;
                        }
                        receivingPhysicalLinks.addTerm(1.0, physicalStreamLinks[i][j]);
                    }
                }

                try {
                    model.addConstr(receivingPhysicalLinks, GRB.LESS_EQUAL, 1.0, null);
                } catch (GRBException ex) {
                    infoOnException(ex, "Adding constraint failed.");
                    this.matchFound = false;
                    return;
                }
            }
        }

        // RumHD constraint - there has to be at least the same number of egress streams as ingress
        // streams; for inactive reflectors, number of egress streams has to be zero
        if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityLogical")
                || matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityPhysical")) {

            MatchMaker.logger.debug("");
            MatchMaker.logger.debug("'max'-based RumHD constraints...");
            for (EndpointNetworkNode networkNode : rumHDlist) {
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    GRBLinExpr logicalReceivingLinks = new GRBLinExpr();
                    GRBLinExpr logicalSendingList = new GRBLinExpr();
                    GRBLinExpr logicalReceivingLinksCoef = new GRBLinExpr();
                    GRBLinExpr physicalReceivingLinks = new GRBLinExpr();
                    GRBLinExpr physicalReceivingLinksCoef = new GRBLinExpr();
                    GRBLinExpr physicalSendingList = new GRBLinExpr();
                    GRBLinExpr physicalReceivingFlows = new GRBLinExpr();
                    GRBLinExpr physicalSendingFlows = new GRBLinExpr();
                    if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityLogical")) {
                        for (int i = 0; i < activeLogicalEdges.size(); i++) {
                            if (!logicalStreamLinksActive[i][j]) {
                                continue;
                            }
                            if (g.getEdgeTarget(activeLogicalEdges.get(i)) == networkNode) {
                                logicalReceivingLinks.addTerm(1.0, logicalStreamLinks[i][j]);
                            } else if (g.getEdgeSource(activeLogicalEdges.get(i)) == networkNode) {
                                logicalSendingList.addTerm(1.0, logicalStreamLinks[i][j]);
                            }
                        }
                        if (logicalSendingList.size() > 0) {
                            try {
                                logicalReceivingLinksCoef.multAdd(logicalSendingList.size(), logicalReceivingLinks);
                            } catch (GRBException ex) {
                                infoOnException(ex, "Adding constraint failed.");
                                this.matchFound = false;
                                return;
                            }
                        }
                    }

                    if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityPhysical")) {
                        for (int i = 0; i < activePhysicalLinks.size(); i++) {
                            if (!physicalStreamLinksActive[i][j]) {
                                continue;
                            }
                            if (activePhysicalLinks.get(i).getToNode() == networkNode) {
                                physicalReceivingLinks.addTerm(1.0, physicalStreamLinks[i][j]);
                                if (flowsEnabled) {
                                    physicalReceivingFlows.addTerm(1.0, physicalLinkFlows[i][j]);
                                }
                            } else if (activePhysicalLinks.get(i).getFromNode() == networkNode) {
                                physicalSendingList.addTerm(1.0, physicalStreamLinks[i][j]);
                                if (flowsEnabled) {
                                    physicalSendingFlows.addTerm(1.0, physicalLinkFlows[i][j]);
                                }
                            }
                        }
                        if (physicalSendingList.size() > 0) {
                            try {
                                physicalReceivingLinksCoef.multAdd(physicalSendingList.size(), physicalReceivingLinks);
                            } catch (GRBException ex) {
                                infoOnException(ex, "Adding constraint failed.");
                                this.matchFound = false;
                                return;
                            }
                        }
                    }

                    try {
                        if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityLogical")) {
                            // There are more outlinks than inlinks
                            model.addConstr(logicalReceivingLinks, GRB.LESS_EQUAL, logicalSendingList, null);
                            // There is no output without an input
                            model.addConstr(logicalReceivingLinksCoef, GRB.GREATER_EQUAL, logicalSendingList, null);
                        }
                        if (matchMakerConfig.getBooleanProperty("enableConstrDistributorSanityPhysical")) {

                            // There are more outlinks than inlinks
                            model.addConstr(physicalReceivingLinks, GRB.LESS_EQUAL, physicalSendingList, null);
                            // There is no output without an input
                            model.addConstr(physicalReceivingLinksCoef, GRB.GREATER_EQUAL, physicalSendingList, null);
                            if (flowsEnabled) {
                                model.addConstr(physicalReceivingFlows, GRB.EQUAL, physicalSendingFlows, null);
                            }
                        }
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint or variable failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }

            MatchMaker.logger.debug("done.\n");
        }

        /**
         * This is an efficient constraint that the link capacity is not
         * exceeded by scheduled mediaStreams. This is easier to propagate the
         * more efficiently than originally implemented tuple test (both AC and
         * FC).
         */
        MatchMaker.logger.debug("Adding link capacity constraints...");
        /**
         * BWMap is an auxiliary class to maintain capacity constraints
         * per-NodeInterface (over multiple links)
         */
        class BWMap {

            HashMap<EndpointNodeInterface, ArrayList<GRBVar>> streamLinkMap;
            HashMap<EndpointNodeInterface, ArrayList<Integer>> bandwidthMap;
            HashMap<EndpointNodeInterface, ArrayList<GRBVar>> streamLinkInMap;
            HashMap<EndpointNodeInterface, ArrayList<Integer>> bandwidthInMap;
            HashMap<EndpointNodeInterface, ArrayList<GRBVar>> streamLinkOutMap;
            HashMap<EndpointNodeInterface, ArrayList<Integer>> bandwidthOutMap;

            public BWMap() {
                streamLinkMap = new HashMap<EndpointNodeInterface, ArrayList<GRBVar>>();
                bandwidthMap = new HashMap<EndpointNodeInterface, ArrayList<Integer>>();
                streamLinkInMap = new HashMap<EndpointNodeInterface, ArrayList<GRBVar>>();
                bandwidthInMap = new HashMap<EndpointNodeInterface, ArrayList<Integer>>();
                streamLinkOutMap = new HashMap<EndpointNodeInterface, ArrayList<GRBVar>>();
                bandwidthOutMap = new HashMap<EndpointNodeInterface, ArrayList<Integer>>();
            }

            /**
             * Adds a record for given NetworkLink - adds all the streamLinks
             * and required application bitrates
             * <p/>
             *
             * @param link link to work with
             * @param streamLinkList input list of streamlinks
             * @param bandwidthList input list of bandwidths (scalar
             * multiplication coefficients)
             */
            public void add(LogicalNetworkLink link, GRBVar[] streamLinkList, int[] bandwidthList) {
                this.addInternal(link.getFromInterface(), this.streamLinkOutMap, this.bandwidthOutMap, streamLinkList, bandwidthList);
                this.addInternal(link.getToInterface(), this.streamLinkInMap, this.bandwidthInMap, streamLinkList, bandwidthList);
            }

            /**
             * Adds a record for nodeInterface - typically it adds all the
             * streamLinks and required application bitrates for one NetworkLink
             * (activeEdge).
             * <p/>
             *
             * @param nodeInterface node interface to work with
             * @param streamLinkList input list of streamlinks
             * @param bandwidthList input list of bandwidths (scalar
             * multiplication coefficients)
             * @param streamLinkMap streamLinkMap to operate upon
             * @param bandwidthMap bandwidthMap to operate upon
             */
            private void addInternal(EndpointNodeInterface nodeInterface, HashMap<EndpointNodeInterface, ArrayList<GRBVar>> streamLinkMap, HashMap<EndpointNodeInterface, ArrayList<Integer>> bandwidthMap, GRBVar[] streamLinkList, int[] bandwidthList) {
                assert streamLinkList.length == bandwidthList.length : "streamLinkList and bandwidthList has to be of the same size!";
                if (!streamLinkMap.containsKey(nodeInterface)) {
                    streamLinkMap.put(nodeInterface, new ArrayList<GRBVar>());
                    assert !bandwidthMap.containsKey(nodeInterface);
                    bandwidthMap.put(nodeInterface, new ArrayList<Integer>());
                }
                assert streamLinkMap.containsKey(nodeInterface) && bandwidthMap.containsKey(nodeInterface);
                // append all the elements to the array
                for (int i = 0; i < streamLinkList.length; i++) {
                    GRBVar intVar = streamLinkList[i];
                    Integer bw = bandwidthList[i];
                    streamLinkMap.get(nodeInterface).add(intVar);
                    bandwidthMap.get(nodeInterface).add(bw);
                }
            }

            /**
             * Get all the NodeInterfaces to iterate through
             * <p/>
             *
             * @return array of NodeInterfaces stored in the BWMap object
             */
            public EndpointNodeInterface[] getInterfaces() {
                ArrayList<EndpointNodeInterface> ifaceArray = new ArrayList<EndpointNodeInterface>();
                for (EndpointNodeInterface nodeInterface : streamLinkMap.keySet()) {
                    ifaceArray.add(nodeInterface);
                }
                for (EndpointNodeInterface nodeInterface : streamLinkInMap.keySet()) {
                    assert !ifaceArray.contains(nodeInterface) : "A bug that means that the interface is both full-duplex and half-duplex.";
                    ifaceArray.add(nodeInterface);
                }
                for (EndpointNodeInterface nodeInterface : streamLinkOutMap.keySet()) {
                    if (!ifaceArray.contains(nodeInterface)) {
                        ifaceArray.add(nodeInterface);
                    }
                }
                EndpointNodeInterface[] array = new EndpointNodeInterface[ifaceArray.size()];
                array = ifaceArray.toArray(array);
                return array;
            }

            /**
             * Get all the streamLinks for given half-duplex NodeInterface
             * <p/>
             *
             * @param nodeInterface to get streamLinks for
             * @return array of streamLinks
             */
            public GRBVar[] getStreamLinks(EndpointNodeInterface nodeInterface) {
//                assert !nodeInterface.isFullDuplex() : "When calling this method, the interface must not be full duplex.";
                return getStreamLinksInternal(nodeInterface, this.streamLinkMap);
            }

            /**
             * Get all the ingress streamLinks for given full-duplex
             * NodeInterface
             * <p/>
             *
             * @param nodeInterface to get streamLinks for
             * @return array of streamLinks
             */
            public GRBVar[] getStreamInLinks(EndpointNodeInterface nodeInterface) {
//                assert nodeInterface.isFullDuplex() : "When calling this method, the interface must be full duplex.";
                return getStreamLinksInternal(nodeInterface, this.streamLinkInMap);
            }

            /**
             * Get all the egress streamLinks for given full-duplex
             * NodeInterface
             * <p/>
             *
             * @param nodeInterface to get streamLinks for
             * @return array of streamLinks
             */
            public GRBVar[] getStreamOutLinks(EndpointNodeInterface nodeInterface) {
//                assert nodeInterface.isFullDuplex() : "When calling this method, the interface must be full duplex.";
                return getStreamLinksInternal(nodeInterface, this.streamLinkOutMap);
            }

            private GRBVar[] getStreamLinksInternal(EndpointNodeInterface nodeInterface, HashMap<EndpointNodeInterface, ArrayList<GRBVar>> streamLinkMap) {
                if (!streamLinkMap.containsKey(nodeInterface)) {
                    return null;
                }
                GRBVar[] array = new GRBVar[streamLinkMap.get(nodeInterface).size()];
                array = streamLinkMap.get(nodeInterface).toArray(array);
                return array;
            }

            /**
             * Get all the bandwidth requirements for given half-duplex
             * NodeInterface
             * <p/>
             *
             * @param nodeInterface to get the array for
             * @return array of bandwidth requirements
             */
            public int[] getBandwidthArray(EndpointNodeInterface nodeInterface) {
//                assert !nodeInterface.isFullDuplex() : "When calling this method, the interface must not be full duplex.";
                return getBandwidthArrayInternal(nodeInterface, this.bandwidthMap);
            }

            /**
             * Get all the bandwidth requirements for given half-duplex
             * NodeInterface
             * <p/>
             *
             * @param nodeInterface to get the array for
             * @return array of bandwidth requirements
             */
            public int[] getBandwidthInArray(EndpointNodeInterface nodeInterface) {
//                assert nodeInterface.isFullDuplex() : "When calling this method, the interface must be full duplex.";
                return getBandwidthArrayInternal(nodeInterface, this.bandwidthInMap);
            }

            /**
             * Get all the bandwidth requirements for given half-duplex
             * NodeInterface
             * <p/>
             *
             * @param nodeInterface to get the array for
             * @return array of bandwidth requirements
             */
            public int[] getBandwidthOutArray(EndpointNodeInterface nodeInterface) {
//                assert nodeInterface.isFullDuplex() : "When calling this method, the interface must be full duplex.";
                return getBandwidthArrayInternal(nodeInterface, this.bandwidthOutMap);
            }

            public int[] getBandwidthArrayInternal(EndpointNodeInterface nodeInterface, HashMap<EndpointNodeInterface, ArrayList<Integer>> bandwidthMap) {
                if (!bandwidthMap.containsKey(nodeInterface)) {
                    return null;
                }
                int[] array = new int[bandwidthMap.get(nodeInterface).size()];
                for (int i = 0; i < bandwidthMap.get(nodeInterface).size(); i++) {
                    array[i] = bandwidthMap.get(nodeInterface).get(i);
                }
                return array;
            }
        }
        BWMap ifaceConstraint = new BWMap();
        if (matchMakerConfig.getBooleanProperty("enableConstrLinkTotalCapacityLogical")) {
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                GRBLinExpr streamBws = new GRBLinExpr();
                GRBVar[] tmplist = new GRBVar[activeSrcApps.size()];
                System.arraycopy(logicalStreamLinks[i], 0, tmplist, 0, activeSrcApps.size());
                int[] mullist = new int[activeSrcApps.size()];
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    double sum = 0.0;
                    for (MediaStream mediaStream : activeSrcApps.get(j).getMediaStreams()) {
                        // we assume that MediaStream is specified in bps, so we divide by 1M
                        sum += mediaStream.getBandwidth_max() / 1000000;
                    }
                    mullist[j] = (int) Math.ceil(sum);
                    streamBws.addTerm(Math.ceil(sum), logicalStreamLinks[i][j]);
                }
                ifaceConstraint.add(activeLogicalEdges.get(i), tmplist, mullist);
                // the getCapacity returns current measured (in real-time) capacity of the edge
                try {
                    model.addConstr(streamBws, GRB.LESS_EQUAL, Math.floor(activeLogicalEdges.get(i).getCapacity()), null);
                } catch (GRBException ex) {
                    infoOnException(ex, "Adding constraint failed.");
                    this.matchFound = false;
                    return;
                }
            }
        }
        MatchMaker.logger.debug("done");
        // we must not exceed capacity of each interface, though aggregated over multiple links going from
        // that interface (remember, links are not actual topology, so that there are p2p links going to all the
        // reachable nodes in given subnetwork
        if (matchMakerConfig.getBooleanProperty("enableConstrIfaceCapacity")) {
            MatchMaker.logger.debug("Adding interface capacity constraints...");
            for (EndpointNodeInterface nodeInterface : ifaceConstraint.getInterfaces()) {
//                if (!nodeInterface.isFullDuplex()) {
//                    assert ifaceConstraint.getStreamLinks(nodeInterface) != null : "This is supposed to be non-null";
//                    GRBLinExpr lh = new GRBLinExpr();
//                    for (int i = 0; i < ifaceConstraint.getStreamLinks(nodeInterface).length; i++) {
//                        lh.addTerm((double) ifaceConstraint.getBandwidthArray(nodeInterface)[i], ifaceConstraint.getStreamLinks(nodeInterface)[i]);
//                    }
//                    try {
//                        model.addConstr(lh, GRB.LESS_EQUAL, Math.floor(nodeInterface.getBandwidth()), null);
//                    } catch (GRBException ex) {
//                        infoOnException(ex, "Adding constraint failed.");
//                        this.matchFound = false;
//                        return;
//                    }
//                    MatchMaker.logger.debug("   Constraint list (bidi) for interface " + nodeInterface);
//                    for (int i = 0; i < ifaceConstraint.getStreamLinks(nodeInterface).length; i++) {
//                        GRBVar intVar = ifaceConstraint.getStreamLinks(nodeInterface)[i];
//                        int bw = ifaceConstraint.getBandwidthArray(nodeInterface)[i];
//                        MatchMaker.logger.debug("      " + intVar + " with factor " + bw);
//                    }
//                } else {
                assert ifaceConstraint.getStreamInLinks(nodeInterface) != null || ifaceConstraint.getStreamOutLinks(nodeInterface) != null : "Either ingress or egress (or both) streamlinks have to be non-null.";
                if (ifaceConstraint.getStreamInLinks(nodeInterface) != null) {
                    GRBLinExpr lh = new GRBLinExpr();
                    for (int i = 0; i < ifaceConstraint.getStreamInLinks(nodeInterface).length; i++) {
                        lh.addTerm(ifaceConstraint.getBandwidthInArray(nodeInterface)[i], ifaceConstraint.getStreamInLinks(nodeInterface)[i]);
                    }
                    try {
                        model.addConstr(lh, GRB.LESS_EQUAL, Math.floor(nodeInterface.getBandwidth()), null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                    MatchMaker.logger.debug("   Constraint list (in) for interface " + nodeInterface);
                    for (int i = 0; i < ifaceConstraint.getStreamInLinks(nodeInterface).length; i++) {
                        GRBVar intVar = ifaceConstraint.getStreamInLinks(nodeInterface)[i];
                        int bw = ifaceConstraint.getBandwidthInArray(nodeInterface)[i];
                        MatchMaker.logger.debug("      " + intVar + " with factor " + bw);
                    }
                }
                if (ifaceConstraint.getStreamOutLinks(nodeInterface) != null) {
                    GRBLinExpr lh = new GRBLinExpr();
                    for (int i = 0; i < ifaceConstraint.getStreamOutLinks(nodeInterface).length; i++) {
                        lh.addTerm(ifaceConstraint.getBandwidthOutArray(nodeInterface)[i], ifaceConstraint.getStreamOutLinks(nodeInterface)[i]);
                    }
                    try {
                        model.addConstr(lh, GRB.LESS_EQUAL, Math.floor(nodeInterface.getBandwidth()), null);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint failed.");
                        this.matchFound = false;
                        return;
                    }
                    MatchMaker.logger.debug("   Constraint list (out) for interface " + nodeInterface);
                    if (MatchMaker.logger.isDebugEnabled()) {
                        for (int i = 0; i < ifaceConstraint.getStreamOutLinks(nodeInterface).length; i++) {
                            GRBVar intVar = ifaceConstraint.getStreamOutLinks(nodeInterface)[i];
                            int bw = ifaceConstraint.getBandwidthOutArray(nodeInterface)[i];
                            MatchMaker.logger.debug("      " + intVar + " with factor " + bw);
                        }
                    }
                }
//                }
            }
            MatchMaker.logger.debug("done");
        }

        if (matchMakerConfig.getBooleanProperty("enableConstrLinkTotalCapacityPhysical")) {
            for (int i = 0; i < activePhysicalLinks.size(); i++) {
                GRBLinExpr streamBws = new GRBLinExpr();
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    double sum = 0.0;
                    for (MediaStream mediaStream : activeSrcApps.get(j).getMediaStreams()) {
                        // we assume that MediaStream is specified in bps, so divide by 1M
                        sum += mediaStream.getBandwidth_max() / 10e6;
                    }
                    streamBws.addTerm(Math.ceil(sum), physicalStreamLinks[i][j]);
                }
                // the getCapacity returns current measured (in real-time) capacity of the edge
                try {
                    model.addConstr(streamBws, GRB.LESS_EQUAL, Math.floor(activePhysicalLinks.get(i).getCapacity()), null);
                } catch (GRBException ex) {
                    infoOnException(ex, "Adding constraint failed.");
                    this.matchFound = false;
                    return;
                }
            }
        }
        MatchMaker.logger.debug("done");

        if (matchMakerConfig.getBooleanProperty("enableConstrTieLogicalPhysical")) {
            for (int j = 0; j < activeSrcApps.size(); j++) {
                for (int i = 0; i < activePhysicalLinks.size(); i++) {
                    GRBLinExpr logicalSum = new GRBLinExpr();
                    for (LogicalNetworkLink logical : activePhysicalLinks.get(i).getTraversingLogicalLinks()) {
                        int logicalIndex = activeLogicalEdges.indexOf(logical);
                        if (logicalIndex != -1) {
                            logicalSum.addTerm(1.0, logicalStreamLinks[logicalIndex][j]);
                        }
                    }
                    if (logicalSum.size() > 0) {
                        try {
                            model.addConstr(logicalSum, GRB.EQUAL, physicalStreamLinks[i][j], null);
                        } catch (GRBException ex) {
                            infoOnException(ex, "Adding constraint failed.");
                            this.matchFound = false;
                            return;
                        }
                    }
                }
            }
        }

        if (matchMakerConfig.getBooleanProperty("enableDistributorCycleAvoidance")) {
            int[] indices;
            class NodeNodeStreamIndex {

                EndpointNetworkNode fromNode, toNode;
                Integer streamLink;

                NodeNodeStreamIndex(EndpointNetworkNode fromNode, EndpointNetworkNode toNode, Integer streamLink) {
                    this.fromNode = fromNode;
                    this.toNode = toNode;
                    this.streamLink = streamLink;
                }

                @Override
                public boolean equals(Object o) {
                    if (this == o) {
                        return true;
                    }
                    if (o == null || getClass() != o.getClass()) {
                        return false;
                    }

                    NodeNodeStreamIndex that = (NodeNodeStreamIndex) o;

                    if (!fromNode.equals(that.fromNode)) {
                        return false;
                    }
                    if (!streamLink.equals(that.streamLink)) {
                        return false;
                    }
                    if (!toNode.equals(that.toNode)) {
                        return false;
                    }

                    return true;
                }

                @Override
                public int hashCode() {
                    int result = fromNode.hashCode();
                    result = 31 * result + toNode.hashCode();
                    result = 31 * result + streamLink.hashCode();
                    return result;
                }
            }
            class StreamLinkIndex {

                HashMap<NodeNodeStreamIndex, GRBVarList> streamLinkMap = new HashMap<NodeNodeStreamIndex, GRBVarList>();

                StreamLinkIndex(GRBVar[][] streamLinks, boolean[][] streamLinksActive) {
                    for (int i = 0; i < activeLogicalEdges.size(); i++) {
                        for (int j = 0; j < activeSrcApps.size(); j++) {
                            NodeNodeStreamIndex nnsi = new NodeNodeStreamIndex(activeLogicalEdges.get(i).getFromNode(), activeLogicalEdges.get(i).getToNode(), j);
                            if (!streamLinkMap.containsKey(nnsi)) {
                                streamLinkMap.put(nnsi, new GRBVarList());
                            }
                            // now we add it to the list
                            if (streamLinksActive[i][j]) {
                                streamLinkMap.get(nnsi).add(streamLinks[i][j]);
                            }
                        }
                    }
                }

                GRBVarList getStreamLinks(EndpointNetworkNode fromNode, EndpointNetworkNode toNode, int streamIndex) {
                    NodeNodeStreamIndex nnsi = new NodeNodeStreamIndex(fromNode, toNode, streamIndex);
                    return streamLinkMap.get(nnsi);
                }
            }

            StreamLinkIndex sli = new StreamLinkIndex(logicalStreamLinks, logicalStreamLinksActive);

            long numberOfConstraints = 0;
            for (int i = 2; i <= rumHDlist.size(); i++) {
                CombinationGenerator cg = new CombinationGenerator(rumHDlist.size(), i);
                System.out.println("Combinations: " + cg.getTotal());
                while (cg.hasMore()) {
                    indices = cg.getNext();
                    for (int s = 0; s < activeSrcApps.size(); s++) {
                        GRBVarList linkList = new GRBVarList();
                        for (int j = 0; j < indices.length; j++) {
                            for (int k = 0; k < indices.length; k++) {
                                if (k != j) {
                                    linkList.addAll(sli.getStreamLinks(rumHDlist.get(indices[j]), rumHDlist.get(indices[k]), s));
                                }
                            }
                        }

                        if (linkList.size() >= i - 1) {
                            /*
                             System.out.print("Posting constraint for s=" + s + ":");
                             for (int j = 0; j < indices.length; j++) {
                             System.out.print(" " + rumHDlist.get(indices[j]).toString());
                             }
                             System.out.println(" - maximum number of " + (i-1) + " streamlinks out of " + linkList.size() + " can be active.");
                             */

                            GRBLinExpr lh = new GRBLinExpr();
                            for (GRBVar v : linkList) {
                                lh.addTerm(1.0, v);
                            }
                            try {
                                model.addConstr(lh, GRB.LESS_EQUAL, i - 1, null);
                                numberOfConstraints++;
                            } catch (GRBException ex) {
                                infoOnException(ex, "Adding constraint or variable failed.");
                                this.matchFound = false;
                                return;
                            }
                        }
                    }
                }
            }
            System.out.println("Made " + numberOfConstraints + " constraints to eliminate cycles among reflectors.");
        }

        if (matchMakerConfig.getBooleanProperty("enableDistanceBasedCycleAvoidance")) {
            // create maximum length of distribution path
            int n_reflectors = streamReflectors.size();
            // this differs from the one proposed by the reviewer
            int max_path_dist = n_reflectors + 1;

            // create list of all nodes based on active edges
            HashSet<EndpointNetworkNode> activeNodeSet = new HashSet<EndpointNetworkNode>();
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                activeNodeSet.add(activeLogicalEdges.get(i).getFromNode());
                activeNodeSet.add(activeLogicalEdges.get(i).getToNode());
            }
            // make order of nodes fixed
            ArrayList<EndpointNetworkNode> activeNodeList = new ArrayList<EndpointNetworkNode>(activeNodeSet);

            // create dist variables
            GRBVar[][] dists = new GRBVar[activeNodeList.size()][activeSrcApps.size()];
            for (int i = 0; i < activeNodeList.size(); i++) {
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    try {
                        dists[i][j] = model.addVar(1, max_path_dist + 1, 0, GRB.INTEGER, null);
                        // is this necessary for bounded int var or is this implicit?
                    } catch (GRBException ex) {
                        infoOnException(ex, "Adding constraint or variable failed.");
                        this.matchFound = false;
                        return;
                    }
                }
            }
            try {
                model.update();
            } catch (GRBException ex) {
                infoOnException(ex, "Updating the model failed.");
                this.matchFound = false;
                return;
            }

            try {
                for (int i = 0; i < activeSrcApps.size(); i++) {
                    EndpointNetworkNode appNode = activeSrcApps.get(i).getParentNode();
                    int appNodeIndex = activeNodeList.indexOf(appNode);
                    model.addConstr(dists[appNodeIndex][i], GRB.EQUAL, 1, null);
                }
            } catch (GRBException ex) {
                infoOnException(ex, "Adding distance-based cycle avoidance constraints failed.");
                this.matchFound = false;
                return;
            }

            try {
                for (int i = 0; i < activeLogicalEdges.size(); i++) {
                    for (int j = 0; j < activeSrcApps.size(); j++) {
                        EndpointNetworkNode fromNode = activeLogicalEdges.get(i).getFromNode();
                        EndpointNetworkNode toNode = activeLogicalEdges.get(i).getToNode();
                        if (activeSrcApps.get(j).getParentNode() == fromNode
                                || activeSrcApps.get(j).getParentNode() == toNode) {
                            continue;
                        }
                        int fromNodeIndex = activeNodeList.indexOf(fromNode);
                        int toNodeIndex = activeNodeList.indexOf(toNode);
                        GRBLinExpr lh = new GRBLinExpr();
                        lh.addTerm(1.0, dists[fromNodeIndex][j]);
                        lh.addTerm(-1.0, dists[toNodeIndex][j]);
                        lh.addTerm(max_path_dist - 1.0, logicalStreamLinks[i][j]);
                        model.addConstr(lh, GRB.LESS_EQUAL, max_path_dist - 2.0, null);
                    }
                }
            } catch (GRBException ex) {
                infoOnException(ex, "Adding distance-based cycle avoidance constraints failed.");
                this.matchFound = false;
                return;
            }
        }

        ///TODO: Tuples test used to be here in Choco-based solver. What shall we do instead?

        //4- Minimization criteria
        // TODO: this needs to get reimplemented as scalar multiplication of active links and their latencies (but how to fit it in some reasonable int range????)
        { // local scoping slList below
            if (matchMakerConfig.getBooleanProperty("enableObjectiveFunc")) {
                MatchMaker.logger.debug("Adding minimization criteria...");
                ArrayList<GRBVar> objVarList = new ArrayList<GRBVar>();
                ArrayList<Double> latencyList = new ArrayList<Double>();
                double maxLatency = 0;
                //for all edges
                for (int i = 0; i < activeLogicalEdges.size(); i++) {
                    assert activeLogicalEdges.get(i).getLatency() > 0 : "Latency has to be greater than zero";
                    //and for all data sources
                    for (int j = 0; j < activeSrcApps.size(); j++) {
                        if (!logicalStreamLinksActive[i][j]) {
                            continue;
                        }
                        //add latency to the list
                        latencyList.add(activeLogicalEdges.get(i).getLatency());
                        //add link to the list
                        objVarList.add(logicalStreamLinks[i][j]);
                        //get maximum latency
                        maxLatency = (activeLogicalEdges.get(i).getLatency() > maxLatency) ? activeLogicalEdges.get(i).getLatency() : maxLatency;
                    }
                }
                int[] normLatencyList = new int[latencyList.size()];
                for (int l = 0; l < objVarList.size(); l++) {
                    // convert to [1,11] integer range
                    normLatencyList[l] = (int) (Math.round(10.0 * latencyList.get(l) / maxLatency) + 1);
                    try {
                        objVarList.get(l).set(GRB.DoubleAttr.Obj, (double) normLatencyList[l]);
                    } catch (GRBException ex) {
                        infoOnException(ex, "Setting variable parameter failed");
                        this.matchFound = false;
                        return;
                    }
                }
                MatchMaker.logger.debug("done.");
            }
        }

        // for measurements purposes:
        System.out.println("Total of " + getActiveSL(logicalStreamLinksActive) + " logical streamlinks not fixed to 0 before propagation.");

        solvingStartTime = System.currentTimeMillis();

        if (matchMakerConfig.getBooleanProperty("enableWrite")) {
            try {
                model.update();
                model.write((String) matchMakerConfig.getProperty("writeDir") + networkTopology.getName() + (String) matchMakerConfig.getProperty("writeType"));
            } catch (GRBException ex) {
                infoOnException(ex, "update+write() call failed");
                /* Stop - there is some problem. */
                this.matchFound = false;
                return;
            }
        }

        if (matchMakerConfig.getBooleanProperty("enableCallbackCycleAvoidance") && !flowsEnabled) {
            class cycleCallback extends GRBCallback {

                private GRBVar[][] logicalStreamLinks;
                DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink> g;
                ArrayList<LogicalNetworkLink> links;
                ArrayList<MediaApplication> sources;

                cycleCallback(GRBVar[][] logicalStreamLinks, DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink> g, ArrayList<LogicalNetworkLink> links, ArrayList<MediaApplication> sources) {
                    this.logicalStreamLinks = logicalStreamLinks;
                    this.g = g;
                    this.links = links;
                    this.sources = sources;
                }

                @Override
                protected void callback() {
                    if (where == GRB.CB_MIPSOL) {
                        try {
                            double[][] values = getSolution(logicalStreamLinks);
                            for (int j = 0; j < sources.size(); j++) {
                                DirectedWeightedSubgraph<EndpointNetworkNode, LogicalNetworkLink> streamGraph;
                                streamGraph = new DirectedWeightedSubgraph<EndpointNetworkNode, LogicalNetworkLink>(
                                        this.g, g.vertexSet(), new HashSet<LogicalNetworkLink>());
                                for (int i = 0; i < links.size(); i++) {
                                    if (values[i][j] > 0.5) {
                                        streamGraph.addEdge(this.links.get(i).getFromNode(), this.links.get(i).getToNode());
                                    }
                                }
                                StrongConnectivityInspector<EndpointNetworkNode, LogicalNetworkLink> inspector;
                                inspector = new StrongConnectivityInspector<EndpointNetworkNode, LogicalNetworkLink>(streamGraph);
                                java.util.List<Set<EndpointNetworkNode>> components;
                                components = inspector.stronglyConnectedSets();
                                java.util.List<Set<EndpointNetworkNode>> cycles = new ArrayList<Set<EndpointNetworkNode>>();
                                ArrayList<GRBLinExpr> cycleRemovalConstraints = new ArrayList<GRBLinExpr>();
                                for (Set<EndpointNetworkNode> component : components) {
                                    if (component.size() > 1) {
                                        cycles.add(component);
                                        cycleRemovalConstraints.add(new GRBLinExpr());
                                    }
                                }
                                if (!cycles.isEmpty()) {
                                    Set<EndpointNetworkNode> cycle;
                                    for (int i = 0; i < links.size(); i++) {
                                        if (values[i][j] > 0.5) {
                                            for (int c = 0; c < cycles.size(); c++) {
                                                cycle = cycles.get(c);
                                                if (cycle.contains(links.get(i).getToNode())
                                                        && cycle.contains(links.get(i).getFromNode())) {
                                                    cycleRemovalConstraints.get(c).addTerm(1.0, logicalStreamLinks[i][j]);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    for (int c = 0; c < cycles.size(); c++) {
                                        addLazy(cycleRemovalConstraints.get(c), GRB.LESS_EQUAL, cycleRemovalConstraints.get(c).size() - 1);
                                    }
                                }
                            }
                        } catch (GRBException ex) {
                            infoOnException(ex, "Gurobi error in callback.");
                        }
                    }
                }
            }

            model.setCallback(new cycleCallback(logicalStreamLinks, g, activeLogicalEdges, activeSrcApps));
            try {
                // We need to disable duar reductions to allow adding of lazy constraints (as of Gurobi 5.0.1)
                env.set(GRB.IntParam.DualReductions, 0);
            } catch (GRBException ex) {
                infoOnException(ex, "Can't set Gurobi enfironment parameter (dual reductions).");
                this.matchFound = false;
                return;
            }
        }

        //5- Search for a solution
        MatchMaker.logger.debug("Optimization started at: " + new Date().toString());
        if (matchMakerConfig.getBooleanProperty("enableOptimize")) {
            try {
                model.optimize();
            } catch (GRBException ex) {
                infoOnException(ex, "optimize() call failed.");
                this.matchFound = false;
                return;
            }
        }

        solvingStopTime = System.currentTimeMillis();

        System.out.println("Preparation time: " + (solvingStartTime - preparationStartTime));
        System.out.println("Solving time: " + (solvingStopTime - solvingStartTime));

        // If we didn't optimize at all, return gracefully, so testing goes on. Not
        // very nice hack, yet works for the purpose
        if (!matchMakerConfig.getBooleanProperty("enableOptimize")) {
            this.matchFound = true;
            return;
        }

        int isFeasible;
        try {
            isFeasible = model.get(GRB.IntAttr.Status);
        } catch (GRBException ex) {
            infoOnException(ex, "Getting model status failed.");
            this.matchFound = false;
            return;
        }
        if (isFeasible == GRB.OPTIMAL) {
            this.matchFound = true;
        }
        if (!this.matchFound) {
            MatchMaker.logger.info("\nThe problem found not feasible!");
            return;
        }

        // price of the solution
        {
            ArrayList<Double> latencyList = new ArrayList<Double>();
            double maxLatency = 0;
            for (int i = 0; i < activeLogicalEdges.size(); i++) {
                assert activeLogicalEdges.get(i).getLatency() > 0 : "Latency has to be greater than zero";
                for (int j = 0; j < activeSrcApps.size(); j++) {
                    try {
                        if (logicalStreamLinks[i][j].get(GRB.DoubleAttr.X) == 1.0) {
                            latencyList.add(activeLogicalEdges.get(i).getLatency());
                            maxLatency = (activeLogicalEdges.get(i).getLatency() > maxLatency) ? activeLogicalEdges.get(i).getLatency() : maxLatency;
                        }
                    } catch (GRBException ex) {
                        infoOnException(ex, "Getting variable value failed");
                        this.matchFound = false;
                        return;
                    }
                }
            }
            int[] normLatencyList = new int[latencyList.size()];
            for (int l = 0; l < latencyList.size(); l++) {
                // convert to [1,11] integer range
                normLatencyList[l] = (int) (Math.round(10.0 * latencyList.get(l) / maxLatency) + 1);
            }
            // k * 11, because we use [1,11] latency range for k variables {0,1}
            long overallLatency = 0;
            for (int i : normLatencyList) {
                overallLatency += i;
            }
            System.out.println("Overall price of the first solution: " + overallLatency);
        }


        //6- Copy the resulting streamlinks into some more persistant storage
        resultStreamLinks = new int[activeLogicalEdges.size()][activeSrcApps.size()];
        resultLogicalLinks = new ArrayList<LogicalNetworkLink>();
        for (int i = 0;
                i < activeLogicalEdges.size(); i++) {
            resultLogicalLinks.add(activeLogicalEdges.get(i));
            for (int j = 0; j < activeSrcApps.size(); j++) {
                try {
                    resultStreamLinks[i][j] = (int) logicalStreamLinks[i][j].get(GRB.DoubleAttr.X);
                } catch (GRBException ex) {
                    infoOnException(ex, "Getting variable value failed.");
                    this.matchFound = false;
                    return;
                }
            }
        }

        resultProducers = new ArrayList<MediaApplication>();
        for (int i = 0;
                i < activeSrcApps.size(); i++) {
            resultProducers.add(i, activeSrcApps.get(i));
        }

        //7- Print the number of solution found

        int solutionsCount = 0;
        try {
            if (env.get(GRB.IntParam.SolutionLimit) != 1) {
                solutionsCount = model.get(GRB.IntAttr.SolCount);
                System.out.println("Found " + solutionsCount + " solution(s).");
            }
        } catch (GRBException ex) {
            infoOnException(ex, "Getting parameter or attribute value failed.");
            this.matchFound = false;
            return;
        }
        int solNo = 1;
        do {
            MatchMaker.logger.info("--------- The master plan " + solNo + " ---------");
            MatchMaker.logger.info("");
            for (int j = 0; j < activeSrcApps.size(); j++) {
                MatchMaker.logger.info("Plan for application " + activeSrcApps.get(j).getApplicationName()
                        + " running on node " + activeSrcApps.get(j).getParentNode() + " ("
                        + activeSrcApps.get(j) + ")");
                try {
                    for (int i = 0; i < activeLogicalEdges.size(); i++) {
                        GRBVar var = logicalStreamLinks[i][j];
                        if (var.get(GRB.DoubleAttr.X) == 1) {
                            MatchMaker.logger.info("link " + g.getEdgeSource(activeLogicalEdges.get(i)) + " ("
                                    + activeLogicalEdges.get(i).getFromInterface().getIpAddress() + ") --> "
                                    + g.getEdgeTarget(activeLogicalEdges.get(i)) + " ("
                                    + activeLogicalEdges.get(i).getToInterface().getIpAddress()
                                    + "):   scheduled " + var.get(GRB.DoubleAttr.X) + " stream");
                        }
                    }
                } catch (GRBException ex) {
                    infoOnException(ex, "Getting variable value failed.");
                    this.matchFound = false;
                    return;
                }
                MatchMaker.logger.info("other links scheduled to 0 for this application.");
                MatchMaker.logger.info("");
            }
            MatchMaker.logger.info("--------- End of he master plan " + solNo + " ---------");
            MatchMaker.logger.info("");
            solNo++;
        } while (false);

        MatchMaker.logger.info("");
        MatchMaker.logger.info("Found " + solutionsCount + " solution(s).");
        MatchMaker.logger.info("");

    }

    private int getActiveSL(boolean[][] streamLinksActive) {
        int activeSL = 0;
        for (int i = 0; i < activeLogicalEdges.size(); i++) {
            for (int j = 0; j < activeSrcApps.size(); j++) {
                if (streamLinksActive[i][j]) {
                    activeSL++;
                }
            }
        }
        return activeSL;
    }

    /**
     * Fix the value of variable v to value i.
     *
     * @param v variable to be fixed
     * @param i fixed value for the variable
     * @return True if everything was ok, false otherwise
     */
    private boolean setGRBVar(GRBModel m, GRBVar v, double i, String rowId) {
        try {
            v.set(GRB.DoubleAttr.LB, i);
            v.set(GRB.DoubleAttr.UB, i);
        } catch (GRBException ex) {
            infoOnException(ex, "Setting variable value failed.");
            return false;
        }
        return true;
    }

    static private void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace(System.err);
        }
        logger.error(s);
        System.out.println(e.getMessage());
        if (e instanceof GRBException) {
            System.out.println(e.getMessage());
        }
    }
}
