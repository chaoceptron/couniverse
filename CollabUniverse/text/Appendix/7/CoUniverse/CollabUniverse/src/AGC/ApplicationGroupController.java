package AGC;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationParametersFactory;
import MediaApplications.*;
import Monitoring.LambdaMonitor;
import NetworkRepresentation.*;
import myGUI.AGCDialog;
import myJXTA.MessageType;
import myJXTA.MyJXTAConnector;
import myJXTA.MyJXTAUtils;
import net.jxta.document.AdvertisementFactory;
import net.jxta.id.IDFactory;
import net.jxta.pipe.InputPipe;
import net.jxta.pipe.OutputPipe;
import net.jxta.pipe.PipeID;
import net.jxta.pipe.PipeService;
import net.jxta.platform.ModuleClassID;
import net.jxta.platform.ModuleSpecID;
import net.jxta.protocol.ModuleClassAdvertisement;
import net.jxta.protocol.ModuleSpecAdvertisement;
import net.jxta.protocol.PipeAdvertisement;
import org.apache.log4j.Logger;
import utils.TimeUtils;

import javax.swing.*;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Application Group Controller running in a thread
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 17:55:24
 */
public class ApplicationGroupController extends Thread {

    private MyJXTAConnector myJXTAConnector;

    private PipeService pipeService;

    private ModuleClassAdvertisement agcMcAdv = null;
    private ModuleSpecAdvertisement agcMdAdv = null;
    private ModuleClassID agcMcID = null;
    private PipeAdvertisement agcPipeAdv = null;

    private Thread agcPlanThread;
    private InputPipe agcInputPipe;

    private final PartiallyKnownNetworkTopology networkTopology;

    private AtomicBoolean terminateFlag;

    private LambdaMonitor lambdaMonitor;

    static Logger logger = Logger.getLogger(ApplicationGroupController.class);

    /**
     * AGC constructor
     * <p/>
     *
     * @param myJXTAConnector JXTA connector to operate upon
     */
    public ApplicationGroupController(MyJXTAConnector myJXTAConnector) {
        this.pipeService = null;
        terminateFlag = new AtomicBoolean(false);
        networkTopology = new PartiallyKnownNetworkTopology();
        this.myJXTAConnector = myJXTAConnector;
        lambdaMonitor = new LambdaMonitor();
    }

    private void registerJXTAConnectorCallbacks() {
        // register processing callbacks
        this.myJXTAConnector.registerMessageListener(agcInputPipe, MessageType.NODE_UNREACHABLE_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                synchronized (networkTopology) {
                    ApplicationGroupController.logger.info("NetworkNode Unreachable via NetworkLink message received.");
                    GeneralNetworkLink inactiveNetworkLink;
                    try {
                        inactiveNetworkLink = (GeneralNetworkLink) objects[0];
                        // Set the link (linkFromNode, linkToNode) where linkToNode has interface with remote IP inactive
                        ApplicationGroupController.logger.info("Setting link " + inactiveNetworkLink + " inactive.");
                        /* TODO: inactivate both physical and logical links */
                        networkTopology.switchNetworkLinkDown(inactiveNetworkLink);
                    } catch (Exception e) {
                        infoOnException(e, "Failed to decode incoming Message.");
                    }
                    sendNetworkTopology();
                }
            }
        });
        
        this.myJXTAConnector.registerMessageListener(agcInputPipe, MessageType.TOPOLOGY_DESCRIPTION_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                synchronized (networkTopology) {
                    ApplicationGroupController.logger.info("Network update messege received.");
                    networkTopology.updateFromXML((String) objects[0]);                    
                }
            }
        });

        this.myJXTAConnector.registerMessageListener(agcInputPipe, MessageType.NODE_REACHABLE_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                synchronized (networkTopology) {
                    ApplicationGroupController.logger.info("NetworkNode Reachable via NetworkLink message received.");
                    GeneralNetworkLink activeNetworkLink;
                    try {
                        activeNetworkLink = (GeneralNetworkLink) objects[0];
                        // Set the link (linkFromNode, linkToNode) where linkToNode has interface with remote IP inactive
                        ApplicationGroupController.logger.info("Setting link " + activeNetworkLink + " active.");
                        networkTopology.switchNetworkLinkUp(activeNetworkLink);
                    } catch (Exception e) {
                        infoOnException(e, "Failed to decode incoming MessageElement.");
                    }
                    sendNetworkTopology();
                }
            }
        });
        //TODO register new message
        this.myJXTAConnector.registerMessageListener(agcInputPipe, MessageType.NEW_NODE_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                synchronized (networkTopology) {
                    ApplicationGroupController.logger.info("New NetworkNode message received.");
                    GeneralNetworkNode newNetworkNode;
                    try {
                        newNetworkNode = (GeneralNetworkNode) objects[0];
                        ApplicationGroupController.logger.info("Adding new endpoint network node " + newNetworkNode.getNodeName() + " to the network topology");
                        try {
                            switch (newNetworkNode.getNodeType()) {
                                case GeneralNetworkNode.NODE_TYPE_ENDPOINT :
                                    networkTopology.addNetworkNode((EndpointNetworkNode) newNetworkNode);
                                    ApplicationGroupController.logger.info("New EndpointNetworkNode " + newNetworkNode.getNodeName() + " message processed.");
                                    break;
                                case GeneralNetworkNode.NODE_TYPE_PHYSICAL :
                                    networkTopology.addNetworkNode((PhysicalNetworkNode) newNetworkNode);
                                    ApplicationGroupController.logger.info("New PhysicalNetworkNode " + newNetworkNode.getNodeName() + " message processed.");
                                    break;
                                case GeneralNetworkNode.NODE_TYPE_UNKNOWN_NETWORK :
                                    networkTopology.addNetworkNode((UnknownNetworkNode) newNetworkNode);
                                    ApplicationGroupController.logger.info("New PhysicalNetworkNode " + newNetworkNode.getNodeName() + " message processed.");
                                    break;
                                default:
                                    ApplicationGroupController.logger.warn("Unknown type of network node, could not be added!");
                                    break;
                            }
                        } catch (Exception e) {
                            infoOnException(e, "Failed to add Network Node to the network topology.");
                        }
                        // Check for any new lambdas and add them into the monitoring
                        // TODO: Now, lambdas are expected to be associated with physical links, hence check has to be done somewhere else
//                        ArrayList<LambdaLink> monitoredLambdas = lambdaMonitor.getMonitoredLinks();
//                        for (LogicalNetworkLink networkLink : networkTopology.getNetworkTopologyGraph().edgeSet()) {
//                            for (LambdaLink lambdaLink : networkLink.getAssociatedLambdas()) {
//                                if (!monitoredLambdas.contains(lambdaLink)) {
//                                    lambdaMonitor.addLambdaLink(lambdaLink);
//                                }
//                            }
//                        }
                    } catch (Exception e) {
                        infoOnException(e, "Failed to decode incomming Message.");
                    }
                    sendNetworkTopology();
                }
            }
        });

        this.myJXTAConnector.registerMessageListener(agcInputPipe, MessageType.REMOVE_NODE_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                synchronized (networkTopology) {
                    ApplicationGroupController.logger.info("NetworkNode Removed message received.");
                    GeneralNetworkNode networkNodeToRemove;
                    try {
                        networkNodeToRemove = (GeneralNetworkNode) objects[0];
                        // Remove the node from the networkTopology
                        ApplicationGroupController.logger.debug("Removing network node " + networkNodeToRemove.getNodeName() + " from the network topology.");
                        try {
                            switch (networkNodeToRemove.getNodeType()) {
                                case GeneralNetworkNode.NODE_TYPE_ENDPOINT :
                                    networkTopology.removeNetworkNode((EndpointNetworkNode) networkNodeToRemove);
                                    ApplicationGroupController.logger.info("Remove EndpointNetworkNode " + networkNodeToRemove.getNodeName() + " message processed.");
                                    break;
                                case GeneralNetworkNode.NODE_TYPE_PHYSICAL :
                                    networkTopology.removeNetworkNode((PhysicalNetworkNode) networkNodeToRemove);
                                   ApplicationGroupController.logger.info("Remove PhysicalNetworkNode " + networkNodeToRemove.getNodeName() + " message processed.");
                                    break;
                                case GeneralNetworkNode.NODE_TYPE_UNKNOWN_NETWORK :
                                    networkTopology.removeNetworkNode((UnknownNetworkNode) networkNodeToRemove);
                                    ApplicationGroupController.logger.info("Remove UnknownNetworkNode " + networkNodeToRemove.getNodeName() + " message processed.");
                                    break;
                                default:
                                    ApplicationGroupController.logger.warn("Unknown type of network node, could not be removed!");
                                    break;
                            }
                        } catch (Exception e) {
                            infoOnException(e, "Failed to remove the node from the network topology.");
                        }
                        // Check if all monitored lambdas are still in the network topology
                        // TODO: not needed anymore, lambdas are associated with physical links now
//                        ArrayList<LambdaLink> lambdasToRemove = new ArrayList<LambdaLink>();
//                        for (LambdaLink lambdaLink : lambdaMonitor.getMonitoredLinks()) {
//                            boolean lambdaAssociated = false;
//                            for (NetworkLink networkLink : lambdaLink.getAssociatedNetworkLinks()) {
//                                if (networkTopology.getNetworkTopologyGraph().containsEdge(networkLink)) {
//                                    lambdaAssociated = true;
//                                }
//                            }
//                            if (!lambdaAssociated) {
//                                lambdasToRemove.add(lambdaLink);
//                            }
//                        }
//                        for (LambdaLink lambdaLink : lambdasToRemove) {
//                            lambdaMonitor.removeLambdaLink(lambdaLink);
//                            if (lambdaLink.isAllocated()) {
//                                ApplicationGroupController.logger.info("About to deallocate lambda " + lambdaLink);
//                                LambdaLinkFactory lambdaLinkFactory = new LambdaLinkFactory(lambdaLink);
//                                lambdaLinkFactory.deallocate(lambdaLink);
//                            }
//                        }
                    } catch (Exception e) {
                        infoOnException(e, "Failed to decode incomming MessageElement.");
                    }
                    sendNetworkTopology();
                }
            }
        });

    }

    public void setPipeService(PipeService pipeService) {
        this.pipeService = pipeService;
    }

    /**
     * Creates and returns AGC Class advertisement
     * <p/>
     * Class advertisement is used just to advertise an existence of AGC
     *
     * @return Application Group Class advertisement
     */
    public ModuleClassAdvertisement getAGCClassAdv() {
        if (agcMcAdv == null) {
            try {
                agcMcAdv = (ModuleClassAdvertisement) AdvertisementFactory.newAdvertisement(ModuleClassAdvertisement.getAdvertisementType());

                agcMcAdv.setName("MOD:Universe-AGC");
                agcMcAdv.setDescription("Collab Universe Application Group Controller");

                agcMcID = IDFactory.newModuleClassID();
                agcMcAdv.setModuleClassID(agcMcID);
            }
            catch (Exception e) {
                infoOnException(e, "Failed to create AGC Class advertisement.");

                return null;
            }
        }

        return agcMcAdv;
    }


    /**
     * Creates and returns AGC Module Spec advertisement
     * <p/>
     * Spec advertisement contains all information necessary to reach the
     * AGC (i.e. a jxta pipe). For further communication with AGC it is
     * necessary to use the advertised input pipe (i.e. create an input pipe
     * using createInputPipe(this.getPipeAdv)).
     *
     * @return Application Group Module Specification advertisement
     */
    public ModuleSpecAdvertisement getAGCModuleAdv() {
        if (agcMdAdv == null) {
            try {
                agcMdAdv = (ModuleSpecAdvertisement) AdvertisementFactory.newAdvertisement(ModuleSpecAdvertisement.getAdvertisementType());

                // Setup the module spec information
                agcMdAdv.setName("SPEC:Universe-AGC");
                agcMdAdv.setVersion("Version 0.2");
                agcMdAdv.setCreator("sitola.fi.muni.cz");

                // Module spec ID is based on the AGC Class ID
                ModuleSpecID agcMdID = IDFactory.newModuleSpecID(agcMcID);
                agcMdAdv.setModuleSpecID(agcMdID);
                agcMdAdv.setSpecURI("http://sitola.fi.muni.cz/CollabUniverse/AGC");

                // Store the pipe advertisement in the Spec advertisement
                agcMdAdv.setPipeAdvertisement(this.getAGCPipeAdv());

                return agcMdAdv;
            }
            catch (Exception e) {
                infoOnException(e, "Failed to create AGC Module advertisement.");

                return null;
            }
        }

        return agcMdAdv;
    }

    /**
     * Creates and returns AGC input pipe advertisement
     * <p/>
     * If the pipe adv doesn't exit a new one is created. We MUST advertise
     * always the same pipe.
     *
     * @return Application Group Controller Pipe advertisement
     */
    private PipeAdvertisement getAGCPipeAdv() {
        String agcPipeIDStr = "urn:jxta:uuid-362B1F61F4D9481A90A386D9CB7BC4549FF129072F53476EA299BB57090C9A7B04";

        if (agcPipeAdv == null) {
            try {
                // Create the module pipe advertisement
                // We MUST advertise always the same pipe

                agcPipeAdv = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());

                // Create pipe adv ID
                PipeID agcPipeID = (PipeID) IDFactory.fromURI(new URI(agcPipeIDStr));

                agcPipeAdv.setPipeID(agcPipeID);
                agcPipeAdv.setType(PipeService.UnicastType);
                agcPipeAdv.setName("AGC input pipe");
            }
            catch (Exception e) {
                infoOnException(e, "Failed to create AGC input pipe advertisement.");
                return null;
            }

        }

        return agcPipeAdv;
    }

    @SuppressWarnings("unchecked")
    private void sendNetworkTopology() {//TODO use this to send message
        if (networkTopology.getStatus().isChanged()) {
            synchronized (networkTopology) {
                for (EndpointNetworkNode targetNode : networkTopology.getEndpointNodes()) {
                    OutputPipe targetNodeOutputPipe;
                    try {
                        targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                    } catch (IOException e) {
                        infoOnException(e, "Failed to create output pipe to peer " + targetNode.getNodeName());
                        continue;
                    }

                    ArrayList<EndpointNetworkNode> endpointNetworkNodes = new ArrayList<EndpointNetworkNode>(networkTopology.getEndpointNodes());
                    ArrayList<PhysicalNetworkNode> physicalNetworkNodes = new ArrayList<PhysicalNetworkNode>(networkTopology.getPhysicalNodes());
                    ArrayList<UnknownNetworkNode> unknownNetworkNodes = new ArrayList<UnknownNetworkNode>(networkTopology.getUnknownNetworkNodes());
                    ArrayList<LogicalNetworkLink> logicalNetworkLinks = new ArrayList<LogicalNetworkLink>(networkTopology.getLogicalLinks());
                    ArrayList<PhysicalNetworkLink> physicalNetworkLinks = new ArrayList<PhysicalNetworkLink>(networkTopology.getPhysicalLinks());
                    

                    // Send the network topology message to the peer node
                    ApplicationGroupController.logger.info("Sending updated network topology to " + targetNode);
                    try {
                        this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.NETWORK_UPDATE_MESSAGE,
                                endpointNetworkNodes, physicalNetworkNodes, unknownNetworkNodes, logicalNetworkLinks, physicalNetworkLinks);//TODO use this to send message
                    } catch (IOException e) {
                        infoOnException(e, "Failed to send network topology update message to peer.");
                    }
                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private void sendNetworkTopologyXML() { //TODO make sure this is ok.
        if (networkTopology.getStatus().isChanged()) {
            synchronized (networkTopology) {
                for (EndpointNetworkNode targetNode : networkTopology.getEndpointNodes()) {
                    OutputPipe targetNodeOutputPipe;
                    try {
                        targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                    } catch (IOException e) {
                        infoOnException(e, "Failed to create output pipe to peer " + targetNode.getNodeName());
                        continue;
                    }
                    // Send the network topology message to the peer node
                    ApplicationGroupController.logger.info("Sending updated network topology to " + targetNode);
                    try {
                        this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.TOPOLOGY_DESCRIPTION_MESSAGE, networkTopology.encodeToXML());
                    } catch (IOException e) {
                        infoOnException(e, "Failed to send network topology update message to peer.");
                    }
                }
            }
        }
    }

    /**
     * Application Group Controller Plan Loop
     */
    @SuppressWarnings("unchecked")
    private void agcPlanLoop() {
        MapVisualizer gMap = null;

        ArrayList<PlanElement> agcPlan;
        ArrayList<PlanElement> previousAgcPlan = null;
        MatchMakerConfig agcMatchMakerConfig = new MatchMakerConfig();
        agcMatchMakerConfig.timeout = 30000;
        MatchMaker agcMatchMaker = new MatchMakerPKTGurobi(networkTopology, agcMatchMakerConfig);

        int threadPoolSize = 10;  // TODO how many lambda link allocations do we want to perform simultaneously?
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(threadPoolSize, 2 * threadPoolSize, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

        ApplicationGroupController.logger.info("Plan Thread started.");

        final AGCDialog agcDialog = new AGCDialog();
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    agcDialog.setModal(false);
                    agcDialog.setVisible(true);
                }
            });
        } catch (InterruptedException e) {
            infoOnException(e, "Failed to run AGCDialog!");
        } catch (InvocationTargetException e) {
            infoOnException(e, "Failed to run AGCDialog!");
        }
        while (agcDialog.isVisible()) {
            TimeUtils.sleepFor(500);
        }

        while (!terminateFlag.get()) {
            PartiallyKnownNetworkTopology networkTopologyWorkingSnapshot = null;
            boolean proceedWithPlanning = false;
            synchronized (networkTopology) {
                NetworkTopologyStatus nts = networkTopology.getStatus();
                if (nts.isChanged() && (nts.getChangedStamp() < System.currentTimeMillis() - 1000 || nts.getChangesCount() > 10)) {

                    try {
                        ApplicationGroupController.logger.trace("Analyzing original berfore cloning:");
                        for (LogicalNetworkLink networkLink : networkTopology.getLogicalLinks()) {
                            if (!networkLink.isActive()) {
                                ApplicationGroupController.logger.trace("Link " + networkLink + " with RTT " + networkLink.getLatency() + "is inactive.");
                            }
                        }
                        networkTopologyWorkingSnapshot = PartiallyKnownNetworkTopology.getCurrentSnapshot(networkTopology);
                        proceedWithPlanning = true;
                        ApplicationGroupController.logger.trace("Analyzing snapshot after cloning:");
                        for (LogicalNetworkLink networkLink : networkTopologyWorkingSnapshot.getLogicalLinks()) {
                            if (!networkLink.isActive()) {
                                ApplicationGroupController.logger.trace("Link " + networkLink + " with RTT " + networkLink.getLatency() + "is inactive.");
                            }
                        }
                    } catch (CloneNotSupportedException e) {
                        infoOnException(e, "Failed to create a snapshot of network topology!");
                        continue;
                    }
                }
            }
            if (proceedWithPlanning) {
                ApplicationGroupController.logger.info("Matching consumers and producers for given network topology.");
                // TODO: GLIF hack
                /*
                MatchConsumerProducer mcp = new MatchConsumerProducer(networkTopologyWorkingSnapshot);
                boolean mcpSucceeded = mcp.doMatch();
                */
                HashSet<MediaApplication> fullHDApps = new HashSet<MediaApplication>();
                HashSet<MediaApplication> dxtHDApps = new HashSet<MediaApplication>();
                assert networkTopologyWorkingSnapshot != null;
                assert networkTopologyWorkingSnapshot.getNetworkTopologyGraph() != null;
                ApplicationGroupController.logger.trace("Analyzing current snapshot for planning:");
                for (LogicalNetworkLink networkLink : networkTopologyWorkingSnapshot.getLogicalLinks()) {
                    if (!networkLink.isActive()) {
                        ApplicationGroupController.logger.trace("Link " + networkLink + " with RTT " + networkLink.getLatency() + "is inactive.");
                    }
                }
                for (EndpointNetworkNode networkNode : networkTopologyWorkingSnapshot.getEndpointNodes()) {
                    for (MediaApplication mediaApplication : networkNode.getNodeApplications()) {
                        if (mediaApplication instanceof UltraGrid1500Consumer || mediaApplication instanceof UltraGrid1500Producer) {
                            fullHDApps.add(mediaApplication);
                        }
                        if (mediaApplication instanceof UltraGridDXTConsumer || mediaApplication instanceof UltraGridDXTProducer) {
                            dxtHDApps.add(mediaApplication);
                        }
                    }
                }
                MatchConsumerProducer mcp = new MatchConsumerProducer(networkTopologyWorkingSnapshot);
                // TODO: commented out for the I2 DCN demo
                // mcp.setDisabledApplications(dxtHDApps);
                mcp.doMatch();

                /*
                if (!agcMatchMaker.doMatch()) {
                    mcp.setDisabledApplications(fullHDApps);
                    mcp.doMatch();
                }
                */
                boolean mcpSucceeded = true;
                // TODO: end of GLIF hack
                if (mcpSucceeded) {
                    ApplicationGroupController.logger.info("Consumers and producers sucessfuly matched.");

                    ApplicationGroupController.logger.info("Starting MatchMaker.");
                    if (agcMatchMaker.doMatch()) {
                        // We have to ensure that getPlan is called while conditions are unchanged from doMatch()!!!

                        // Get results from the MatchMaker
                        ApplicationGroupController.logger.info("Getting the plan.");
                        agcPlan = agcMatchMaker.getPlan();

                        // Get rid of old visualisation if any
                        if (gMap != null) {
                            gMap.setVisible(false);
                        }
                        // Create map visualisation of the network
                        gMap = agcMatchMaker.getMapVizualization();
                        gMap.setVisible(true);

                        // Create config for particular Media Applications and send it to respective peers

                        if (agcPlan != null) {
                            ApplicationGroupController.logger.trace("Analyzing previous and current plan.");
                            if (previousAgcPlan != null) {
                                for (PlanElement planElem : previousAgcPlan) {
                                    ApplicationGroupController.logger.trace("   previousAgcPlan " + planElem.getMediaApplication());
                                }
                                for (PlanElement planElem : agcPlan) {
                                    ApplicationGroupController.logger.trace("           agcPlan " + planElem.getMediaApplication());
                                }
                            } else {
                                ApplicationGroupController.logger.trace("Previous plan is null");
                            }
                            if (previousAgcPlan == null || (!agcPlan.containsAll(previousAgcPlan) || !previousAgcPlan.containsAll(agcPlan))) {
                                if (previousAgcPlan != null) {
                                    // Stop all applications planned in previousAgcPlan
                                    ApplicationGroupController.logger.info("Cleaning after previous plan.");
                                    // Some nodes/peers may be already inentionally down at this time. Stopping any media applications
                                    // running on these nodes won't make sense.
                                    // TODO: is there any better way how to check which nodes/peers are currently active? 
                                    PartiallyKnownNetworkTopology networkTopologyCurrentSnapshot = null;
                                    synchronized (networkTopology) {
                                        try {
                                            networkTopologyCurrentSnapshot = PartiallyKnownNetworkTopology.getCurrentSnapshot(networkTopology);
                                        } catch (CloneNotSupportedException e) {
                                            infoOnException(e, "Failed to create a snapshot of network topology!");
                                            e.printStackTrace();
                                        }
                                    }
                                    for (PlanElement prevAgcPlanElem : previousAgcPlan) {
                                        EndpointNetworkNode targetNode = prevAgcPlanElem.getMediaApplication().getParentNode();
                                        if (networkTopologyCurrentSnapshot.getNetworkTopologyGraph().containsVertex(targetNode)) {
                                            OutputPipe targetNodeOutputPipe;
                                            try {
                                                targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                                            } catch (IOException e) {
                                                infoOnException(e, "Failed to create output pipe to peer " + targetNode);
                                                continue;
                                            }

                                            // Get the MediaAplication from the plan element
                                            MediaApplication mediaApp = prevAgcPlanElem.getMediaApplication();

                                            // Send the stop app message to the peer node
                                            ApplicationGroupController.logger.info("Sending Stop MediaApp object " + mediaApp + " to " + targetNode);
                                            try {
                                                this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.STOP_MEDIAAPP_MESSAGE, mediaApp);
                                            } catch (IOException e) {
                                                infoOnException(e, "Failed to send stop MedaiApplication message to peer.");
                                            }
                                        } else {
                                            ApplicationGroupController.logger.debug("    " + targetNode + "is probably no longer in the network topology. Skipping...");
                                        }
                                    }
                                }
                                ApplicationGroupController.logger.info("Deploying new plan.");

                                // Check for any links with associated lambdas and allocate them
                                ApplicationGroupController.logger.info("Checking the plan for lambdas to be allocated.");
                                ArrayList<LambdaLink> lambdasToAllocate = new ArrayList<LambdaLink>();
                                for (PlanElement agcPlanElement : agcPlan) {
                                    if (agcPlanElement.getNetworkLinks() != null) {
                                        for (LogicalNetworkLink nl : agcPlanElement.getNetworkLinks()) {
                                            // TODO There might be more lambdas associated with one link
                                            if (!nl.getAssociatedLambdas().isEmpty()) {
                                                for (LambdaLink ll : nl.getAssociatedLambdas()) {
                                                    if (!lambdasToAllocate.contains(ll)) {
                                                        lambdasToAllocate.add(ll);
                                                        ApplicationGroupController.logger.info("New Lambda to allocate: " + ll);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!lambdasToAllocate.isEmpty()) {
                                    ArrayList<FutureTask> lambdaLinkThreads = new ArrayList<FutureTask>();
                                    ApplicationGroupController.logger.info("Initiating allocation of planned lambda links.");
                                    for (final LambdaLink ll : lambdasToAllocate) {
                                        if (ll != null) {
                                            if (!ll.isAllocated() && ll.isToAllocate()) {
                                                ApplicationGroupController.logger.debug("    " + ll);
                                                @SuppressWarnings({"unchecked"})
                                                FutureTask ft = new FutureTask(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        try {
                                                            LambdaLinkFactory lambdaLinkFactory = new LambdaLinkFactory(ll);
                                                            lambdaLinkFactory.allocate(ll);
                                                        } catch (Exception e) {
                                                            ApplicationGroupController.logger.error("Lambda link allocation failed for lambda " + ll);
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                }, null);
                                                lambdaLinkThreads.add(ft);
                                            }
                                        }
                                    }
                                    ApplicationGroupController.logger.info("Executing allocation for planned lambda links.");
                                    for (FutureTask futureTask : lambdaLinkThreads) {
                                        threadPoolExecutor.execute(futureTask);
                                    }

                                    ApplicationGroupController.logger.trace("Collecting allocation results for planned lambda links.");
                                    if (!lambdaLinkThreads.isEmpty()) {
                                        for (FutureTask futureTask : lambdaLinkThreads) {
                                            //noinspection EmptyCatchBlock
                                            if (futureTask != null) {
                                                try {
                                                    futureTask.get();
                                                } catch (InterruptedException e) {
                                                    ApplicationGroupController.logger.debug("Encountered InterruptedException.");
                                                } catch (ExecutionException e) {
                                                    ApplicationGroupController.logger.debug("Encountered ExecutionException.");
                                                }
                                            }
                                        }
                                    }
                                    ApplicationGroupController.logger.debug("Allocation results collected for planned lambda links.");
                                }

                                for (PlanElement anAgcPlan : agcPlan) {
                                    // First of all we need to get the JXTA pipe of the node where we want to run/configure the app
                                    EndpointNetworkNode targetNode = anAgcPlan.getMediaApplication().getParentNode();
                                    OutputPipe targetNodeOutputPipe;
                                    try {
                                        targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                                    } catch (IOException e) {
                                        infoOnException(e, "Failed to create output pipe to peer " + targetNode);
                                        continue;
                                    }

                                    // Create MediaApplication Cmd options
                                    String appParams = "";
                                    MediaApplication mediaApp = anAgcPlan.getMediaApplication();
                                    if (mediaApp.getApplicationName().contains("UltraGrid HD video producer")) {
                                        if (anAgcPlan.getNetworkLinks().size() == 1) {
                                            Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                            LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();
                                            appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                            if (mediaApp instanceof UltraGrid1500Producer) {
                                                UltraGrid1500Producer ultraGrid1500Prod = (UltraGrid1500Producer) mediaApp;
                                                ultraGrid1500Prod.setTargetIP(networkLink.getToInterface().getIpAddress());
                                                ultraGrid1500Prod.setTargetNetworkNode(networkLink.getToNode());
                                            }
                                            if (mediaApp instanceof UltraGrid750Producer) {
                                                UltraGrid750Producer ultraGrid750Prod = (UltraGrid750Producer) mediaApp;
                                                ultraGrid750Prod.setTargetIP(networkLink.getToInterface().getIpAddress());
                                                ultraGrid750Prod.setTargetNetworkNode(networkLink.getToNode());
                                            }
                                        } else {
                                            ApplicationGroupController.logger.warn("UltraGrid HD video producer doesn't have 1 network link.");
                                            continue;
                                        }
                                    }
                                    if (mediaApp.getApplicationName().contains("UltraGrid HD video consumer")) {
                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, anAgcPlan.getConsumerTargetInterface().getIpAddress());
                                    }
                                    if (mediaApp.getApplicationName().contains("UltraGrid DXT compressed HD video producer")) {
                                        if (anAgcPlan.getNetworkLinks().size() == 1) {
                                            Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                            LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();
                                            appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                            if (mediaApp instanceof UltraGridDXTProducer) {
                                                UltraGridDXTProducer ultraGridDXTProd = (UltraGridDXTProducer) mediaApp;
                                                ultraGridDXTProd.setTargetIP(networkLink.getToInterface().getIpAddress());
                                                ultraGridDXTProd.setTargetNetworkNode(networkLink.getToNode());
                                            }
                                        } else {
                                            ApplicationGroupController.logger.warn("UltraGrid HD video consumer doesn't have 1 network link.");
                                        }

                                    }
                                    if (mediaApp.getApplicationName().contains("UltraGrid DXT compressed HD video consumer")) {
                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, anAgcPlan.getConsumerTargetInterface().getIpAddress());
                                    }
                                    if (mediaApp.getApplicationName().contains("Polycom Client Producer")) {
                                        if (anAgcPlan.getNetworkLinks().size() == 1) {
                                            Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                            LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();
                                            appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                            if (mediaApp instanceof PolycomProducer) {
                                                PolycomProducer polycomProducer = (PolycomProducer) mediaApp;
                                                polycomProducer.setTargetIP(networkLink.getToInterface().getIpAddress());
                                                polycomProducer.setTargetNetworkNode(networkLink.getToNode());
                                            }
                                        } else {
                                            ApplicationGroupController.logger.warn("Polycom Client producer doesn't have 1 network link.");
                                            continue;
                                        }
                                    }
                                    if (mediaApp.getApplicationName().contains("RAT Audio Client")) {
                                        Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                        LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();
                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                        if (mediaApp instanceof RAT) {
                                            RAT rat = (RAT) mediaApp;
                                            rat.setTargetIP(networkLink.getToInterface().getIpAddress());
                                            rat.setTargetNetworkNode(networkLink.getToNode());
                                        }
                                    }
                                    if (mediaApp.getApplicationName().contains("VIC Video Client")) {
                                        Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                        LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();
                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                        if (mediaApp instanceof VIC) {
                                            VIC vic = (VIC) mediaApp;
                                            vic.setTargetIP(networkLink.getToInterface().getIpAddress());
                                            vic.setTargetNetworkNode(networkLink.getToNode());
                                        }
                                    }
                                    if (mediaApp.getApplicationName().equals("HD UDP Packet Reflector")) {
                                        Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                        NetworkLink networkLink;
                                        String ipList = "";
                                        while (networkLinkIt.hasNext()) {
                                            networkLink = (NetworkLink) networkLinkIt.next();
                                            ipList += " " + networkLink.getToInterface().getIpAddress();
                                        }
                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, ipList);
                                    }
                                    if (mediaApp.getApplicationName().equals("UDP Packet Reflector")) {
                                        appParams = MediaApplicationParametersFactory.checkAppParams(mediaApp);
                                    }
                                    if (mediaApp.getApplicationName().contains("HDV MPEG-2 TS producer")) {
                                        Iterator networkLinkIt = anAgcPlan.getNetworkLinks().iterator();
                                        LogicalNetworkLink networkLink = (LogicalNetworkLink) networkLinkIt.next();

                                        appParams = MediaApplicationParametersFactory.addTargetIP(mediaApp, networkLink.getToInterface().getIpAddress());

                                        if (mediaApp instanceof HDVMPEG2Producer) {
                                            HDVMPEG2Producer hdvMPEG2Producer = (HDVMPEG2Producer) mediaApp;
                                            hdvMPEG2Producer.setTargetIP(networkLink.getToInterface().getIpAddress());
                                            hdvMPEG2Producer.setTargetNetworkNode(networkLink.getToNode());
                                        }
                                    }
                                    if (mediaApp.getApplicationName().contains("HDV MPEG-2 TS consumer")) {
                                        appParams = MediaApplicationParametersFactory.checkAppParams(mediaApp);

                                    }

                                    // Send the start app message to the peer node
                                    ApplicationGroupController.logger.info("Sending Start MediaApp object " + mediaApp + " to " + targetNode);
                                    try {
                                        this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.START_MEDIAAPP_MESSAGE, mediaApp, appParams);
                                    } catch (IOException e) {
                                        infoOnException(e, "Failed to send start MedaiApplication message to peer.");
                                    }
                                }

                                HashMap<EndpointNetworkNode, ArrayList<LogicalNetworkLink>> activeLinks = new HashMap<EndpointNetworkNode, ArrayList<LogicalNetworkLink>>();
                                for (PlanElement planElement : agcPlan) {
                                    if (planElement.getNetworkLinks() == null) {
                                        ApplicationGroupController.logger.debug("Network links are null for plan element " + planElement + " with media application " + planElement.getMediaApplication());
                                        continue;
                                    }
                                    for (LogicalNetworkLink networkLink : planElement.getNetworkLinks()) {
                                        EndpointNetworkNode n = networkLink.getFromNode();
                                        if (!activeLinks.containsKey(n)) {
                                            activeLinks.put(n, new ArrayList<LogicalNetworkLink>());
                                        }
                                        activeLinks.get(n).add(networkLink);
                                    }
                                }
                                for (EndpointNetworkNode targetNode : activeLinks.keySet()) {
                                    OutputPipe targetNodeOutputPipe;
                                    try {
                                        targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                                    } catch (IOException e) {
                                        infoOnException(e, "Failed to create output pipe to peer " + targetNode.getNodeName());
                                        continue;
                                    }

                                    // Send the failover message to the peer node
                                    ApplicationGroupController.logger.info("Sending active network links to " + targetNode);
                                    try {
                                        this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.ACTIVE_LINKS_MESSAGE, activeLinks.get(targetNode));
                                    } catch (IOException e) {
                                        infoOnException(e, "Failed to send active links message to peer.");
                                    }
                                }

                                // Store the previous plan
                                previousAgcPlan = (ArrayList<PlanElement>) agcPlan.clone();
                            } else {
                                ApplicationGroupController.logger.info("New plan is the same as previous one. Deployment of the plan skipped.");
                            }
                        } else {
                            ApplicationGroupController.logger.warn("Got an empty plan.");
                        }
                    } else {
                        ApplicationGroupController.logger.warn("Failed to find a sollution for given network topology.");

                        // Send MediaApp failover message to all nodes (means start all VICs)
                        for (EndpointNetworkNode targetNode : networkTopologyWorkingSnapshot.getEndpointNodes()) {
                            OutputPipe targetNodeOutputPipe;
                            try {
                                targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                            } catch (IOException e) {
                                infoOnException(e, "Failed to create output pipe to peer " + targetNode.getNodeName());
                                continue;
                            }

                            // Send the failover message to the peer node
                            ApplicationGroupController.logger.info("Sending Start MediaApp Failover to " + targetNode);
                            try {
                                this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.START_FAILOVER_MEDIAAPP_MESSAGE, "AGC: Failed to find feasible plan.");
                            } catch (IOException e) {
                                infoOnException(e, "Failed to send Start MediaApp failover message to peer.");
                            }
                        }

                        // This is necessary to recover from Failover state with next non-empty plan
                        previousAgcPlan.clear();
                    }
                } else {
                    ApplicationGroupController.logger.warn("Failed to match consumers and producers for given network topology.");
                }
            } else {
                // There was no change in the network topology, lets sleep for a while
                TimeUtils.sleepFor(500);
            }
        }

        threadPoolExecutor.shutdown();
    }

    /**
     * Starts Application Group Controller
     */
    public void startAGC() {
        ApplicationGroupController.logger.info("Starting Application Group Conroller.");
        try {
            // Create AGC endpoint pipe based on the pipe advertisement. Clients will use this pipe to connect AGC
            agcInputPipe = myJXTAConnector.createInputPipe(this.getAGCPipeAdv());
        }
        catch (IOException e) {
            infoOnException(e, "Failed to create AGC input pipe.");
        }

        this.registerJXTAConnectorCallbacks();

        this.myJXTAConnector.listenOnInputPipe(agcInputPipe);

        this.lambdaMonitor.startMonitoring();

        try {
            agcPlanThread = new Thread() {
                @Override
                public void run() {
                    agcPlanLoop();
                }
            };
            agcPlanThread.start();
        } catch (Exception e) {
            infoOnException(e, "Failed to create and run AGC Plan thread.");
        }

    }

    /**
     * Terminates Application Group Controller including some cleanup
     */
    public void stopAGC() {
        ApplicationGroupController.logger.info("Stopping Application Group Controller.");
        terminateFlag.set(true);

        this.lambdaMonitor.stopMonitoring();

        try {
            if (agcPlanThread != null) {
                agcPlanThread.join();
            } else {
                ApplicationGroupController.logger.warn("AGC Plan thread was not started.");
            }
        } catch (Exception e) {
            infoOnException(e, "Failed to interrupt and join AGC Plan thread.");
        }

        agcInputPipe.close();
    }

    private static void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
        }
        logger.error(s);
    }
}
