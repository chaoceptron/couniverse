package AGC;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationProducer;
import MediaAppFactory.MediaStream;
import NetworkRepresentation.EndpointNetworkNode;
import NetworkRepresentation.NetworkSite;
import NetworkRepresentation.PartiallyKnownNetworkTopology;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.log4j.Logger;

/**
 * A class that facilitates matching producers to consumers, based on sourceSite specification
 * in the MediaApplicationConsumer. This is supposed to be called from AGC before MatchMaking
 * is done.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 7.9.2007
 * Time: 19:17:20
 */
public class MatchConsumerProducer {
    private PartiallyKnownNetworkTopology topology;
    private HashMap<NetworkSite, ArrayList<MediaApplicationProducer>> siteProducers = new HashMap<NetworkSite, ArrayList<MediaApplicationProducer>>();
    private ArrayList<MediaApplicationConsumer> siteConsumers = new ArrayList<MediaApplicationConsumer>();
    @SuppressWarnings({"MismatchedQueryAndUpdateOfCollection"})
    private HashMap<MediaApplicationProducer, ArrayList<MediaApplicationConsumer>> producerMapping = new HashMap<MediaApplicationProducer, ArrayList<MediaApplicationConsumer>>();
    private HashSet<MediaApplication> disabledApplications = new HashSet<MediaApplication>();

    static Logger logger = Logger.getLogger(MatchConsumerProducer.class);

    public MatchConsumerProducer(PartiallyKnownNetworkTopology topology) {
        this.topology = topology;
    }

    /**
     * Matches consumers to producers.
     * <p/>
     *
     * @return returns true if matching succeeded, false otherwise
     */
    public boolean doMatch() {
        boolean matchSuccess = true;
        for (EndpointNetworkNode networkNode : topology.getEndpointNodes()) {
            for (MediaApplication mediaApplication : networkNode.getNodeApplications()) {
                if (disabledApplications.contains(mediaApplication)) {
                    if (mediaApplication instanceof MediaApplicationConsumer) {
                        MediaApplicationConsumer mediaAppConsumer = (MediaApplicationConsumer) mediaApplication;
                        mediaAppConsumer.setSource(null);
                    }
                    continue;
                }
                if (mediaApplication instanceof MediaApplicationProducer) {
                    if (!siteProducers.containsKey(networkNode.getNodeSite())) {
                        siteProducers.put(networkNode.getNodeSite(), new ArrayList<MediaApplicationProducer>());
                    }
                    ArrayList<MediaApplicationProducer> appList = siteProducers.get(networkNode.getNodeSite());
                    appList.add((MediaApplicationProducer) mediaApplication);
                    producerMapping.put((MediaApplicationProducer) mediaApplication, new ArrayList<MediaApplicationConsumer>());
                } else if (mediaApplication instanceof MediaApplicationConsumer) {
                    MediaApplicationConsumer ma = (MediaApplicationConsumer) mediaApplication;
                    if (ma.getSourceSite() != null) {
                        siteConsumers.add(ma);
                    }
                }
            }
        }

        // matching
        for (MediaApplicationConsumer siteConsumer : siteConsumers) {
            NetworkSite srcSite = siteConsumer.getSourceSite();
            if (siteProducers.containsKey(srcSite) && siteProducers.get(srcSite).size() > 0) {
                MediaApplicationProducer srcMa = findAppropriateApplication(srcSite, siteConsumer);
                if (srcMa != null) {
                    siteConsumer.setSource((MediaApplication) srcMa);
                    //noinspection SuspiciousMethodCalls
                    producerMapping.get(srcMa).add(siteConsumer);
                    MatchConsumerProducer.logger.info("Setting source for " + siteConsumer + " on " + ((MediaApplication) siteConsumer).getParentNode() + " to " + srcMa + " on " + ((MediaApplication) srcMa).getParentNode());
                } else {
                    MatchConsumerProducer.logger.warn("Failed to associate " + siteConsumer + " with any producer -- all producers on " + srcSite + " are already used.");
                    // TODO: is this an error condition?
                    // matchSuccess = false;
                }
            } else {
                // This means that the specified site doesn't have any producer at all - which is weird and sort of
                // suggest misspelled site by Consumer
                MatchConsumerProducer.logger.warn("Unable to find any source for " + siteConsumer + " which requested source site " + srcSite + ". There is no producer running on this site.");
                matchSuccess = false;
            }
        }

        return matchSuccess;
    }

    private MediaApplicationProducer findAppropriateApplication(NetworkSite site, MediaApplicationConsumer consumer) {
        assert siteProducers.containsKey(site) && siteProducers.get(site).size() > 0;
        for (int i = 0; i < siteProducers.get(site).size(); i++) {
            MediaApplicationProducer currentProducer = siteProducers.get(site).get(i);
            boolean isUsedForCurrentConsumerSite = false;
            if (producerMapping.containsKey(currentProducer) && producerMapping.get(currentProducer).size() > 0) {
                for (MediaApplicationConsumer mediaApplicationConsumer : producerMapping.get(currentProducer)) {
                    if (((MediaApplication) mediaApplicationConsumer).getParentNode().getNodeSite().equals(((MediaApplication) consumer).getParentNode().getNodeSite())) {
                        isUsedForCurrentConsumerSite = true;
                    }
                }
            }
            if (!isUsedForCurrentConsumerSite) {
                if (mediaStreamsMatch((MediaApplication) consumer, (MediaApplication) siteProducers.get(site).get(i))) {
                    return siteProducers.get(site).get(i);
                }
            }
        }
        return null;
    }

    private boolean mediaStreamsMatch(MediaApplication ma1, MediaApplication ma2) {
        boolean match = true;
        for (MediaStream mediaStream : ma1.getMediaStreams()) {
            boolean streamMatch = false;
            for (MediaStream stream : ma2.getMediaStreams()) {
                if (stream.equals(mediaStream)) {
                    streamMatch = true;
                }
            }
            if (!streamMatch) {
                match = false;
            }
        }
        return match;
    }


    public void setDisabledApplications(HashSet<MediaApplication> disabledApplications) {
        if (disabledApplications == null) {
            this.disabledApplications.clear();
        } else {
            this.disabledApplications = disabledApplications;
        }
        MatchConsumerProducer.logger.info("MatchConsumerProducer now works with " + this.disabledApplications.size() + " applications disabled.");
        if(MatchConsumerProducer.logger.isDebugEnabled()) {
            for (MediaApplication disabledApplication : this.disabledApplications) {
                MatchConsumerProducer.logger.debug("   disabled: " + disabledApplication);
            }
        }
    }
}
