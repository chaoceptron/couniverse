package AGC;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationProducer;
import NetworkRepresentation.EndpointNodeInterface;
import NetworkRepresentation.LogicalNetworkLink;
import java.util.ArrayList;

/**
 * An attempt to express the MatchMaker plan in some reasonable way
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 23.8.2007
 * Time: 13:19:29
 */
public class PlanElement {
    private MediaApplication mediaApplication;
    private ArrayList<LogicalNetworkLink> networkLinks = null;
    private EndpointNodeInterface consumerTargetInterface = null;

    /**
     * Constructor for plan element
     *
     * @param mediaApplication application to work with
     * @param networkLinks     link to send data over (should be null for consumers)
     */
    public PlanElement(MediaApplication mediaApplication, ArrayList<LogicalNetworkLink> networkLinks) {
        assert (!(mediaApplication instanceof MediaApplicationConsumer)) : "Use the other constructor for MediaApplicationConsumer";
        assert networkLinks != null : "networkLink must not be null";
        if (mediaApplication instanceof MediaApplicationProducer) {
            assert networkLinks.size() == 1 : "must not have more than one link for MediaApplicationProducer";
        }
        this.mediaApplication = mediaApplication;
        this.networkLinks = networkLinks;
    }

    /**
     * Constructor for plan element
     *
     * @param mediaApplication application to work with
     * @param consumerTargetInterface interface of corresponding sender interface to report back to
     */
    public PlanElement(MediaApplicationConsumer mediaApplication, EndpointNodeInterface consumerTargetInterface) {
        this.mediaApplication = (MediaApplication) mediaApplication;
        this.networkLinks = null;
        this.consumerTargetInterface = consumerTargetInterface;
    }


    public EndpointNodeInterface getConsumerTargetInterface() {
        return consumerTargetInterface;
    }

    public MediaApplication getMediaApplication() {
        return mediaApplication;
    }

    public ArrayList<LogicalNetworkLink> getNetworkLinks() {
        return networkLinks;
    }


    @Override
    public String toString() {
        assert mediaApplication != null;
        if (mediaApplication instanceof MediaApplicationConsumer) {
            assert consumerTargetInterface != null;
            return mediaApplication.toString() + " will be reporting back to " + consumerTargetInterface.getIpAddress();
        } else {
            StringBuilder targets = new StringBuilder();
            for (LogicalNetworkLink networkLink : networkLinks) {
                if (targets.length() != 0) {
                    targets.append(" ");
                }
                targets.append(networkLink.getToInterface().getIpAddress());
            }
            return mediaApplication.toString() + " will be sending to " + targets.toString();
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlanElement that = (PlanElement) o;

        if (consumerTargetInterface != null ? !consumerTargetInterface.equals(that.consumerTargetInterface) : that.consumerTargetInterface != null)
            return false;
        if (!mediaApplication.equals(that.mediaApplication)) return false;
        if (networkLinks != null ? !networkLinks.equals(that.networkLinks) : that.networkLinks != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = mediaApplication.hashCode();
        result = 31 * result + (networkLinks != null ? networkLinks.hashCode() : 0);
        result = 31 * result + (consumerTargetInterface != null ? consumerTargetInterface.hashCode() : 0);
        return result;
    }
}
