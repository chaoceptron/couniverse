package AGC;

/**
 * Created by IntelliJ IDEA.
 * User: hopet
 * Date: 28.11.2009
 * Time: 0:58:39
 * To change this template use File | Settings | File Templates.
 */
public class MatchMakerGurobiEvaluationConfig {
    public final boolean run_all,
            run_first,
            run_minimization,
            run_nored,
            run_nored_nosubopt,
            run_distances,
            run_eq_3_off,
            run_eq_4_off,
            run_eq_5_off,
            run_eq_6_off,
            run_eq_12a,
            run_eq_3_5_6_off,
            run_eq_3_4_5_6_off,
            run_eq_15_off,
            run_eq_16_off,
            run_eq_17_off,
            run_eq_18_off,
            run_eq_15_16_17_18_off,
            run_selector_1,
            run_selector_2,
            run_selector_3,
            run_selector_4,
            run_selector_5,
            run_selector_6,
            run_selector_7,
            run_selector_8;

    public MatchMakerGurobiEvaluationConfig(boolean run_all, boolean run_first, boolean run_minimization, boolean run_nored, boolean run_nored_nosubopt, boolean run_distances, boolean run_eq_3_off, boolean run_eq_4_off, boolean run_eq_5_off, boolean run_eq_6_off, boolean run_eq_12a, boolean run_eq_3_5_6_off, boolean run_eq_3_4_5_6_off, boolean run_eq_15_off, boolean run_eq_16_off, boolean run_eq_17_off, boolean run_eq_18_off, boolean run_eq_15_16_17_18_off, boolean run_selector_1, boolean run_selector_2, boolean run_selector_3, boolean run_selector_4, boolean run_selector_5, boolean run_selector_6, boolean run_selector_7, boolean run_selector_8) {
        this.run_all = run_all;
        this.run_first = run_first;
        this.run_minimization = run_minimization;
        this.run_nored = run_nored;
        this.run_nored_nosubopt = run_nored_nosubopt;
        this.run_distances =  run_distances;
        this.run_eq_3_off = run_eq_3_off;
        this.run_eq_4_off = run_eq_4_off;
        this.run_eq_5_off = run_eq_5_off;
        this.run_eq_6_off = run_eq_6_off;
        this.run_eq_12a = run_eq_12a;
        this.run_eq_3_5_6_off = run_eq_3_5_6_off;
        this.run_eq_3_4_5_6_off = run_eq_3_4_5_6_off;
        this.run_eq_15_off = run_eq_15_off;
        this.run_eq_16_off = run_eq_16_off;
        this.run_eq_17_off = run_eq_17_off;
        this.run_eq_18_off = run_eq_18_off;
        this.run_eq_15_16_17_18_off = run_eq_15_16_17_18_off;
        this.run_selector_1 = run_selector_1;
        this.run_selector_2 = run_selector_2;
        this.run_selector_3 = run_selector_3;
        this.run_selector_4 = run_selector_4;
        this.run_selector_5 = run_selector_5;
        this.run_selector_6 = run_selector_6;
        this.run_selector_7 = run_selector_7;
        this.run_selector_8 = run_selector_8;
    }

    @Override
    public String toString() {
        return "MatchMakerGurobiEvaluationConfig{" +
                "run_all=" + run_all +
                ", run_first=" + run_first +
                ", run_minimization=" + run_minimization +
                ", run_nored=" + run_nored +
                ", run_nored_nosubopt=" + run_nored_nosubopt +
                ", run_distances=" + run_distances +
                ", run_eq_3_off=" + run_eq_3_off +
                ", run_eq_4_off=" + run_eq_4_off +
                ", run_eq_5_off=" + run_eq_5_off +
                ", run_eq_6_off=" + run_eq_6_off +
                ", run_eq_12a=" + run_eq_12a +
                ", run_eq_3_5_6_off=" + run_eq_3_5_6_off +
                ", run_eq_3_4_5_6_off=" + run_eq_3_4_5_6_off +
                ", run_eq_15_off=" + run_eq_15_off +
                ", run_eq_16_off=" + run_eq_16_off +
                ", run_eq_17_off=" + run_eq_17_off +
                ", run_eq_18_off=" + run_eq_18_off +
                ", run_eq_15_16_17_18_off=" + run_eq_15_16_17_18_off +
                ", run_selector_1=" + run_selector_1 +
                ", run_selector_2=" + run_selector_2 +
                ", run_selector_3=" + run_selector_3 +
                ", run_selector_4=" + run_selector_4 +
                ", run_selector_5=" + run_selector_5 +
                ", run_selector_6=" + run_selector_6 +
                ", run_selector_7=" + run_selector_7 +
                ", run_selector_8=" + run_selector_8 +
                '}';
    }
}
