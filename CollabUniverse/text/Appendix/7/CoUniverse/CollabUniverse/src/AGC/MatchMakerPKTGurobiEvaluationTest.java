package AGC;

import MediaAppFactory.MediaApplication;
import MediaApplications.RumHD;
import MediaApplications.UltraGrid1500Consumer;
import MediaApplications.UltraGrid1500Producer;
import NetworkRepresentation.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CopyOnWriteArraySet;
import junit.framework.TestCase;
import org.junit.Test;
import utils.MyLogger;

/**
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class MatchMakerPKTGurobiEvaluationTest extends TestCase {

    private static final int DISTRIBUTORS_PER_ACTIVE_SITE = 2;
    private static final int PRODUCERS_PER_ACTIVE_SITE = 1;
    private static final String MAIN_SUBNET_NAME = "10GE Network";
    private static final String MAIN_INTERFACE_NAME = "10GE";
    private static final String INTERNET_INTERFACE_NAME = "1GE";

    private enum TestSite {

        BRNO_HD(new NetworkSite("Brno HD"), 1),
        CHICAGO_EVL(new NetworkSite("Chicago EVL"), 2),
        PRAGUE_MEDIANET(new NetworkSite("Prague Medianet"), 3),
        SCINET(new NetworkSite("SCinet"), 4),
        LSU_LONI(new NetworkSite("LSU/LONI"), 5),
        UCSD(new NetworkSite("UCSD CalIT"), 6),
        CHICAGO_ICAIR(new NetworkSite("Chicago ICAIR"), 7),
        BRNO_SAGE(new NetworkSite("Brno SAGE"), 8),
        BETHESDA(new NetworkSite("NIH/NLM Bethesda"), 9);
        
        private final int order;
        private final NetworkSite site;

        private TestSite(NetworkSite site, int order) {
            this.site = site;
            this.order = order;
        }

        public int getOrder() {
            return this.order;
        }

        public static int getOrder(NetworkSite site) {
            for (TestSite s : TestSite.values()) {
                if (s.site.equals(site)) {
                    return s.order;
                }
            }
            return -1;
        }

        public NetworkSite getNetworkSite() {
            return this.site;
        }

        public static NetworkSite getNetworkSiteByOrder(int order) {
            for (TestSite s : TestSite.values()) {
                if (s.order == order) {
                    return s.site;
                }
            }
            return null;
        }

        public TestSite getByOrder(int order) {
            for (TestSite s : TestSite.values()) {
                if (s.order == order) {
                    return s;
                }
            }
            return null;
        }
    }

    private enum TestNetworkNode {

        BRNO_JUNIPER("Brno Juniper EX 4500", 1, true),
        BRNO_HP("Brno HP 54xx", 2, true),
        PRAGUE_FORCE10("Prague Medianet Force10 E300", 3, true),
        CHICAGO_FORCE10("Chicago Force10 E1200", 4, true),
        CHICAGO_CISCO("Chicago Cisco 6500", 5, true),
        LSU_1GE("LSU 1GE", 6, true),
        LSU_10GE("LSU 10GE", 7, true),
        NETWORK_PUBLIC_INTERNET("Public IP Internet", 8, false),
        NETWORK_OPTIPUTER("Optiputer Wave Network", 9, false),
        NETWORK_PRAGUE_MEDIANET("Prague Medianet Network", 10, false),
        NETWORK_CHICAGO_EVL("Chicago EVL Network", 11, false),
        NETWORK_CHICAGO_ICAIR("Chicago iCAIR Network", 12, false),
        NETWORK_BETHESDA("Bethesda NIH/NLM Network", 13, false),
        NETWORK_SCINET("SCinet Network", 14, false),
        NETWORK_UCSD("UCSD Network", 15, false);
        private final String name;
        private final int order;
        private final boolean known;

        private TestNetworkNode(String name, int order, boolean known) {
            this.name = name;
            this.order = order;
            this.known = known;

//            for (TestNetworkNode node : TestNetworkNode.values()) {
//                if (node.order == order) {
//                    System.out.println("ERROR: Network node order collision: " + order);
//                }
//            }
        }

        public boolean isKnown() {
            return known;
        }

        public String getName() {
            return name;
        }

        public int getOrder() {
            return order;
        }

        public static int maxOrder() {
            int maxOrder = -1;
            for (TestNetworkNode n : TestNetworkNode.values()) {
                if (n.order > maxOrder) {
                    maxOrder = n.order;
                }
            }
            return maxOrder;
        }

        public static ArrayList<GeneralNetworkNode> buildNodeList() {
            ArrayList<GeneralNetworkNode> list = new ArrayList<GeneralNetworkNode>(TestNetworkNode.maxOrder() + 1);
            list.add(0, null);

            for (TestNetworkNode node : TestNetworkNode.values()) {
                GeneralNetworkNode tempNode = null;
                try {
                    tempNode = list.get(node.order);
                } catch (IndexOutOfBoundsException ex) {
                    // empty - there is no such node and we're lucky
                }
                if (tempNode != null) {
                    System.out.println("ERROR: Network node " + node.order + " added twice.");
                }
                if (node.isKnown()) {
                    list.add(node.order, new PhysicalNetworkNode(node.name));
                } else {
                    list.add(node.order, new UnknownNetworkNode(node.name));
                }
            }

            return list;
        }
    }

    private class NetworkNodeList {

        private ArrayList<GeneralNetworkNode> nodes;

        public NetworkNodeList() {
            this.nodes = TestNetworkNode.buildNodeList();
        }

        public GeneralNetworkNode get(int index) {
            return this.nodes.get(index);
        }

        public PhysicalNetworkNode getPhysicalNode(int index) {
            if (this.nodes.get(index) instanceof PhysicalNetworkNode) {
                return (PhysicalNetworkNode) this.nodes.get(index);
            } else {
                return null;
            }
        }

        public UnknownNetworkNode getUnknownNode(int index) {
            if (this.nodes.get(index) instanceof UnknownNetworkNode) {
                return (UnknownNetworkNode) this.nodes.get(index);
            } else {
                return null;
            }
        }

        public ArrayList<PhysicalNetworkNode> getAllPhysicalNodes() {
            ArrayList<PhysicalNetworkNode> list = new ArrayList<PhysicalNetworkNode>();

            for (GeneralNetworkNode node : this.nodes) {
                if (node == null) {
                    continue;
                }
                if (node instanceof PhysicalNetworkNode) {
                    list.add((PhysicalNetworkNode) node);
                }
            }

            return list;
        }

        public ArrayList<UnknownNetworkNode> getAllUnknownNodes() {
            ArrayList<UnknownNetworkNode> list = new ArrayList<UnknownNetworkNode>();

            for (GeneralNetworkNode node : this.nodes) {
                if (node == null) {
                    continue;
                }
                if (node instanceof UnknownNetworkNode) {
                    list.add((UnknownNetworkNode) node);
                }
            }

            return list;
        }
    }

    private enum TestPhysicalLink {

        BRNO_JUNIPER_HP(TestNetworkNode.BRNO_JUNIPER, TestNetworkNode.BRNO_HP, 10000, 1, 2),
        BRNO_HP_JUNIPER(TestNetworkNode.BRNO_HP, TestNetworkNode.BRNO_JUNIPER, 10000, 2, 1),
        BRNO_INTERNET(TestNetworkNode.BRNO_HP, TestNetworkNode.NETWORK_PUBLIC_INTERNET, 10000, 3, 4),
        INTERNET_BRNO(TestNetworkNode.NETWORK_PUBLIC_INTERNET, TestNetworkNode.BRNO_HP, 10000, 4, 3),
        BRNO_PRAGUE(TestNetworkNode.BRNO_JUNIPER, TestNetworkNode.PRAGUE_FORCE10, 10000, 5, 6),
        PRAGUE_BRNO(TestNetworkNode.PRAGUE_FORCE10, TestNetworkNode.BRNO_JUNIPER, 10000, 6, 5),
        PRAGUE_FORCE10_MEDIANET(TestNetworkNode.PRAGUE_FORCE10, TestNetworkNode.NETWORK_PRAGUE_MEDIANET, 10000, 7, 8),
        PRAGUE_MEDIANET_FORCE10(TestNetworkNode.NETWORK_PRAGUE_MEDIANET, TestNetworkNode.PRAGUE_FORCE10, 10000, 8, 7),
        PRAGUE_CHICAGO(TestNetworkNode.PRAGUE_FORCE10, TestNetworkNode.CHICAGO_FORCE10, 10000, 9, 10),
        CHICAGO_PRAGUE(TestNetworkNode.CHICAGO_FORCE10, TestNetworkNode.PRAGUE_FORCE10, 10000, 10, 9),
        CHICAGO_FORCE10_EVL(TestNetworkNode.CHICAGO_FORCE10, TestNetworkNode.NETWORK_CHICAGO_EVL, 10000, 11, 12),
        CHICAGO_EVL_FORCE10(TestNetworkNode.NETWORK_CHICAGO_EVL, TestNetworkNode.CHICAGO_FORCE10, 10000, 12, 11),
        CHICAGO_FORCE10_ICAIR(TestNetworkNode.CHICAGO_FORCE10, TestNetworkNode.NETWORK_CHICAGO_ICAIR, 10000, 13, 14),
        CHICAGO_ICAIR_FORCE10(TestNetworkNode.NETWORK_CHICAGO_ICAIR, TestNetworkNode.CHICAGO_FORCE10, 10000, 14, 13),
        CHICAGO_FORCE10_CISCO(TestNetworkNode.CHICAGO_FORCE10, TestNetworkNode.CHICAGO_CISCO, 10000, 15, 16),
        CHICAGO_CISCO_FORCE10(TestNetworkNode.CHICAGO_CISCO, TestNetworkNode.CHICAGO_FORCE10, 10000, 16, 15),
        CHICAGO_FORCE10_OPTIPUTER(TestNetworkNode.CHICAGO_FORCE10, TestNetworkNode.NETWORK_OPTIPUTER, 10000, 17, 18),
        OPTIPUTER_CHICAGO_FORCE10(TestNetworkNode.NETWORK_OPTIPUTER, TestNetworkNode.CHICAGO_FORCE10, 10000, 18, 17),
        UCSD_OPTIPUTER(TestNetworkNode.NETWORK_UCSD, TestNetworkNode.NETWORK_OPTIPUTER, 10000, 19, 20),
        OPTIPUTER_UCSD(TestNetworkNode.NETWORK_OPTIPUTER, TestNetworkNode.NETWORK_UCSD, 10000, 20, 19),
        SCINET_OPTIPUTER(TestNetworkNode.NETWORK_SCINET, TestNetworkNode.NETWORK_OPTIPUTER, 10000, 21, 22),
        OPTIPUTER_SCINET(TestNetworkNode.NETWORK_OPTIPUTER, TestNetworkNode.NETWORK_SCINET, 10000, 22, 21),
        SCINET_INTERNET(TestNetworkNode.NETWORK_SCINET, TestNetworkNode.NETWORK_PUBLIC_INTERNET, 10000, 23, 24),
        INTERNET_SCINET(TestNetworkNode.NETWORK_PUBLIC_INTERNET, TestNetworkNode.NETWORK_SCINET, 10000, 24, 23),
        CHICAGO_LSU(TestNetworkNode.CHICAGO_CISCO, TestNetworkNode.LSU_10GE, 10000, 25, 26),
        LSU_CHICAGO(TestNetworkNode.LSU_10GE, TestNetworkNode.CHICAGO_CISCO, 10000, 26, 25),
        INTERNET_LSU(TestNetworkNode.NETWORK_PUBLIC_INTERNET, TestNetworkNode.LSU_1GE, 10000, 27, 28),
        LSU_INTERNET(TestNetworkNode.LSU_1GE, TestNetworkNode.NETWORK_PUBLIC_INTERNET, 10000, 28, 27),
        INTERNET_BETHESDA(TestNetworkNode.NETWORK_PUBLIC_INTERNET, TestNetworkNode.NETWORK_BETHESDA, 10000, 29, 30),
        BETHESDA_INTERNET(TestNetworkNode.NETWORK_BETHESDA, TestNetworkNode.NETWORK_PUBLIC_INTERNET, 10000, 30, 29),
        EVL_INTERNET(TestNetworkNode.NETWORK_CHICAGO_EVL, TestNetworkNode.NETWORK_PUBLIC_INTERNET, 10000, 31, 32),
        INTERNET_EVL(TestNetworkNode.NETWORK_PUBLIC_INTERNET, TestNetworkNode.NETWORK_CHICAGO_EVL, 10000, 32, 31);
        private final TestNetworkNode fromNode;
        private final TestNetworkNode toNode;
        private final int capacity; // in Mbits per second
        private final int order;
        private final int reverse_order;

        private TestPhysicalLink(TestNetworkNode fromNode, TestNetworkNode toNode, int capacity, int order, int reverse_order) {
            this.fromNode = fromNode;
            this.toNode = toNode;
            this.capacity = capacity;
            this.order = order;
            this.reverse_order = reverse_order;

//            for (TestPhysicalLink l : TestPhysicalLink.values()) {
//                if (l.order == order) {
//                    System.out.println("ERROR: Physical link order collision: " + order);
//                }
//            }
        }

        private TestPhysicalLink(TestNetworkNode fromNode, TestNetworkNode toNode, int capacity, int order) {
            this(fromNode, toNode, capacity, order, -1);
        }

        public int getOrder() {
            return this.order;
        }

        @Override
        public String toString() {
            return this.fromNode + " --> " + this.toNode;
        }

        public static int getMaxOrder() {
            int maxOrder = -1;
            for (TestPhysicalLink l : TestPhysicalLink.values()) {
                if (l.order > maxOrder) {
                    maxOrder = l.order;
                }
            }
            return maxOrder;
        }

        public static ArrayList<PhysicalNetworkLink> buildLinkList(NetworkNodeList nodes) {
            ArrayList<PhysicalNetworkLink> list = new ArrayList<PhysicalNetworkLink>(TestPhysicalLink.getMaxOrder() + 1);
            list.add(0, null);

            for (TestPhysicalLink link : TestPhysicalLink.values()) {
                PhysicalNetworkLink tempLink = null;
                try {
                    tempLink = list.get(link.order);
                } catch (IndexOutOfBoundsException ex) {
                    // empty - index not found, everything's fine
                }
                if (tempLink != null) {
                    System.out.println("ERROR: Physical link " + link.order + " added twice.");
                }
                if (nodes.get(link.fromNode.getOrder()) == null
                        || nodes.get(link.toNode.getOrder()) == null) {
                    System.out.println("ERROR: Link ending at null node.");
                    continue;
                }

                list.add(link.order, new PhysicalNetworkLink(link.toString(), link.capacity, nodes.get(link.fromNode.getOrder()), nodes.get(link.toNode.getOrder()), true));
            }

            PhysicalNetworkLink backLink;
            for (TestPhysicalLink link : TestPhysicalLink.values()) {
                try {
                    backLink = list.get(link.reverse_order);
                } catch (IndexOutOfBoundsException e) {
                    backLink = null;
                }

                list.get(link.order).setBackLink(backLink);
            }

            return list;
        }
    }

    private class PhysicalLinkList {

        private ArrayList<PhysicalNetworkLink> links;

        public PhysicalLinkList(NetworkNodeList nodes) {
            this.links = TestPhysicalLink.buildLinkList(nodes);
        }

        public PhysicalNetworkLink get(int index) {
            return links.get(index);
        }

        public ArrayList<PhysicalNetworkLink> getAllLinks() {
            return links;
        }
    }

    private enum IntersiteLink {

        BRNO_HD_BRNO_SAGE(TestSite.BRNO_HD, TestSite.BRNO_SAGE, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_JUNIPER_HP)), 1),
        BRNO_HD_PRAGUE_MEDIANET(TestSite.BRNO_HD, TestSite.PRAGUE_MEDIANET, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_FORCE10_MEDIANET)), 2),
        BRNO_SAGE_PRAGUE_MEDIANET(TestSite.BRNO_SAGE, TestSite.PRAGUE_MEDIANET, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_FORCE10_MEDIANET)), 3),
        BRNO_HD_CHICAGO_EVL(TestSite.BRNO_HD, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 4),
        BRNO_SAGE_CHICAGO_EVL(TestSite.BRNO_SAGE, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 5),
        BRNO_HD_CHICAGO_ICAIR(TestSite.BRNO_HD, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 6),
        BRNO_SAGE_CHICAGO_ICAIR(TestSite.BRNO_SAGE, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 7),
        BRNO_HD_UCSD(TestSite.BRNO_HD, TestSite.UCSD, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_UCSD)), 8),
        BRNO_SAGE_UCSD(TestSite.BRNO_SAGE, TestSite.UCSD, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_UCSD)), 9),
        BRNO_HD_SCINET(TestSite.BRNO_HD, TestSite.SCINET, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_SCINET)), 10),
        BRNO_SAGE_SCINET(TestSite.BRNO_SAGE, TestSite.SCINET, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_SCINET)), 11),
        BRNO_HD_LSU(TestSite.BRNO_HD, TestSite.LSU_LONI, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_CISCO, TestPhysicalLink.CHICAGO_LSU)), 12),
        BRNO_SAGE_LSU(TestSite.BRNO_SAGE, TestSite.LSU_LONI, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_HP_JUNIPER, TestPhysicalLink.BRNO_PRAGUE, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_CISCO, TestPhysicalLink.CHICAGO_LSU)), 13),
        PRAGUE_CHICAGO_EVL(TestSite.PRAGUE_MEDIANET, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.PRAGUE_MEDIANET_FORCE10, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 14),
        PRAGUE_CHICAGO_ICAIR(TestSite.PRAGUE_MEDIANET, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.PRAGUE_MEDIANET_FORCE10, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 15),
        CHICAGO_EVL_CHICAGO_ICAIR(TestSite.CHICAGO_EVL, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.CHICAGO_EVL_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 16),
        UCSD_CHICAGO_EVL(TestSite.UCSD, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.UCSD_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 17),
        UCSD_CHICAGO_ICAIR(TestSite.UCSD, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.UCSD_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 18),
        SCINET_CHICAGO_EVL(TestSite.SCINET, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.SCINET_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 19),
        SCINET_CHICAGO_ICAIR(TestSite.SCINET, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.SCINET_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 20),
        LSU_CHICAGO_EVL(TestSite.LSU_LONI, TestSite.CHICAGO_EVL, new ArrayList(Arrays.asList(TestPhysicalLink.LSU_CHICAGO, TestPhysicalLink.CHICAGO_CISCO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_EVL)), 21),
        LSU_CHICAGO_ICAIR(TestSite.LSU_LONI, TestSite.CHICAGO_ICAIR, new ArrayList(Arrays.asList(TestPhysicalLink.LSU_CHICAGO, TestPhysicalLink.CHICAGO_CISCO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_ICAIR)), 22),
        PRAGUE_UCSD(TestSite.PRAGUE_MEDIANET, TestSite.UCSD, new ArrayList(Arrays.asList(TestPhysicalLink.PRAGUE_MEDIANET_FORCE10, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_UCSD)), 23),
        PRAGUE_SCINET(TestSite.PRAGUE_MEDIANET, TestSite.SCINET, new ArrayList(Arrays.asList(TestPhysicalLink.PRAGUE_MEDIANET_FORCE10, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_OPTIPUTER, TestPhysicalLink.OPTIPUTER_SCINET)), 24),
        PRAGUE_LSU(TestSite.PRAGUE_MEDIANET, TestSite.LSU_LONI, new ArrayList(Arrays.asList(TestPhysicalLink.PRAGUE_MEDIANET_FORCE10, TestPhysicalLink.PRAGUE_CHICAGO, TestPhysicalLink.CHICAGO_FORCE10_CISCO, TestPhysicalLink.CHICAGO_LSU)), 25),
        UCSD_SCINET(TestSite.UCSD, TestSite.SCINET, new ArrayList(Arrays.asList(TestPhysicalLink.UCSD_OPTIPUTER, TestPhysicalLink.OPTIPUTER_SCINET)), 26),
        UCSD_LSU(TestSite.UCSD, TestSite.LSU_LONI, new ArrayList(Arrays.asList(TestPhysicalLink.UCSD_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_CISCO, TestPhysicalLink.CHICAGO_LSU)), 27),
        SCINET_LSU(TestSite.SCINET, TestSite.LSU_LONI, new ArrayList(Arrays.asList(TestPhysicalLink.SCINET_OPTIPUTER, TestPhysicalLink.OPTIPUTER_CHICAGO_FORCE10, TestPhysicalLink.CHICAGO_FORCE10_CISCO, TestPhysicalLink.CHICAGO_LSU)), 28);
//        BRNO_HD_BETHESDA(TestSite.BRNO_HD, TestSite.BETHESDA, new ArrayList(Arrays.asList(TestPhysicalLink.BRNO_JUNIPER_HP, TestPhysicalLink.BRNO_INTERNET, TestPhysicalLink.INTERNET_BETHESDA, TestPhysicalLink.CHICAGO_LSU)), XXX),
        private final String subnet;
        private final TestSite fromSite;
        private final TestSite toSite;
        private final ArrayList<TestPhysicalLink> physicals;
        private int order;

        private IntersiteLink(String subnet, TestSite fromSite, TestSite toSite, ArrayList<TestPhysicalLink> physicals, int order) {
            this.subnet = subnet;
            this.fromSite = fromSite;
            this.toSite = toSite;
            this.physicals = physicals;
            this.order = order;

//            for (IntersiteLink link : IntersiteLink.values()) {
//                if (fromSite.equals(link.getFromSite()) &&
//                    toSite.equals(link.getToSite()) && 
//                    subnet.equals(link.getSubnet())) {
//                    System.out.println("ERROR: Intersite link association collision: " + order + "vs. " + link.getOrder());
//                    break;
//                }
//            }
        }

        private IntersiteLink(TestSite fromSite, TestSite toSite, ArrayList<TestPhysicalLink> physicals, int order) {
            this(MAIN_SUBNET_NAME, fromSite, toSite, physicals, order);
        }

        public String getSubnet() {
            return subnet;
        }

        public TestSite getFromSite() {
            return fromSite;
        }

        public TestSite getToSite() {
            return toSite;
        }

        public ArrayList<TestPhysicalLink> getPhysicals() {
            return physicals;
        }

        public int getOrder() {
            return order;
        }
    }

    private void addAllKnownNetworkNodes(PartiallyKnownNetworkTopology topology, NetworkNodeList nodes) {
        for (PhysicalNetworkNode node : nodes.getAllPhysicalNodes()) {
            topology.addNetworkNode(node);
        }

        for (UnknownNetworkNode node : nodes.getAllUnknownNodes()) {
            topology.addNetworkNode(node);
        }
    }

    private void addAllKnownNetworkLinks(PartiallyKnownNetworkTopology topology, PhysicalLinkList links) {
        for (PhysicalNetworkLink link : links.getAllLinks()) {
            if (link != null) {
                topology.addPhysicalLink(link);
            }
        }
    }

    private ArrayList<EndpointNetworkNode> createSiteEndpointNodes(NetworkSite site, String namePrefix, int count) {
        ArrayList<EndpointNetworkNode> nodes = new ArrayList<EndpointNetworkNode>(count);
        nodes.add(0, null);
        for (int i = 1; i <= count; i++) {
            nodes.add(i, new EndpointNetworkNode(namePrefix + i, "", null, false, false, site, new CopyOnWriteArraySet<MediaApplication>(), new CopyOnWriteArraySet<EndpointNodeInterface>()));
        }
        return nodes;
    }

    private void addEndpointInterfaces(ArrayList<EndpointNetworkNode> nodes, String name, String ipPrefix, String mask, int bandwidth, String subnet) {
        for (EndpointNetworkNode node : nodes) {
            if (node != null) {
                node.addInterface(new EndpointNodeInterface(name, ipPrefix + "." + nodes.indexOf(node), mask, bandwidth, subnet, node));
            }
        }
    }

    private void connectEndpointsToNode(PartiallyKnownNetworkTopology topology, ArrayList<EndpointNetworkNode> endpoints, GeneralNetworkNode node, int capacity, String nodeInterfaceName) {
        PhysicalNetworkLink link;
        EndpointNodeInterface iface;

        for (EndpointNetworkNode endpoint : endpoints) {
            if (endpoint == null) {continue;}
            iface = endpoint.getNodeInterface(nodeInterfaceName);
            link = new PhysicalNetworkLink("", capacity, endpoint, node, true, iface);
            topology.addPhysicalLink(link);
            link = new PhysicalNetworkLink("", capacity, node, endpoint, true, iface);
            topology.addPhysicalLink(link);
        }
    }

    private void addAllEndpointsToTopology(PartiallyKnownNetworkTopology topology, ArrayList<EndpointNetworkNode> nodes) {
        for (EndpointNetworkNode node : nodes) {
            if (node != null) {
                topology.addNetworkNode(node);
            }
        }
    }

    private void addApplications(ArrayList<EndpointNetworkNode> nodes, int siteOrder, int siteCount, String topologyCfg, int primarySite) {
        boolean passedSelfSite = false;

        for (EndpointNetworkNode node : nodes) {
            if (node == null) { continue; }
            int order = nodes.indexOf(node);
            if (order <= DISTRIBUTORS_PER_ACTIVE_SITE) {
                // add distributor application
                node.addApplication(new RumHD());
            } else if (order <= DISTRIBUTORS_PER_ACTIVE_SITE + PRODUCERS_PER_ACTIVE_SITE) {
                // add producer application
                node.addApplication(new UltraGrid1500Producer());
            } else {
                int consumerOrder = order - DISTRIBUTORS_PER_ACTIVE_SITE - PRODUCERS_PER_ACTIVE_SITE;
                int producingSiteOrder = (consumerOrder + primarySite - 2) % siteCount + 1;
                if (producingSiteOrder == siteOrder) { // we do not need producer for ourselves
                    passedSelfSite = true;
                }
                if (passedSelfSite) {
                    producingSiteOrder++;
                    if (producingSiteOrder > siteCount) {
                        producingSiteOrder = 1;
                    }
                }
                UltraGrid1500Consumer ug1500cons = new UltraGrid1500Consumer();
                ug1500cons.setSourceSite(TestSite.getNetworkSiteByOrder(producingSiteOrder));
                node.addApplication(ug1500cons);
                // only one consumer is created for non-primary sites in 1:n scenarios
                if (siteOrder != primarySite && topologyCfg.equals("1")) {
                    break;
                }
            }
        }
    }

    private void addApplicationsUnknown(ArrayList<EndpointNetworkNode> nodes, int siteOrder, ArrayList<NetworkSite> siteList, int siteCount, String topologyCfg, int primarySite) {
        boolean passedSelfSite = false;

        for (EndpointNetworkNode node : nodes) {
            if (node == null) { continue; }
            int order = nodes.indexOf(node);
            if (order <= DISTRIBUTORS_PER_ACTIVE_SITE) {
                // add distributor application
                node.addApplication(new RumHD());
            } else if (order <= DISTRIBUTORS_PER_ACTIVE_SITE + PRODUCERS_PER_ACTIVE_SITE) {
                // add producer application
                node.addApplication(new UltraGrid1500Producer());
            } else {
                int consumerOrder = order - DISTRIBUTORS_PER_ACTIVE_SITE - PRODUCERS_PER_ACTIVE_SITE;
                int producingSiteOrder = (consumerOrder + primarySite - 2) % siteCount + 1;
                if (producingSiteOrder == siteOrder) { // we do not need producer for ourselves
                    passedSelfSite = true;
                }
                if (passedSelfSite) {
                    producingSiteOrder++;
                    if (producingSiteOrder > siteCount) {
                        producingSiteOrder = 1;
                    }
                }
                UltraGrid1500Consumer ug1500cons = new UltraGrid1500Consumer();
                ug1500cons.setSourceSite(siteList.get(producingSiteOrder));
                node.addApplication(ug1500cons);
                // only one consumer is created for non-primary sites in 1:n scenarios
                if (siteOrder != primarySite && topologyCfg.equals("1")) {
                    break;
                }
            }
        }
    }

    private ArrayList<ArrayList<EndpointNetworkNode>> createSites(int siteCount, int primarySite, String topologyCfg, PartiallyKnownNetworkTopology topology) {
        ArrayList<ArrayList<EndpointNetworkNode>> siteNodes = new ArrayList<ArrayList<EndpointNetworkNode>>();
        int consumersPerTypicalSite = (topologyCfg.equals("1")) ? 1 : siteCount - 1;

        siteNodes.add(0, null);
        for (TestSite site : TestSite.values()) {
            if (site.getOrder() > siteCount) {
                continue;
            }
            int nodeCount = DISTRIBUTORS_PER_ACTIVE_SITE + PRODUCERS_PER_ACTIVE_SITE + (site.getOrder() == primarySite ? siteCount - 1 : consumersPerTypicalSite);
            siteNodes.add(site.getOrder(), createSiteEndpointNodes(site.getNetworkSite(), site.getNetworkSite().getSiteName() + " ", nodeCount));
            addEndpointInterfaces(siteNodes.get(site.getOrder()), MAIN_INTERFACE_NAME, "10.10." + site.getOrder(), "255.255.255.0", 10000, MAIN_SUBNET_NAME);
            addAllEndpointsToTopology(topology, siteNodes.get(site.getOrder()));
            addApplications(siteNodes.get(site.getOrder()), site.getOrder(), siteCount, topologyCfg, primarySite);
        }

        return siteNodes;
    }

    private void buildIntersiteAssociations(PartiallyKnownNetworkTopology topology, PhysicalLinkList physicalLinks) {
        for (IntersiteLink link : IntersiteLink.values()) {
            ArrayList<PhysicalNetworkLink> forthLinks = new ArrayList<PhysicalNetworkLink>(link.getPhysicals().size());
            ArrayList<PhysicalNetworkLink> backLinks = new ArrayList<PhysicalNetworkLink>(link.getPhysicals().size());
            for (TestPhysicalLink physical : link.getPhysicals()) {
                forthLinks.add(physicalLinks.get(physical.getOrder()));
                backLinks.add(physicalLinks.get(physical.getOrder()).getBackLink());
            }
            topology.associateIntersiteLinks(link.getFromSite().getNetworkSite(), link.getToSite().getNetworkSite(), link.getSubnet(), forthLinks);
            topology.associateIntersiteLinks(link.getToSite().getNetworkSite(), link.getFromSite().getNetworkSite(), link.getSubnet(), backLinks);
        }
    }

    private PartiallyKnownNetworkTopology createNetworkTopology(int sites, String topologyCfg, String reflectorsCfg, int primarySite) {
        PartiallyKnownNetworkTopology networkTopology = new PartiallyKnownNetworkTopology();
        networkTopology.setName(topologyCfg + "n" + reflectorsCfg + sites);
        ArrayList<ArrayList<EndpointNetworkNode>> siteNodes;
        NetworkNodeList networkNodes = new NetworkNodeList();
        PhysicalLinkList physicalLinks = new PhysicalLinkList(networkNodes);
        final int consumersPerSite = (topologyCfg.equals("1") ? 1 : (sites));

        // create and add all known network nodes and physical links
        addAllKnownNetworkNodes(networkTopology, networkNodes);
        addAllKnownNetworkLinks(networkTopology, physicalLinks);

        // create sites
        siteNodes = createSites(sites, primarySite, topologyCfg, networkTopology);

        // Brno HD
        addEndpointInterfaces(siteNodes.get(TestSite.BRNO_HD.getOrder()), INTERNET_INTERFACE_NAME, "10.20." + TestSite.BRNO_HD.getOrder(), "255.255.255.0", 1000, EndpointSubNetwork.getPublicNetName());
        connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.BRNO_HD.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.BRNO_HP.getOrder()), 1000, INTERNET_INTERFACE_NAME);
        connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.BRNO_HD.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.BRNO_JUNIPER.getOrder()), 10000, MAIN_INTERFACE_NAME);

        // Brno SAGE endpoint nodes
        if (TestSite.BRNO_SAGE.getOrder() <= sites) {
            addEndpointInterfaces(siteNodes.get(TestSite.BRNO_SAGE.getOrder()), INTERNET_INTERFACE_NAME, "10.20." + TestSite.BRNO_SAGE.getOrder(), "255.255.255.0", 1000, EndpointSubNetwork.getPublicNetName());
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.BRNO_SAGE.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.BRNO_HP.getOrder()), 1000, INTERNET_INTERFACE_NAME);
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.BRNO_SAGE.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.BRNO_JUNIPER.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }
        // Prague

        if (TestSite.PRAGUE_MEDIANET.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.PRAGUE_MEDIANET.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_PRAGUE_MEDIANET.getOrder()), 10000, MAIN_INTERFACE_NAME);
            // Prague Medianet endpoint nodes
        }
       
        // Chicago EVL network
        if (TestSite.CHICAGO_EVL.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.CHICAGO_EVL.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_CHICAGO_EVL.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // Chicago iCAIR network
        if (TestSite.CHICAGO_ICAIR.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.CHICAGO_ICAIR.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_CHICAGO_ICAIR.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // Bethesda
        if (TestSite.BETHESDA.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.BETHESDA.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_BETHESDA.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // LSU/LONI
        if (TestSite.LSU_LONI.getOrder() <= sites) {
            addEndpointInterfaces(siteNodes.get(TestSite.LSU_LONI.getOrder()), INTERNET_INTERFACE_NAME, "10.20." + TestSite.LSU_LONI.getOrder(), "255.255.255.0", 1000, EndpointSubNetwork.getPublicNetName());
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.LSU_LONI.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.LSU_1GE.getOrder()), 1000, INTERNET_INTERFACE_NAME);
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.LSU_LONI.getOrder()), networkNodes.getPhysicalNode(TestNetworkNode.LSU_10GE.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // SCinet
        if (TestSite.SCINET.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.SCINET.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_SCINET.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // UCSD CalIT
        if (TestSite.UCSD.getOrder() <= sites) {
            connectEndpointsToNode(networkTopology, siteNodes.get(TestSite.UCSD.getOrder()), networkNodes.getUnknownNode(TestNetworkNode.NETWORK_UCSD.getOrder()), 10000, MAIN_INTERFACE_NAME);
        }

        // physcial - logical link associations (intersite)
        buildIntersiteAssociations(networkTopology, physicalLinks);

        // physical - logical link associations ("last mile")
        // Brno HD
        if (TestSite.BRNO_HD.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.BRNO_HD.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.BRNO_JUNIPER.getOrder()));
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.BRNO_HD.getOrder()), INTERNET_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.BRNO_HP.getOrder()));
        }
        // Brno SAGE
        if (TestSite.BRNO_SAGE.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.BRNO_SAGE.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.BRNO_HP.getOrder()));
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.BRNO_SAGE.getOrder()), INTERNET_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.BRNO_HP.getOrder()));
        }
        // Prague
        if (TestSite.PRAGUE_MEDIANET.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.PRAGUE_MEDIANET.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_PRAGUE_MEDIANET.getOrder()));
        }
        // Chicago EVL
        if (TestSite.CHICAGO_EVL.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.CHICAGO_EVL.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_CHICAGO_EVL.getOrder()));
        }
        // Chicago iCAIR
        if (TestSite.CHICAGO_ICAIR.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.CHICAGO_ICAIR.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_CHICAGO_ICAIR.getOrder()));
        }
        // Bethesda
        if (TestSite.BETHESDA.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.BETHESDA.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_BETHESDA.getOrder()));
        }
        // LSU
        if (TestSite.LSU_LONI.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.LSU_LONI.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.LSU_10GE.getOrder()));
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.LSU_LONI.getOrder()), INTERNET_INTERFACE_NAME, networkNodes.getPhysicalNode(TestNetworkNode.LSU_1GE.getOrder()));
        }
        // SCinet
        if (TestSite.SCINET.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.SCINET.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_SCINET.getOrder()));
        }
        // UCSD
        if (TestSite.UCSD.getOrder() <= sites) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(TestSite.UCSD.getOrder()), MAIN_INTERFACE_NAME, networkNodes.getUnknownNode(TestNetworkNode.NETWORK_UCSD.getOrder()));
        }
        return networkTopology;
    }

    private PartiallyKnownNetworkTopology createUnknownNetworkTopology(int sites, String topologyCfg, String reflectorsCfg, int primarySite) {
        PartiallyKnownNetworkTopology networkTopology = new PartiallyKnownNetworkTopology();
        networkTopology.setName(topologyCfg + "n" + reflectorsCfg + sites);
        final int consumersPerSite = (topologyCfg.equals("1") ? 1 : (sites));

        ArrayList<PhysicalNetworkLink> inLinks = new ArrayList<PhysicalNetworkLink>(sites);
        inLinks.add(0, null);
        ArrayList<PhysicalNetworkLink> outLinks = new ArrayList<PhysicalNetworkLink>(sites);
        outLinks.add(0, null);
        ArrayList<NetworkSite> siteList = new ArrayList<NetworkSite>(sites);
        siteList.add(0, null);
        ArrayList<ArrayList<EndpointNetworkNode>> siteNodes = new ArrayList<ArrayList<EndpointNetworkNode>>(sites);
        siteNodes.add(0, null);
        ArrayList<UnknownNetworkNode> siteNetworks = new ArrayList<UnknownNetworkNode>(sites);
        siteNetworks.add(0, null);
        UnknownNetworkNode centralNode = new UnknownNetworkNode("central");
        networkTopology.addNetworkNode(centralNode);

        for (int i = 1; i <= sites; i++) {
          siteList.add(i, new NetworkSite("Site " + i));
        }
        int consumersPerTypicalSite = (topologyCfg.equals("1")) ? 1 : sites - 1;
        for (int i = 1; i <= sites; i++) {
          int nodeCount = DISTRIBUTORS_PER_ACTIVE_SITE + PRODUCERS_PER_ACTIVE_SITE + (i == primarySite ? sites - 1 : consumersPerTypicalSite);
          ArrayList<EndpointNetworkNode> nodes = createSiteEndpointNodes(siteList.get(i), siteList.get(i).getSiteName() + " ", nodeCount);
          siteNodes.add(i, nodes);
          addEndpointInterfaces(nodes, MAIN_INTERFACE_NAME, "10.10." + i, "255.255.255.0", 10000, MAIN_SUBNET_NAME);
          addAllEndpointsToTopology(networkTopology, nodes);
          addApplicationsUnknown(nodes, i, siteList, sites, topologyCfg, primarySite);
          UnknownNetworkNode siteNetwork = new UnknownNetworkNode("siteNet" + i);
          networkTopology.addNetworkNode(siteNetwork);
          siteNetworks.add(i, siteNetwork);
          PhysicalNetworkLink forthLink = new PhysicalNetworkLink("", 10000, siteNetwork, centralNode, true);
          inLinks.add(i, forthLink);
          PhysicalNetworkLink backLink = new PhysicalNetworkLink("", 10000, centralNode, siteNetwork, true);
          outLinks.add(i, backLink);
          networkTopology.addPhysicalLink(forthLink);
          networkTopology.addPhysicalLink(backLink);
          connectEndpointsToNode(networkTopology, nodes, siteNetwork, 10000, MAIN_INTERFACE_NAME);
        }

        for (int i = 1; i <= sites; i++) {
          for (int j = 1; j <= sites; j++) {
            if (i == j) continue;
            ArrayList<PhysicalNetworkLink> physicals = new ArrayList<PhysicalNetworkLink>(2);
            physicals.add(outLinks.get(i));
            physicals.add(inLinks.get(j));
            networkTopology.associateIntersiteLinks(siteList.get(i), siteList.get(j), MAIN_SUBNET_NAME, physicals);
          }
        }

        for (int i = 1; i <= sites; i++) {
            networkTopology.buildEndpointLinkAssociationsMultinode(siteNodes.get(i), MAIN_INTERFACE_NAME, siteNetworks.get(i));
        }

        return networkTopology;
    }

    private void testEvaluateMatchMaker(MatchMakerConfig matchMakerConfig, int sites, String topologyCfg, String reflectorsCfg, boolean printConfig, boolean printResults) {
        MyLogger.setup();

        PartiallyKnownNetworkTopology networkTopology = createNetworkTopology(sites, topologyCfg, reflectorsCfg, 1);

        int mediaApps = 0;
        int reflectors = 0;
        for (EndpointNetworkNode node : networkTopology.getEndpointNodes()) {
            for (MediaApplication mediaApp : node.getNodeApplications()) {
                mediaApps++;
                if (mediaApp instanceof RumHD) {
                    reflectors++;
                }
            }
        }
        if (printConfig) {
            System.out.println("Network topology  is " + topologyCfg + ":n,");
            System.out.println("                 has " + sites + " sites (without reflector sites),");
            System.out.println("                     " + networkTopology.getPhysicalNodes().size() + " physical nodes,");
            System.out.println("                     " + networkTopology.getUnknownNetworkNodes().size() + " unknown nodes,");
            System.out.println("                     " + networkTopology.getEndpointNodes().size() + " endpoint nodes,");
            System.out.println("                     " + networkTopology.getPhysicalLinks().size() + " physical links,");
            System.out.println("                     " + networkTopology.getLogicalLinks().size() + " logical links,");
            System.out.println("                     " + mediaApps + " total configured media applications,");
            System.out.println("                 and " + reflectors + " packet reflector(s).");
            System.out.println("");
        }

        MatchMaker mm = new MatchMakerPKTGurobi(networkTopology, matchMakerConfig);

        boolean mcpSucceeded;
        boolean mmSucceeded;

        long mcpInitTime, mcpFinishTime, mmInitTime, mmFinishTime;

        MatchConsumerProducer mcp = new MatchConsumerProducer(networkTopology);

        mcpInitTime = System.currentTimeMillis();
        mcpSucceeded = mcp.doMatch();
        mcpFinishTime = System.currentTimeMillis();

        long mcpTime = mcpFinishTime - mcpInitTime;
        if (printResults) {
            System.out.println("MatchConsumerProducer run time: " + mcpTime + " ms.");
        }

        assertTrue("Failed to match consumers and producers for given network topology", mcpSucceeded);

        mmInitTime = System.currentTimeMillis();
        mmSucceeded = mm.doMatch();
        mmFinishTime = System.currentTimeMillis();

        long mmTime = mmFinishTime - mmInitTime;
        long planTime = mcpTime + mmTime;
        if (printResults) {
            System.out.println("MatchMaker run time: " + mmTime + " ms.");
            System.out.println("Plan time: " + planTime + " ms.");
        }

        assertTrue("Sample network has no solution", mmSucceeded);
    }

    private void testEvaluateMatchMakerUnknown(MatchMakerConfig matchMakerConfig, int sites, String topologyCfg, String reflectorsCfg, boolean printConfig, boolean printResults) {
        MyLogger.setup();

        PartiallyKnownNetworkTopology networkTopology = createUnknownNetworkTopology(sites, topologyCfg, reflectorsCfg, 1);

        int mediaApps = 0;
        int reflectors = 0;
        for (EndpointNetworkNode node : networkTopology.getEndpointNodes()) {
            for (MediaApplication mediaApp : node.getNodeApplications()) {
                mediaApps++;
                if (mediaApp instanceof RumHD) {
                    reflectors++;
                }
            }
        }
        if (printConfig) {
            System.out.println("Network topology  is " + topologyCfg + ":n,");
            System.out.println("                 has " + sites + " sites (without reflector sites),");
            System.out.println("                     " + networkTopology.getPhysicalNodes().size() + " physical nodes,");
            System.out.println("                     " + networkTopology.getUnknownNetworkNodes().size() + " unknown nodes,");
            System.out.println("                     " + networkTopology.getEndpointNodes().size() + " endpoint nodes,");
            System.out.println("                     " + networkTopology.getPhysicalLinks().size() + " physical links,");
            System.out.println("                     " + networkTopology.getLogicalLinks().size() + " logical links,");
            System.out.println("                     " + mediaApps + " total configured media applications,");
            System.out.println("                 and " + reflectors + " packet reflector(s).");
            System.out.println("");
        }

        MatchMaker mm = new MatchMakerPKTGurobi(networkTopology, matchMakerConfig);

        boolean mcpSucceeded;
        boolean mmSucceeded;

        long mcpInitTime, mcpFinishTime, mmInitTime, mmFinishTime;

        MatchConsumerProducer mcp = new MatchConsumerProducer(networkTopology);

        mcpInitTime = System.currentTimeMillis();
        mcpSucceeded = mcp.doMatch();
        mcpFinishTime = System.currentTimeMillis();

        long mcpTime = mcpFinishTime - mcpInitTime;
        if (printResults) {
            System.out.println("MatchConsumerProducer run time: " + mcpTime + " ms.");
        }

        assertTrue("Failed to match consumers and producers for given network topology", mcpSucceeded);

        mmInitTime = System.currentTimeMillis();
        mmSucceeded = mm.doMatch();
        mmFinishTime = System.currentTimeMillis();

        long mmTime = mmFinishTime - mmInitTime;
        long planTime = mcpTime + mmTime;
        if (printResults) {
            System.out.println("MatchMaker run time: " + mmTime + " ms.");
            System.out.println("Plan time: " + planTime + " ms.");
        }

        assertTrue("Sample network has no solution", mmSucceeded);
    }

    @Test
    public void testMultiRunEvaluateMatchMaker(MatchMakerGurobiEvaluationConfig mmc) {
        int runs = 20, take_runs = 7;
        int sites, max_sites;     // number of sites in the network
        String topologyCfg;       // "1" = 1:n topology; "m" = m:n topology
        String reflectorsCfg;     // "a" = ad hoc reflectors, "n" = no reflectors, "r" = site of reflectors, "s" = single reflector
        final boolean topology_1_s = true,
                topology_1_r = true,
                topology_m_r = true;


        MatchMakerConfig matchMakerConfig = new MatchMakerConfig();

        // warm-up configuration
        matchMakerConfig.setProperty("enableElimIntrasiteLinksLogical", true);
        matchMakerConfig.setProperty("enableElimLinkCapacityLogical", true);
        matchMakerConfig.setProperty("enableElimNoappLogical", true);
        matchMakerConfig.setProperty("enableElimLinkCapacityPhysical", true);
        matchMakerConfig.setProperty("enableElimNoappPhysical", true);
        matchMakerConfig.setProperty("enableFlows", true);
        matchMakerConfig.setProperty("enableConstrSingleLogicalStreamLinkCapacity", false);
        matchMakerConfig.setProperty("enableConstrSinglePhysicalStreamLinkCapacity", true);
        matchMakerConfig.setProperty("enableConstrWhoMayReceiveLogical", false);
        matchMakerConfig.setProperty("enableConstrWhoMayReceivePhysical", true);
        matchMakerConfig.setProperty("enableConstrWhoMaySendLogical", false);
        matchMakerConfig.setProperty("enableConstrWhoMaySendPhysical", true);
        matchMakerConfig.setProperty("enableConstrForbidDirectLinksLogical", false);
        matchMakerConfig.setProperty("enableConstrProducerSingleOutlinkLogical", false);
        matchMakerConfig.setProperty("enableConstrProducerSingleOutlinkPhysical", true);
        matchMakerConfig.setProperty("enableConstrConsumerSingleInlinkLogical", false);
        matchMakerConfig.setProperty("enableConstrConsumerSingleInlinkPhysical", true);
        matchMakerConfig.setProperty("enableConstrDistributorSingleInlinkLogical", false);
        matchMakerConfig.setProperty("enableConstrDistributorSingleInlinkPhysical", true);
        matchMakerConfig.setProperty("enableConstrDistributorSanityLogical", false);
        matchMakerConfig.setProperty("enableConstrDistributorSanityPhysical", true);
        matchMakerConfig.setProperty("enableConstrPhysicalNodeSanityFlows", false);
        matchMakerConfig.setProperty("enableConstrPhysicalNodeSanityStreamlinks", true);
        matchMakerConfig.setProperty("enableConstrLinkTotalCapacityLogical", true);
        matchMakerConfig.setProperty("enableConstrLinkTotalCapacityPhysical", true);
        matchMakerConfig.setProperty("enableConstrIfaceCapacity", true);
        matchMakerConfig.setProperty("enableDistributorCycleAvoidance", false);
        matchMakerConfig.setProperty("enableDistanceBasedCycleAvoidance", false);
        matchMakerConfig.setProperty("enableCallbackCycleAvoidance", false);
        matchMakerConfig.setProperty("enableObjectiveFunc", true);
        matchMakerConfig.setProperty("enableWrite", false);
        matchMakerConfig.setProperty("writeDir", "");
        matchMakerConfig.setProperty("writeType", "");
        matchMakerConfig.setProperty("enableOptimize", true);


        System.out.println("MatchMakerGurobiConfig: " + matchMakerConfig.toString());


        // we need at least this calculation to provide reasonable warm-up for the JVM. Alas.
        System.out.println("");
        System.out.println("======================== WARM-UP STARTED ==========================");
        System.out.println("");

        /*
         topologyCfg = "1";
         reflectorsCfg = "s";
         max_sites = 32;
         for (sites = 2; sites <= max_sites; sites *= 2) {
         for (int r = 1; r <= runs; r++) {
         testEvaluateMatchMaker(matchMakerGurobiConfig, sites, topologyCfg, reflectorsCfg, false, false);
         System.out.println("");
         }
         System.out.println("---------------------------------------------------------");
         System.out.println("");
         }
         */

        System.out.println("");
        System.out.println("======================== WARM-UP FINISHED ==========================");
        System.out.println("");


        // ---------------------
        // now individual runs
        // ---------------------

        if (mmc.run_minimization) {
            System.out.println("");
            System.out.println("======================== RUN: minimum solution ==========================");
            System.out.println("");

            matchMakerConfig.setProperty("enableElimIntrasiteLinksLogical", true);
            matchMakerConfig.setProperty("enableElimLinkCapacityLogical", true);
            matchMakerConfig.setProperty("enableElimNoappLogical", true);
            matchMakerConfig.setProperty("enableElimLinkCapacityPhysical", false); // not needed
            matchMakerConfig.setProperty("enableElimNoappPhysical", true);
            matchMakerConfig.setProperty("enableFlows", false); 
            matchMakerConfig.setProperty("enableConstrSingleLogicalStreamLinkCapacity", false);
            matchMakerConfig.setProperty("enableConstrSinglePhysicalStreamLinkCapacity", false);
            matchMakerConfig.setProperty("enableConstrWhoMayReceiveLogical", false);
            matchMakerConfig.setProperty("enableConstrWhoMayReceivePhysical", false);
            matchMakerConfig.setProperty("enableConstrWhoMaySendLogical", false);
            matchMakerConfig.setProperty("enableConstrWhoMaySendPhysical", false);
            matchMakerConfig.setProperty("enableConstrForbidDirectLinksLogical", false);
            matchMakerConfig.setProperty("enableConstrProducerSingleOutlinkLogical", false);
            matchMakerConfig.setProperty("enableConstrProducerSingleOutlinkPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrConsumerSingleInlinkLogical", false);
            matchMakerConfig.setProperty("enableConstrConsumerSingleInlinkPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrDistributorSingleInlinkLogical", false);
            matchMakerConfig.setProperty("enableConstrDistributorSingleInlinkPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrDistributorSanityLogical", false);
            matchMakerConfig.setProperty("enableConstrDistributorSanityPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrPhysicalNodeSanityFlows", false);
            matchMakerConfig.setProperty("enableConstrPhysicalNodeSanityStreamlinks", true); // min
            matchMakerConfig.setProperty("enableConstrLinkTotalCapacityLogical", true); // min
            matchMakerConfig.setProperty("enableConstrLinkTotalCapacityPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrTieLogicalPhysical", true); // min
            matchMakerConfig.setProperty("enableConstrIfaceCapacity", false);
            matchMakerConfig.setProperty("enableDistributorCycleAvoidance", false);
            matchMakerConfig.setProperty("enableDistanceBasedCycleAvoidance", false);
            matchMakerConfig.setProperty("enableCallbackCycleAvoidance", false);
            matchMakerConfig.setProperty("enableObjectiveFunc", true);
            matchMakerConfig.setProperty("enableWrite", true);
            matchMakerConfig.setProperty("writeDir", "/home/pavel/temp/model/");
            matchMakerConfig.setProperty("writeType", ".lp");
            matchMakerConfig.setProperty("enableOptimize", true);

            System.out.println("MatchMakerGurobiConfig: " + matchMakerConfig.toString());

            if (!matchMakerConfig.getBooleanProperty("enableOptimize") && matchMakerConfig.getBooleanProperty("enableWrite")) {
                runs = 1;
            }

            if (topology_1_s) {
                topologyCfg = "1";
                reflectorsCfg = "s";
                max_sites = 9;
                for (sites = 2; sites <= max_sites; sites += 1) {
                    for (int r = 1; r <= runs; r++) {
                        testEvaluateMatchMaker(matchMakerConfig, sites, topologyCfg, reflectorsCfg, (r == 1), (runs - r < take_runs));
                        System.out.println("");
                    }
                    System.out.println("---------------------------------------------------------");
                    System.out.println("");
                }
            }

//            if (topology_1_r) {
//                topologyCfg = "1";
//                reflectorsCfg = "r";
//                max_sites = 10;
//                for (sites = 2; sites <= max_sites; sites += 1) {
//                    for (int r = 1; r <= runs; r++) {
//                        testEvaluateMatchMaker(matchMakerConfig, sites, topologyCfg, reflectorsCfg, (r == 1), (runs - r < take_runs));
//                        System.out.println("");
//                    }
//                    System.out.println("---------------------------------------------------------");
//                    System.out.println("");
//                }
//            }

            if (topology_m_r) {
                topologyCfg = "m";
                reflectorsCfg = "r";
                max_sites = 9;
                for (sites = 2; sites <= max_sites; sites += 1) {
                    for (int r = 1; r <= runs; r++) {
                        testEvaluateMatchMaker(matchMakerConfig, sites, topologyCfg, reflectorsCfg, (r == 1), (runs - r < take_runs));
                        System.out.println("");
                    }
                    System.out.println("---------------------------------------------------------");
                    System.out.println("");
                }

            }
            
            if (topology_1_s) {
                topologyCfg = "1";
                reflectorsCfg = "s";
                max_sites = 9;
                for (sites = 2; sites <= max_sites; sites += 1) {
                    for (int r = 1; r <= runs; r++) {
                        testEvaluateMatchMakerUnknown(matchMakerConfig, sites, topologyCfg, reflectorsCfg, (r == 1), (runs - r < take_runs));
                        System.out.println("");
                    }
                    System.out.println("---------------------------------------------------------");
                    System.out.println("");
                }
            }

            if (topology_m_r) {
                topologyCfg = "m";
                reflectorsCfg = "r";
                max_sites = 9;
                for (sites = 2; sites <= max_sites; sites += 1) {
                    for (int r = 1; r <= runs; r++) {
                        testEvaluateMatchMakerUnknown(matchMakerConfig, sites, topologyCfg, reflectorsCfg, (r == 1), (runs - r < take_runs));
                        System.out.println("");
                    }
                    System.out.println("---------------------------------------------------------");
                    System.out.println("");
                }

            }
        }
    }
}
