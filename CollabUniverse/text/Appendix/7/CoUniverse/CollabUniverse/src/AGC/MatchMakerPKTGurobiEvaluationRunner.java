package AGC;

/**
 * Created by IntelliJ IDEA.
 * User: hopet
 * Date: 28.11.2009
 * Time: 0:43:51
 * To change this template use File | Settings | File Templates.
 */
public class MatchMakerPKTGurobiEvaluationRunner {
    public static void main(String[] args) {
        boolean[] parts = new boolean[26];
        for (boolean part : parts) {
            part = false;
        }
        if (args.length < 1) {
            System.out.println("No parts specified!");
            System.exit(1);
        }
        for (String arg : args) {
            Integer i = new Integer(arg);
            parts[i-1] = true;
        }
        MatchMakerPKTGurobiEvaluationTest mmeval = new MatchMakerPKTGurobiEvaluationTest();
        MatchMakerGurobiEvaluationConfig mmc = new MatchMakerGurobiEvaluationConfig(parts[0], parts[1], parts[2], parts[3], parts[4], parts[5], parts[6], parts[7], parts[8], parts[9], parts[10], parts[11], parts[12], parts[13], parts[14], parts[15], parts[16], parts[17], parts[18], parts[19], parts[20], parts[21], parts[22], parts[23], parts[24], parts[25]);
        mmeval.testMultiRunEvaluateMatchMaker(mmc);
        System.exit(0);
    }
}
