package myJXTA;

import net.jxta.credential.AuthenticationCredential;
import net.jxta.credential.Credential;
import net.jxta.discovery.DiscoveryEvent;
import net.jxta.discovery.DiscoveryListener;
import net.jxta.discovery.DiscoveryService;
import net.jxta.document.*;
import net.jxta.endpoint.Message;
import net.jxta.exception.PeerGroupException;
import net.jxta.exception.ProtocolNotSupportedException;
import net.jxta.id.IDFactory;
import net.jxta.membership.Authenticator;
import net.jxta.membership.MembershipService;
import net.jxta.peergroup.NetPeerGroupFactory;
import net.jxta.peergroup.PeerGroup;
import net.jxta.peergroup.PeerGroupID;
import net.jxta.pipe.InputPipe;
import net.jxta.pipe.OutputPipe;
import net.jxta.pipe.PipeID;
import net.jxta.pipe.PipeService;
import net.jxta.platform.ModuleSpecID;
import net.jxta.platform.NetworkConfigurator;
import net.jxta.protocol.*;
import net.jxta.rendezvous.RendezVousService;
import net.jxta.rendezvous.RendezvousEvent;
import net.jxta.rendezvous.RendezvousListener;
import org.apache.log4j.Logger;
import utils.TimeUtils;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.io.IOException;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import static java.text.MessageFormat.format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * My idea of JXTA connector abstraction
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 18.10.2007
 * Time: 9:04:50
 */
public class MyJXTAConnector implements RendezvousListener, DiscoveryListener {

    static Logger logger = Logger.getLogger(MyJXTAConnector.class);

    private final AtomicBoolean terminate;
    private final AtomicBoolean joinedUniverse;

    private CopyOnWriteArrayList<MessageType> messageTypes;

    public interface MessageListener {
        public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects);
    }

    public class MessageParsingError extends Exception {
    }

    private ConcurrentHashMap<MessageType, CopyOnWriteArrayList<MessageListener>> specificMessageListeners;
    private ConcurrentHashMap<InputPipe, CopyOnWriteArrayList<MessageListener>> pipeSpecificMessageListeners;
    private CopyOnWriteArrayList<MessageListener> globalMessageListeners;

    private ConcurrentHashMap<InputPipe, Thread> messageReceivers = null;
    private ConcurrentHashMap<InputPipe, Thread> messageHandlers = null;
    private ConcurrentHashMap<InputPipe, BlockingQueue<Message>> messageQueues;

    private PeerGroup netPeerGroup = null;
    private PeerGroup universePeerGroup = null;
    private DiscoveryService discoveryService = null;
    private PipeService pipeService = null;

    /**
     * Initializes MyJXTAConnector including initializing some basic network functionality (which should be however
     * very limited in order to keep constructor runtime as low as possible).
     */
    public MyJXTAConnector() {
        terminate = new AtomicBoolean(true);
        joinedUniverse = new AtomicBoolean(false);
        messageTypes = new CopyOnWriteArrayList<MessageType>();
        specificMessageListeners = new ConcurrentHashMap<MessageType, CopyOnWriteArrayList<MessageListener>>();
        pipeSpecificMessageListeners = new ConcurrentHashMap<InputPipe, CopyOnWriteArrayList<MessageListener>>();
        globalMessageListeners = new CopyOnWriteArrayList<MessageListener>();
        messageQueues = new ConcurrentHashMap<InputPipe, BlockingQueue<Message>>();
        messageReceivers = new ConcurrentHashMap<InputPipe, Thread>();
        messageHandlers = new ConcurrentHashMap<InputPipe, Thread>();
    }

    /**
     * Joins a collaborative universe.
     * <p/>
     *
     * @param jxtaHome jxta configuration directory
     * @param isInfra  become JXTA infrastructure node
     */
    public void joinUniverse(String jxtaHome, boolean isInfra) {
        terminate.set(false);

        logger.info("Configuring platform");
        logger.debug("RDV_HOME = " + jxtaHome);

        MyJXTAUtils.purgeCache(jxtaHome);


        NetworkConfigurator config = new NetworkConfigurator();
        if (!config.exists()) {
            try {
                InetAddress localMachine = InetAddress.getLocalHost();
                config.setName("Collab Universe Peer - " + localMachine.getHostName());
            }
            catch (Exception e) {
                infoOnException(e, "Unable to get local hostname.");
            }
            config.setPrincipal("username");
            config.setPassword("password");
            config.setDescription("Collaborative Universe, " + (isInfra ? "infra peer" : "edge peer"));
            if (isInfra) {
                config.setMode(NetworkConfigurator.RDV_NODE | NetworkConfigurator.RELAY_NODE);
            } else {
                config.setMode(NetworkConfigurator.EDGE_NODE);
            }
            config.setUseMulticast(false);
            config.setRendezvousSeedURIs(new ArrayList<String>() {
                {
                    add("http://frakira.fi.muni.cz/~xliska/universe/seeds.txt");
                }
            });
            config.setRelaySeedURIs(new ArrayList<String>() {
                {
                    add("http://frakira.fi.muni.cz/~xliska/universe/seeds.txt");
                }
            });

            try {
                config.save();
            } catch (IOException e) {
                infoOnException(e, "Failed to save config.properties.");
                System.exit(1);
            }
        } else {
            logger.error("PlatformConfig was not removed!");
            System.exit(1);
        }

        logger.info("Platform configured and saved");

        logger.info("Starting JXTA platform");

        try {
            netPeerGroup = getNetPeerGroup(config);
        } catch (PeerGroupException e) {
            infoOnException(e, "Failed to obtain NetPeerGroup");
            System.exit(1);
        }

        logger.info("NetworkManager PeerID = " + netPeerGroup.getPeerID());
        logger.info("netPeerGroup = " + netPeerGroup.getPeerGroupID().toString());

        //create and join private group
        if (isInfra) {
            netPeerGroup.getRendezVousService().startRendezVous();
        } else {
            connectToRendezVous(netPeerGroup);
        }
        try {
            universePeerGroup = createUniversePeerGroup(netPeerGroup);
        } catch (Exception e) {
            infoOnException(e, "Failed to create UniversePeerGroup!");
        }

        logger.info("Private Application PeerGroup " + universePeerGroup.getPeerGroupName() + " created and published.");
        try {
            joinGroup(universePeerGroup);
        } catch (ProtocolNotSupportedException e) {
            infoOnException(e, "Failed to join universePeerGroup");
        } catch (PeerGroupException e) {
            infoOnException(e, "Failed to join universePeerGroup");
        }

        //rendezvous service
        if (isInfra) {
            universePeerGroup.getRendezVousService().startRendezVous();
        } else {
            connectToRendezVous(universePeerGroup);
        }

        discoveryService = netPeerGroup.getDiscoveryService();

        // Application peer group pipe service
        if (universePeerGroup != null) {
            pipeService = universePeerGroup.getPipeService();
        }

        logger.info("Platform started.");

        synchronized (joinedUniverse) {
            joinedUniverse.set(true);
            joinedUniverse.notifyAll();
        }
    }

    public void leaveUniverse() {
        logger.info("Leaving JXTA universe.");

        terminate.set(true);

        logger.info("Unregistering DiscoveryListener.");
        discoveryService.removeDiscoveryListener(this);

        logger.info("Stopping universePeerGroup");
        universePeerGroup.stopApp();
        universePeerGroup.unref();
        logger.info("Stoping the network.");
        netPeerGroup.stopApp();
        netPeerGroup.unref();

        // messageReceivers may be wating on receiving messages from pipe
        for (Thread thread : messageReceivers.values()) {
            if (thread != null && thread.isAlive()) {
                thread.interrupt();
            }
        }
        for (Thread thread : messageReceivers.values()) {
            //noinspection EmptyCatchBlock
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }
        for (Thread thread : messageHandlers.values()) {
            //noinspection EmptyCatchBlock
            try {
                thread.join();
            } catch (InterruptedException e) {
            }
        }

        logger.info("Leaved JXTA universe.");
    }

    /**
     * Registers new message type for the connector. All the message types need to be registered before being
     * sent or received.
     * <p/>
     *
     * @param messageType message type to register
     */
    public void registerMessageType(MessageType messageType) {
        for (MessageType m : messageTypes) {
            if (m.equals(messageType)) {
                throw new IllegalArgumentException("Message of this type is already registered!" + messageType);
            }
            if (m.getMsgTypeId().equals(messageType.getMsgTypeId())) {
                throw new IllegalArgumentException("Message of this type is already registered: Type is already registered!\n" + messageType.getMsgTypeId());
            }
            if (m.matchMessageType(messageType.getMsgTypeId(), messageType.getElementIdentifiers())) {
                throw new IllegalArgumentException("Message of this type is already registered: Elements are already registered!\n" + Arrays.toString(messageType.getElementIdentifiers()));
            }
        }

        messageTypes.add(messageType);
    }

    /**
     * Creates new pipe advertisement.
     * <p/>
     *
     * @param pipeIDStr to build the advertisment upon
     * @return pipe advertisment
     * @throws URISyntaxException in case of problems with conversion of pipeIDStr to an URI
     */
    public static PipeAdvertisement createNewPipeAdvertisement(String pipeIDStr) throws URISyntaxException {
        PipeAdvertisement pipeAdv;

        pipeAdv = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());
        PipeID pipeID = (PipeID) IDFactory.fromURI(new URI(pipeIDStr));
        pipeAdv.setPipeID(pipeID);
        pipeAdv.setType(PipeService.UnicastType);
        pipeAdv.setName(pipeIDStr);

        return pipeAdv;
    }

    /**
     * Creates an input pipe based on its advertisement.
     * <p/>
     *
     * @param pipeAdvertisement to build the input pipe upon
     * @return input pipe
     * @throws IOException in case of failure
     */
    public InputPipe createInputPipe(PipeAdvertisement pipeAdvertisement) throws IOException {
        InputPipe inputPipe = pipeService.createInputPipe(pipeAdvertisement);
        pipeSpecificMessageListeners.put(inputPipe, new CopyOnWriteArrayList<MessageListener>());
        messageQueues.put(inputPipe, new LinkedBlockingQueue<Message>());

        return inputPipe;
    }

    /**
     * Creates an output pipe given its advertisment.
     * <p/>
     *
     * @param pipeAdvertisement to build the output pipe upon
     * @param timeout           for each attempt
     * @param attempts          number of attempts
     * @return output pipe or null in case of failure
     */
    public OutputPipe createOutputPipe(PipeAdvertisement pipeAdvertisement, long timeout, int attempts) {
        OutputPipe outputPipe = null;
        try {
            for (int i = 0; i < attempts; i++) {
                outputPipe = pipeService.createOutputPipe(pipeAdvertisement, timeout);
            }
        }
        catch (IOException e) {
            infoOnException(e, "Failed to bind the output pipe based on pipe advertisment: " + pipeAdvertisement);
            return null;
        }

        return outputPipe;
    }

    /**
     * Listen on specified pipe for incomming messages. It is implemented as two-threaded model in order
     * to prevent losing messages because of long processing time per message. It creates a queue used to
     * pass the messages between the threads.
     * <p/>
     *
     * @param pipe to listen on
     */
    public void listenOnInputPipe(final InputPipe pipe) {
        if (messageReceivers.contains(pipe)) {
            throw new IllegalArgumentException("Failed to register a pipe: " + pipe + " is already registered!");
        }

        synchronized (joinedUniverse) {
            if (!joinedUniverse.get()) {
                try {
                    joinedUniverse.wait();
                } catch (InterruptedException e) {
                    infoOnException(e, "InputPipe registration failed: failed to wait for connector to join the universe!");
                    return;
                }
            }
        }

        messageReceivers.put(pipe, new Thread() {
            public void run() {
                while (!terminate.get()) {
                    Message msg;
                    try {
                        msg = pipe.waitForMessage();
                    } catch (InterruptedException e) {
                        // we need to retest terminate flag - termination is the most likely reason for being interrupted
                        continue;
                    }
                    if ((messageQueues != null) && (msg != null)) {
                        messageQueues.get(pipe).offer(msg);
                    }
                    TimeUtils.sleepFor(500);
                }
            }
        });

        messageHandlers.put(pipe, new Thread() {
            public void run() {
                Message msg = null;
                while (!terminate.get()) {
                    //noinspection EmptyCatchBlock
                    try {
                        msg = messageQueues.get(pipe).poll(500, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                    }
                    if (msg == null) {
                        // we were disturbed for some reason and need to recheck the terminate flag
                        continue;
                    }

                    MessageType msgType = null;
                    Object[] elements = null;
                    for (MessageType type : messageTypes) {
                        try {
                            if ((elements = MyJXTAUtils.validateAndDecodeMessage(msg, type.getElementIdentifiers())) != null) {
                                msgType = type;
                                break;
                            }
                        } catch (IOException e) {
                            infoOnException(e, "Failed to decode the received message!");
                        }
                    }
                    synchronized (this) {
                        boolean calledListener = false;
                        for (MessageListener listener : specificMessageListeners.get(msgType)) {
                            if (pipeSpecificMessageListeners.get(pipe).contains(listener)) {
                                listener.onMessageArrived(pipe, msgType, elements);
                                calledListener = true;
                            }
                        }
                        if (!calledListener) {
                            logger.warn("Received " + msgType + " on pipe " + pipe.getName() + " while there are no valid listeners for it.");
                        }
                    }
                    for (MessageListener listener : globalMessageListeners) {
                        listener.onMessageArrived(pipe, msgType, elements);
                    }
                    TimeUtils.sleepFor(300);
                }
            }
        });

        messageReceivers.get(pipe).start();
        messageHandlers.get(pipe).start();
    }

    /**
     * Sends unreliable message using underlying JXTA substrate
     * <p/>
     *
     * @param pipe        used to send message over
     * @param messageType type of the message
     * @param objects     to be included in the message (have to correspond to the messageType specified)
     * @throws IOException in case that something went wrong during sending
     */
    public void sendUnreliableMesssage(OutputPipe pipe, MessageType messageType, Object... objects) throws IOException {
        if (!messageTypes.contains(messageType)) {
            throw new IllegalArgumentException("Message type " + messageType.getMsgTypeId() + " is not registered!");
        }
        if (!validateObjectsForSending(messageType, objects)) {
            throw new IllegalArgumentException("Message type " + messageType.getMsgTypeId() + " has different arguments than specified!");
        }
        Object[] objectsToSend = new Object[2 * objects.length];
        for (int i = 0, j = 0; i < objectsToSend.length;) {
            objectsToSend[i++] = objects[j];
            objectsToSend[i++] = messageType.getElementIdentifiers()[j++];
        }
        pipe.send(MyJXTAUtils.encodeMessage(objectsToSend));
    }

    /**
     * Sends reliable message using underlying JXTA substrate
     * <p/>
     *
     * @param pipe        used to send message over
     * @param messageType type of the message
     * @param objects     to be included in the message (have to correspond to the messageType specified)
     * @throws IOException in case that something went wrong during sending
     */
    public void sendReliableMesssage(OutputPipe pipe, MessageType messageType, Object... objects) throws IOException {
        if (!validateObjectsForSending(messageType, objects)) {
            throw new IllegalArgumentException();
        }
        // TODO: send *reliable* message
        sendUnreliableMesssage(pipe, messageType, objects);
    }

    /**
     * Registers message-type specific listener.
     * <p/>
     * If the listener is supposed to be pipe-specific, it needs to handle that on its own by
     * checking the pipe parameter.
     * <p/>
     *
     * @param inputPipe   input pipe to listen on
     * @param messageType message type to listen for
     * @param listener    listener to register
     */
    public void registerMessageListener(InputPipe inputPipe, MessageType messageType, MessageListener listener) {
        assert inputPipe != null : "Message listener must be registered for a specific pipe!";
        assert listener != null : "Message listener to be registered for a specific pipe must not be null!";
        synchronized (this) {
            if (!specificMessageListeners.contains(messageType)) {
                specificMessageListeners.put(messageType, new CopyOnWriteArrayList<MessageListener>());
            }
            specificMessageListeners.get(messageType).add(listener);
            pipeSpecificMessageListeners.get(inputPipe).add(listener);
        }
    }

    /**
     * Registers general specific listener for all the message-types.
     * <p/>
     * If the listener is supposed to be pipe-specific, it needs to handle that on its own by
     * checking the pipe parameter.
     * <p/>
     *
     * @param listener listener to register
     */
    public void registerGlobalMessageListener(MessageListener listener) {
        globalMessageListeners.add(listener);
    }

    public PeerGroup getNetPeerGroup() {
        return netPeerGroup;
    }

    public PeerGroup getUniversePeerGroup() {
        return universePeerGroup;
    }

    public DiscoveryService getDiscoveryService() {
        return discoveryService;
    }

    public PipeService getPipeService() {
        return pipeService;
    }

    /**
     * Gets advertisment of an AGC. The advertisments has had to be created and published by the AGC prior to
     * calling this method on the client side.
     * <p/>
     *
     * @return AGC advertisment
     */
    public ModuleSpecAdvertisement getAGCAdv() {
        Enumeration en;
        while (true) {
            try {
                // Try to look in our local cache. This might work when local node is AGC
                en = discoveryService.getLocalAdvertisements(DiscoveryService.ADV, "Name", "SPEC:Universe-AGC");

                // Universe-AGC adv is in our local cache
                if ((en != null) && (en.hasMoreElements())) {
                    break;
                }

                // Try to look for Universe-AGC adv remotely. Any new advertisements should be cached in local cache
                discoveryService.getRemoteAdvertisements(null, DiscoveryService.ADV, "Name", "SPEC:Universe-AGC", 1, null);

                TimeUtils.sleepFor(3000);
            }
            catch (Exception e) {
                infoOnException(e, "Universe-AGC discovery failed.");
            }
        }

        logger.info("Universe-AGC service adv found.");

        // Get the Universe-AGC specificaton advertisement
        ModuleSpecAdvertisement agcMdsAdv = (ModuleSpecAdvertisement) en.nextElement();

        // Debug: print the adv
        if (logger.isDebugEnabled()) {
            StructuredTextDocument agcAdvDoc = (StructuredTextDocument) agcMdsAdv.getDocument(MimeMediaType.TEXT_DEFAULTENCODING);
            StringWriter out = new StringWriter();
            try {
                agcAdvDoc.sendToWriter(out);
                logger.debug(out.toString());
            }
            catch (IOException e) {
                infoOnException(e, "Failed to dump Universe-AGC advertisement to stdout.");
            }
        }

        return agcMdsAdv;
    }


     /**
     * Gets advertisment of an AGC. The advertisments has had to be created and published by the AGC prior to
     * calling this method on the client side.
     * <p/>
     *
     * @return AGC advertisment
     */
    public ModuleSpecAdvertisement getGuiControllerAdv() {
        Enumeration en;
        while (true) {
            try {
                // Try to look in our local cache. This might work when local node is AGC
                en = discoveryService.getLocalAdvertisements(DiscoveryService.ADV, "Name", "SPEC:Universe-AGC");

                // Universe-AGC adv is in our local cache
                if ((en != null) && (en.hasMoreElements())) {
                    break;
                }

                // Try to look for Universe-AGC adv remotely. Any new advertisements should be cached in local cache
                discoveryService.getRemoteAdvertisements(null, DiscoveryService.ADV, "Name", "SPEC:Universe-AGC", 1, null);

                TimeUtils.sleepFor(3000);
            }
            catch (Exception e) {
                infoOnException(e, "Universe-GuiController discovery failed.");
            }
        }

        logger.info("Universe-AGC service adv found.");

        // Get the Universe-AGC specificaton advertisement
        ModuleSpecAdvertisement agcMdsAdv = (ModuleSpecAdvertisement) en.nextElement();

        // Debug: print the adv
        if (logger.isDebugEnabled()) {
            StructuredTextDocument agcAdvDoc = (StructuredTextDocument) agcMdsAdv.getDocument(MimeMediaType.TEXT_DEFAULTENCODING);
            StringWriter out = new StringWriter();
            try {
                agcAdvDoc.sendToWriter(out);
                logger.debug(out.toString());
            }
            catch (IOException e) {
                infoOnException(e, "Failed to dump Universe-AGC advertisement to stdout.");
            }
        }

        return agcMdsAdv;
    }

    // --- inherited part

    public void rendezvousEvent(RendezvousEvent event) {
        String eventDescription;
        int eventType;

        eventType = event.getType();

        switch (eventType) {
            case RendezvousEvent.RDVCONNECT:
                eventDescription = "RDVCONNECT";
                break;
            case RendezvousEvent.RDVRECONNECT:
                eventDescription = "RDVRECONNECT";
                break;
            case RendezvousEvent.RDVDISCONNECT:
                eventDescription = "RDVDISCONNECT";
                break;
            case RendezvousEvent.RDVFAILED:
                eventDescription = "RDVFAILED";
                break;
            case RendezvousEvent.CLIENTCONNECT:
                eventDescription = "CLIENTCONNECT";
                break;
            case RendezvousEvent.CLIENTRECONNECT:
                eventDescription = "CLIENTRECONNECT";
                break;
            case RendezvousEvent.CLIENTDISCONNECT:
                eventDescription = "CLIENTDISCONNECT";
                break;
            case RendezvousEvent.CLIENTFAILED:
                eventDescription = "CLIENTFAILED";
                break;
            case RendezvousEvent.BECAMERDV:
                eventDescription = "BECAMERDV";
                break;
            case RendezvousEvent.BECAMEEDGE:
                eventDescription = "BECAMEEDGE";
                break;
            default:
                eventDescription = "UNKNOWN RENDEZVOUS EVENT";
        }

        logger.info(new Date().toString() + "  RendezvousEvent:  event =  "
                + eventDescription + " from peer = " + event.getPeer());
    }

    public void discoveryEvent(DiscoveryEvent ev) {
        try {
            DiscoveryResponseMsg res = ev.getResponse();
            logger.info(format("[ Got {0} responses for discovery request from peer {1} ] ", res.getResponseCount(), ev.getSource().toString()));

            Advertisement adv;

            for (Enumeration en = res.getAdvertisements(); en != null && en.hasMoreElements();) {
                adv = (Advertisement) en.nextElement();
                logger.info(format("[ Ad type = {0}, ID = {1}, details = {2} ] ", adv.getAdvType(), adv.getID(), adv));
            }

            /*
            // This gives just very long and largely useless output >:(
            for (Enumeration en = res.getResponses(); en != null && en.hasMoreElements();) {
                UniversePeer.logger.info("[ Ad response = " + en.toString() + " ] ");
            }
             */
        }
        catch (Exception e) {
            infoOnException(e, "Failed to process DiscoveryEvent");
        }
    }

    // --- private part

    /**
     * Checks that the objects are valid for given message type.
     * <p/>
     *
     * @param messageType to check against
     * @param objects     to be checked
     * @return true if object comply with the message type, false otherwise
     */
    private boolean validateObjectsForSending(MessageType messageType, Object... objects) {
        if (messageType.getElementIdentifiers().length != objects.length) {
            return false;
        }

        for (Object object : objects) {
            if (!(object instanceof java.io.Serializable)) {
                return false;
            }
        }

        // TODO: validate, that conform to JavaBean structure
        try {
            for (Object object : objects) {
                Introspector.getBeanInfo(object.getClass());
            }
        } catch (IntrospectionException e) {
            return false;
        }

        return true;
    }

    /**
     * Returns message type that matches the message being tested.
     * <p/>
     *
     * @param msg message to test
     * @return message type detected
     * @throws MessageParsingError for unknown messages
     */
    // TODO: make first element of the message MessageType
    private MessageType getMessageType(Message msg) throws MessageParsingError {
        String messageType = new String(msg.getMessageElement("MessageType").getBytes(true));
        for (MessageType type : messageTypes) {
            if (type.getMsgTypeId().equals(messageType)) {
                return type;
            }
        }
        throw new MessageParsingError();
    }


    /**
     * Creates our own NetPeerGroup with a hardcoded ID.
     *
     * @param config platform configuration
     * @return NetPeerGroup
     * @throws PeerGroupException when something is wrong
     */
    private static PeerGroup getNetPeerGroup(NetworkConfigurator config) throws PeerGroupException {
        String NETPEERGROUPID = "urn:jxta:uuid-A7F6D54F86C44BA3A34ABC62C14D989D02";
        try {
            PeerGroupID pgId = (PeerGroupID) IDFactory.fromURI(new URI(NETPEERGROUPID));
            XMLDocument desc =
                    (XMLDocument) StructuredDocumentFactory.newStructuredDocument(MimeMediaType.XMLUTF8, "desc", "Collaborative Universe NetPeerGroup");
            String name = "Collab Universe Net Peer Group";
            PeerGroup netPG = new NetPeerGroupFactory(
                    config.getPlatformConfig(),
                    config.getHome().toURI(),
                    pgId,
                    name,
                    desc).getInterface();

            logger.info("NetPeerGroup: " + netPG.getPeerGroupName() + "/" + netPG.getPeerGroupID() + " created.");

            return netPG;
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private static RendezVousService connectToRendezVous(PeerGroup peerGroup) {
        RendezVousService rdv = peerGroup.getRendezVousService();
        if (!rdv.isConnectedToRendezVous()) {
            logger.info("Waiting to connect to rendezvous for group " + peerGroup.getPeerGroupName() + " ...");
            RdvSemaphore rs = new RdvSemaphore(rdv);
            rs.waitForRdv();
            logger.info("Connected to RDV!");
        } else {
            logger.info("Rendezvous for group " + peerGroup.getPeerGroupName() + " already connected.");
        }
        return rdv;
    }


    /**
     * Joins a group without any authentication.
     *
     * @param grp group to be joined
     * @throws net.jxta.exception.PeerGroupException
     *          if something is wrong
     * @throws net.jxta.exception.ProtocolNotSupportedException
     *          if something is wrong
     */
    private void joinGroup(PeerGroup grp) throws ProtocolNotSupportedException, PeerGroupException {
        Credential credential;
        MembershipService membership = grp.getMembershipService();
        AuthenticationCredential authCred = new AuthenticationCredential(grp, null, null);
        Authenticator auth = membership.apply(authCred);
        // Check if everything is okay to join the group
        if (auth.isReadyForJoin()) {
            //noinspection UnusedAssignment
            credential = membership.join(auth);
            logger.info("Successfully joined group " + grp.getPeerGroupName());
        } else {
            throw new RuntimeException();
        }
    }

    /**
     * Creates peer group for the universe. This is the basic collaborative space.
     * <p/>
     *
     * @param rootGroup to base the universe upon
     * @return created peer group
     * @throws Exception in case of any failure
     */
    private PeerGroup createUniversePeerGroup(PeerGroup rootGroup) throws Exception {

        // The new-application subgroup parameters.
        String name = "Collab Universe App Group";
        String desc = "Collab Universe playground";
        String gmsid = "urn:jxta:uuid-8B5DBA3AB32748F287F3A238235352E16E691351F90A4B47AEE18590569B0AF506";
        String gid = "urn:jxta:uuid-D05DDDD3A48046F8AE3AFB5A15102D1B02";

        logger.info("Creating group:  " + name + ", " + desc + ", " + gid);

        DiscoveryService disco = rootGroup.getDiscoveryService();

        ModuleImplAdvertisement implAdv = rootGroup.getAllPurposePeerGroupImplAdvertisement();
        implAdv.setModuleSpecID((ModuleSpecID) IDFactory.fromURI(new URI(gmsid)));
        implAdv.setDescription("Collaborative Universe Implementation");
        implAdv.setProvider("sitola.fi.muni.cz");
        implAdv.setUri("http://sitola.fi.muni.cz/CollabUniverse");

        // Publish the implAdv
        disco.publish(implAdv);
        disco.remotePublish(implAdv);

        // Create the new group using non-random group ID, advertisement, name, and description
        PeerGroupID groupID = IDFactory.newPeerGroupID(rootGroup.getPeerGroupID(), name.getBytes());
        PeerGroup newGroup = rootGroup.newGroup(groupID, implAdv, name, desc);

        // Publish the group remotely. appPeerGroup() handles the local publishing.
        PeerGroupAdvertisement groupAdv = newGroup.getPeerGroupAdvertisement();
        disco.publish(groupAdv);
        disco.remotePublish(null, groupAdv);

        return newGroup;
    }

    /**
     * Helper method for reporting exceptions in user-friendly way.
     * <p/>
     *
     * @param e exception to report
     * @param s message to log using logger (useful namely when debugging is disabled)
     */
    private static void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
        }
        logger.error(s);
    }

}
