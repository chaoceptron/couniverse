package myJXTA;

import java.util.Arrays;

/**
 * Abstraction of message types being sent by JXTA.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 21.10.2007
 * Time: 20:45:28
 */
public class MessageType {

    // -- static message types

    public static final MessageType REMOVE_NODE_MESSAGE = new MessageType("REMOVE_NODE_MESSAGE", "Collab Universe: NetworkNode Removed Message");
    public static final MessageType START_MEDIAAPP_MESSAGE = new MessageType("START_MEDIAAPP_MESSAGE", "Collab Universe: Start MediaApp Object", "Collab Universe: Start MediaApp Config");
    public static final MessageType STOP_MEDIAAPP_MESSAGE = new MessageType("STOP_MEDIAAPP_MESSAGE", "Collab Universe: Stop MediaApp Object");
    public static final MessageType START_FAILOVER_MEDIAAPP_MESSAGE = new MessageType("START_FAILOVER_MEDIAAPP_MESSAGE", "Collab Universe: Start MediaApp Failover");
    public static final MessageType NETWORK_UPDATE_MESSAGE = new MessageType("NETWORK_UPDATE_MESSAGE", "Collab Universe: NetworkNodes update", "Collab Universe: NetworkLinks update");
    public static final MessageType ACTIVE_LINKS_MESSAGE  = new MessageType("ACTIVE_LINKS_MESSAGE", "Collab Universe: Active NetworkLinks update");

    public static final MessageType NODE_UNREACHABLE_MESSAGE = new MessageType("NODE_UNREACHABLE_MESSAGE", "Collab Universe: NetworkNode Unreachable via NetworkLink");
    public static final MessageType NODE_REACHABLE_MESSAGE = new MessageType("NODE_REACHABLE_MESSAGE", "Collab Universe: NetworkNode Reachable via NetworkLink");
    public static final MessageType NEW_NODE_MESSAGE = new MessageType("NEW_NODE_MESSAGE", "Collab Universe: NetworkNode Information Message");  
    
    public static final MessageType TOPOLOGY_DESCRIPTION_MESSAGE = new MessageType("TOPOLOGY_DESCRIPTION_MESSAGE", "Collab Universe: Network Topology Description", "Collab Universe: Network Topology Description");
    
    // -- class itself

    private final String msgTypeId;
    private final String[] elementIdentifiers;

    protected MessageType(String msgTypeId, String... elementIdentifiers) {
        if (!(msgTypeId != null && msgTypeId.length() > 0 && elementIdentifiers != null)) {
            throw new IllegalArgumentException();
        }
        this.msgTypeId = msgTypeId;
        this.elementIdentifiers = elementIdentifiers;
    }

    public String getMsgTypeId() {
        return msgTypeId;
    }

    public String[] getElementIdentifiers() {
        return elementIdentifiers;
    }

    public boolean matchMessageType(String typeId, String... elements) {
        if (msgTypeId.equals(typeId) && elements.length == elementIdentifiers.length) {
            boolean matchFound = true;
            for (int i = 0; i < elements.length; i++) {
                if (!elementIdentifiers[i].equals(elements[i])) {
                    matchFound = false;
                }
            }
            if (matchFound) {
                return true;
            }
        }
        return false;
    }

    public int getElementPos(String typeId) {
        for(int i = 0; i < this.elementIdentifiers.length; i++) {
            if(this.elementIdentifiers[i].equals(typeId)) {
                return i;
            }
        }
        throw new IllegalArgumentException("Requested bad element " + typeId + " from MessageType " + getMsgTypeId());
    }


    public String toString() {
        StringBuffer s = new StringBuffer();
        for (int i = 0; i < elementIdentifiers.length; i++) {
            if(i != 0) {
                s.append(", ");
            }
            s.append(elementIdentifiers[i]);
        }
        return msgTypeId + ": " + s.toString();
    }


    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageType that = (MessageType) o;

        return Arrays.equals(elementIdentifiers, that.elementIdentifiers) && msgTypeId.equals(that.msgTypeId);

    }

    public int hashCode() {
        int result;
        result = msgTypeId.hashCode();
        result = 31 * result + Arrays.hashCode(elementIdentifiers);
        return result;
    }
}
