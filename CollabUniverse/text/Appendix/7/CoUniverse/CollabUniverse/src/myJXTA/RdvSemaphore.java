package myJXTA;

import net.jxta.rendezvous.RendezvousListener;
import net.jxta.rendezvous.RendezVousService;
import net.jxta.rendezvous.RendezvousEvent;

/**
 * Created by IntelliJ IDEA.
 * User: xliska
 * Date: 9.9.2007
 * Time: 16:26:33
 * To change this template use File | Settings | File Templates.
 */

public class RdvSemaphore implements RendezvousListener {
    public RdvSemaphore(RendezVousService rdv) {
        rdv.addListener(this);
    }

    public synchronized void rendezvousEvent(RendezvousEvent event) {
        if (event.getType() == RendezvousEvent.RDVCONNECT) {
            notify();
        }
    }

    public synchronized void waitForRdv() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

