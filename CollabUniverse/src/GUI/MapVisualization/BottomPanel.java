package GUI.MapVisualization;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Apr 7, 2008
 * Time: 1:08:08 PM
 * To change this template use File | Settings | File Templates.
 */


import GUI.MyButton;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.*;
import javax.swing.event.ChangeEvent;

/**
 *
 * @author martin
 */
public class BottomPanel extends JPanel {
    public MyButton googleButton = new MyButton("Google Maps");
    public MyButton openStreetsButton = new MyButton(" OpenStreet Maps");
    public MyButton nasaButton = new MyButton(" Nasa Maps");
    public MyButton fullSizeButton = new MyButton("Full Size");
    public MyButton normalSizeButton = new MyButton("Normal Size");
    public MyButton goHomeButton = new MyButton("Go Home");


    
    public BottomPanel(boolean init) {
        this.setLayout(new GridBagLayout());
        
        if(init){
            init();
        }
        this.setOpaque(true);
    }


    private void init() {
        GridBagConstraints c = new GridBagConstraints();
        c.weightx = 0.4;
        c.gridy = 0;
        c.gridx = 0;
        c.weighty = 1;
        c.anchor = GridBagConstraints.LINE_END;
        this.add( this.googleButton, c);

        c.gridx = 1;
        c.weightx = 0.001;
        c.anchor = GridBagConstraints.CENTER;
        this.add(this.openStreetsButton, c);

        c.gridx = 2;
        c.weightx = 0.509;
        c.anchor = GridBagConstraints.LINE_START;
        this.add(this.nasaButton, c);

        c.anchor = GridBagConstraints.CENTER;
        //c.weightx = 0.15;
        c.gridx = 2;
        this.add(this.goHomeButton, c);

        c.insets = new Insets(7, 0,0,10);
        c.gridx = 2;
        //c.weightx = 0.1;
        c.anchor = GridBagConstraints.FIRST_LINE_END;
        this.add(this.normalSizeButton, c);

        c.insets = new Insets(0, 0,7,10);
        c.anchor = GridBagConstraints.LAST_LINE_END;
        this.add(this.fullSizeButton, c);
    }

    @Override
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setPaint(Color.white);
        g2.fillRect(1, 1, getWidth() - 3, getHeight() - 3);

        GradientPaint background = new GradientPaint(0, 0,  new Color(255, 255, 255),
                0, getHeight() / 2 ,new Color(0, 0, 0), true);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(background);
        g2.fillRoundRect(0, 0, getWidth(), getHeight(), 40, 40);


        g2.setColor(new Color(0,0, 0));
        Font font = new Font("Lucida Bright", Font.ITALIC, 14);
        g2.setFont(font);

        g2.setColor(new Color(0, 0, 0));
        g2.drawString("© tlachy@mail.muni.cz", 10 , getHeight() / 2 + 7 );


        g2.setColor(new Color(255, 255, 255));
        g2.drawString("© tlachy@mail.muni.cz" , 12, getHeight() / 2 + 5 );

        paintChildren(g2);
        g.dispose();
    }

}