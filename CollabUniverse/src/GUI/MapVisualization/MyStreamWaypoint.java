package GUI.MapVisualization;

import MediaAppFactory.MediaApplication;
import NetworkRepresentation.NetworkLink;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: May 5, 2008
 * Time: 3:56:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyStreamWaypoint extends Waypoint {


    final MediaApplication mediaApplication;
        final NetworkLink networkLink;
        GeoPosition fromPosition;
        GeoPosition toPosition;
        Color color;

        public MyStreamWaypoint(MediaApplication mediaApplication, NetworkLink networkLink) {
            this.mediaApplication = mediaApplication;
            this.networkLink = networkLink;
            this.fromPosition = new GeoPosition(networkLink.getFromNode().getGeoLatitude(), networkLink.getFromNode().getGeoLongitude());
            this.toPosition = new GeoPosition(networkLink.getToNode().getGeoLatitude(), networkLink.getToNode().getGeoLongitude());
            this.color = new Color(255, 0, 0, 120);
        }

        public MyStreamWaypoint(MediaApplication mediaApplication, NetworkLink networkLink, Color color) {
            this.mediaApplication = mediaApplication;
            this.networkLink = networkLink;
            this.fromPosition = new GeoPosition(networkLink.getFromNode().getGeoLatitude(), networkLink.getFromNode().getGeoLongitude());
            this.toPosition = new GeoPosition(networkLink.getToNode().getGeoLatitude(), networkLink.getToNode().getGeoLongitude());
            this.color = color;
        }

        @SuppressWarnings({"DesignForExtension"})
        public void setPosition(GeoPosition geoPosition) {
        }

        public GeoPosition getPosition() {
            return this.fromPosition;
        }

        public GeoPosition getFromPosition() {
            return fromPosition;
        }

        public GeoPosition getToPosition() {
            return toPosition;
        }

        public int getFromShimX() {
            return networkLink.getFromNode().getShim().getShimX();
        }

        public int getFromShimY() {
            return networkLink.getFromNode().getShim().getShimY();
        }

        public int getToShimX() {
            return networkLink.getToNode().getShim().getShimX();
        }

        public int getToShimY() {
            return networkLink.getToNode().getShim().getShimY();
        }

        public MediaApplication getMediaApplication() {
            return mediaApplication;
        }

        public NetworkLink getNetworkLink() {
            return networkLink;
        }

        public Color getColor() {
            return color;
        }
}
