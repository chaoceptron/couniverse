package GUI.MapVisualization;

import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import NetworkRepresentation.NetworkNode;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: May 5, 2008
 * Time: 3:30:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class MyNodeWaypoint extends Waypoint {
    final NetworkNode networkNode;
    GeoPosition nodePosition;
    Rectangle areaOnMap = new Rectangle(0,0,15,8);



    public void setMapOriginPosition(int x, int y) {
        this.areaOnMap.setLocation(x - this.getShimX(), y + this.getShimY());
    }


    //because the size of waypoint on map can be various

    public void setSize(int width, int height) {
        this.areaOnMap.setSize( width, height);
    }

    public boolean wasClickedOnMap(int x, int y){
        if(this.networkNode.getNodeName().equals("senderPrg")){
            System.err.println( this.networkNode.getNodeName() +"  "+ this.areaOnMap);            
        }
        return this.areaOnMap.contains(x , y );
    }

    public MyNodeWaypoint(NetworkNode n) {
        this.networkNode = n;
        this.nodePosition = new GeoPosition(n.getGeoLatitude(), n.getGeoLongitude());
    }

    public NetworkNode getNetworkNode() {
        return networkNode;
    }

    @SuppressWarnings({"DesignForExtension"})
    public void setPosition(GeoPosition geoPosition) {
    }

    public GeoPosition getPosition() {
        return nodePosition;
    }

    public int getShimX() {
        return this.networkNode.getShim().getShimX();
    }

    public int getShimY() {
        return this.networkNode.getShim().getShimY();
    }
}
