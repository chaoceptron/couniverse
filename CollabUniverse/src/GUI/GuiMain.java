package GUI;

import javax.swing.*;
import java.awt.*;


public class GuiMain {
    //possible extension for JXMapVisualizer. Screen can span multiple physical devices
    public static boolean isMultipleScreen = false;
    public static Rectangle virtualBounds = new Rectangle();


    static {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: ClassNotFoundException");
        } catch (InstantiationException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: InstantiationException");
        } catch (IllegalAccessException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: IllegalAccessException");
        } catch (UnsupportedLookAndFeelException e) {
            //NodeDialog.logger.error("NetworkNode Configurator: UnsupportedLookAndFeelException");
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
    }

    public static void main(String[] args) {

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] screenDevices = ge.getScreenDevices();

        for (GraphicsDevice gd : screenDevices) {
            GraphicsConfiguration[] gc = gd.getConfigurations();

            for (GraphicsConfiguration aGc : gc) {
                if (aGc.getBounds().getX() != 0.0d || aGc.getBounds().getY() != 0.0d) isMultipleScreen = true;
                virtualBounds = virtualBounds.union(aGc.getBounds());
            }
        }

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }


    private static void createAndShowGUI() {
        BaseFrame frame = new BaseFrame("CoUniverse");
        frame.pack();
        frame.setVisible(true);
        frame.setMinimumSize(frame.getPreferredSize());

    }
}

