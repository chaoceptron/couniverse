package Monitoring;

import junit.framework.TestCase;
import NetworkRepresentation.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.After;
import static org.junit.Assert.*;
import NetworkRepresentation.NetworkNode;
import static org.hamcrest.CoreMatchers.*;
import utils.ApplicationProxyJNI;
import utils.ApplicationProxy;
import utils.MyLogger;

/**
 * Created by IntelliJ IDEA.
 * User: xsuchom1
 * Date: Dec 8, 2008
 * Time: 3:53:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class MediaApplicationMonitorTest extends TestCase {
    static {
       MyLogger.setup();
    }
    private ApplicationProxyJNI application1;
    private MediaApplicationMonitor mediaAppMonitor; 

    private class MyMediaApplicationMonitorListener implements MediaApplicationMonitorListener {
        // this is to be called when Media Application is found terminated
       public void onMediaApplicationDown(ApplicationProxy mediaApplication) {
            System.out.println("Listener " +this + " have onMediaApplicationDown " + mediaApplication);
        }// this is to be called when Media Application is found running
      public void onMediaApplicationStart(ApplicationProxy mediaApplication) {
         System.out.println("Listener " + this +" have onMediaApplicationStart " + mediaApplication);
      }
    }
    @Before
    public void runBeforeTestCase() {
         try {
        this.mediaAppMonitor = new MediaApplicationMonitor();
        this.application1 = new ApplicationProxyJNI("/home/xsuchom1/pokus/hello", "ahoj", mediaAppMonitor);

        }
        catch (ApplicationProxy.ApplicationProxyException ex){
            ex.printStackTrace();
        }

    }
    @After
    public void runAfterTestCase() {
        this.mediaAppMonitor = null;
        this.application1 = null;
    }

    @Test
    public void testApplicationRunWithoutMonitoring(){
        try {
            System.out.println("running testApplicationRunWithoutMonitoring()");  
        this.application1 = new ApplicationProxyJNI("/home/xsuchom1/pokus/hello", "ahoj");


        application1.run();
        Thread.sleep(1000);
        application1.kill();
        runAfterTestCase();
        }
        catch (ApplicationProxy.ApplicationProxyException ex){
            ex.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace(); 
        }
    }

    @Test
    public void testApplicationRunWithMonitoring() {
       try{
           System.out.println("running testApplicationRunWithMonitoring()");
           runBeforeTestCase();
           assertThat("Application wasen't started",application1.checkRunning(), is(false));
           application1.run();
           assertThat("Application should be running",application1.checkRunning(), is(true));
           Thread.sleep(1000);
           application1.kill();
           assertThat("Application wasen't started",application1.checkRunning(), is(false));
           runAfterTestCase();
       }
       catch (InterruptedException e) {
            e.printStackTrace();
        }
       catch (ApplicationProxy.ApplicationProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    @Test
    public void testApplicationData() {
        try{
            System.out.println("running testApplicationData()");
            runBeforeTestCase();
            application1.run();
            Thread.sleep(500);
            System.out.println("CPU usage is : " + mediaAppMonitor.getCPUUsage(application1));
            System.out.println("Allocated memory is: " + mediaAppMonitor.getAllocatedMemory(application1));
            application1.kill();
            runAfterTestCase();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    @Test
    public void testApplicationDataWithMonitoring() {
        try {
            System.out.println("running testApplicationDataWithMonitoring()");
            runBeforeTestCase();
            application1.run();
            mediaAppMonitor.startMonitoring();
            Thread.sleep(5000);
            System.out.println("CPU usage is : " + mediaAppMonitor.getCPUUsage(application1));
            System.out.println("Allocated memory is: " + mediaAppMonitor.getAllocatedMemory(application1));
            mediaAppMonitor.stopMonitoring();
            assertThat("Application should be running",application1.checkRunning(), is(true));
            application1.kill();
            runAfterTestCase();
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ApplicationProxy.ApplicationProxyException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testListeners() {
        try {
            System.out.println("running testListeners()");
            runBeforeTestCase();
            MyMediaApplicationMonitorListener lis1 = new MyMediaApplicationMonitorListener();
            MyMediaApplicationMonitorListener lis2 = new MyMediaApplicationMonitorListener();
            mediaAppMonitor.startMonitoring();   //should write onMediaApplicationStart
            mediaAppMonitor.registerMediaApplicationMonitorListener(application1, lis1);
            mediaAppMonitor.registerMediaApplicationMonitorListener(application1, lis2);
            application1.run();
            Thread.sleep(5000);
            application1.kill();
            Thread.sleep(200);
            mediaAppMonitor.stopMonitoring();
            runAfterTestCase();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
