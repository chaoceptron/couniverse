package Monitoring.lifeservice;

/**
 * Liveness testing classes
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 23.10.2007
 * Time: 16:47:26
 */
public class LifeSetup {

    final static int LIFE_PORT_NUMBER = 9999;
    final static byte[] TEST_BYTES = "CoUniverse: Liveness test".getBytes();

}
