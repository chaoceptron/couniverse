package Monitoring;


import utils.ApplicationProxy;

/**
 * Created by IntelliJ IDEA.
 * User: xsuchom1
 * Date: Nov 12, 2008
 * Time: 6:45:40 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MediaApplicationMonitorListener {

     // this is to be called when Media Application is found terminated
    public void onMediaApplicationDown(ApplicationProxy mediaApplication);

     // this is to be called when Media Application is found running
    public void onMediaApplicationStart(ApplicationProxy mediaApplication);

    
}
