/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package NetworkRepresentation;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matus
 */
public class CoNMLDecodeResult {
    private Map<String, CoNMLInterface> decodedNodes;   
    private Map<String, CoNMLInterface> decodedPorts;
    private Map<String, CoNMLInterface> decodedLinks;
    private Map<String, CoNMLInterface> decodedApps;

    public CoNMLDecodeResult() {
       this.decodedNodes = new HashMap<String, CoNMLInterface>();  
       this.decodedPorts = new HashMap<String, CoNMLInterface>();
       this.decodedLinks = new HashMap<String, CoNMLInterface>();
       this.decodedApps = new HashMap<String, CoNMLInterface>();
    }

    public Map<String, CoNMLInterface> getDecodedNodes() {
        return decodedNodes;
    }

    public void setDecodedNodes(Map<String, CoNMLInterface> decodedNodes) {
        if(decodedNodes == null){
            throw new IllegalArgumentException("Map<String, CoNML> cannot be null!");
        }
        this.decodedNodes = decodedNodes;
    }

    public Map<String, CoNMLInterface> getDecodedPorts() {
        return decodedPorts;
    }

    public void setDecodedPorts(Map<String, CoNMLInterface> decodedPorts) {
        if(decodedPorts == null){
            throw new IllegalArgumentException("Map<String, CoNML> cannot be null!");
        }
        this.decodedPorts = decodedPorts;
    }

    public Map<String, CoNMLInterface> getDecodedLinks() {
        return decodedLinks;
    }

    public void setDecodedLinks(Map<String, CoNMLInterface> decodedLinks) {
        if(decodedLinks == null){
            throw new IllegalArgumentException("Map<String, CoNML> cannot be null!");
        }
        this.decodedLinks = decodedLinks;
    }
    
    public Map<String, CoNMLInterface> getDecodedApps() {
        return decodedApps;
    }

    public void setDecodedApps(Map<String, CoNMLInterface> decodedApps) {
        if(decodedApps == null){
            throw new IllegalArgumentException("Map<String, CoNML> cannot be null!");
        }
        this.decodedApps = decodedApps;
    }
}
