package NetworkRepresentation;

import MediaAppFactory.MediaApplication;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.*;
import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.*;
import org.jdesktop.swingx.mapviewer.wms.WMSService;
import org.jdesktop.swingx.mapviewer.wms.WMSTileFactory;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import utils.CircleShimGenerator;
import utils.Converters;
import utils.ShimGenerator;

/**
 * Visualizer for the network and data streams using Google maps. It is based on JXMapView component
 * from SwingX and SwingX-WS projects. Alternatively, it can also use NASA maps, that have more
 * permissive license but that are much less descriptive.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 30.8.2007
 * Time: 16:47:18
 */
public class MapVisualizer extends JFrame {
    static Logger logger = Logger.getLogger("NetworkRepresentation");
    

    @SuppressWarnings({"FieldCanBeLocal"})
    private final DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink> networkTopologyGraph;
    private WaypointPainter<JXMapViewer> nodePainter;
    private boolean paintLinkLegend = false;

    private static final Dimension DEFAULT_SIZE = new Dimension(1000, 800);


    class NodeWaypoint extends Waypoint {
        final EndpointNetworkNode networkNode;
        GeoPosition nodePosition;

        public NodeWaypoint(EndpointNetworkNode n) {
            this.networkNode = n;
            this.nodePosition = new GeoPosition(n.getGeoLatitude(), n.getGeoLongitude());
        }

        public EndpointNetworkNode getNetworkNode() {
            return networkNode;
        }

        @SuppressWarnings({"DesignForExtension"})
        @Override
        public void setPosition(GeoPosition geoPosition) {
        }

        @Override
        public GeoPosition getPosition() {
            return nodePosition;
        }

        public int getShimX() {
            return this.networkNode.getShim().getShimX();
        }

        public int getShimY() {
            return this.networkNode.getShim().getShimY();
        }
    }

    class StreamWayponit extends Waypoint {
        final MediaApplication mediaApplication;
        final LogicalNetworkLink networkLink;
        GeoPosition fromPosition;
        GeoPosition toPosition;
        Color color;

        public StreamWayponit(MediaApplication mediaApplication, LogicalNetworkLink networkLink) {
            this.mediaApplication = mediaApplication;
            this.networkLink = networkLink;
            this.fromPosition = new GeoPosition(networkLink.getFromNode().getGeoLatitude(), networkLink.getFromNode().getGeoLongitude());
            this.toPosition = new GeoPosition(networkLink.getToNode().getGeoLatitude(), networkLink.getToNode().getGeoLongitude());
            this.color = new Color(255, 0, 0, 120);
        }

        public StreamWayponit(MediaApplication mediaApplication, LogicalNetworkLink networkLink, Color color) {
            this.mediaApplication = mediaApplication;
            this.networkLink = networkLink;
            this.fromPosition = new GeoPosition(networkLink.getFromNode().getGeoLatitude(), networkLink.getFromNode().getGeoLongitude());
            this.toPosition = new GeoPosition(networkLink.getToNode().getGeoLatitude(), networkLink.getToNode().getGeoLongitude());
            this.color = color;
        }

        @SuppressWarnings({"DesignForExtension"})
        @Override
        public void setPosition(GeoPosition geoPosition) {
        }

        @Override
        public GeoPosition getPosition() {
            return this.fromPosition;
        }

        public GeoPosition getFromPosition() {
            return fromPosition;
        }

        public GeoPosition getToPosition() {
            return toPosition;
        }

        public int getFromShimX() {
            return networkLink.getFromNode().getShim().getShimX();
        }

        public int getFromShimY() {
            return networkLink.getFromNode().getShim().getShimY();
        }

        public int getToShimX() {
            return networkLink.getToNode().getShim().getShimX();
        }

        public int getToShimY() {
            return networkLink.getToNode().getShim().getShimY();
        }

        public MediaApplication getMediaApplication() {
            return mediaApplication;
        }

        public LogicalNetworkLink getNetworkLink() {
            return networkLink;
        }

        public Color getColor() {
            return color;
        }
    }

    /**
     * This is a custom renderer, that support StreamWaypoints that have one of their ends out of map.
     */
    class NetworkPainter extends WaypointPainter<JXMapViewer> {
        @Override
        protected void doPaint(Graphics2D g, JXMapViewer map, int width, int height) {
            // TODO: for now we hope this won't happen
            /*
            if (this.renderer == null) {
                return;
            }
            */

            //figure out which waypoints are within this map viewport
            //so, get the bounds
            Rectangle viewportBounds = map.getViewportBounds();
            int zoom = map.getZoom();
            Dimension sizeInTiles = map.getTileFactory().getMapSize(zoom);
            int tileSize = map.getTileFactory().getTileSize(zoom);
            Dimension sizeInPixels = new Dimension(sizeInTiles.width * tileSize, sizeInTiles.height * tileSize);

            double vpx = viewportBounds.getX();
            // normalize the left edge of the viewport to be positive
            while (vpx < 0) {
                vpx += sizeInPixels.getWidth();
            }
            // normalize the left edge of the viewport to no wrap around the world
            while (vpx > sizeInPixels.getWidth()) {
                vpx -= sizeInPixels.getWidth();
            }

            // create two new viewports next to eachother
            Rectangle2D vp2 = new Rectangle2D.Double(vpx,
                    viewportBounds.getY(), viewportBounds.getWidth(), viewportBounds.getHeight());
            Rectangle2D vp3 = new Rectangle2D.Double(vpx - sizeInPixels.getWidth(),
                    viewportBounds.getY(), viewportBounds.getWidth(), viewportBounds.getHeight());

            //for each waypoint within these bounds
            for (Waypoint w : getWaypoints()) {
                Point2D point = map.getTileFactory().geoToPixel(w.getPosition(), map.getZoom());
                if (vp2.contains(point)) {
                    int x = (int) (point.getX() - vp2.getX());
                    int y = (int) (point.getY() - vp2.getY());
                    g.translate(x, y);
                    paintWaypoint(w, map, g);
                    g.translate(-x, -y);
                }
                if (vp3.contains(point)) {
                    int x = (int) (point.getX() - vp3.getX());
                    int y = (int) (point.getY() - vp3.getY());
                    g.translate(x, y);
                    paintWaypoint(w, map, g);
                    g.translate(-x, -y);
                }
                if (w instanceof StreamWayponit) {
                    // StreamWaypoints get painted independent of whether they are in viewports
                    {
                        int x = (int) (point.getX() - vp2.getX());
                        int y = (int) (point.getY() - vp2.getY());
                        g.translate(x, y);
                        paintWaypoint(w, map, g);
                        g.translate(-x, -y);
                    }
                }
            }
        }
    }

    @SuppressWarnings({"WeakerAccess"})
    public MapVisualizer(DirectedWeightedMultigraph<EndpointNetworkNode, LogicalNetworkLink> g) throws HeadlessException {
        super("Network Visualizer");

        this.networkTopologyGraph = g;
        checkAndAdjustShims();

        final int osm_max_zoom = 18;
        TileFactoryInfo osmTileFactoryInfo = new TileFactoryInfo(1,osm_max_zoom,osm_max_zoom,
                        256, true, true, // tile size is 256 and x/y orientation is normal
                        "http://tile.openstreetmap.org",
                        "x","y","z") {
                    @Override
                    public String getTileUrl(int x, int y, int zoom) {
                        zoom = osm_max_zoom-zoom;
                        String url = this.baseURL +"/"+zoom+"/"+x+"/"+y+".png";
                        return url;
                    }

                };
        TileFactory osmTileFactory = new DefaultTileFactory(osmTileFactoryInfo);


        JXMapViewer map = new JXMapViewer();
        //map.setTileFactory(getGoogleMapsTileFactory());
        //map.setZoom(GoogleMapsProviderTransparent.maximumZoomLevel);
        map.setTileFactory(osmTileFactory);
        map.setZoom(16);
        map.setAddressLocation(new GeoPosition(50.086226, 14.423289));

        this.nodePainter = new NetworkPainter();

        WaypointRenderer nodeRenderer = new WaypointRenderer() {
            @Override
            public boolean paintWaypoint(Graphics2D graphics2D, JXMapViewer jxMapViewer, Waypoint waypoint) {
                if (waypoint instanceof NodeWaypoint) {
                    EndpointNetworkNode node = ((NodeWaypoint) waypoint).getNetworkNode();
                    graphics2D = (Graphics2D) graphics2D.create();
                    // convert to map space
                    Point2D center = jxMapViewer.getTileFactory().geoToPixel(
                            waypoint.getPosition(), jxMapViewer.getZoom());
                    Rectangle bounds = jxMapViewer.getViewportBounds();
                    // this is a random (but constant) shim to avoid overlapping nodes
                    graphics2D.translate(((NodeWaypoint) waypoint).getShimX(), ((NodeWaypoint) waypoint).getShimY());

                    // get size of node description
                    Font f = graphics2D.getFont();
                    FontRenderContext frc = graphics2D.getFontRenderContext();
                    Rectangle2D tRect = f.getStringBounds(node.getNodeName(), frc);
                    LineMetrics lMetrics = f.getLineMetrics(node.getNodeName(), frc);
                    int boxWidth = (int) Math.round(tRect.getWidth() + 10);
                    int boxHeight = (int) Math.round(tRect.getHeight() + 5);

                    // draw the node
                    graphics2D.setColor(new Color(255, 0, 0, 50));
                    graphics2D.fillRoundRect((int) Math.round(-0.5 * boxWidth), (int) Math.round(-0.5 * boxHeight), boxWidth, boxHeight, 20, 20);
                    graphics2D.setColor(Color.BLACK);
                    graphics2D.drawString(node.getNodeName(), Math.round(-0.5 * tRect.getWidth()), Math.round(-0.5 * tRect.getHeight() + lMetrics.getAscent()));

                    // draw the anchor to physical location
                    if (((NodeWaypoint) waypoint).getShimX() != 0 || ((NodeWaypoint) waypoint).getShimY() != 0) {
                        graphics2D.setStroke(new BasicStroke(1));
                        graphics2D.drawLine(0, 0, -((NodeWaypoint) waypoint).getShimX(), -((NodeWaypoint) waypoint).getShimY());
                        graphics2D.fillOval(-((NodeWaypoint) waypoint).getShimX() - 2, -((NodeWaypoint) waypoint).getShimY() - 2, 4, 4);
                    }

                    // restore the original position
                    graphics2D.translate(-((NodeWaypoint) waypoint).getShimX(), -((NodeWaypoint) waypoint).getShimY());
                } else if (waypoint instanceof StreamWayponit) {
                    StreamWayponit streamWayponit = (StreamWayponit) waypoint;
                    Point2D fromPoint = jxMapViewer.getTileFactory().geoToPixel(
                            streamWayponit.getFromPosition(), jxMapViewer.getZoom());
                    fromPoint.setLocation(fromPoint.getX() + streamWayponit.getFromShimX(), fromPoint.getY() + streamWayponit.getFromShimY());
                    Point2D toPoint = jxMapViewer.getTileFactory().geoToPixel(
                            streamWayponit.getToPosition(), jxMapViewer.getZoom());
                    toPoint.setLocation(toPoint.getX() + streamWayponit.getToShimX(), toPoint.getY() + streamWayponit.getToShimY());
                    graphics2D.translate(streamWayponit.getFromShimX(), streamWayponit.getFromShimY());

                    // draw the line
                    MapVisualizer.logger.debug("Painting link from " + jxMapViewer.getTileFactory().geoToPixel(streamWayponit.getFromPosition(), jxMapViewer.getZoom()) + " to " + jxMapViewer.getTileFactory().geoToPixel(
                            streamWayponit.getToPosition(), jxMapViewer.getZoom()));
                    MapVisualizer.logger.debug("   i.e. with shim " + (int) fromPoint.getX() + " " + (int) fromPoint.getY() + " to " + (int) toPoint.getX() + " " + (int) toPoint.getY());
                    graphics2D.setColor(streamWayponit.getColor());
                    graphics2D.setStroke(new BasicStroke(3));
                    // graphics2D.drawLine(0, 0, (int) (toPoint.getX() - fromPoint.getX()), (int) (toPoint.getY() - fromPoint.getY()));
                    int posShimX = (int) Math.round(((toPoint.getX() - fromPoint.getX()) / 2) + 0.5 * streamWayponit.networkLink.getLinkShim() * toPoint.distance(fromPoint));
                    int posShimY = (int) Math.round(((toPoint.getY() - fromPoint.getY()) / 2) - 0.5 * streamWayponit.networkLink.getLinkShim() * toPoint.distance(fromPoint));
                    QuadCurve2D link = new QuadCurve2D.Double(0, 0, posShimX, posShimY, (int) (toPoint.getX() - fromPoint.getX()), (int) (toPoint.getY() - fromPoint.getY()));
                    graphics2D.draw(link);
                    graphics2D.fillOval((int) (toPoint.getX() - fromPoint.getX()) - 5, (int) (toPoint.getY() - fromPoint.getY()) - 5, 10, 10);

                    if (paintLinkLegend) {
                        // generate StreamLink description
                        // get size of node description
                        Font origFont = graphics2D.getFont();
                        Font smallFont = new Font(origFont.getFontName(), origFont.getStyle(), origFont.getSize() - 3);
                        graphics2D.setFont(smallFont);
                        Font f = graphics2D.getFont();
                        FontRenderContext frc = graphics2D.getFontRenderContext();
                        String slDescription = "" + streamWayponit.getNetworkLink().getFromInterface().getIpAddress() + " --> " + streamWayponit.getNetworkLink().getToInterface().getIpAddress() + ", " + Converters.bandwidthToString(streamWayponit.getMediaApplication().getMediaMaxBandwidth());
                        Rectangle2D tRect = f.getStringBounds(slDescription, frc);
                        LineMetrics lMetrics = f.getLineMetrics(slDescription, frc);
                        int boxWidth = (int) Math.round(tRect.getWidth() + 10);
                        int boxHeight = (int) Math.round(tRect.getHeight() + 5);
                        // draw it
                        // TODO - it would be great to find some better (more precise) position, as the shim{X,Y} may be rather way off the link
                        final float posCoeff = 0.6f;
                        graphics2D.setColor(new Color(streamWayponit.getColor().getRed(), streamWayponit.getColor().getGreen(), streamWayponit.getColor().getBlue(), 50));
                        graphics2D.fillRoundRect((int) Math.round(posCoeff * posShimX) + (int) Math.round(-0.5 * boxWidth), (int) Math.round(posCoeff * posShimY) + (int) Math.round(-0.5 * boxHeight), boxWidth, boxHeight, 20, 20);
                        graphics2D.setColor(new Color(0, 0, 0, 50));
                        graphics2D.drawString(slDescription, (int) Math.round(posCoeff * posShimX) + Math.round(-0.5 * tRect.getWidth()), (int) Math.round(posCoeff * posShimY) + Math.round(-0.5 * tRect.getHeight() + lMetrics.getAscent()));

                        // restore original font
                        graphics2D.setFont(origFont);
                    }

                    // restore the original position
                    graphics2D.translate(-streamWayponit.getFromShimX(), -streamWayponit.getFromShimY());
                }
                return false;
            }
        };

        this.nodePainter.setRenderer(nodeRenderer);
        map.setOverlayPainter(this.nodePainter);

        for (EndpointNetworkNode networkNode : this.networkTopologyGraph.vertexSet()) {
            this.nodePainter.getWaypoints().add(new NodeWaypoint(networkNode));
        }

        // this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().add(map);
        this.pack();
        this.setSize(DEFAULT_SIZE);
    }

    class GoogleMapsProviderTransparent {
        public static final String VERSION = "2.92";
        public static final int minimumZoomLevel = 1;
        public static final int maximumZoomLevel = 15;
        private static final int totalMapZoom = 17;
        private static final int tileSize = 256;
        private static final boolean xr2l = true;
        private static final boolean yt2b = true;
        private static final String baseURL = "http://mt2.google.com/mt?n=404&v=w" + VERSION;
        private static final String xparam = "x";
        private static final String yparam = "y";
        private static final String zparam = "zoom";

        /**
         * get the map providers info
         */
        public final TileFactoryInfo aTileFactoryInfo =
                new TileFactoryInfo(
                        minimumZoomLevel,
                        maximumZoomLevel,
                        totalMapZoom,
                        tileSize,// tile size is 256 and x/y orientation is normal
                        xr2l,
                        yt2b,
                        baseURL,
                        xparam,
                        yparam,
                        zparam
                );

        public TileFactory getDefaultTileFactory() {
            return (new DefaultTileFactory(aTileFactoryInfo));
        }
    }

    private TileFactory getGoogleMapsTileFactory() {

        // GoogleMaps TileFactory
        GoogleMapsProviderTransparent googleMaps = new GoogleMapsProviderTransparent();
        return googleMaps.getDefaultTileFactory();
    }

    private TileFactory getNASATileFactory() {
        WMSService wms = new WMSService();
        wms.setLayer("BMNG");
        wms.setBaseUrl("http://wms.jpl.nasa.gov/wms.cgi?");

        return new WMSTileFactory(wms);
    }

    public void addLink(MediaApplication ma, LogicalNetworkLink nl) {
        assert ma != null;
        assert nl != null;
        nodePainter.getWaypoints().add(new StreamWayponit(ma, nl));
    }

    public void addLink(MediaApplication ma, LogicalNetworkLink nl, Color c) {
        assert ma != null;
        assert nl != null;
        nodePainter.getWaypoints().add(new StreamWayponit(ma, nl, c));
    }

    public void removeLink(MediaApplication ma, LogicalNetworkLink nl) {
        StreamWayponit wp = null;
        for (Waypoint waypoint : nodePainter.getWaypoints()) {
            if (waypoint instanceof StreamWayponit) {
                StreamWayponit streamWayponit = (StreamWayponit) waypoint;
                if (streamWayponit.getMediaApplication() == ma && streamWayponit.getNetworkLink() == nl) {
                    wp = streamWayponit;
                }
            }
        }
        if (wp == null) {
            MapVisualizer.logger.error("Failed to find the streamWaypoint!");
            return;
        }
        nodePainter.getWaypoints().remove(wp);
    }

    public boolean isPaintLinkLegend() {
        return paintLinkLegend;
    }

    public void setPaintLinkLegend(boolean paintLinkLegend) {
        this.paintLinkLegend = paintLinkLegend;
    }

    /**
     * This automagically adjusts shims for nodes that have identical physical location
     */
    private void checkAndAdjustShims() {
        ArrayList<EndpointNetworkNode> nodeList = new ArrayList<EndpointNetworkNode>(networkTopologyGraph.vertexSet());
        class Position {
            double geoLongitude;
            double geoLatitude;

            public Position(double geoLongitude, double geoLatitude) {
                this.geoLongitude = geoLongitude;
                this.geoLatitude = geoLatitude;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) {
                    return true;
                }
                if (o == null || getClass() != o.getClass()) {
                    return false;
                }

                Position position = (Position) o;

                if (Double.compare(position.geoLatitude, geoLatitude) != 0) {
                    return false;
                }
                if (Double.compare(position.geoLongitude, geoLongitude) != 0) {
                    return false;
                }

                return true;
            }

            @Override
            public int hashCode() {
                int result;
                long temp;
                temp = geoLongitude != +0.0d ? Double.doubleToLongBits(geoLongitude) : 0L;
                result = (int) (temp ^ (temp >>> 32));
                temp = geoLatitude != +0.0d ? Double.doubleToLongBits(geoLatitude) : 0L;
                result = 31 * result + (int) (temp ^ (temp >>> 32));
                return result;
            }
        }
        HashMap<Position, ArrayList<EndpointNetworkNode>> identicalPositionList = new HashMap<Position, ArrayList<EndpointNetworkNode>>();
        for (int i = 0; i < nodeList.size(); i++) {
            Position p1 = new Position(nodeList.get(i).getGeoLongitude(), nodeList.get(i).getGeoLatitude());

            for (int j = 0; j < nodeList.size(); j++) {
                if (i != j) {
                    Position p2 = new Position(nodeList.get(j).getGeoLongitude(), nodeList.get(j).getGeoLatitude());
                    if (p1.equals(p2)) {
                        if (!identicalPositionList.containsKey(p1)) {
                            identicalPositionList.put(p1, new ArrayList<EndpointNetworkNode>());
                        }
                        ArrayList<EndpointNetworkNode> l = identicalPositionList.get(p1);
                        if (!l.contains(nodeList.get(i)) && !nodeList.get(i).getShim().isShimSet()) {
                            l.add(nodeList.get(i));
                        }
                        if (!l.contains(nodeList.get(j)) && !nodeList.get(j).getShim().isShimSet()) {
                            l.add(nodeList.get(j));
                        }
                    }
                }
            }
        }

        for (ArrayList<EndpointNetworkNode> networkNodes : identicalPositionList.values()) {
            final int shimCircleRadius = 40;
            ShimGenerator shimGenerator = new CircleShimGenerator(shimCircleRadius, networkNodes.size());
            for (EndpointNetworkNode networkNode : networkNodes) {
                networkNode.setShim(shimGenerator.generateShim());
            }
        }
    }
}
