package NetworkRepresentation;

import java.util.ArrayList;
import java.io.Serializable;

/**
 * Representation of lambda aspect of NetworkLink implementing Internet2 DCN OSCARS protocol
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 3.10.2007
 * Time: 18:31:30
 */
public class LambdaLink implements Serializable {

    LambdaLinkEndPoint fromLambdaLinkEndPoint;
    LambdaLinkEndPoint toLambdaLinkEndPoint;
    String lambdaLinkIDC;
    double bandwidth;
    long lastAllocationAttempt;
    boolean toAllocate;
    boolean isAllocated;
    String status;
    ArrayList<NetworkLink> associatedNetworkLinks;

    String lambdaReservationId;

    public LambdaLink() {
        this.fromLambdaLinkEndPoint = null;
        this.toLambdaLinkEndPoint = null;
        this.lambdaLinkIDC = "";
        this.bandwidth = 0;
        this.lastAllocationAttempt = 0;
        this.toAllocate = false;
        this.isAllocated = false;
        this.status = "UNKNOWN";
        this.associatedNetworkLinks = null;

        this.lambdaReservationId = null;
    }

    public LambdaLink(LambdaLinkEndPoint fromLambdaLinkEndPoint, LambdaLinkEndPoint toLambdaLinkEndPoint, double bandwidth) {
        this.toAllocate = false;
        this.isAllocated = false;
        this.status = "UNKNOWN";
        this.associatedNetworkLinks = new ArrayList<NetworkLink>();
        this.lastAllocationAttempt = 0;

        this.fromLambdaLinkEndPoint = fromLambdaLinkEndPoint;
        this.toLambdaLinkEndPoint = toLambdaLinkEndPoint;
        this.lambdaLinkIDC = fromLambdaLinkEndPoint.getLambdaLinkEndpointIDC();
        this.bandwidth = bandwidth;

    }

    Boolean isActive() {
        return status.equals("ACTIVE");
    }

    void addNetworkLink(NetworkLink networkLink) {
        associatedNetworkLinks.add(networkLink);
    }

    void removeNetworkLink(NetworkLink networkLink) {
        assert(associatedNetworkLinks.contains(networkLink));
        associatedNetworkLinks.remove(networkLink);
    }

    public double getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
    }

    public LambdaLinkEndPoint getFromLambdaLinkEndPoint() {
        return fromLambdaLinkEndPoint;
    }

    public void setFromLambdaLinkEndPoint(LambdaLinkEndPoint fromLambdaLinkEndPoint) {
        this.fromLambdaLinkEndPoint = fromLambdaLinkEndPoint;
    }

    public LambdaLinkEndPoint getToLambdaLinkEndPoint() {
        return toLambdaLinkEndPoint;
    }

    public void setToLambdaLinkEndPoint(LambdaLinkEndPoint toLambdaLinkEndPoint) {
        this.toLambdaLinkEndPoint = toLambdaLinkEndPoint;
    }

    public String getLambdaLinkIDC() {
        return lambdaLinkIDC;
    }

    public void setLambdaLinkIDC(String lambdaLinkIDC) {
        this.lambdaLinkIDC = lambdaLinkIDC;
    }

    public long getLastAllocationAttempt() {
        return lastAllocationAttempt;
    }

    public void setLastAllocationAttempt(long lastAllocationAttempt) {
        this.lastAllocationAttempt = lastAllocationAttempt;
    }

    public boolean isAllocated() {
        return isAllocated;
    }

    public void setAllocated(boolean allocated) {
        isAllocated = allocated;
    }

    public ArrayList<NetworkLink> getAssociatedNetworkLinks() {
        return associatedNetworkLinks;
    }

    public void setAssociatedNetworkLinks(ArrayList<NetworkLink> associatedNetworkLinks) {
        this.associatedNetworkLinks = associatedNetworkLinks;
    }

    public String getLambdaReservationId() {
        return lambdaReservationId;
    }

    public void setLambdaReservationId(String lambdaReservationId) {
        this.lambdaReservationId = lambdaReservationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isToAllocate() {
        return toAllocate;
    }

    public void setToAllocate(boolean toAllocate) {
        this.toAllocate = toAllocate;
    }

    @Override
    public String toString() {
        return "" + this.getFromLambdaLinkEndPoint() + "  --->  " + this.getToLambdaLinkEndPoint();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LambdaLink that = (LambdaLink) o;

        if (this.getBandwidth() != that.getBandwidth()) return false;

        if (this.getFromLambdaLinkEndPoint().equals(that.getFromLambdaLinkEndPoint()) && this.getToLambdaLinkEndPoint().equals(that.getToLambdaLinkEndPoint())) return true;
        if (this.getFromLambdaLinkEndPoint().equals(that.getToLambdaLinkEndPoint()) && this.getToLambdaLinkEndPoint().equals(that.getFromLambdaLinkEndPoint())) return true;

        return false;
    }
}
