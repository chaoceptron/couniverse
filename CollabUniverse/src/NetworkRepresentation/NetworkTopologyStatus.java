package NetworkRepresentation;

import java.text.SimpleDateFormat;
import java.util.Date;
import nu.xom.Attribute;
import nu.xom.Element;

/**
 * This is a representation of status of NetworkTopology - if it was changed and when.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 9.9.2007
 * Time: 20:24:27
 */
public class NetworkTopologyStatus implements CoNMLInterface, Comparable {
    private boolean changed;
    private long changedStamp;
    private long changesCount;

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public long getChangedStamp() {
        return changedStamp;
    }

    public void setChangedStamp(long changedStamp) {
        this.changedStamp = changedStamp;
    }

    public long getChangesCount() {
        return changesCount;
    }

    public void setChangesCount(long changesCount) {
        this.changesCount = changesCount;
    }

    /**
     * 
     * @param o object to compare
     * @return 1 if this object's stamp is newer, -1 if it's older, 0 if they are both equally old
     */
    @Override
    public int compareTo(Object o) {        
        NetworkTopologyStatus otherStatus = (NetworkTopologyStatus) o; 
        long stampThis = this.changedStamp;
        long stampOther = otherStatus.changedStamp;
        if(stampThis < stampOther){
            return -1;
        }
        if(stampThis == stampOther){
            return 0;
        }
        return 1;
    }    

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        Element baseElementNode = new Element("cou:NetworkTopologyStatus", CoNMLInterface.CONML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().appendChild(baseElementNode);            
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElementNode);
            
            //adding "version" attribute to parent element node (note: according to NML schema, any network object may have this attribute)
            Element parent = (Element) parameters.getParentNode();
            SimpleDateFormat stf = new SimpleDateFormat ("yyyyMMDD'T'hhmmss'Z'");//TODO check correctness
            Date d = new Date();
            d.setTime(this.changedStamp);
            parent.addAttribute(new Attribute("version", stf.format(d)));
        }  
        
        //writing changed
        Element changedElem = new Element("cou:changed", CoNMLInterface.CONML_NS);
        changedElem.appendChild(Boolean.toString(this.changed));
        baseElementNode.appendChild(changedElem);
        
        //writing changes count
        Element changesCountElem = new Element("cou:changesCount", CoNMLInterface.CONML_NS);
        changesCountElem.appendChild(Long.toString(this.changesCount));
        baseElementNode.appendChild(changesCountElem);
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
    }
}
