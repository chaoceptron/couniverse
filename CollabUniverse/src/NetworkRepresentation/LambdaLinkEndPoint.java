package NetworkRepresentation;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: xliska
 * Date: 28.4.2009
 * Time: 18:51:16
 */
public class LambdaLinkEndPoint implements Serializable {
    private String lambdaLinkEndpoint;
    private String lambdaLinkEndpointIDC;
    private boolean lambdaLinkEndpointTagged;
    private String lambdaLinkEndpointVlan;

    public LambdaLinkEndPoint() {
        this.lambdaLinkEndpoint = null;
        this.lambdaLinkEndpointIDC = null;
        this.lambdaLinkEndpointTagged = false;
        this.lambdaLinkEndpointVlan = "any";
    }

    public LambdaLinkEndPoint(String lambdaLinkEndpoint, String lambdaLinkEndpointIDC, boolean lambdaLinkEndpointTagged, String lambdaLinkEndpointVlan) {
        this.lambdaLinkEndpoint = lambdaLinkEndpoint;
        this.lambdaLinkEndpointIDC = lambdaLinkEndpointIDC;
        this.lambdaLinkEndpointTagged = lambdaLinkEndpointTagged;
        this.lambdaLinkEndpointVlan = lambdaLinkEndpointVlan;
    }

    public String getLambdaLinkEndpoint() {
        return lambdaLinkEndpoint;
    }

    public void setLambdaLinkEndpoint(String lambdaLinkEndpoint) {
        this.lambdaLinkEndpoint = lambdaLinkEndpoint;
    }

    public String getLambdaLinkEndpointIDC() {
        return lambdaLinkEndpointIDC;
    }

    public void setLambdaLinkEndpointIDC(String lambdaLinkEndpointIDC) {
        this.lambdaLinkEndpointIDC = lambdaLinkEndpointIDC;
    }

    public boolean isLambdaLinkEndpointTagged() {
        return lambdaLinkEndpointTagged;
    }

    public void setLambdaLinkEndpointTagged(boolean lambdaLinkEndpointTagged) {
        this.lambdaLinkEndpointTagged = lambdaLinkEndpointTagged;
    }

    public String getLambdaLinkEndpointVlan() {
        return lambdaLinkEndpointVlan;
    }

    public void setLambdaLinkEndpointVlan(String lambdaLinkEndpointVlan) {
        this.lambdaLinkEndpointVlan = lambdaLinkEndpointVlan;
    }

    @Override
    public String toString() {
        return "" + this.getLambdaLinkEndpoint();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LambdaLinkEndPoint that = (LambdaLinkEndPoint) o;

        return this.getLambdaLinkEndpoint().equals(that.getLambdaLinkEndpoint());

    }
}
