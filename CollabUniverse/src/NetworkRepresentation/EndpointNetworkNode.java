package NetworkRepresentation;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationConsumer;
import MediaAppFactory.MediaApplicationDistributor;
import MediaAppFactory.MediaApplicationProducer;
import MediaAppFactory.MediaStream;
import MediaApplications.HDVMPEG2Consumer;
import MediaApplications.HDVMPEG2Producer;
import MediaApplications.Polycom;
import MediaApplications.PolycomConsumer;
import MediaApplications.PolycomProducer;
import MediaApplications.RAT;
import MediaApplications.Rum;
import MediaApplications.RumHD;
import MediaApplications.UltraGrid1500Consumer;
import MediaApplications.UltraGrid1500Producer;
import MediaApplications.UltraGrid750Consumer;
import MediaApplications.UltraGrid750Producer;
import MediaApplications.UltraGridDXTConsumer;
import MediaApplications.UltraGridDXTProducer;
import MediaApplications.VIC;
import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArraySet;
import net.jxta.document.MimeMediaType;
import net.jxta.endpoint.ByteArrayMessageElement;
import net.jxta.endpoint.Message;
import net.jxta.endpoint.MessageElement;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.XPathContext;
import utils.ProxyNodeConnection;
import utils.ProxyNodeInterfacePolycomFX;
import utils.ProxyNodeInterfacePolycomFake;
import utils.ProxyNodeInterfacePolycomHDX;

/**
 * Network node representation. Each network node should maintain information on itself. The NetworkNode
 * class supports serializing the instance into a XML to be sent over JXTA pipe.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 16:33:43
 */
public class EndpointNetworkNode extends GeneralNetworkNode implements Serializable, CoNMLInterface {
    
    private String nodePeerID;  // peer ID in JXTA network
    private byte[] nodeInputPipeAdvDoc; // local node JXTA input pipe advertisement to communicate with the node
    private boolean localNode;  // is this node me?
    private boolean proxyNode;  // is this node actually a proxy node that controls the real node which can't run CoUniverse
    private String proxyString;  // specification of the proxy, used to create connection to the proxied box
    private transient ProxyNodeConnection proxyNodeConnection;  // connection to proxy node
    private NetworkSite nodeSite;
    private CopyOnWriteArraySet<MediaApplication> nodeApplications;  // applications available on the node
    private CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces;  // node interfaces described using its IP addresses
    
    /**
     * This is an empty JavaBean constructor in order to support XMLEncoder and XMLDecoder
     */
    public EndpointNetworkNode() {
        super();

        this.nodePeerID = "";
        this.nodeInputPipeAdvDoc = null;
        this.localNode = true;
        this.proxyNode = false;
        this.proxyString = "";
        this.nodeSite = new NetworkSite("");
        this.nodeApplications = new CopyOnWriteArraySet<MediaApplication>();
        this.nodeInterfaces = new CopyOnWriteArraySet<EndpointNodeInterface>();
        this.nodeType = GeneralNetworkNode.NODE_TYPE_ENDPOINT;
    }

    /**
     * EndpointNetworkNode constructor.
     * <p/>
     * @param nodeName         name of the node
     * @param nodePeerID       ID of the node in the JXTA network
     * @param nodeInputPipeAdv byte array form of JXTA InputPipe advertisement used for communication with the node Peer
     * @param localNode        flags whether the node is local, i.e. whether its me ;) Generally, it should be true as the NetworkNode should be constructed only on the node where it's local
     * @param proxyNode        flags whether the node is proxy to some other equipment like Polycom
     * @param nodeSite         site the node is belonging to
     * @param nodeApplications applications/capabilites of the node
     * @param nodeInterfaces   network interfaces available on the node
     */
    public EndpointNetworkNode(String nodeName, String nodePeerID, byte[] nodeInputPipeAdv, boolean localNode, boolean proxyNode, NetworkSite nodeSite,
                       CopyOnWriteArraySet<MediaApplication> nodeApplications, CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces) {
        super(nodeName);

        this.nodePeerID = nodePeerID;
        this.nodeInputPipeAdvDoc = nodeInputPipeAdv;
        this.localNode = localNode;
        this.proxyNode = proxyNode;
        this.proxyString = "";
        this.nodeSite = nodeSite;
        this.nodeApplications = new CopyOnWriteArraySet<MediaApplication>();
        if(nodeApplications != null){
            this.nodeApplications = nodeApplications;
        }
        this.nodeInterfaces = new CopyOnWriteArraySet<EndpointNodeInterface>();
        if(nodeInterfaces != null){
            this.nodeInterfaces = nodeInterfaces;
        }
        this.nodeType = GeneralNetworkNode.NODE_TYPE_ENDPOINT;
        InitApplicationParentNodes();
        InitInterfaceParentNodes();
    }

    private void InitApplicationParentNodes() {
        if (!this.nodeApplications.isEmpty()) {
            for (MediaApplication nodeApplication : nodeApplications) {
                nodeApplication.setParentNode(this);
            }
        }
    }
    
    private void InitInterfaceParentNodes() {
        if (!this.nodeInterfaces.isEmpty()) {
            for (EndpointNodeInterface nodeInterface : nodeInterfaces) {
                nodeInterface.setParentNode(this);
            }
        }
    }

    /**
     * Gets node ID in JXTA network.
     * <p/>
     *
     * @return ID of the node
     */
    public String getNodePeerID() {
        return nodePeerID;
    }

    /**
     * Gets node input pipe advertisement
     * <p/>
     *
     * @return XMLDocument form of pipe advertisement of the node peer
     */
    public byte[] getNodeInputPipeAdv() {
        return nodeInputPipeAdvDoc;
    }

    /**
     * Does this object represent a local node?
     * </p>
     *
     * @return true iff the node is local
     */
    public boolean isLocalNode() {
        return localNode;
    }

    /**
     * Returns the site the node is belonging to.
     * <p/>
     *
     * @return site representation
     */
    public NetworkSite getNodeSite() {
        return nodeSite;
    }

    /**
     * Serializes NetworkNode representation into a UTF-8 XML message
     * <p/>
     *
     * @return byte array with UTF-8 XML representation
     */
    @Deprecated
    @Override
    public byte[] serializeIntoXML() {
        boolean wasMe = this.localNode;

        this.localNode = false; //  when serializing, we never want to tell that the copied object is a localnode -
        // thus the receiver of this object is not tempted to apply any commands on this node
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        XMLEncoder xmlenc = new XMLEncoder(output);
        xmlenc.writeObject(this);
        xmlenc.close();
        
        this.localNode = wasMe;
        // this is important to set the localNode value back!!!
 //System.out.println("serializeIntoXML: " + output.toString());
        NetworkNode.logger.trace("Network node message size: " + output.size());
        NetworkNode.logger.trace("Network node message:");
        NetworkNode.logger.trace(output.toString());        
        NetworkNode.logger.trace("");
        return output.toByteArray();
    }

    /**
     * Converts the UTF-8 XML message (from XMLEncoder) to JXTA pipe MessageElement.
     * <p/>
     *
     * @param messageName messageElement name
     * @return MessageElement to be put into JXTA Message
     */
    @Deprecated
    @Override
    public MessageElement toPipeMessageElement(String messageName) {
        return new ByteArrayMessageElement(messageName,
                MimeMediaType.XMLUTF8, this.serializeIntoXML(), null);
    }

    /**
     * Converts the UTF-8 XML message (from XMLEncoder) to JXTA pipe Message.
     * <p/>
     *
     * @param messageName messageElement name
     * @return Message to be put into JXTA pipe
     */
    @Deprecated
    @Override
    public Message toPipeMessage(String messageName) {
        Message outMessage = new Message();
        outMessage.addMessageElement(this.toPipeMessageElement(messageName));
        return outMessage;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param nodePeerID node JXTA peer ID
     */
    public void setNodePeerID(String nodePeerID) {
        this.nodePeerID = nodePeerID;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param inputPipeAdv node JXTA inputPipe advertisement
     */
    public void setNodeInputPipeAdv(byte[] inputPipeAdv) {
        this.nodeInputPipeAdvDoc = inputPipeAdv;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param localNode is it a local node?
     */
    public void setLocalNode(boolean localNode) {
        this.localNode = localNode;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param nodeSite home site of the node
     */
    public void setNodeSite(NetworkSite nodeSite) {
        this.nodeSite = nodeSite;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param nodeApplications applications running on the node
     */
    public void setNodeApplications(CopyOnWriteArraySet<MediaApplication> nodeApplications) {
        this.nodeApplications = nodeApplications;
        if (!nodeApplications.isEmpty()) {
            for (MediaApplication nodeApplication : nodeApplications) {
                nodeApplication.setParentNode(this);
            }
        }
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param nodeInterfaces node interfaces
     */
    public void setNodeInterfaces(CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces) {
        this.nodeInterfaces = nodeInterfaces;
    }

    /**
     * JavaBean enforced getter
     * <p/>
     *
     * @return list of MediaApplications of the node
     */
    public CopyOnWriteArraySet<MediaApplication> getNodeApplications() {
        return nodeApplications;
    }

    /**
     * JavaBean enforced getter
     * <p/>
     *
     * @return list of NodeInterfaces of the node
     */
    public CopyOnWriteArraySet<EndpointNodeInterface> getNodeInterfaces() {
        return nodeInterfaces;
    }
    
    public EndpointNodeInterface getNodeInterface(String nodeInterfaceName) {
        for (EndpointNodeInterface iface : nodeInterfaces) {
            if (iface.getNodeInterfaceName().equals(nodeInterfaceName)) {
                return iface;                
            }
        }
        return null;
    }
    
//    public EndpointNodeInterface getNodeInterface(EndpointSubNetwork subnet) {
//        for (EndpointNodeInterface iface : this.nodeInterfaces) {
//            if (iface.getSubnet().equals(subnet.getSubNetName()))
//                return iface;
//        }
//        return null;
//    }

    /**
     * Adds an application to the existing list
     * <p/>
     *
     * @param ma application to be added
     */
    public void addApplication(MediaApplication ma) {
        nodeApplications.add(ma);
        ma.setParentNode(this);
    }

    /**
     * Adds an interface to the existing list
     * <p/>
     *
     * @param address interface object for the node
     */
    public void addInterface(EndpointNodeInterface address) {
        if (address.getParentNode() != this) {
            throw new RuntimeException("Attempting to add a non-local interface!!!");
        }
        nodeInterfaces.add(address);
    }

    /**
     * Removes an exsting application from the list
     * <p/>
     *
     * @param ma the MediaApplication to remove
     */
    public void removeApplication(MediaApplication ma) {
        nodeApplications.remove(ma);
    }

    /**
     * Removes an existing interface from the list
     * <p/>
     *
     * @param ni the NetworkInterface to remove
     */
    public void removeInterface(EndpointNodeInterface ni) {
        nodeInterfaces.remove(ni);
    }

    public boolean hasDistributor() {
        for (MediaApplication ma : this.nodeApplications) {
            if (ma instanceof MediaApplicationDistributor) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean hasProducer() {
        for (MediaApplication ma : this.nodeApplications) {
            if (ma instanceof MediaApplicationProducer) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean hasConsumer() {
        for (MediaApplication ma : this.nodeApplications) {
            if (ma instanceof MediaApplicationConsumer) {
                return true;
            }
        }
        
        return false;
    }
    
    public boolean isProxyNode() {
        return proxyNode;
    }

    public void setProxyNode(boolean proxyNode) {
        this.proxyNode = proxyNode;
    }

    public String getProxyString() {
        return proxyString;
    }

    public void setProxyString(String proxyString) {
        this.proxyString = proxyString;
    }

    public void initProxyNodeConnection() {
        String[] parts = proxyString.split(":"); 
        if (parts[0].equals(ProxyNodeConnection.PolycomHDXType)) {
            assert parts.length == 4;
            proxyNodeConnection = new ProxyNodeConnection(new ProxyNodeInterfacePolycomHDX(parts[1], Integer.decode(parts[2]), parts[3]));
            proxyNodeConnection.start();
        }
        else if (parts[0].equals(ProxyNodeConnection.PolycomFXType)) {
            assert parts.length == 4;
            proxyNodeConnection = new ProxyNodeConnection(new ProxyNodeInterfacePolycomFX(parts[1], Integer.decode(parts[2]), parts[3]));
            proxyNodeConnection.start();
        }
        else if (parts[0].equals(ProxyNodeConnection.PolycomFakeType)) {
            assert parts.length == 4;
            proxyNodeConnection = new ProxyNodeConnection(new ProxyNodeInterfacePolycomFake(parts[1], Integer.decode(parts[2]), parts[3]));
            proxyNodeConnection.start();
        }
    }

    public void stopProxyNodeConnection() {
        assert proxyNodeConnection != null;
        proxyNodeConnection.stop();
    }

    public ProxyNodeConnection getProxyNodeConnection() {
        return proxyNodeConnection;
    }

    public void setProxyNodeConnection(ProxyNodeConnection proxyNodeConnection) {
        this.proxyNodeConnection = proxyNodeConnection;
    }

    @Override
    public String toString() {
        return "" + this.getNodeName() + " (" + this.getNodePeerID() + ") of " + this.getNodeSite().getSiteName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!super.equals(obj)){
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        } 
        
        Element extensionElem = new Element("cou:EndpointNetworkNode", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(extensionElem);        
        
        if(this.nodePeerID != null){
            Element nodePeerIDElem = new Element("cou:nodePeerID", CoNMLInterface.CONML_NS);        
            nodePeerIDElem.appendChild(this.nodePeerID); 
            extensionElem.appendChild(nodePeerIDElem);
        }

        if(this.proxyNode && this.proxyString != null){
            Element proxyStringElem = new Element("cou:proxyString", CoNMLInterface.CONML_NS);        
            proxyStringElem.appendChild(this.proxyString); 
            extensionElem.appendChild(proxyStringElem);
        }
        
        if(this.nodeSite != null){
            this.nodeSite.encodeToCoNML(result, new CoNMLEncodeParameters(extensionElem));            
        }
        
        //writing node media applications
        CoNMLEncodeParameters mediaParameters = new CoNMLEncodeParameters();
        mediaParameters.setParentNode(extensionElem);//this node is a parent of media applications        
        for(MediaApplication application: this.nodeApplications){
            application.encodeToCoNML(result, mediaParameters);            
        }
        
        //writing interfaces
        for(EndpointNodeInterface iface: this.nodeInterfaces){
            CoNMLEncodeParameters inboundPortParameters = new CoNMLEncodeParameters(parameters.getParentNode());//each interface needs its own parameters
            inboundPortParameters.setInputsRelation(parameters.getInputsRelation());
            iface.encodeToCoNML(result, inboundPortParameters);
            
            CoNMLEncodeParameters outboundPortParameters = new CoNMLEncodeParameters(parameters.getParentNode());//each interface needs its own parameters
            outboundPortParameters.setOutputsRelation(parameters.getOutputsRelation());
            iface.encodeToCoNML(result, outboundPortParameters);
        }
        
        return new CoNMLExtensionNodes(extNodes.getMainNode(), extensionElem, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS);
        
        String generalMediaAppQuery = "cou:MediaApplication[";     
        String xPathPrefix = "cou:MediaApplication/cou:";
        String appName;
        Nodes mediaApps;
        
        if(parameters.getDecodeMode() == 0){
            super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));        

            this.localNode = false;        

            Element nodePeerIDElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("nodePeerID", CoNMLInterface.CONML_NS);
            if(nodePeerIDElem != null){
                this.nodePeerID = nodePeerIDElem.getValue();
            }

            Element proxyStringElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("proxyString", CoNMLInterface.CONML_NS);
            if(proxyStringElem != null){
                this.proxyString = proxyStringElem.getValue();
                this.proxyNode = true;
            }

            Element nodeSiteElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("networkSite", CoNMLInterface.CONML_NS);
            if(nodeSiteElem != null){
                this.nodeSite.decodeFromCoNML(result, new CoNMLDecodeParameters(nodeSiteElem));
            }            
            
            appName = "HDVMPEG2Producer";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                HDVMPEG2Producer app = new HDVMPEG2Producer();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
            
            appName = "PolycomProducer";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                PolycomProducer app = new PolycomProducer();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
            
            appName = "Rum";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                Rum app = new Rum();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }

            appName = "RumHD";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                RumHD app = new RumHD();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
            
            appName = "UltraGrid1500Producer";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                UltraGrid1500Producer app = new UltraGrid1500Producer();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
            
            appName = "UltraGrid750Producer";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                UltraGrid750Producer app = new UltraGrid750Producer();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
            
            appName = "UltraGridDXTProducer";
            generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
            mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
            for(int i = 0; i < mediaApps.size(); i++){
                UltraGridDXTProducer app = new UltraGridDXTProducer();
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
        }
        
        //decoding applications  
        appName = "HDVMPEG2Consumer";
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy, because its source media application may not be present in result.decodedApps
                result.getDecodedApps().put(id, new HDVMPEG2Consumer());                
            }else{//then we decode the whole thing. dummy version of source media application should now be contained in result.decodedApps
                HDVMPEG2Consumer app = (HDVMPEG2Consumer) result.getDecodedApps().get(id);//presence of application in result.decodedApps is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }
        
        appName = "Polycom";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new Polycom());                
            }else{//then we decode the whole thing
                Polycom app = (Polycom) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "PolycomConsumer";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new PolycomConsumer());                
            }else{//then we decode the whole thing
                PolycomConsumer app = (PolycomConsumer) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "RAT";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new RAT());                
            }else{//then we decode the whole thing
                RAT app = (RAT) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "UltraGrid1500Consumer";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new UltraGrid1500Consumer());                
            }else{//then we decode the whole thing
                UltraGrid1500Consumer app = (UltraGrid1500Consumer) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "UltraGrid750Consumer";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new UltraGrid750Consumer());                
            }else{//then we decode the whole thing
                UltraGrid750Consumer app = (UltraGrid750Consumer) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "UltraGridDXTConsumer";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new UltraGridDXTConsumer());
            }else{//then we decode the whole thing
                UltraGridDXTConsumer app = (UltraGridDXTConsumer) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        appName = "VIC";
        generalMediaAppQuery = generalMediaAppQuery.concat("not(cou:" + appName + ") and ");
        mediaApps = parameters.getCurrentNode().query(xPathPrefix + appName, context);
        for(int i = 0; i < mediaApps.size(); i++){
            String id = ((Element) mediaApps.get(i).getParent()).getAttributeValue("id");
            if(id == null){
                continue;
            }
            if(parameters.getDecodeMode() == 0){//first, we create a dummy
                result.getDecodedApps().put(id, new VIC());
            }else{//then we decode the whole thing
                VIC app = (VIC) result.getDecodedApps().get(id);//presence of application is assumed!!!
                if(app != null){
                    app.decodeFromCoNML(result, new CoNMLDecodeParameters(mediaApps.get(i)));
                    app.setParentNode(this);
                    this.nodeApplications.add(app);
                }
            }
        }

        //general ones
        if(parameters.getDecodeMode() == 0){
            generalMediaAppQuery = generalMediaAppQuery.concat("true()]");        
            Nodes generalMediaApps = parameters.getCurrentNode().query(generalMediaAppQuery, context);
            for(int i = 0; i < generalMediaApps.size(); i++){
                MediaApplication app = new MediaApplication("", new CopyOnWriteArraySet<MediaStream>());
                app.decodeFromCoNML(result, new CoNMLDecodeParameters(generalMediaApps.get(i)));
                app.setParentNode(this);
                this.nodeApplications.add(app);
            }
        }
    }
    
    /**
     * Updates attributes of this node relevant to network topology
     * according to attributes of a given node.
     * Attributes nodeApplications and nodeInterfaces of given node
     * are linked to this node. Superclass also calls it's update.
     * @param newerNode If null, no changes are made.
     */
    public void update(EndpointNetworkNode newerNode){
        if(newerNode == null){
            return;
        }
        super.update(newerNode);        
        this.nodeApplications = newerNode.getNodeApplications();
        this.nodeInterfaces = newerNode.getNodeInterfaces();        
        this.nodePeerID = newerNode.getNodePeerID();
        this.nodeSite = newerNode.getNodeSite();
        this.proxyNode = newerNode.isProxyNode();
        this.proxyString = newerNode.getProxyString();
    }
}
