/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import MediaAppFactory.MediaApplication;
import MediaApplications.RAT;
import MediaApplications.UltraGridDXTConsumer;
import MediaApplications.UltraGridDXTProducer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

/**
 *
 * @author Matus
 */
public class CoNML_TEST {
    public static void main(String[] args) throws TransformerConfigurationException, TransformerException {
        PartiallyKnownNetworkTopology topology = new PartiallyKnownNetworkTopology();  
        topology.setName("Topologia 1");
        
        NetworkSite site1 = new NetworkSite("Brno");
        
        //node 1
        EndpointNetworkNode eNode1 = new EndpointNetworkNode();
        eNode1.setNodeName("Koncovy uzol A");
        eNode1.setNodeSite(site1);
        
        //apps for node1
        UltraGridDXTConsumer app = new UltraGridDXTConsumer("UltraGrid konzument");
        app.setSourceSite(site1);
        //app.setMediaStreams(streams);
        eNode1.addApplication(app);
        
        RAT rat = new RAT("RAT application");        
        eNode1.addApplication(rat);  
        
        //ports for node 1
        EndpointNodeInterface iface1 = new EndpointNodeInterface("Sietove rozhranie uzlu A", "94.113.217.73", "255.255.240.0", 333, "subnet_1", eNode1); 
        eNode1.addInterface(iface1);
        
        //node 2
        EndpointNetworkNode eNode2 = new EndpointNetworkNode(); 
        eNode2.setNodeSite(site1);
        eNode2.setProxyNode(true);
        eNode2.setProxyString("This is a proxy string.");
        eNode2.setNodeName("Koncovy uzol B");
        
        UltraGridDXTProducer app2 = new UltraGridDXTProducer("UltraGrid producent");
        app.setSource(app2);        
        app2.setTargetIP("94.113.217.73");
        app2.setApplicationCmdOptions("none");
        app2.setTargetNetworkNode(eNode1);
        eNode2.addApplication(app2);
        
        //ports for node 2
        EndpointNodeInterface iface2 = new EndpointNodeInterface("Sietove rozhranie uzlu B", "94.113.217.74", "253.253.253.253", 512, "subnet_2", eNode2);        
        eNode2.addInterface(iface2); 
        
        //node 3
        PhysicalNetworkNode pNode = new PhysicalNetworkNode("Fyzicky uzol");
        pNode.setGeoLatitude(49.203544);
        pNode.setGeoLongitude(16.603721);
        
        //physical links
        PhysicalNetworkLink pLinkA1 = new PhysicalNetworkLink("Fyzicky spoj A1", 84, eNode1, pNode, true, iface1);
        PhysicalNetworkLink pLinkA2 = new PhysicalNetworkLink("Fyzicky spoj A2", 85, pNode, eNode1, true, iface1);
        pLinkA1.setBackLink(pLinkA2);
        pLinkA2.setBackLink(pLinkA1);
        
        PhysicalNetworkLink pLinkB2 = new PhysicalNetworkLink("Fyzicky spoj B2", 86, eNode2, pNode, true, iface2);
        PhysicalNetworkLink pLinkB1 = new PhysicalNetworkLink("Fyzicky spoj B1", 87, pNode, eNode2, true, iface2);
        pLinkB2.setBackLink(pLinkB1);
        pLinkB1.setBackLink(pLinkB2);
        
        UnknownNetworkNode uNode1 = new UnknownNetworkNode("Neznamy uzol");
        PhysicalNetworkLink pLinkX1 = new PhysicalNetworkLink("Fyzicky spoj X1", 102, uNode1, pNode, true);
        PhysicalNetworkLink pLinkX2 = new PhysicalNetworkLink("Fyzicky spoj X2", 105, pNode, uNode1, true);
        pLinkX1.setBackLink(pLinkX2);
        pLinkX2.setBackLink(pLinkX1);        
       
        
        //logical links between node 1 and 2
        LogicalNetworkLink lLink1 = new LogicalNetworkLink();
        lLink1.fromNode = eNode1;
        lLink1.toNode = eNode2;
        lLink1.setFromInterface(iface1);
        lLink1.setToInterface(iface2);
        lLink1.setWeight(73.1);
        lLink1.setLatency(3.14);
        
        LogicalNetworkLink lLink2 = new LogicalNetworkLink();
        lLink2.fromNode = eNode2;
        lLink2.toNode = eNode1;
        lLink2.setFromInterface(iface2);
        lLink2.setToInterface(iface1);
        lLink2.setWeight(50.6);
        lLink2.setLatency(1.16);
        
        //adding nodes to topology
        topology.addNetworkNode(eNode1);  
        topology.addNetworkNode(eNode2);
        topology.addNetworkNode(pNode);  
        topology.addNetworkNode(uNode1);
        
        topology.getNetworkTopologyGraph().addEdge(eNode1, eNode2, lLink1);
        topology.getNetworkTopologyGraph().addEdge(eNode2, eNode1, lLink2);        
        
        topology.addPhysicalLink(pLinkA1);
        topology.addPhysicalLink(pLinkA2);
        topology.addPhysicalLink(pLinkB1);
        topology.addPhysicalLink(pLinkB2);
        topology.addPhysicalLink(pLinkX1);
        topology.addPhysicalLink(pLinkX2);
        
        topology.associateLinks(pLinkA1, lLink1);
        topology.associateLinks(pLinkB1, lLink1);
        topology.associateLinks(pLinkA2, lLink2);
        topology.associateLinks(pLinkB2, lLink2);     
       
        /*EndpointSubNetwork subnet = new EndpointSubNetwork();
        subnet.addSubNetworkNodeInterface(iface_1_1);
        subnet.addSubNetworkNodeInterface(iface_2_1);*/
        /*topology.associateInternodeLinks(eNode1, eNode2, subnet, null);
        HashMap<EndpointSubNetwork> hm = new HashMap<EndpointSubNetwork, >();
        topology.setSubNets(subnet);*/        
        
        //encoding and outputting
        String encodedTopology = topology.encodeToXML();
        System.out.println(encodedTopology);
        PartiallyKnownNetworkTopology topology2 = new PartiallyKnownNetworkTopology();
        //////topology2.mergeWithTopology(topology);
        topology2.updateFromXML(encodedTopology);
        System.out.println();//
        System.out.println("********************************************************************");
        System.out.println(topology2.encodeToXML());//
        
        
        PartiallyKnownNetworkTopology topologyX = new PartiallyKnownNetworkTopology();
        topologyX.setName("Topologia 2");
        //node 1
        EndpointNetworkNode eNode3 = new EndpointNetworkNode();
        eNode3.setNodeName("Koncovy uzol C");
        eNode3.setNodeSite(new NetworkSite("site_X"));
        
        //ports for node 1
        EndpointNodeInterface iface3 = new EndpointNodeInterface("Sietove rozhranie uzlu C", "55.38.111.152", "255.255.240.205", 42, "subnet_3", eNode3); 
        eNode3.addInterface(iface3);
        
        //node 3
        UnknownNetworkNode uNode2 = new UnknownNetworkNode("Neznamy uzol");
        
        //physical links to C
        PhysicalNetworkLink pLinkC1 = new PhysicalNetworkLink("Fyzicky spoj C1", 14, eNode3, uNode2, true, iface3);
        PhysicalNetworkLink pLinkC2 = new PhysicalNetworkLink("Fyzicky spoj C2", 15, uNode2, eNode3, true, iface3);
        pLinkC1.setBackLink(pLinkC2);
        pLinkC2.setBackLink(pLinkC1);
        
        //adding nodes to topology
        topologyX.addNetworkNode(eNode3);
        topologyX.addNetworkNode(uNode2);
       
        topologyX.addPhysicalLink(pLinkC1);
        topologyX.addPhysicalLink(pLinkC2);
        
        topology.updateFromXML(topologyX.encodeToXML());
        
        System.out.println();
        //System.out.println(topologyX.encodeToXML());//
        
        System.out.println();
        System.out.println(topology.encodeToXML());
        
        
        
        
        
        topology2.removeNetworkNode(eNode2);
        //topology.updateFromXML(topology2.encodeToXML());
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("----------------------------------------------------------------------------------");
        System.out.println(topology2.encodeToXML());
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("//////////////////////////////////////////////////////////////////////////////////////");
        
        PartiallyKnownNetworkTopology topologyZ = new PartiallyKnownNetworkTopology();
        topologyZ.updateFromXML(topologyX.encodeToXML());
        topologyZ.updateFromXML(topology2.encodeToXML());
        System.out.println(topologyZ.encodeToXML());
        
        topologyX.removeLink(pLinkC1);
        topologyZ.updateFromXML(topologyX.encodeToXML());
        System.out.println("without C1 link//////////////////////////////////////////////////////////////////////////////////////");
        System.out.println(topologyZ.encodeToXML());
        
        
        /*topology.addPhysicalLink(new PhysicalNetworkLink("added phys link", 5.4, eNode2, eNode3, false));//
        System.out.println();
        System.out.println(topology.getAllPhysicalPaths(eNode1, eNode3).toString());//*/
    }
}
