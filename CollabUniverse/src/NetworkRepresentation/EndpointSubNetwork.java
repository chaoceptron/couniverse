package NetworkRepresentation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArraySet;
import org.apache.log4j.Logger;

/**
 * Endpoints SubNetwork representation.
 * EndpointSubNetwork maintains information on endpoints connnected to a subnetwork via their interfaces.
 * <p/>
 * User: Milos Liska (xliska@fi.mui.cz), Pavel Troubil (pavel@ics.muni.cz)
 */
public class EndpointSubNetwork {
    static Logger logger = Logger.getLogger("NetworkRepresentation");

    String subNetName;
    CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces;
    static final String publicNetName = "Public IP Internet";

    /**
     * JavaBean constructor
     */
    public EndpointSubNetwork() {
        this.subNetName = "";
        nodeInterfaces = new CopyOnWriteArraySet<EndpointNodeInterface>();
    }

    /**
     * ReallyUseful(TM) constructor
     * <p/>
     *
     * @param name           subnet name (to identify it)
     * @param nodeInterfaces set of nodeInterfaces to add to it (maybe empty)
     */
    public EndpointSubNetwork(String name, CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces) {
        this.subNetName = name;
        setSubNetworkNodeInterfaces(nodeInterfaces);
    }

    /**
     * Returns set of network interfaces belonging to this subnet
     * <p/>
     *
     * @return set of interfaces
     */
    public CopyOnWriteArraySet<EndpointNodeInterface> getSubNetworkNodeInterfaces() {
        return nodeInterfaces;
    }

    /**
     * Sets list of subnet interfaces to the given list
     * <p/>
     *
     * @param nodeInterfaces list of interfaces to set the subnet list to
     */
    public final void setSubNetworkNodeInterfaces(CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces) {
        if (!CheckUnique(nodeInterfaces)) {
            throw new IllegalArgumentException("IP addresses of nodeInterface collection are not unique!");
        }
        this.nodeInterfaces = nodeInterfaces;
    }

    /**
     * Adds a list of interfaces to existing list for this subnet
     * <p/>
     *
     * @param nodeInterfaces list of interfaces to add
     */
    public void addSubNetworkNodeInterfaces(final CopyOnWriteArraySet<EndpointNodeInterface> nodeInterfaces) {
        if (!CheckUnique(nodeInterfaces)) {
            throw new IllegalArgumentException("IP addresses of nodeInterface collection are not unique!");
        }
        if (!CheckUnique(this.nodeInterfaces, nodeInterfaces)) {
            throw new IllegalArgumentException("IP addresses of at least one nodeInterface is already existing in the subnetwork!");
        }
        this.nodeInterfaces.addAll(nodeInterfaces);
    }

    /**
     * Adds a single interface to existing list for this subnet
     * <p/>
     *
     * @param nodeInterface an interface to add
     */
    public void addSubNetworkNodeInterface(final EndpointNodeInterface nodeInterface) {
        //noinspection CloneableClassInSecureContext
        if (!CheckUnique(this.nodeInterfaces, new ArrayList<EndpointNodeInterface>() {
            {
                add(nodeInterface);
            }            
        })) {
            if (SubNetwork.logger.isTraceEnabled()) {
                SubNetwork.logger.trace("Already existing interfaces:");
                for (EndpointNodeInterface anInterface : nodeInterfaces) {
                    SubNetwork.logger.trace("   " + anInterface);
                }
            }
            throw new IllegalArgumentException("IP address " + nodeInterface.getIpAddress() + " already exists in the subnetwork!");
        }
        this.nodeInterfaces.add(nodeInterface);
    }

    /**
     * Removes a single interface from a list in this subnet
     *
     * @param nodeInterface an interface to remove
     */
    public void removeSubNetworkNodeInterface(final EndpointNodeInterface nodeInterface) {
        if (this.nodeInterfaces.contains(nodeInterface)) {
            this.nodeInterfaces.remove(nodeInterface);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubNetwork that = (SubNetwork) o;

        return !(subNetName != null ? !subNetName.equals(that.subNetName) : that.subNetName != null);

    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.subNetName != null ? this.subNetName.hashCode() : 0);
        return hash;
    }

    public static String getPublicNetName() {
        return publicNetName;
    }

    /**
     * Checks whether interfaces inside an array have unique IP addresses
     * <p/>
     *
     * @param nodeInterfaces collection of interfaces
     * @return true iff all IP addresses are unique
     */
    private boolean CheckUnique(Collection<EndpointNodeInterface> nodeInterfaces) {
        ArrayList<EndpointNodeInterface> a = new ArrayList<EndpointNodeInterface>(nodeInterfaces);
        if (a.isEmpty()) {
            return true;
        }
        for (int i = 0; i < a.size(); i++) {
            for (int j = 0; j < a.size(); j++) {
                if (i != j && a.get(i).getIpAddress().equals(a.get(j).getIpAddress())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Checks two arrays, whether they don't have duplicate IP addresses. Doesn't check the same
     * inside each of the array - for this purpose, see the same method with single parameter.
     * <p/>
     *
     * @param nodeInterfacesA a collection to compare
     * @param nodeInterfacesB a collection to compare
     * @return true iff IP addresses are unique under the condition stated above
     */
    private boolean CheckUnique(Collection<EndpointNodeInterface> nodeInterfacesA, Collection<EndpointNodeInterface> nodeInterfacesB) {
        ArrayList<EndpointNodeInterface> a = new ArrayList<EndpointNodeInterface>(nodeInterfacesA);
        ArrayList<EndpointNodeInterface> b = new ArrayList<EndpointNodeInterface>(nodeInterfacesB);
        if (a.isEmpty() || b.isEmpty()) {
            return true;
        }
        for (EndpointNodeInterface anA : a) {
            for (EndpointNodeInterface aB : b) {
                if (anA.getIpAddress().equals(aB.getIpAddress())) {
                    return false;
                }
            }
        }
        return true;
    }

    // JavaBean enforced setters/getters

    public String getSubNetName() {
        return subNetName;
    }

    public void setSubNetName(String subNetName) {
        this.subNetName = subNetName;
    }

}
