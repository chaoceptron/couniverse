package NetworkRepresentation;

import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import net.jxta.document.MimeMediaType;
import net.jxta.endpoint.ByteArrayMessageElement;
import net.jxta.endpoint.Message;
import net.jxta.endpoint.MessageElement;
import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Nodes;
import nu.xom.XPathContext;
import org.apache.log4j.Logger;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import utils.Shim;
import utils.ShimGenerator;

/**
 * General network node representation, including endpoints, network devices, and unknown networks.. The GeneralNetworkNode
 * class supports serializing the instance into a XML to be sent over JXTA pipe.
 * <p/>
 * User: Pavel Troubil (pavel@ics.muni.cz)
 */
public class GeneralNetworkNode implements Serializable, CoNMLInterface {
    
    public static final int NODE_TYPE_UNSET = -1;
    public static final int NODE_TYPE_ENDPOINT = 1;
    public static final int NODE_TYPE_PHYSICAL = 2;
    public static final int NODE_TYPE_UNKNOWN_NETWORK = 3;
    
    static Logger logger = Logger.getLogger("NetworkRepresentation");

    private String nodeName;  // human readable node name
    protected String uuid;
    private double geoLongitude;
    private double geoLatitude;
    private Shim shim;
    protected int nodeType;
    private DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> networkTopologyGraph;
    private String parentTopologyName;
    //TODO add time stamp (needed for updating topology)

    /**
     * This is an empty JavaBean constructor in order to support XMLEncoder and XMLDecoder
     */
    public GeneralNetworkNode() {
        this.nodeName = "";
        if (this.uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        }
        this.geoLongitude = 0.0;
        this.geoLatitude = 0.0;
        this.shim = new Shim(0, 0);
        this.nodeType = NODE_TYPE_UNSET;    
        this.parentTopologyName = "";
    }

    /**
     * GeneralNetworkNode constructor.
     * <p/>
     *
     * @param nodeName         name of the node
     */
    public GeneralNetworkNode(String nodeName) {
        this.nodeName = nodeName;
        this.uuid = UUID.randomUUID().toString();

        this.geoLongitude = 0.0;
        this.geoLatitude = 0.0;
        this.shim = new Shim(0, 0);
        this.parentTopologyName = "";
    }
    
    /**
     * Gets node name.
     * <p/>
     *
     * @return node name
     */
    public String getNodeName() {
        return nodeName;
    }

    /**
     * Serializes NetworkNode representation into a UTF-8 XML message
     * <p/>
     *
     * @return byte array with UTF-8 XML representation
     */
    @Deprecated
    public byte[] serializeIntoXML() {

        // thus the receiver of this object is not tempted to apply any commands on this node
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        XMLEncoder xmlenc = new XMLEncoder(output);
        xmlenc.writeObject(this);
        xmlenc.close();

        NetworkNode.logger.trace("Network node message size: " + output.size());
        NetworkNode.logger.trace("Network node message:");
        NetworkNode.logger.trace(output.toString());
        NetworkNode.logger.trace("");
        return output.toByteArray();
    }

    /**
     * Converts the UTF-8 XML message (from XMLEncoder) to JXTA pipe MessageElement.
     * <p/>
     *
     * @param messageName messageElement name
     * @return MessageElement to be put into JXTA Message
     */
    @Deprecated
    public MessageElement toPipeMessageElement(String messageName) {
        return new ByteArrayMessageElement(messageName,
                MimeMediaType.XMLUTF8, this.serializeIntoXML(), null);
    }

    /**
     * Converts the UTF-8 XML message (from XMLEncoder) to JXTA pipe Message.
     * <p/>
     *
     * @param messageName messageElement name
     * @return Message to be put into JXTA pipe
     */
    @Deprecated
    public Message toPipeMessage(String messageName) {
        Message outMessage = new Message();
        outMessage.addMessageElement(this.toPipeMessageElement(messageName));
        return outMessage;
    }

    /**
     * JavaBean enforced setter
     * <p/>
     *
     * @param nodeName node name
     */
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public double getGeoLongitude() {
        return geoLongitude;
    }

    public void setGeoLongitude(double geoLongitude) {
        this.geoLongitude = geoLongitude;
    }

    public double getGeoLatitude() {
        return geoLatitude;
    }

    public void setGeoLatitude(double geoLatitude) {
        this.geoLatitude = geoLatitude;
    }

    public Shim getShim() {
        return shim;
    }

    public void setShim(Shim shim) {
        this.shim = shim;
    }

    public void setShim(ShimGenerator shimGenerator) {
        this.shim = shimGenerator.generateShim();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getNodeType() {
        return nodeType;
    }

    public DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> getNetworkTopologyGraph() {
        return networkTopologyGraph;
    }

    public void setNetworkTopologyGraph(DirectedWeightedMultigraph<GeneralNetworkNode, GeneralNetworkLink> networkTopologyGraph) {
        this.networkTopologyGraph = networkTopologyGraph;
    }
    
    public Set<GeneralNetworkLink> getOutputLinks() {
        return new HashSet<GeneralNetworkLink>(networkTopologyGraph.outgoingEdgesOf(this));
    }
    
    public Set<GeneralNetworkLink> getInputLinks() {
        return new HashSet<GeneralNetworkLink>(networkTopologyGraph.incomingEdgesOf(this));
    }

    public String getParentTopologyName() {
        return parentTopologyName;
    }

    public void setParentTopologyName(String parentTopologyName) {
        this.parentTopologyName = parentTopologyName;
    }

    @Override
    public String toString() {
        return "" + this.getNodeName();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeneralNetworkNode other = (GeneralNetworkNode) obj;
        if ((this.uuid == null) ? (other.uuid != null) : !this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }    

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        if(result.getUuidSet() != null && result.getUuidSet().add(this.uuid) == false){
            return null;
        }
        
        Element baseElem = new Element("nml:Node", CoNMLInterface.NML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(baseElem);            
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElem);
        }        
        baseElem.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));
        
        if(this.nodeName != null){
            Element nameElem = new Element("nml:name", CoNMLInterface.NML_NS);
            nameElem.appendChild(this.nodeName);
            baseElem.appendChild(nameElem); 
        }
        
        //writing location element to root/parent node
        Element locationElem = new Element("nml:Location", CoNMLInterface.NML_NS);        
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(locationElem);
        }else{
            ((Element) parameters.getParentNode()).appendChild(locationElem);
        }
        locationElem.addAttribute(new Attribute("id", this.uuid + "-location", Attribute.Type.ID));

        Element locationLatElem = new Element("nml:lat", CoNMLInterface.NML_NS);
        locationLatElem.appendChild(String.valueOf((long) this.geoLatitude));            
        locationElem.appendChild(locationLatElem);

        Element locationLongElem = new Element("nml:long", CoNMLInterface.NML_NS);
        locationLongElem.appendChild(String.valueOf((long) this.geoLongitude));        
        locationElem.appendChild(locationLongElem);
        //end of location element        
        
        Element locationReference = new Element("nml:Location", CoNMLInterface.NML_NS);
        baseElem.appendChild(locationReference);        
        locationReference.addAttribute(new Attribute(locationElem.getAttribute("id")));
        
        //writing anyOther element       
        Element extensionElem = new Element("cou:GeneralNetworkNode", CoNMLInterface.CONML_NS);
        baseElem.appendChild(extensionElem);        
        
        //relations
        Element outputRelationElem = new Element("nml:Relation", CoNMLInterface.NML_NS);        
        baseElem.appendChild(outputRelationElem);
        outputRelationElem.addAttribute(new Attribute("type", "http://schemas.ogf.org/nml/2013/05/base#hasOutboundPort"));

        Element inputRelationElem = new Element("nml:Relation", CoNMLInterface.NML_NS);
        baseElem.appendChild(inputRelationElem);
        inputRelationElem.addAttribute(new Attribute("type", "http://schemas.ogf.org/nml/2013/05/base#hasInboundPort"));
        
        //setting relation element nodes
        parameters.setInputsRelation(inputRelationElem);
        parameters.setOutputsRelation(outputRelationElem);
        
        return new CoNMLExtensionNodes(baseElem, extensionElem, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        XPathContext context = new XPathContext("nml", CoNMLInterface.NML_NS);
        context.addNamespace("cou", CoNMLInterface.CONML_NS); 
        
        //reading id attribute and registering it to result 
        String id = ((Element) parameters.getCurrentNode().getParent()).getAttributeValue("id");
        if(id != null){
            this.uuid = id;
            result.getDecodedNodes().put(id, this);                
        }

        //decoding name
        Element nameElem = ((Element) parameters.getCurrentNode().getParent()).getFirstChildElement("name", CoNMLInterface.NML_NS);
        if(nameElem != null){
            this.nodeName = nameElem.getValue();
        }

        //decoding location
        Element locationElem = ((Element) parameters.getCurrentNode().getParent()).getFirstChildElement("Location", CoNMLInterface.NML_NS);
        if(locationElem != null){
            if(locationElem.getChildCount() == 0){
                Nodes loc = parameters.getCurrentNode().getParent().getParent().query("nml:Location[@id='" + locationElem.getAttributeValue("id") + "']", context);
                if(loc.size() > 0){
                    locationElem = (Element) loc.get(0);
                }                    
            }
            if(locationElem != null){
                Element latElem = locationElem.getFirstChildElement("lat", CoNMLInterface.NML_NS);
                if(latElem.getValue().isEmpty() == false){
                    try{
                        this.geoLatitude = Double.valueOf(latElem.getValue());
                    }catch(NumberFormatException ex){}
                }
                Element longElem = locationElem.getFirstChildElement("long", CoNMLInterface.NML_NS);
                if(longElem.getValue().isEmpty() == false){                    
                    try{
                        this.geoLongitude = Double.valueOf(longElem.getValue());
                    }catch(NumberFormatException ex){}
                }
            }
        }
    }
    
    /**
     * Updates geo location attributes of this node according to attributes of a given node.
     * Instances of GeneralNetworkLink from newerNode.getNetworkTopologyGraph() change their to/from node attribute to this node 
     * 
     * @param newerNode If null, no changes are made.
     */
    public void update(GeneralNetworkNode newerNode){
        if(newerNode == null){
            return;
        }
        this.geoLatitude = newerNode.getGeoLatitude();
        this.geoLongitude = newerNode.getGeoLongitude();
        HashSet<GeneralNetworkLink> edgesToRemove = new HashSet<GeneralNetworkLink>();
        for(GeneralNetworkLink link : this.getNetworkTopologyGraph().incomingEdgesOf(this)){
            if(link.getFromNode().getParentTopologyName().equals(newerNode.getParentTopologyName())){
                edgesToRemove.add(link);//this.getNetworkTopologyGraph().removeEdge(link);                
            }
        }
        for(GeneralNetworkLink link : this.getNetworkTopologyGraph().outgoingEdgesOf(this)){
            if(link.getToNode().getParentTopologyName().equals(newerNode.getParentTopologyName())){
                edgesToRemove.add(link);//this.getNetworkTopologyGraph().removeEdge(link);
            }
        }        
        for(GeneralNetworkLink link : edgesToRemove){
            this.getNetworkTopologyGraph().removeEdge(link);
        }
        
        for(GeneralNetworkLink link : newerNode.getNetworkTopologyGraph().incomingEdgesOf(newerNode)){
            link.setToNode(this);
        }
        for(GeneralNetworkLink link : newerNode.getNetworkTopologyGraph().outgoingEdgesOf(newerNode)){
            link.setFromNode(this);
        }
    }
}