/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import java.util.UUID;
import nu.xom.Attribute;
import nu.xom.Element;

/**
 *
 * @author Matus
 */
public class GeneralNodeInterface implements CoNMLInterface {    
    private String nodeInterfaceName;
    protected String uuid;
    
    public GeneralNodeInterface() {
        this.nodeInterfaceName = "general_node_interface";
        this.uuid = UUID.randomUUID().toString();
    }

    public GeneralNodeInterface(String nodeInterfaceName) {
        this.nodeInterfaceName = nodeInterfaceName;
        this.uuid = UUID.randomUUID().toString();
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNodeInterfaceName() {
        return nodeInterfaceName;
    }

    public void setNodeInterfaceName(String interfaceName) {
        this.nodeInterfaceName = interfaceName;
    }

    @Override
    public String toString() {
        return "GeneralNodeInterface{" + "interfaceName=" + nodeInterfaceName + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GeneralNodeInterface other = (GeneralNodeInterface) obj;
        if ((this.uuid == null) ? (other.uuid != null) : !this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }
    
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }        
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        final String OUT_SUFFIX = "-out"; //non-empty suffix for outbound port
        final String IN_SUFFIX = "-in"; //non-empty suffix for inbound port
        String id = ""; //id of port currently being written
        String relationType = ""; //type of relation of port currently being written (outbound has sink links, inbound has source links)
        Element parentNodeRelationElem = null; //element node of caller, which describes its relation to this interface
        Element relationElem = new Element("nml:Relation", CoNMLInterface.NML_NS); //element node of interface currently being written, which describes a relation to its links 
        
        if(parameters.getInputsRelation() != null){
            parentNodeRelationElem = (Element) parameters.getInputsRelation(); 
            parameters.setInputsRelation(relationElem);
            id = this.uuid + IN_SUFFIX;
            relationType = "http://schemas.ogf.org/nml/2013/05/base#isSource";            
        }else if(parameters.getOutputsRelation() != null){
            parentNodeRelationElem = (Element) parameters.getOutputsRelation();
            parameters.setOutputsRelation(relationElem);
            id = this.uuid + OUT_SUFFIX;
            relationType = "http://schemas.ogf.org/nml/2013/05/base#isSink";
        }

        //writing interface/port to callers relation element node
        Element portReference = new Element("nml:Port", CoNMLInterface.NML_NS);
        portReference.addAttribute(new Attribute("id", id, Attribute.Type.ID));
        if(parentNodeRelationElem != null){
            parentNodeRelationElem.appendChild(portReference);
        }

        //writing interface/port to root/parent node, unless it's already there
        if(result.getUuidSet() != null && result.getUuidSet().add(id) == false){
            return new CoNMLExtensionNodes(null, null, portReference);
        }

        Element baseElementNode = new Element("nml:Port", CoNMLInterface.NML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(baseElementNode);
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElementNode);
        }
        baseElementNode.addAttribute(new Attribute("id", id, Attribute.Type.ID));
        
        //writing name
        if(this.nodeInterfaceName != null){
            Element nameElem = new Element("nml:name", CoNMLInterface.NML_NS);
            nameElem.appendChild(this.nodeInterfaceName);
            baseElementNode.appendChild(nameElem);
        }

        //writing anyOther element        
        Element extensionElem = new Element("cou:GeneralNodeInterface", CoNMLInterface.CONML_NS);
        baseElementNode.appendChild(extensionElem);        
        
        relationElem.addAttribute(new Attribute("type", relationType));
        baseElementNode.appendChild(relationElem);
        
        //writing bidirectional port, unless it's already in root/parent node
        if(result.getUuidSet() != null && result.getUuidSet().add(this.uuid)){
            Element bidirectionalPortElem = new Element("nml:BidirectionalPort", CoNMLInterface.NML_NS);
            if(parameters.getParentNode() == null){
                result.getDoc().getRootElement().appendChild(bidirectionalPortElem);
            }else{
                ((Element) parameters.getParentNode()).appendChild(bidirectionalPortElem);
            }
            bidirectionalPortElem.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));

            Element outboundPortReference = new Element("nml:Port", CoNMLInterface.NML_NS);  
            outboundPortReference.addAttribute(new Attribute("id", this.uuid + OUT_SUFFIX, Attribute.Type.ID));
            bidirectionalPortElem.appendChild(outboundPortReference);

            Element inboundPortReference = new Element("nml:Port", CoNMLInterface.NML_NS);            
            inboundPortReference.addAttribute(new Attribute("id", this.uuid + IN_SUFFIX, Attribute.Type.ID));
            bidirectionalPortElem.appendChild(inboundPortReference);
        }
        
        return new CoNMLExtensionNodes(baseElementNode, extensionElem, portReference);
    }    
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        //reading id attribute and registering it to result
        String id = ((Element) parameters.getCurrentNode().getParent()).getAttributeValue("id");
        if(id != null){
            this.uuid = id;
            result.getDecodedPorts().put(id, this);                
        }

        //decoding name
        Element nameElem = ((Element) parameters.getCurrentNode().getParent()).getFirstChildElement("name", CoNMLInterface.NML_NS); 
        if(nameElem != null){
            this.nodeInterfaceName = nameElem.getValue();
        }
    }
}
