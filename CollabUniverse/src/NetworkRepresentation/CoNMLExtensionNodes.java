/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import nu.xom.Node;

/**
 *
 * @author matus
 */
public class CoNMLExtensionNodes {
    private Node mainNode;
    private Node subclassExtensionNode;
    private Node referenceNode;

    public CoNMLExtensionNodes() {
    }

    public CoNMLExtensionNodes(Node mainNode, Node extensionNode, Node referenceNode) {
        this.mainNode = mainNode;
        this.subclassExtensionNode = extensionNode;
        this.referenceNode = referenceNode;
    }

    public Node getSubclassExtensionNode() {
        return subclassExtensionNode;
    }

    public void setSubclassExtensionNode(Node extensionNode) {
        this.subclassExtensionNode = extensionNode;
    }

    public Node getMainNode() {
        return mainNode;
    }

    public void setMainNode(Node mainNode) {
        this.mainNode = mainNode;
    }

    public Node getReferenceNode() {
        return referenceNode;
    }

    public void setReferenceNode(Node referenceNode) {
        this.referenceNode = referenceNode;
    }    
}
