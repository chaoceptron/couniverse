/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package NetworkRepresentation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import nu.xom.Document;
import nu.xom.Serializer;

/**
 * Holds result for CoNML interface implementations of encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters).
 * <p/> 
 * 
 * @author Matus
 */
public class CoNMLEncodeResult {
    private Document doc;
    private Set<String> uuidSet;

    public CoNMLEncodeResult() {
        this.uuidSet = new HashSet<String>();         
        this.doc = null;
    }

    /**
     * 
     * @return Document holding the result. Should not be null.
     */
    public Document getDoc() {
        return doc;
    }

    public void setDoc(Document doc) {
        this.doc = doc;
    }

    public Set<String> getUuidSet() {        
        return uuidSet;
    }

    public void setUuidSet(Set<String> uuidSet) {
        this.uuidSet = uuidSet;
    }
    
    /*public ByteBuffer toEncodedByteBuffer(String encoding){//example: encoding = "UTF-8"
        return Charset.forName(encoding).encode(this.toString());
    }*/

    @Override
    public String toString() {
        //return this.doc.toXML();//xml string with no formating
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        String result = "";
        try{
            Serializer serializer = new Serializer(out, "UTF-8");
            serializer.setIndent(2);
            serializer.setMaxLength(128);
            serializer.write(doc); 
            result = out.toString();
            out.close();
        }
        catch (IOException ex) {
           System.err.println(ex); 
        }
        
        return result;
    }
    
    //TODO output to file, output to bytestream, ...
}
