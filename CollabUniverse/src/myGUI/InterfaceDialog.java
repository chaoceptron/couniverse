package myGUI;

import NetworkRepresentation.EndpointNodeInterface;
import NetworkRepresentation.LambdaLinkEndPoint;
import NetworkRepresentation.SubNetwork;
import java.awt.event.*;
import javax.swing.*;

public class InterfaceDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JFormattedTextField addressField;
    private JLabel addressLabel;
    private JTextField subnetNameField;
    private JCheckBox fullDuplexBox;
    private JTextField deviceField;
    private JLabel deviceLabel;
    private JLabel bwLabel;
    private JFormattedTextField bandwidthField;
    private JFormattedTextField lambdaEndPointField;
    private JFormattedTextField lambdaEndpointIDCField;
    private JCheckBox taggedBox;
    private JTextField lambdaEndPointVlanField;

    private EndpointNodeInterface nodeInterface = null;
    private LambdaLinkEndPoint lambdaLinkEndPoint = null;

    public InterfaceDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        if (nodeInterface == null) {
            // we don't have any nodeInterface yet
            subnetNameField.setText(SubNetwork.getPublicNetName());
        }
        taggedBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (taggedBox.isSelected()) {
                    lambdaEndPointVlanField.setEditable(true);
                } else {
                    lambdaEndPointVlanField.setEditable(false);
                }
            }
        });
    }

    private void onOK() {
        if (nodeInterface == null) {
            // this happens only when adding a new node
            nodeInterface = new EndpointNodeInterface();
        }
        nodeInterface.setNodeInterfaceName(deviceField.getText());
        nodeInterface.setIpAddress(addressField.getText());
        nodeInterface.setSubnet(subnetNameField.getText());
        try {
            nodeInterface.setBandwidth(Double.parseDouble(bandwidthField.getText()));
        }
        catch (NumberFormatException e) {
            nodeInterface.setBandwidth(0.0);
        }
// TODO assign lambda links to physical links and physical links to logical links
        if (!this.lambdaEndPointField.getText().equals("")) {
            if (lambdaLinkEndPoint == null) {
                lambdaLinkEndPoint = new LambdaLinkEndPoint(lambdaEndPointField.getText(), lambdaEndpointIDCField.getText(), taggedBox.isSelected(), lambdaEndPointVlanField.getText());
            } else {
                lambdaLinkEndPoint.setLambdaLinkEndpoint(lambdaEndPointField.getText());
                lambdaLinkEndPoint.setLambdaLinkEndpointIDC(lambdaEndpointIDCField.getText());
                lambdaLinkEndPoint.setLambdaLinkEndpointTagged(taggedBox.isSelected());
                if (lambdaLinkEndPoint.isLambdaLinkEndpointTagged()) {
                    lambdaLinkEndPoint.setLambdaLinkEndpointVlan(lambdaEndPointVlanField.getText());
                } else {
                    lambdaLinkEndPoint.setLambdaLinkEndpointVlan("any");
                }
            }
        }

//        nodeInterface.setLambdaLinkEndpoint(lambdaLinkEndPoint);

        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public EndpointNodeInterface getNodeInterface() {
        return nodeInterface;
    }

    public void setValues(EndpointNodeInterface nd) {
        nodeInterface = nd;
        deviceField.setText(nodeInterface.getNodeInterfaceName());
        addressField.setText(nodeInterface.getIpAddress());
        subnetNameField.setText(nodeInterface.getSubnet());
        bandwidthField.setText(Double.toString(nodeInterface.getBandwidth()));
/*
        if (nd.getLambdaLinkEndpoint() != null) {
            lambdaEndPointField.setText(nodeInterface.getLambdaLinkEndpoint().getLambdaLinkEndpoint());
            lambdaEndpointIDCField.setText(nodeInterface.getLambdaLinkEndpoint().getLambdaLinkEndpointIDC());
            taggedBox.setSelected(nodeInterface.getLambdaLinkEndpoint().isLambdaLinkEndpointTagged());
            lambdaEndPointVlanField.setText(nodeInterface.getLambdaLinkEndpoint().getLambdaLinkEndpointVlan());
            lambdaEndPointVlanField.setEditable(nodeInterface.getLambdaLinkEndpoint().isLambdaLinkEndpointTagged());
        }
        fullDuplexBox.setSelected(nodeInterface.isFullDuplex());
        
*/
    }

}
