package myGUI;

import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.*;

public class AGCDialog extends JDialog {
    static Logger logger = Logger.getLogger("myGUI");

    private JPanel contentPane;
    private JButton buttonExit;
    private JButton startMatchMakerButton;

    private boolean shouldStartMatchMaker = false;

    public AGCDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonExit);

        buttonExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        });

// call onExit() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onExit();
            }
        });

// call onExit() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        startMatchMakerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startMatchMaker();
            }
        });
        this.pack();
    }

    public boolean shouldStartMatchMaker() {
        return this.shouldStartMatchMaker;
    }

    private void startMatchMaker() {
        shouldStartMatchMaker = true;
        startMatchMakerButton.setEnabled(false);
        this.setVisible(false);
        dispose();
    }

    private void onExit() {
        this.setVisible(false);
        dispose();
        System.exit(0);
    }

    public static void main(String[] args) {
        AGCDialog dialog = new AGCDialog();
        dialog.setVisible(true);
        if (dialog.shouldStartMatchMaker()) {
            AGCDialog.logger.info("Starting MatchMaker...");
        }
        System.exit(0);
    }
}
