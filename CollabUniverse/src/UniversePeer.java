import AGC.ApplicationGroupController;
import AppControllers.Controller;
import AppControllers.LocalController;
import AppControllers.ProxyController;
import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationProxy;
import MediaApplications.*;
import Monitoring.NetworkMonitor;
import Monitoring.NetworkMonitorListener;
import NetworkRepresentation.*;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import myGUI.ControllerFrame;
import myGUI.NodeDialog;
import myJXTA.MessageType;
import myJXTA.MyJXTAConnector;
import net.jxta.discovery.DiscoveryService;
import net.jxta.document.AdvertisementFactory;
import net.jxta.document.MimeMediaType;
import net.jxta.id.IDFactory;
import net.jxta.peergroup.PeerGroup;
import net.jxta.pipe.InputPipe;
import net.jxta.pipe.OutputPipe;
import net.jxta.pipe.PipeService;
import net.jxta.protocol.ModuleClassAdvertisement;
import net.jxta.protocol.ModuleSpecAdvertisement;
import net.jxta.protocol.PipeAdvertisement;
import org.apache.log4j.Logger;
import utils.ApplicationProxy;
import utils.MyLogger;
import utils.TimeUtils;

public class UniversePeer {

    private String jxtaHome;
    private static MyJXTAConnector myJXTAConnector;
    private static PeerGroup netPeerGroup;
    private static PeerGroup universePeerGroup;
    private static DiscoveryService discoveryService;
    private static PipeService pipeService;
    private static InputPipe nodeInputPipe = null;
    private static EndpointNetworkNode localNode = null;
    private static OutputPipe agcOutputPipe;

    private static HashMap<MediaApplication, LocalController> localApplicationControllers;
    private static HashMap<MediaApplication, ProxyController> proxyApplicationControllers;

    private final AtomicBoolean terminateFlag;
    private final NetworkMonitor.NetworkMonitorClass DEFAULT_MONITOR_CLASS = new NetworkMonitor.NetworkMonitorClass(10000);
    private final NetworkMonitor.NetworkMonitorClass PRIORITY_MONITOR_CLASS = new NetworkMonitor.NetworkMonitorClass(1000);
    private NetworkMonitor networkMonitor;

    // The name of the logger needs to be referenced as String as it is not part of any package and thus not referable from MyLogger
    static Logger logger = Logger.getLogger("UniversePeer");

    public UniversePeer() {
        MyLogger.setup();

        terminateFlag = new AtomicBoolean(false);

        jxtaHome = System.getProperty("JXTA_HOME");
        if (null == jxtaHome) {
            UniversePeer.logger.info("System property JXTA_HOME null. Using environment variable.");
            jxtaHome = System.getenv("JXTA_HOME");
            if (null == jxtaHome) {
                UniversePeer.logger.error("Unable to get JXTA_HOME from environment variable. Exiting.");
                System.exit(1);
            } else {
                System.setProperty("JXTA_HOME", jxtaHome);
            }
        }

        networkMonitor = new NetworkMonitor();
        networkMonitor.addMonitoringClass(DEFAULT_MONITOR_CLASS);
        networkMonitor.addMonitoringClass(PRIORITY_MONITOR_CLASS);
        networkMonitor.addNetworkMonitorListenerForAll(new NetworkMonitorListener() {
            // this is to be called when network link is found non-functional
            @Override
            public void onNetworkLinkLost(LogicalNetworkLink networkLink) {
                UniversePeer.logger.info("" + networkLink.getToInterface().getIpAddress() + " is unreachable.");
                UniversePeer.logger.info("Sending NetworkNode Unreachable message to AGC.");
                try {
                    myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NODE_UNREACHABLE_MESSAGE, networkLink);
                } catch (IOException e) {
                    infoOnException(e, "Failed to send NetworkNode Unreachable message to AGC.");
                }
            }

            // this is to be called when network link is found functional again
            @Override
            public void onNetworkLinkReestablished(LogicalNetworkLink networkLink) {
                UniversePeer.logger.info("" + networkLink.getToInterface().getIpAddress() + " is reachable.");
                UniversePeer.logger.info("Sending NetworkNode Reachable message to AGC.");
                try {
                    myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NODE_REACHABLE_MESSAGE, networkLink);
                } catch (IOException e) {
                    infoOnException(e, "Failed to send NetworkNode Reachable message to AGC.");
                }
            }

            @Override
            public void onNetworkLinkFlap(LogicalNetworkLink networkLink) {
                UniversePeer.logger.info("" + networkLink.getToInterface().getIpAddress() + " is flapping.");
            }
        });


        myJXTAConnector = new MyJXTAConnector();
        // register message types
        myJXTAConnector.registerMessageType(MessageType.NODE_UNREACHABLE_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.NODE_REACHABLE_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.NEW_NODE_MESSAGE);        
        myJXTAConnector.registerMessageType(MessageType.REMOVE_NODE_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.START_MEDIAAPP_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.STOP_MEDIAAPP_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.START_FAILOVER_MEDIAAPP_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.NETWORK_UPDATE_MESSAGE);
        myJXTAConnector.registerMessageType(MessageType.ACTIVE_LINKS_MESSAGE);
    }

    private void registerJXTAConnectorCallbacks() {
        myJXTAConnector.registerMessageListener(nodeInputPipe, MessageType.START_MEDIAAPP_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                UniversePeer.logger.info("Received Start MediaApp Object");
                MediaApplication appToStart = null;
                String appToStartParams = null;
                try {
                    appToStart = (MediaApplication) objects[0];
                    appToStartParams = (String) objects[1];
                } catch (Exception e) {
                    if (UniversePeer.logger.isDebugEnabled()) {
                        e.printStackTrace();
                    }
                    UniversePeer.logger.error("Failed to decode incomming Message.");
                }
                assert appToStart != null && appToStartParams != null;

                // Start the MediaApp
                // Set params given by AGC to the localApp object
                if (localNode.getNodeApplications().contains(appToStart)) {
                    // Get the coresponding localApp
                    MediaApplication localApp = null;
                    for (MediaApplication mediaApplication : localNode.getNodeApplications()) {
                        localApp = mediaApplication;
                        if (localApp.equals(appToStart)) {
                            // We have a match
                            break;
                        }
                    }
                    // Set especially the targetIP and targetNode (and maybe some other stuff later) where applicable
                    if (appToStart instanceof UltraGrid1500Producer) {
                        assert localApp instanceof UltraGrid1500Producer;
                        UltraGrid1500Producer localUltraGrid1500Prod = (UltraGrid1500Producer) localApp;
                        UltraGrid1500Producer agcGivenUltraGrid1500Prod = (UltraGrid1500Producer) appToStart;
                        localUltraGrid1500Prod.setTargetIP(agcGivenUltraGrid1500Prod.getTargetIP());
                        localUltraGrid1500Prod.setTargetNetworkNode(agcGivenUltraGrid1500Prod.getTargetNetworkNode());
                    }
                    if (appToStart instanceof UltraGrid750Producer) {
                        assert localApp instanceof UltraGrid750Producer;
                        UltraGrid750Producer localUltraGrid750Producer = (UltraGrid750Producer) localApp;
                        UltraGrid750Producer agcGivenUltraGrid750Producer = (UltraGrid750Producer) appToStart;
                        localUltraGrid750Producer.setTargetIP(agcGivenUltraGrid750Producer.getTargetIP());
                        localUltraGrid750Producer.setTargetNetworkNode(agcGivenUltraGrid750Producer.getTargetNetworkNode());
                    }
                    if (appToStart instanceof UltraGridDXTProducer) {
                        assert localApp instanceof UltraGridDXTProducer;
                        UltraGridDXTProducer localUltraGridDXTProducer = (UltraGridDXTProducer) localApp;
                        UltraGridDXTProducer agcGivenUltraGridDXTProducer = (UltraGridDXTProducer) appToStart;
                        localUltraGridDXTProducer.setTargetIP(agcGivenUltraGridDXTProducer.getTargetIP());
                        localUltraGridDXTProducer.setTargetNetworkNode(agcGivenUltraGridDXTProducer.getTargetNetworkNode());
                    }
                    if (appToStart instanceof HDVMPEG2Producer) {
                        assert localApp instanceof HDVMPEG2Producer;
                        HDVMPEG2Producer localHdvmpeg2Producer = (HDVMPEG2Producer) localApp;
                        HDVMPEG2Producer agcGivenHdvmpeg2Producer = (HDVMPEG2Producer) appToStart;
                        localHdvmpeg2Producer.setTargetIP(agcGivenHdvmpeg2Producer.getTargetIP());
                        localHdvmpeg2Producer.setTargetNetworkNode(agcGivenHdvmpeg2Producer.getTargetNetworkNode());
                    }
                    if (appToStart instanceof PolycomProducer) {
                        assert localApp instanceof PolycomProducer;
                        PolycomProducer polycomProducer = (PolycomProducer) localApp;
                        PolycomProducer agcGivenPolycomProducer = (PolycomProducer) appToStart;
                        polycomProducer.setTargetIP(agcGivenPolycomProducer.getTargetIP());
                        // TODO: does setting target network node make sense?
                        polycomProducer.setTargetNetworkNode(agcGivenPolycomProducer.getTargetNetworkNode());
                    }
                    if (appToStart instanceof RAT) {
                        assert localApp instanceof RAT;
                        RAT localRat = (RAT) localApp;
                        RAT agcGivenRat = (RAT) appToStart;
                        localRat.setTargetIP(agcGivenRat.getTargetIP());
                        localRat.setTargetNetworkNode(agcGivenRat.getTargetNetworkNode());
                    }
                    if (appToStart instanceof VIC) {
                        VIC localVic = (VIC) localApp;
                        VIC agcGivenVic = (VIC) appToStart;
                        if (localVic != null) {
                            localVic.setTargetIP(agcGivenVic.getTargetIP());
                            localVic.setTargetNetworkNode(agcGivenVic.getTargetNetworkNode());
                        }
                    }
                } else {
                    UniversePeer.logger.warn("Cannot find localApplication " + appToStart);
                }
                // Start the app using application proxy
                if (localApplicationControllers.containsKey(appToStart) || proxyApplicationControllers.containsKey(appToStart)) {
                    Controller ctrl;
                    boolean proxied;
                    if (localApplicationControllers.containsKey(appToStart)) {
                        UniversePeer.logger.info("Starting local node application: " + appToStart + " " + appToStartParams);
                        ctrl = localApplicationControllers.get(appToStart);
                        proxied = false;
                    }
                    else {
                        UniversePeer.logger.info("Starting proxy node application: " + appToStart + " " + appToStartParams);
                        ctrl = proxyApplicationControllers.get(appToStart);
                        proxied = true;
                    }
                    assert ctrl != null;
                    try {
                        ctrl.getApplication().setApplicationCmdOptions(appToStartParams);
                        ctrl.runApplication();
                    } catch (ApplicationProxy.ApplicationProxyException e) {
                        infoOnException(e, "Failed to start " + (proxied ? "proxied" : "local") + " MediaApplication: " + appToStart + " " + appToStartParams);
                    }
                } else {
                    UniversePeer.logger.error("Failed to start local MediaApplication " + appToStart + " - no such app here.");
                }
            }
        });

        myJXTAConnector.registerMessageListener(nodeInputPipe, MessageType.STOP_MEDIAAPP_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                UniversePeer.logger.info("Received Stop MediaApp Object");
                MediaApplication appToStop = (MediaApplication) objects[0];
                UniversePeer.logger.info("Stopping local node application: " + appToStop);
                // Stop the app using its local controller
                if (localApplicationControllers.containsKey(appToStop) || proxyApplicationControllers.containsKey(appToStop)) {
                    Controller ctrl;
                    if (localApplicationControllers.containsKey(appToStop)) {
                        ctrl = localApplicationControllers.get(appToStop);
                    }
                    else {
                        ctrl = proxyApplicationControllers.get(appToStop);
                    }
                    assert ctrl != null;
                    if (ctrl.isRunning()) {
                        ctrl.stopApplication();
                    } else {
                        UniversePeer.logger.warn("" + appToStop + " is not running.");
                    }
                } else {
                    UniversePeer.logger.warn("Cannot stop application " + appToStop + " - no such app here.");
                }
            }
        });

        myJXTAConnector.registerMessageListener(nodeInputPipe, MessageType.START_FAILOVER_MEDIAAPP_MESSAGE, new MyJXTAConnector.MessageListener() {
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                UniversePeer.logger.info("Received Failover MediaApp Message");
                // Start VIC
                if (!localNode.getNodeApplications().isEmpty()) {
                    for (MediaApplication appToStart : localNode.getNodeApplications()) {
                        assert appToStart != null;
                        if (appToStart instanceof VIC) {
                            UniversePeer.logger.info("Starting local node failover application: " + appToStart + " " + appToStart.getApplicationCmdOptions());
                            try {
                                Controller ctrl;
                                assert localApplicationControllers.containsKey(appToStart) || proxyApplicationControllers.containsKey(appToStart);
                                if (localApplicationControllers.containsKey(appToStart)) {
                                    ctrl = localApplicationControllers.get(appToStart);
                                }
                                else {
                                    ctrl = proxyApplicationControllers.get(appToStart);
                                }
                                assert ctrl != null;
                                ctrl.runApplication();
                            } catch (ApplicationProxy.ApplicationProxyException e) {
                                infoOnException(e, "Failed to start node applicaton: " + appToStart + " " + appToStart.getApplicationCmdOptions());
                            }
                        }
                    }
                    // Stop all apps except VIC, RAT and Rum
                    for (MediaApplication appToStop : localNode.getNodeApplications()) {
                        assert appToStop != null;
                        if (!((appToStop instanceof VIC) || (appToStop instanceof RAT) || (appToStop instanceof Rum))) {
                            UniversePeer.logger.info("Stopping local node application: " + appToStop);
                            Controller ctrl;
                            assert localApplicationControllers.containsKey(appToStop) || proxyApplicationControllers.containsKey(appToStop);
                            if (localApplicationControllers.containsKey(appToStop)) {
                                ctrl = localApplicationControllers.get(appToStop);
                            }
                            else {
                                ctrl = proxyApplicationControllers.get(appToStop);
                            }
                            assert ctrl != null;
                            ctrl.stopApplication();
                        }
                    }
                } else {
                    UniversePeer.logger.warn("No failover applications on the local node and nothing to stop here.");
                }
            }
        });

        myJXTAConnector.registerMessageListener(nodeInputPipe, MessageType.NETWORK_UPDATE_MESSAGE, new MyJXTAConnector.MessageListener() {
            @SuppressWarnings({"unchecked"})
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                UniversePeer.logger.info("Received NetworkTopology Update Message");
                ArrayList<EndpointNetworkNode> endpointNetworkNodes = (ArrayList<EndpointNetworkNode>) objects[0];
                ArrayList<PhysicalNetworkNode> physicalNetworkNodes = (ArrayList<PhysicalNetworkNode>) objects[1];
                ArrayList<UnknownNetworkNode> unknownNetworkNodes = (ArrayList<UnknownNetworkNode>) objects[2];
                ArrayList<LogicalNetworkLink> logicalNetworkLinks = (ArrayList<LogicalNetworkLink>) objects[3];
                ArrayList<PhysicalNetworkLink> physicalNetworkLinks = (ArrayList<PhysicalNetworkLink>) objects[4];
                UniversePeer.logger.debug("Recieved NetworkTopology with " + endpointNetworkNodes.size() + " endpoint nodes, "
                                                                           + physicalNetworkNodes.size() + " physical nodes, "
                                                                           + unknownNetworkNodes.size() + " unknown network nodes, "
                                                                           + logicalNetworkLinks.size() + " logical edges and "
                                                                           + physicalNetworkLinks.size() + "physical edges.");
                Set<LogicalNetworkLink> monitoredLinks = networkMonitor.getMonitoredLinks();
                for (LogicalNetworkLink logicalLink : logicalNetworkLinks) {
                    UniversePeer.logger.trace("About to add link " + logicalLink + " to monitoring.");
                    if (logicalLink.getFromNode().equals(localNode) && !monitoredLinks.contains(logicalLink)) {
                        UniversePeer.logger.info("Adding network link " + logicalLink + " to be monitored.");
                        networkMonitor.addNetworkLink(logicalLink, DEFAULT_MONITOR_CLASS);
                        // register callbacks here if the global callback is not enough
                    }
                }
                for (LogicalNetworkLink monitoredLink : monitoredLinks) {
                    // remove the link only if it belongs to the DEFAULT_MONITOR_CLASS - it may belong to PRIORITY_MONITOR_CLASS, which
                    // is not being handled here by this message
                    if (!logicalNetworkLinks.contains(monitoredLink) && networkMonitor.getNetworkMonitorClass(monitoredLink).equals(DEFAULT_MONITOR_CLASS)) {
                        UniversePeer.logger.info("Removing network link " + monitoredLink + " from monitoring.");
                        networkMonitor.removeNetworkLink(monitoredLink);
                    }
                }
            }
        });        

        myJXTAConnector.registerMessageListener(nodeInputPipe, MessageType.ACTIVE_LINKS_MESSAGE, new MyJXTAConnector.MessageListener() {
            @SuppressWarnings({"unchecked"})
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                UniversePeer.logger.info("Received Active NetworkLinks Update Message");
                ArrayList<LogicalNetworkLink> logicalNetworkLinks = (ArrayList<LogicalNetworkLink>) objects[0];
                ArrayList<PhysicalNetworkLink> physicalNetworkLinks = (ArrayList<PhysicalNetworkLink>) objects[1];
                // add new logical links to monitoring
                Set<LogicalNetworkLink> monitoredLinks = networkMonitor.getMonitoredLinks();
                for (LogicalNetworkLink link : logicalNetworkLinks) {
                    UniversePeer.logger.trace("About to add link " + link + " to aggressive active link monitoring.");
                    if (link.getFromNode().equals(localNode) && (!monitoredLinks.contains(link) || (monitoredLinks.contains(link) && !networkMonitor.getNetworkMonitorClass(link).equals(PRIORITY_MONITOR_CLASS)))) {
                        UniversePeer.logger.info("Adding network link " + link + " to be monitored in PRIORITY_MONITOR_CLASS.");
                        networkMonitor.addNetworkLink(link, PRIORITY_MONITOR_CLASS);
                        // register callbacks here if the global callback is not enough
                    }
                }
                for (LogicalNetworkLink monitoredLink : monitoredLinks) {
                    // for PRIORITY_MONITOR_CLASS, we remove the link by only downgrading it to DEFAULT_MONITOR_CLASS - if it is lost completely, it will be removed on next topology update
                    if (!logicalNetworkLinks.contains(monitoredLink) && networkMonitor.getNetworkMonitorClass(monitoredLink).equals(PRIORITY_MONITOR_CLASS)) {
                        UniversePeer.logger.info("Demoting network link " + monitoredLink + " to be monitored in DEFAULT_MONITOR_CLASS.");
                        networkMonitor.addNetworkLink(monitoredLink, DEFAULT_MONITOR_CLASS);
                    }
                }
            }
        });

    }

    private void joinUniverse(boolean isInfra) {
        myJXTAConnector.joinUniverse(jxtaHome, isInfra);
        netPeerGroup = myJXTAConnector.getNetPeerGroup();
        universePeerGroup = myJXTAConnector.getUniversePeerGroup();
        discoveryService = myJXTAConnector.getDiscoveryService();
        pipeService = myJXTAConnector.getPipeService();
    }

    private void leaveUniverse() {
        UniversePeer.logger.info("Terminating application.");
        terminateFlag.set(true);

        // Send a network node removed information to AGC
        UniversePeer.logger.info("Sending localNetworkNode removed information to AGC.");
        try {
            myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.REMOVE_NODE_MESSAGE, localNode);
        }
        catch (Exception e) {
            infoOnException(e, "Failed to send localNetworkNode removed information to AGC.");
        }

        // this is to help the main thread do the cleanup
        TimeUtils.sleepFor(1000);

        myJXTAConnector.leaveUniverse();

        UniversePeer.logger.info("Finished.");
    }

    private OutputPipe waitForAGC() {
        UniversePeer.logger.info("Searching for Universe-AGC advertisement.");
        // This is the AGC pipe advertisement
        ModuleSpecAdvertisement agcMdsAdv = myJXTAConnector.getAGCAdv();
        OutputPipe myAgcOutputPipe = myJXTAConnector.createOutputPipe(agcMdsAdv.getPipeAdvertisement(), 5000, 3);
        if (myAgcOutputPipe == null) {
            UniversePeer.logger.warn("Failed to bind the output pipe to AGC!");
        }
        return myAgcOutputPipe;
    }


    private void startLocalNodeAdvertising() {
        UniversePeer.logger.info("Starting Local Node Advertising thread.");

        while (!terminateFlag.get()) {
            UniversePeer.logger.info("Sending localNetworkNode information to AGC.");
            try {
                myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NEW_NODE_MESSAGE, localNode);
            }
            catch (Exception e) {
                infoOnException(e, "Failed to send localNetworkNode information to AGC.");
            }

            // Sleep the thread for some time. 2 minutes should be fine.
            TimeUtils.sleepFor(120000);
        }
    }

    private void waitForQuit() {
        while (!terminateFlag.get()) {
            TimeUtils.sleepFor(500);
        }
    }

    public static void main(String[] args) {

        ApplicationGroupController agc = null;
        Boolean isAGC = false;
        Boolean isJXTAInfra = false;
        Boolean isNoGUI = false;
        Boolean isNoNCDialog = false;
        Boolean isNoOutDialog = false;
        String configFileName = "";

        final UniversePeer universePeer = new UniversePeer();

        UniversePeer.logger.info("Start.");

        if (args.length < 1) {
            System.out.println("Usage: UniversePeer [-infra] [-agc] [-nogui] [-noncdialog] [-nooutdialog] configFile");
            System.exit(1);
        } else {
            int i = 0;

            while ((i < args.length - 1) && (args[i].startsWith("-"))) {

                if (args[i].equals("-agc")) {
                    isAGC = true;
                } else if (args[i].equals("-infra")) {
                    isJXTAInfra = true;
                } else if (args[i].equals("-nogui")) {
                    isNoGUI = true;
                } else if (args[i].equals("-noncdialog")) {
                    isNoNCDialog = true;
                } else if (args[i].equals("-nooutdialog")) {
                    isNoOutDialog = true;
                } else {
                    System.out.println("Unknown command-line argument: " + args[i]);
                    System.out.println("Usage: UniversePeer [-infra] [-agc] [-nogui] [-noncdialog] [-nooutdialog] configFile");
                    System.exit(1);
                }

                i++;
            }

            // configFileName
            configFileName = args[i];
        }

        if (isAGC && isNoGUI) {
            UniversePeer.logger.error("AGC must be launched with GUI!");
            System.exit(1);
        }

        if (isJXTAInfra) {
            UniversePeer.logger.info("Universe Peer launched as JXTA infrastructure peer.");
        }
        if (isAGC) {
            UniversePeer.logger.info("Universe Peer launched as AGC.");
        }
        UniversePeer.logger.info("Universe Peer config file name: " + configFileName);

        // Get the file from the web is URI is provided instead filename
        if (configFileName.startsWith("http://")) {
            try {
                URL configURL = new URL(configFileName);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                configURL.openStream()));
                File tempFile = File.createTempFile("nodeConfig", "");
                tempFile.deleteOnExit();
                BufferedWriter out = new BufferedWriter(new FileWriter(tempFile));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    out.write(inputLine);
                }
                out.close();
                configFileName = tempFile.getAbsolutePath();
            } catch (MalformedURLException e) {
                infoOnException(e, "Failed to parse URL: " + configFileName);
                System.exit(1);
            }
            catch (IOException e) {
                infoOnException(e, "Failed to read URI: " + configFileName);
                System.exit(1);
            }
        }

        // Launch NetworkNodeConfigrator
        File configFile = new File(configFileName);
        if (!isNoGUI && !isNoNCDialog) {
            UniversePeer.logger.info("Starting Network Node Configurator.");
            NodeDialog nodeConfigDialog = new NodeDialog(configFile);
            nodeConfigDialog.setModal(true);
            nodeConfigDialog.setVisible(true);
        }



        try {
            universePeer.joinUniverse(isJXTAInfra);

            // stop JXTA on exit
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    universePeer.leaveUniverse();
                }
            });

        }
        catch (Exception e) {
            infoOnException(e, "Failed to start JXTA platform. Exiting.");
            System.exit(1);
        }

        // Start AGC or wait for AGC advertisement
        if (isAGC) {
            try {
                agc = new ApplicationGroupController(myJXTAConnector);

                // Publish the AGC advertisements
                ModuleClassAdvertisement agcMcAdv = agc.getAGCClassAdv();
                discoveryService.publish(agcMcAdv);
                discoveryService.remotePublish(agcMcAdv);

                ModuleSpecAdvertisement agcMdAdv = agc.getAGCModuleAdv();
                discoveryService.publish(agcMdAdv);
                discoveryService.remotePublish(agcMdAdv);

                // Set the PipeService to be used by AGC
                agc.setPipeService(pipeService);

                //Start AGC
                agc.startAGC();
            }
            catch (Exception e) {
                infoOnException(e, "Failed to start AGC. Continuing as normal peer.");
            }
        }

        // Load network node infromation
        localNode = new EndpointNetworkNode();
        try {
            XMLDecoder xmlDec = new XMLDecoder(new BufferedInputStream(new FileInputStream(configFileName)));

            localNode = (EndpointNetworkNode) xmlDec.readObject();
            xmlDec.close();
        }
        catch (FileNotFoundException e) {
            UniversePeer.logger.error("Failed to read node config file.");
            System.exit(1);
        } catch (NoSuchElementException e) {
            UniversePeer.logger.error("Failed to decode node config file.");
            System.exit(1);
        }

        // Check localNode.PeerID and save nodeConfig
        try {
            if ((localNode.getNodePeerID().equals("")) || (localNode.getNodePeerID().equals("urn:jxta:uuid-")) || (IDFactory.fromURI(new URI(localNode.getNodePeerID())) != netPeerGroup.getPeerID())) {
                UniversePeer.logger.warn("NetworkNode PeerID differs from current networkManager.PeerID. Using networkManager.PeerID.");
                // Use networkManager.PeerID rather then peerID from the config file
                localNode.setNodePeerID(netPeerGroup.getPeerID().toString());
                // Save the configfile
                UniversePeer.logger.info("Saving updated config file.");
                try {
                    XMLEncoder xmlEnc = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(configFileName)));
                    xmlEnc.writeObject(localNode);
                    xmlEnc.close();
                } catch (FileNotFoundException e) {
                    infoOnException(e, "Failed to write node config file.");
                }
            }
        } catch (URISyntaxException e) {
            infoOnException(e, "Bad URI syntax!");
        }


        // If local node is a proxy node, establish connection to the proxied device
        if (localNode.isProxyNode()) {
            localNode.initProxyNodeConnection();
        }
        
        // Generate a local node InputPipe Advertisment and add it to the node information
        PipeAdvertisement nodeInputPipeAdv = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());
        nodeInputPipeAdv.setPipeID(IDFactory.newPipeID(universePeerGroup.getPeerGroupID()));
        nodeInputPipeAdv.setType(PipeService.UnicastType);
        nodeInputPipeAdv.setName(universePeerGroup.getPeerName());

        try {
            nodeInputPipe = myJXTAConnector.createInputPipe(nodeInputPipeAdv);
        } catch (IOException e) {
            infoOnException(e, "Failed to create local node input pipe.");
        }

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            nodeInputPipeAdv.getDocument(MimeMediaType.XMLUTF8).sendToStream(output);
            localNode.setNodeInputPipeAdv(output.toByteArray());
            output.close();
        } catch (IOException e) {
            infoOnException(e, "Failed to encode local node pipe advertisement.");
        }

        universePeer.registerJXTAConnectorCallbacks();

        // Get MediaApplications config and controll them this should run in a thread
        myJXTAConnector.listenOnInputPipe(nodeInputPipe);        

        // Create a application controllers and add them to the {local,proxy}ApplicationControllers hashMaps
        localApplicationControllers = new HashMap<MediaApplication, LocalController>();
        proxyApplicationControllers = new HashMap<MediaApplication, ProxyController>();
        for (MediaApplication nodeApplication : localNode.getNodeApplications()) {
            if (nodeApplication instanceof MediaApplicationProxy) {
                UniversePeer.logger.info("Creating proxy controller for " + nodeApplication + ", " + nodeApplication.getUuid());
                ProxyController ctrl = new ProxyController(nodeApplication);
                proxyApplicationControllers.put(nodeApplication, ctrl);
            }
            else {
                UniversePeer.logger.info("Creating local controller for " + nodeApplication + ", " + nodeApplication.getUuid());
                LocalController ctrl = null;
                try {
                    ctrl = new LocalController(nodeApplication);
                } catch (ApplicationProxy.ApplicationProxyException e) {
                    infoOnException(e, "Failed to create local controller for: " + nodeApplication);
                }
                localApplicationControllers.put(nodeApplication, ctrl);
            }
        }
        if (!isNoGUI && !isNoOutDialog) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        ControllerFrame localControllerFrame = new ControllerFrame();
                        for (LocalController localController : localApplicationControllers.values()) {
                            localControllerFrame.addController(localController, localController.getApplicationProxy());
                            localController.setControllerFrame(localControllerFrame);
                        }
                        for (ProxyController proxyController : proxyApplicationControllers.values()) {
                            localControllerFrame.addController(proxyController, null);
                            proxyController.setControllerFrame(localControllerFrame);
                        }
                        localControllerFrame.setSize(localControllerFrame.getPreferredSize());
                        localControllerFrame.setVisible(true);
                    }
                });
            } catch (InterruptedException e) {
            } catch (InvocationTargetException e) {
            }
        } else {
            for (LocalController localController : localApplicationControllers.values()) {
                localController.setControllerFrame(null);
            }
            for (ProxyController proxyController : proxyApplicationControllers.values()) {
                proxyController.setControllerFrame(null);
            }
        }

        // Start local static apps
        for (MediaApplication nodeApplication : localNode.getNodeApplications()) {
            // XXX: why do we start RAT and Rum arbitrarily here? hacks should be denoted hacks :(
            if ((nodeApplication instanceof RAT) || (nodeApplication instanceof Rum)) {
                // Get controller for the mediaApp
                assert localApplicationControllers != null;
                LocalController ctrl = localApplicationControllers.get(nodeApplication);
                assert ctrl != null;
                UniversePeer.logger.info("Starting local node application: " + nodeApplication + " " + nodeApplication.getApplicationCmdOptions());
                try {
                    ctrl.runApplication();
                    UniversePeer.logger.info("" + nodeApplication + " started.");
                } catch (ApplicationProxy.ApplicationProxyException e) {
                    infoOnException(e, "Failed to start local MediaApplication: " + nodeApplication + " " + nodeApplication.getApplicationCmdOptions());
                }
            }
        }

        // Wait for AGC and get the pipe to communicate with it
        agcOutputPipe = universePeer.waitForAGC();

        // Start localNode Advertising thread
        Thread localNodeAdvertisingThread = null;
        try {
            localNodeAdvertisingThread = new Thread() {
                @Override
                public void run() {
                    universePeer.startLocalNodeAdvertising();
                }
            };
            localNodeAdvertisingThread.start();
        } catch (Exception e) {
            infoOnException(e, "Failed to run localNode Advertising thread");
        }

        universePeer.networkMonitor.startMonitoring();


        universePeer.waitForQuit();

        // Stop local apps
        for (MediaApplication nodeApplication : localNode.getNodeApplications()) {
            assert nodeApplication != null;
            assert localApplicationControllers != null;
            assert proxyApplicationControllers != null;

            Controller ctrl;
            boolean proxied;
            if (localApplicationControllers.containsKey(nodeApplication) ) {
                ctrl = localApplicationControllers.get(nodeApplication);
                proxied = false;
            }
            else {
                ctrl = proxyApplicationControllers.get(nodeApplication);
                proxied = true;
            }
            if (ctrl != null) {
                UniversePeer.logger.info("Stopping "+ (proxied ? "proxied" : "local") + " node application: " + nodeApplication + " " + nodeApplication.getApplicationCmdOptions());
              if (ctrl.isRunning()) {
                    ctrl.stopApplication();
                }
            }
        }

        universePeer.networkMonitor.stopMonitoring();

        // Join the threads
        if (localNodeAdvertisingThread != null) {
            localNodeAdvertisingThread.interrupt();
            try {
                UniversePeer.logger.info("Joining localNode Advertising thread.");
                localNodeAdvertisingThread.join();
            } catch (InterruptedException e) {
                infoOnException(e, "Failed to join localNode Advertising thread.");
            }
        }

        // Stop AGC
        if (isAGC) {
            UniversePeer.logger.info("Stopping AGC.");
            agc.stopAGC();
        }

        // If proxy node, disconnect from the proxied node
        if (localNode.isProxyNode()) {
            localNode.stopProxyNodeConnection();
        }

        UniversePeer.logger.info("Exit.");
    }

    private static void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
        }
        logger.error(s);
    }

}
