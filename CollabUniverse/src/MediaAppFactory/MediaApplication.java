package MediaAppFactory;

import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import NetworkRepresentation.EndpointNetworkNode;
import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import nu.xom.Attribute;
import nu.xom.Element;
import nu.xom.Elements;

/**
 * This is an abstract class wrapping all the media applications that may run on any of the nodes
 * in the collaborative network.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 9:37:28
 */

public class MediaApplication implements Serializable, CoNMLInterface {

    private String applicationName;   // application name
    private CopyOnWriteArraySet<MediaStream> mediaStreams;   // thread-safe collection of supported media streams
    private String applicationPath = null;   // application path
    private String applicationCmdOptions = null;   // command-line options for the application
    private EndpointNetworkNode parentNode = null;
    private String uuid;
    private int pid;


    /**
     * MediaApplication constructor
     *
     * @param applicationName name of the application
     * @param mediaStreams    supported media streams
     */
    public MediaApplication(String applicationName, Collection<MediaStream> mediaStreams) {
        this.applicationName = applicationName;
        this.mediaStreams = new CopyOnWriteArraySet<MediaStream>();
        assert mediaStreams != null;
        this.mediaStreams.addAll(mediaStreams);
        this.uuid = UUID.randomUUID().toString();
    }

    /**
     * Returns application name
     *
     * @return application name
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Returns supported media streams
     *
     * @return thread-safe collection of supported meda streams
     */
    public CopyOnWriteArraySet<MediaStream> getMediaStreams() {
        return mediaStreams;
    }

    /**
     * Sets up application before running it
     *
     * @param path path to the binary/script
     * @param opts command line options
     */
    public void setupApplication(String path, String opts) {
        applicationPath = path;
        applicationCmdOptions = opts;
    }


    /**
     * Gets application path
     *
     * @return application path
     */
    public String getApplicationPath() {
        return (applicationPath == null) ? "" : applicationPath;
    }

    /**
     * Get application command-line options
     *
     * @return
     */
    public String getApplicationCmdOptions() {
        return (applicationCmdOptions == null) ? "" : applicationCmdOptions;
    }

    /**
     * (Re)sets the command-line options for the application
     *
     * @param applicationCmdOptions
     */
    public void setApplicationCmdOptions(String applicationCmdOptions) {
        this.applicationCmdOptions = applicationCmdOptions;
    }

    /**
     * Gets the node the application is residion on.
     * <p/>
     *
     * @return
     */
    public EndpointNetworkNode getParentNode() {
        return parentNode;
    }

    /**
     * Sets the parent node for this application.
     * <p/>
     *
     * @param parentNode
     */
    public void setParentNode(EndpointNetworkNode parentNode) {
        this.parentNode = parentNode;
    }

    // JavaBean enforced methods

    protected MediaApplication() {
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public void setMediaStreams(CopyOnWriteArraySet<MediaStream> mediaStreams) {
        this.mediaStreams = mediaStreams;
    }

    public void setApplicationPath(String applicationPath) {
        this.applicationPath = applicationPath;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * Custom comparator
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MediaApplication that = (MediaApplication) o;

        if (!uuid.equals(that.uuid)) return false;

        return true;
    }

    /**
     * Custom hashCode for keys in HashMaps
     * @return
     */
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    /**
     * Returns maximum total bandwidth used by this MediaApplication. Iterates through media steams
     * and counts up maximum bandwidths together.
     * <p/>
     * @return maximum bandwidth in bps
     */
    public long getMediaMaxBandwidth() {
        long bw = 0L;
        for (MediaStream mediaStream : this.getMediaStreams()) {
            bw += mediaStream.getBandwidth_max();
        }
        return bw;
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        Element baseElem = new Element("cou:MediaApplication", CoNMLInterface.CONML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(baseElem);
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElem);
        }
        baseElem.addAttribute(new Attribute("id", this.uuid, Attribute.Type.ID));

        if(this.applicationName != null){
            Element applicationNameElem = new Element("cou:applicationName", CoNMLInterface.CONML_NS);
            applicationNameElem.appendChild(this.applicationName); 
            baseElem.appendChild(applicationNameElem);
        }
        
        if(this.applicationCmdOptions != null){
            Element cmdOptionsElem = new Element("cou:cmdOptions", CoNMLInterface.CONML_NS);
            cmdOptionsElem.appendChild(this.applicationCmdOptions); 
            baseElem.appendChild(cmdOptionsElem);
        }
        
        CoNMLEncodeParameters streamParameters = new CoNMLEncodeParameters();
        streamParameters.setParentNode(baseElem);
        for(MediaStream stream: this.mediaStreams){            
            stream.encodeToCoNML(result, streamParameters);
        }
        
        return new CoNMLExtensionNodes(baseElem, baseElem, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        String id = ((Element) parameters.getCurrentNode()).getAttributeValue("id");
        if(id != null){
            this.uuid = id;
            if(result.getDecodedApps().containsKey(id) == false){
                result.getDecodedApps().put(id, this);
            }    
        }
        
        Element appNameElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("applicationName", CoNMLInterface.CONML_NS); 
        if(appNameElem != null){
            this.applicationName = appNameElem.getValue();
        }
        
        Element cmdOptionsElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("cmdOptions", CoNMLInterface.CONML_NS); 
        if(cmdOptionsElem != null){
            this.applicationCmdOptions = cmdOptionsElem.getValue();
        }

        Elements streams = ((Element) parameters.getCurrentNode()).getChildElements("MediaStream", CoNMLInterface.CONML_NS);
        for(int i = 0; i < streams.size(); i++){
            MediaStream stream = new MediaStream();
            stream.decodeFromCoNML(result, new CoNMLDecodeParameters(streams.get(i)));
            this.mediaStreams.add(stream);
        }
    }
}
