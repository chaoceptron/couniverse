package MediaAppFactory;

/**
 * Created by IntelliJ IDEA.
 * User: xliska
 * Date: 1.12.2007
 * Time: 20:26:29
 */
public class MediaApplicationParametersFactory {

    public static String addTargetIP(MediaApplication mediaApp, String targetIP) {
        String finalAppParams = null;
        String appParams = mediaApp.getApplicationCmdOptions();

        if (mediaApp.getApplicationName().contains("UltraGrid")) {
            finalAppParams = appParams + " " + targetIP;
        }

        if (mediaApp.getApplicationName().contains("Polycom Client")) {
            finalAppParams = appParams + " " + targetIP;
        }

        if ((mediaApp.getApplicationName().contains("RAT Audio Client")) || (mediaApp.getApplicationName().contains("VIC Video Client"))) {
            if (appParams.matches("^\\d+\\.\\d+\\.\\d+\\.\\d+\\/\\d+$")) {
                // It seems that a default port was already set
                finalAppParams = appParams.replaceFirst("^\\d+\\.\\d+\\.\\d+\\.\\d+", targetIP);
            } else {
                // Hrdcode some port
                finalAppParams = targetIP + "/10002";
            }
        }

        if (mediaApp.getApplicationName().equals("HD UDP Packet Reflector")) {
            if (appParams.matches("^\\d.\\ \\d+$")) {
                finalAppParams = appParams + targetIP;
            } else {
                // Some warning
                finalAppParams = "8M 5004 " + targetIP;
            }
        }

        if (mediaApp.getApplicationName().equals("UDP Packet Reflector")) {
            // TODO: This should never happen
        }

        if (mediaApp.getApplicationName().contains("HDV MPEG-2 TS producer")) {
            // This requires FreeBSD fwcontroll
            if (appParams.matches("-R\\ \\/dev\\/fw0\\ \\|\\ vlc\\ --sout\\ udp:\\/\\/\\d+\\.\\d+\\.\\d+\\.\\d+\\ -")){
                finalAppParams = appParams.replaceFirst("^\\d+\\.\\d+\\.\\d+\\.\\d+", targetIP);
            }
            if (appParams.matches("-R\\ \\/dev\\/fw0\\ \\|\\ vlc\\ --sout\\ udp:\\/\\/\\ -")){
                finalAppParams = appParams.replaceFirst("udp:\\/\\/+", "udp://" + targetIP);
            }
            // TODO: add an option for dvgrab on Linux
        }

        if (mediaApp.getApplicationName().contains("HDV MPEG-2 TS consumer")) {
            // TODO: This should never happen
        }

        return finalAppParams;
    }

    public static String checkAppParams(MediaApplication mediaApp) {
        String finalAppParams = null;
        String appParams = mediaApp.getApplicationCmdOptions();

        if (mediaApp.getApplicationName().equals("UDP Packet Reflector")) {
            if (appParams.matches("-rtp\\ \\d+")) {
                finalAppParams = appParams;
            } else {
                // Some warning
                finalAppParams = "-rtp 10002";
            }
        }

        if (mediaApp.getApplicationName().contains("HDV MPEG-2 TS consumer")) {
            finalAppParams = "udp://@";
        }

        return finalAppParams;
    }
}
