package MediaAppFactory;

import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import java.io.Serializable;
import nu.xom.Element;

/**
 * Object containing information on media stream
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:43:36
 */

public class MediaStream implements Serializable, CoNMLInterface {
    private String description; // stream description
    private long bandwidth_min; // minimum bandwidth in [bps]
    private long bandwidth_max; // maximum bandwidth in [bps]
    private int latency; // stream latency in given element (not end to end of course) in [ms]
    private long bursts_max; // maximum bursts when stream is bursty [bps] - when equal to bandwidth_max, the stream is considered smooth
    private float quality; // media quality (0.0 .. 10.0), where 10 is non-achievable ideal; designed for being able to compare different streams when building the environment

    /**
     * Full-fledged MediaStream constructor
     *
     * @param description   description of the stream
     * @param bandwidth_min minimum bandwidth in [bps]
     * @param bandwidth_max maximum bandwidth in [bps]
     * @param latency       stream latency in given element (not end to end of course) in [ms]
     * @param bursts_max    maximum bursts when stream is bursty [bps] - when equal to bandwidth_max, the stream is considered smooth
     * @param quality       edia quality (0.0 .. 10.0), where 10 is non-achievable ideal
     */
    public MediaStream(String description, long bandwidth_min, long bandwidth_max, int latency, long bursts_max, float quality) {
        this.description = description;
        this.bandwidth_min = bandwidth_min;
        this.bandwidth_max = bandwidth_max;
        this.latency = latency;
        this.bursts_max = bursts_max;
        this.quality = quality;
    }

    /**
     * Simpler MediaStream constructor with bandwidth and latecny specification only
     *
     * @param description description description of the stream
     * @param bandwidth   bandwidth in [bps]
     * @param latency     latency stream latency in given element (not end to end of course) in [ms]
     */
    public MediaStream(String description, long bandwidth, int latency) {
        this.description = description;
        this.bandwidth_min = bandwidth;
        this.bandwidth_max = bandwidth;
        this.latency = latency;
        this.bursts_max = bandwidth;
    }

    /**
     * Gets MediaStream description
     *
     * @return MediaStream description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets MediaStream minimum bandwidth
     *
     * @return MediaStream minimum bandwidth
     */
    public long getBandwidth_min() {
        return bandwidth_min;
    }

    /**
     * Gets MediaStream maximum bandwidth
     *
     * @return MediaStream maximum bandwidth
     */
    public long getBandwidth_max() {
        return bandwidth_max;
    }

    /**
     * Gets MediaStream latency
     *
     * @return MediaStream latency
     */
    public int getLatency() {
        return latency;
    }

    /**
     * Gets MediaStream maximum bursts
     *
     * @return MediaStream maximum bursts
     */
    public long getBursts_max() {
        return bursts_max;
    }

    /**
     * Custom matching
     * <p/>
     * @param o object to match
     * @return true if it matches
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MediaStream that = (MediaStream) o;

        if (bandwidth_max != that.bandwidth_max) return false;
        if (bandwidth_min != that.bandwidth_min) return false;
        if (bursts_max != that.bursts_max) return false;
        if (latency != that.latency) return false;
        if (Float.compare(that.quality, quality) != 0) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 83 * hash + (int) (this.bandwidth_min ^ (this.bandwidth_min >>> 32));
        hash = 83 * hash + (int) (this.bandwidth_max ^ (this.bandwidth_max >>> 32));
        hash = 83 * hash + this.latency;
        hash = 83 * hash + (int) (this.bursts_max ^ (this.bursts_max >>> 32));
        hash = 83 * hash + Float.floatToIntBits(this.quality);
        return hash;
    }
    
    // JavaBean enforced methods

    public MediaStream() {
    }

    public float getQuality() {
        return quality;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBandwidth_min(long bandwidth_min) {
        this.bandwidth_min = bandwidth_min;
    }

    public void setBandwidth_max(long bandwidth_max) {
        this.bandwidth_max = bandwidth_max;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public void setBursts_max(long bursts_max) {
        this.bursts_max = bursts_max;
    }

    public void setQuality(float quality) {
        this.quality = quality;
    }

    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {
        if(result == null){
            throw new IllegalArgumentException("CoNMLEncodeResult cannot be null.");
        }
        if(parameters == null){
            parameters = new CoNMLEncodeParameters();
        }
        
        Element baseElementNode = new Element("cou:MediaStream", CoNMLInterface.CONML_NS);
        if(parameters.getParentNode() == null){
            result.getDoc().getRootElement().appendChild(baseElementNode);
        }else{
            ((Element) parameters.getParentNode()).appendChild(baseElementNode);
        }

        if(this.description != null){
            Element descriptionElem = new Element("cou:description", CoNMLInterface.CONML_NS);
            descriptionElem.appendChild(this.description); 
            baseElementNode.appendChild(descriptionElem);
        }
        
        Element minBandwidthElem = new Element("cou:bandwidth_min", CoNMLInterface.CONML_NS);
        minBandwidthElem.appendChild(Long.toString(this.bandwidth_min)); 
        baseElementNode.appendChild(minBandwidthElem);
        
        Element maxBandwidthElem = new Element("cou:bandwidth_max", CoNMLInterface.CONML_NS);
        maxBandwidthElem.appendChild(Long.toString(this.bandwidth_max)); 
        baseElementNode.appendChild(maxBandwidthElem);
        
        Element latencyElem = new Element("cou:latency", CoNMLInterface.CONML_NS);
        latencyElem.appendChild(Integer.toString(this.latency)); 
        baseElementNode.appendChild(latencyElem);
        
        Element maxBrustsElem = new Element("cou:bursts_max", CoNMLInterface.CONML_NS);
        maxBrustsElem.appendChild(Long.toString(this.bursts_max)); 
        baseElementNode.appendChild(maxBrustsElem);
        
        Element qualityElem = new Element("cou:quality", CoNMLInterface.CONML_NS);
        qualityElem.appendChild(Float.toString(this.quality)); 
        baseElementNode.appendChild(qualityElem);
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        Element descriptionElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("description", CoNMLInterface.CONML_NS);
        if(descriptionElem != null){
            this.description = descriptionElem.getValue();
        }

        Element minBandwidthElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("bandwidth_min", CoNMLInterface.CONML_NS); 
        if(minBandwidthElem != null && minBandwidthElem.getValue().isEmpty() == false){
            this.bandwidth_min = Long.valueOf(minBandwidthElem.getValue());
        }

        Element maxBandwidthElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("bandwidth_max", CoNMLInterface.CONML_NS); 
        if(maxBandwidthElem != null && maxBandwidthElem.getValue().isEmpty() == false){
            this.bandwidth_max = Long.valueOf(maxBandwidthElem.getValue());
        }

        Element latencyElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("latency", CoNMLInterface.CONML_NS); 
        if(latencyElem != null && latencyElem.getValue().isEmpty() == false){
            this.latency = Integer.valueOf(latencyElem.getValue());
        }

        Element maxBrustsElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("bursts_max", CoNMLInterface.CONML_NS);
        if(maxBrustsElem != null && maxBrustsElem.getValue().isEmpty() == false){
            this.bursts_max = Long.valueOf(maxBrustsElem.getValue());
        }

        Element qualityElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("quality", CoNMLInterface.CONML_NS); 
        if(qualityElem != null && qualityElem.getValue().isEmpty() == false){
            this.quality = Float.valueOf(qualityElem.getValue());
        }
    }
}
