package MediaAppFactory;

import NetworkRepresentation.NetworkSite;

/**
 * Interface extending MediaApplication for media consumer applications
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:16:36
 */
public interface MediaApplicationConsumer {

    /**
     * Sets media application producer for this consumer. This is intended to be used
     * by ApplicationGroupController for MatchMaking purposes. This shouln't be set
     * by NetworkNode locally (simply because NetworkNode doesn't have notion of MediaApplications
     * running on other nodes).
     * <p/>
     * @param source MediaApplicationProducer for this consumer
     */
    public void setSource(MediaApplication source);

    public MediaApplication getSource();

    /**
     * Sets source site for this consumer. This is intended to be used by NetworkNode
     * locally.
     * <p/>
     * @param sourceSite source site to set; null for intentionally inactive consumers
     */
    public void setSourceSite(NetworkSite sourceSite);

    /**
     * Gets source site for this consumer.
     * <p/>
     * @return source site for this consumer; it is null for intentionally inactive consumers 
     */
    public NetworkSite getSourceSite();
    
}
