package GuiController;

import net.jxta.protocol.ModuleClassAdvertisement;
import net.jxta.protocol.ModuleSpecAdvertisement;
import net.jxta.protocol.PipeAdvertisement;
import net.jxta.document.AdvertisementFactory;
import net.jxta.id.IDFactory;
import net.jxta.pipe.PipeService;
import net.jxta.pipe.InputPipe;
import net.jxta.pipe.PipeID;
import net.jxta.pipe.OutputPipe;
import myJXTA.MyJXTAConnector;
import myJXTA.MessageType;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.ArrayList;
import java.util.Set;
import java.io.IOException;
import java.net.URI;

import org.apache.log4j.Logger;
import Monitoring.NetworkMonitor;
import Monitoring.NetworkMonitorListener;
import NetworkRepresentation.EndpointNetworkNode;
import NetworkRepresentation.LogicalNetworkLink;
import NetworkRepresentation.PhysicalNetworkLink;
import NetworkRepresentation.PartiallyKnownNetworkTopology;
import NetworkRepresentation.PhysicalNetworkNode;
import NetworkRepresentation.UnknownNetworkNode;

/**
 * Created by IntelliJ IDEA.
 * User: xtlach
 * Date: Apr 17, 2008
 * Time: 4:58:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class GuiController implements NetworkMonitorListener {
    private final MyJXTAConnector myJXTAConnector;
    private PipeService pipeService;

    private ModuleClassAdvertisement gcMcAdv = null;
    private ModuleSpecAdvertisement gcMdAdv = null;
    private PipeAdvertisement gcPipeAdv = null;

    private InputPipe guiControllerInputPipe;
    private PartiallyKnownNetworkTopology networkTopology;

    private OutputPipe agcOutputPipe = null;

    private final NetworkMonitor.NetworkMonitorClass DEFAULT_MONITOR_CLASS = new NetworkMonitor.NetworkMonitorClass(10000);
    private final NetworkMonitor.NetworkMonitorClass PRIORITY_MONITOR_CLASS = new NetworkMonitor.NetworkMonitorClass(1000);
    private NetworkMonitor networkMonitor;


    private AtomicBoolean terminateFlag;

    static Logger logger = Logger.getLogger(GuiController.class);


    public GuiController(MyJXTAConnector myJXTAConnector) {
        this.pipeService = null;
        terminateFlag = new AtomicBoolean(false);
        this.myJXTAConnector = myJXTAConnector;
        networkMonitor = new NetworkMonitor();
        networkMonitor.addMonitoringClass(DEFAULT_MONITOR_CLASS);
        networkMonitor.addMonitoringClass(PRIORITY_MONITOR_CLASS);
        this.agcOutputPipe = this.getAGCOutputPipe();
        this.networkMonitor.addNetworkMonitorListenerForAll(this);

        if (this.agcOutputPipe != null) {
            networkMonitor.addNetworkMonitorListenerForAll(new NetworkMonitorListener() {
                // this is to be called when network link is found non-functional
                @Override
                public void onNetworkLinkLost(LogicalNetworkLink networkLink) {

                }

                // this is to be called when network link is found functional again
                @Override
                public void onNetworkLinkReestablished(LogicalNetworkLink networkLink) {

                }

                @Override
                public void onNetworkLinkFlap(LogicalNetworkLink networkLink) {
                }
            });
        }
    }

    private OutputPipe getAGCOutputPipe() {
        GuiController.logger.info("Gui Controller searches for Universe-AGC advertisement.");
        // This is the AGC pipe advertisement
        ModuleSpecAdvertisement agcMdsAdv = myJXTAConnector.getAGCAdv();
        OutputPipe agcOutputPipe = myJXTAConnector.createOutputPipe(agcMdsAdv.getPipeAdvertisement(), 5000, 3);
        if (agcOutputPipe == null) {
            GuiController.logger.warn("Gui controller failed to bind the output pipe to AGC, which caused in next step Network monitor" +
                    "failed to add listeners.");
        }
        return agcOutputPipe;
    }

    public void startGuiController() {
        GuiController.logger.info("Starting GUI Conroller.");
        try {
            // Create AGC endpoint pipe based on the pipe advertisement. Clients will use this pipe to connect AGC
            this.guiControllerInputPipe = myJXTAConnector.createInputPipe(this.get_GC_Pipe_Adv());
        }
        catch (IOException e) {
            infoOnException(e, "Failed to create Gropu Controller input pipe.");
        }
        this.registerJXTAConnectorCallbacks();
        this.myJXTAConnector.listenOnInputPipe(guiControllerInputPipe);
    }

    public void stopGuiController() {
        GuiController.logger.info("Stopping Gui Controller.");
        terminateFlag.set(true);
        guiControllerInputPipe.close();
    }


    private PipeAdvertisement get_GC_Pipe_Adv() {
        String gcPipeIDStr = "urn:jxta:uuid-062B1F61F4D9481A90A386D9CB7BC4549FF129072F53476EA299BB57090C9A7B04";
        if (gcPipeAdv == null) {
            try {
                // Create the module pipe advertisement
                // We MUST advertise always the same pipe
                gcPipeAdv = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(PipeAdvertisement.getAdvertisementType());
                // Create pipe adv ID
                PipeID guiControllerPipeID = (PipeID) IDFactory.fromURI(new URI(gcPipeIDStr));

                gcPipeAdv.setPipeID(guiControllerPipeID);
                gcPipeAdv.setType(PipeService.UnicastType);
                gcPipeAdv.setName("Gui Controller input pipe");
            }
            catch (Exception e) {
                infoOnException(e, "Failed to create Group Controller input pipe advertisement.");
                return null;
            }
        }
        return gcPipeAdv;
    }

    /*
    private void sendNewNode(NetworkNode node) {
        if (this.networkTopology != null) {
            for (NetworkNode targetNode : networkTopology.getNetworkTopologyGraph().vertexSet()) {
                OutputPipe targetNodeOutputPipe;
                try {
                    targetNodeOutputPipe = MyJXTAUtils.createOutputPipe(pipeService, targetNode);
                } catch (IOException e) {
                    infoOnException(e, "Failed to create output pipe to peer " + targetNode.getNodeName());
                    continue;
                }

                // Send the network topology message to the peer node
                GuiController.logger.info("Sending NEW_NODE_MESSAGE(" + node +")  to " + targetNode);
                try {
                    this.myJXTAConnector.sendReliableMesssage(targetNodeOutputPipe, MessageType.NEW_NODE_MESSAGE, node);
                } catch (IOException e) {
                    infoOnException(e, "NEW_NODE_MESSAGE(\" + node +\")  to" + targetNode);
                }
            }
        }
    }

    /*
   public ModuleClassAdvertisement get_GC_Class_Adv() {
          if (gcMcAdv == null) {
              try {
                  gcMcAdv = (ModuleClassAdvertisement) AdvertisementFactory.newAdvertisement(ModuleClassAdvertisement.getAdvertisementType());
                  gcMcAdv.setName("MOD:Universe-GuiController");
                  gcMcAdv.setDescription("Collab Universe Gui Controller - Should collect data from monitoring and disturibute it to GUIs");
                  gcMcAdv.setModuleClassID(IDFactory.newModuleClassID());
              }
              catch (Exception e) {
                  infoOnException(e, "Failed to create AGC Class advertisement.");
                  return null;
              }
          }
          return gcMcAdv;
   }

    */

    @SuppressWarnings({"unchecked"})
    private void registerJXTAConnectorCallbacks() {
        myJXTAConnector.registerMessageListener(guiControllerInputPipe, MessageType.NETWORK_UPDATE_MESSAGE, new MyJXTAConnector.MessageListener() {//TODO use this to receive message
            @Override
            public void onMessageArrived(InputPipe pipe, MessageType type, Object[] objects) {
                GuiController.logger.info("Gui controller has received NetworkTopology Update Message");
                ArrayList<EndpointNetworkNode> endpointNetworkNodes = (ArrayList<EndpointNetworkNode>) objects[0];
                ArrayList<PhysicalNetworkNode> physicalNetworkNodes = (ArrayList<PhysicalNetworkNode>) objects[1];
                ArrayList<UnknownNetworkNode> unknownNetworkNodes = (ArrayList<UnknownNetworkNode>) objects[2];
                ArrayList<LogicalNetworkLink> logicalNetworkLinks = (ArrayList<LogicalNetworkLink>) objects[3];
                ArrayList<PhysicalNetworkLink> physicalNetworkLinks = (ArrayList<PhysicalNetworkLink>) objects[4];
                GuiController.logger.debug("Recieved NetworkTopology with " + endpointNetworkNodes.size() + " endpoint nodes, "
                                                                           + physicalNetworkNodes.size() + " physical nodes, "
                                                                           + unknownNetworkNodes.size() + " unknown network nodes, "
                                                                           + logicalNetworkLinks.size() + " logical edges and "
                                                                           + physicalNetworkLinks.size() + "physical edges.");
                Set<LogicalNetworkLink> monitoredLinks = networkMonitor.getMonitoredLinks();

                for (LogicalNetworkLink link : logicalNetworkLinks) {
                    GuiController.logger.trace("About to add link " + link + " to monitoring.");
                    if ( ! monitoredLinks.contains(link)) {
                        GuiController.logger.info("Adding network link " + link + " to be monitored.");
                        networkMonitor.addNetworkLink(link, DEFAULT_MONITOR_CLASS);
                        // register callbacks here if the global callback is not enough
                    }
                }
                       // i have to discuss this with petr
                for (LogicalNetworkLink monitoredLink : monitoredLinks) {
                    // remove the link only if it belongs to the DEFAULT_MONITOR_CLASS - it may belong to PRIORITY_MONITOR_CLASS, which
                    // is not being handled here by this message
                    if ( ! logicalNetworkLinks.contains(monitoredLink) && networkMonitor.getNetworkMonitorClass(monitoredLink).equals(DEFAULT_MONITOR_CLASS)) {
                        GuiController.logger.info("Removing network link " + monitoredLink + " from monitoring.");
                        networkMonitor.removeNetworkLink(monitoredLink);
                    }
                }
            }
        });
    }

    /*
    /////////////////Implementation of methods defined in NetworkMonitorListener///////////////
    */

    /*
   interface should include following two methods according simon suchomel's uml
   i just don't know the difference beetween Down and Lost
   onNetworkLinkUp is implemented in GuiController but it is commented due to UniversePeer
    */
    //public void onNetworkLinkDown(NetworkLink networkLink);

    public void onNetworkLinkUp(LogicalNetworkLink networkLink) {
        if (this.agcOutputPipe != null && this.myJXTAConnector != null) {
            GuiController.logger.info("" + networkLink.getToInterface().getIpAddress() + " newly created.");
            GuiController.logger.info("Sending new link up message to AGC.");
            try {
                GuiController.this.myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NEW_NODE_MESSAGE, networkLink);
            } catch (IOException e) {
                infoOnException(e, "Failed to send new link up message to AGC.");
            }
        }
    }

    // this is to be called when network link is found non-functional
    @Override
    public void onNetworkLinkLost(LogicalNetworkLink networkLink) {
        if (this.agcOutputPipe != null && this.myJXTAConnector != null) {

            GuiController.logger.info("" + networkLink.getToInterface().getIpAddress() + " is unreachable.");
            GuiController.logger.info("Sending NetworkNode Unreachable message to AGC.");
            try {
                GuiController.this.myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NODE_UNREACHABLE_MESSAGE, networkLink);
            } catch (IOException e) {
                infoOnException(e, "Failed to send NetworkNode Unreachable message to AGC.");
            }
        }
    }

    // this is to be called when network link is found functional again
    @Override
    public void onNetworkLinkReestablished(LogicalNetworkLink networkLink) {
        if (this.agcOutputPipe != null && this.myJXTAConnector != null) {
            GuiController.logger.info("" + networkLink.getToInterface().getIpAddress() + " is reachable.");
            GuiController.logger.info("Sending NetworkNode Reachable message to AGC.");
            try {
                GuiController.this.myJXTAConnector.sendReliableMesssage(agcOutputPipe, MessageType.NODE_REACHABLE_MESSAGE, networkLink);
            } catch (IOException e) {
                infoOnException(e, "Failed to send NetworkNode Reachable message to AGC.");
            }
        }
    }

    // this is to be called when network links is found to be flapping
    @Override
    public void onNetworkLinkFlap(LogicalNetworkLink networkLink) {
        GuiController.logger.info("" + networkLink.getToInterface().getIpAddress() + " is flapping.");
    }
    /*
    //////////////   END OF  implementation of methods defined in NetworkMonitorListener///////////////
    */


    private static void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
        }
        logger.error(s);
    }

}
