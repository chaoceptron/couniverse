package myJXTA;

import NetworkRepresentation.EndpointNetworkNode;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import net.jxta.document.AdvertisementFactory;
import net.jxta.document.MimeMediaType;
import net.jxta.document.StructuredDocumentFactory;
import net.jxta.document.XMLDocument;
import net.jxta.endpoint.ByteArrayMessageElement;
import net.jxta.endpoint.Message;
import net.jxta.endpoint.MessageElement;
import net.jxta.pipe.OutputPipe;
import net.jxta.pipe.PipeService;
import net.jxta.protocol.PipeAdvertisement;
import org.apache.log4j.Logger;

/**
 * Varopis utilities to avoid code duplication
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 30.7.2007
 * Time: 16:07:05
 */
public class MyJXTAUtils {
    static Logger logger = Logger.getLogger("myJXTA");


    public static OutputPipe createOutputPipe(PipeService pipeService, EndpointNetworkNode targetNode) throws IOException {
        OutputPipe targetNodeOutputPipe = null;
        int attempt = 0;
        int maxAttempts = 3;

        System.err.println("creating output pipe to :" + targetNode.getNodeName());
        ByteArrayInputStream input = new ByteArrayInputStream(targetNode.getNodeInputPipeAdv());
        XMLDocument xml = (XMLDocument) StructuredDocumentFactory.newStructuredDocument(MimeMediaType.XMLUTF8, input);
        PipeAdvertisement nodeOutputPipeAdvertisement = (PipeAdvertisement) AdvertisementFactory.newAdvertisement(xml);
        while ((targetNodeOutputPipe == null) && (attempt < maxAttempts)){
            //noinspection EmptyCatchBlock
            try {
                attempt++;
                targetNodeOutputPipe = pipeService.createOutputPipe(nodeOutputPipeAdvertisement, 3000);
            } catch (IOException e) {
                e.printStackTrace();
                MyJXTAUtils.logger.warn("Failed to create output pipe to peer " + targetNode.getNodeName() + ". Trying again...");
            }
        }
        if (targetNodeOutputPipe == null) {
            throw new IOException("Failed to create output pipe.");
        }

        return targetNodeOutputPipe;
    }

    /**
     * JXTA Message Decoder
     *
     * @param msg         JXTAMessage to decode
     * @param msgElemName name of the MessageElement to be decoded from the message
     * @return Object encoded within a MessageElement with a specified name
     * @throws IOException on decodeMessageElement() failed
     */
    public static Object decodeMessage(Message msg, String msgElemName) throws IOException {
        MessageElement msgElem = msg.getMessageElement(msgElemName);
        if (msgElem != null) {
            return MyJXTAUtils.decodeMessageElement(msgElem);
        } else {
            return null;
        }
    }

    /**
     * JXTA Message structure validator
     * <p/>
     * Validates if incomming JXTA Message consists of expected message elements given by their names.
     * Message elements must be given in expected orderas they were encoded into the message.
     *
     * @param msg       JXTAMessage to validate
     * @param structure names of the MessageElements included in the message
     * @return true for valid (i.e. with expected MessageElements) JXTA Message, false otherwise
     */
    public static boolean validateMessage(Message msg, String... structure) {
        try {
            return validateAndDecodeMessage(msg, structure) != null;
        } catch (IOException e) {
            // TODO: rewrite using logger when JXTA is wrapped in JXTA Connector            
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Combined validator and decoder for JXTA Message structure
     * <p/>
     * Validates if incomming JXTA Message consists of expected message elements given by their names.
     * Message elements must be given in expected orderas they were encoded into the message. It returns
     * a list of extracted object in the same order as specified by the structure
     *
     * @param msg       JXTAMessage to validate
     * @param structure structure names of the MessageElements included in the message
     * @return iff the Message is valid according to the pattern specified using structure parameter, returns decoded elements in the same order. Otherwise returns null
     * @throws java.io.IOException when decodeMessage() failed
     */
    public static Object[] validateAndDecodeMessage(Message msg, String... structure) throws IOException {
        ArrayList<Object> objects = new ArrayList<Object>();
        for (int i = 0; i < structure.length; i++) {
            for (int j = 0; j < i; j++) {
                if (structure[i].equals(structure[j])) {
                    assert false : "Message structure check failed.";
                }
            }
        }

        Message.ElementIterator msgIterator = msg.getMessageElements();
        assert msgIterator != null;
        
        int i = 0;
        while (msgIterator.hasNext() && i < structure.length) {
            MessageElement me = msgIterator.next();
            if (!me.getElementName().equals(structure[i])) {
                return null;
            } else {
                objects.add(decodeMessageElement(me));
            }
            i++;
        }
        if (msgIterator.hasNext()) {
            return null;
        }
        if (i != structure.length) {
            return null;
        }
        Object[] os = new Object[objects.size()];
        os = objects.toArray(os);
        return os;
    }

    /**
     * JXTA Message encoder
     * <p/>
     * Both object and description must not be null
     *
     * @param elements Tuples Object, String where Object is encoded as JXTAMessage element and described by String
     * @return JXTA Message
     */
    public static Message encodeMessage(Object... elements) {
        Message message = new Message();

        // Some checks first
        assert elements.length >= 2 && elements.length % 2 == 0;

        int i = 0;
        while (i < elements.length) {

            assert elements[i] != null && elements[i + 1] != null;
            // The second element has to be a string (message element name)
            assert elements[i + 1] instanceof String;

            MessageElement msgElem;
            msgElem = encodeMessageElement(elements[i], (String) elements[i + 1]);
            message.addMessageElement(msgElem);

            i += 2;
        }

        return message;
    }

    /**
     * MessageElement encoder
     *
     * @param o        Object to encode
     * @param elemName MessageElement name
     * @return MessageElement
     */

    private static MessageElement encodeMessageElement(Object o, String elemName) {
        MessageElement messageElem;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        XMLEncoder xmlenc = new XMLEncoder(output);

        xmlenc.writeObject(o);
        xmlenc.close();
        messageElem = new ByteArrayMessageElement(elemName, MimeMediaType.XMLUTF8, output.toByteArray(), null);

        return messageElem;
    }

    /**
     * MessgaElement decoder
     *
     * @param messageElem incomming MessageElement
     * @return Object decoded from the incomming MessageElement
     * @throws IndexOutOfBoundsException on creating new input stream from messageElement byte array
     * @throws IOException               on input.close() failed
     */

    private static Object decodeMessageElement(MessageElement messageElem) throws IndexOutOfBoundsException, IOException {
        Object o;
        ByteArrayInputStream input = new ByteArrayInputStream(messageElem.getBytes(true));
        XMLDecoder xmldec = new XMLDecoder(input);

        o = xmldec.readObject();

        xmldec.close();
        input.close();

        return o;
    }

    /**
     * Clears JXTA component cache given its path
     *
     * @param path of the cache
     */

    public static void clearCache(String path) {
        clearCacheCM(new File(path, "cm"));
    }

    /**
     * Purges JXTA cache including all config files
     *
     * @param path of the cache
     */

    public static void purgeCache(String path) {
        File topDir = new File(path);
        if (topDir.exists()) {
            ArrayList<String> filesToDelete = new ArrayList<String>() {
                {
                    add("PlatformConfig");
                    add("config.properties");
                }
            };
            for (String file : filesToDelete) {
                File platformConfig = new File(path, file);
                if (platformConfig.exists()) {
                    platformConfig.delete();
                    MyJXTAUtils.logger.info("Platform config " + platformConfig.toString() + " removed.");
                }
            }
            clearCacheCM(new File(path, "cm"));
        }

    }

    /**
     * Clears JXTA component cache given its path
     *
     * @param rootDir path of the cache
     */

    private static void clearCacheCM(final File rootDir) {
        try {
            if (rootDir.exists()) {
                File[] list = rootDir.listFiles();
                for (File aList : list) {
                    if (aList.isDirectory()) {
                        clearCacheCM(aList);
                    } else {
                        aList.delete();
                    }
                }
            }
            rootDir.delete();
            MyJXTAUtils.logger.info("Cache component " + rootDir.toString() + " cleared.");
        }
        catch (Throwable t) {
            MyJXTAUtils.logger.warn("Unable to clear " + rootDir.toString());
        }
    }
}
