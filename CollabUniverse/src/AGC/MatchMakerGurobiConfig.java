package AGC;

/**
 * Configuration specific to MatchMakerGurobi
 * 
 * User: hopet
 * Date: 26.11.2009
 * Time: 8:05:40
 * To change this template use File | Settings | File Templates.
 */
public class MatchMakerGurobiConfig {
    // for evaluation
    public boolean enable_constr_iface_capacity = true,
            enable_constr_link_total_capacity = true,
            enable_constr_single_stream_link_capacity = true,
            enable_constr_forbid_direct_links = true,
            enable_constr_links_number = true,
            enable_constr_consumer_single_inlink = true,
            enable_constr_who_may_receive = true,
            enable_constr_producer_single_outlink = true,
            enable_constr_who_may_send = true,
            enable_constr_distributor_single_inlink = true,
            enable_constr_distributor_sanity = true,
            enable_objective_func = true, // minimization criteria
            enable_elim_link_capacity = true,
            enable_elim_intrasite = true,
            enable_elim_noapp = true,
            enable_distributor_cycle_avoidance = true,
            enable_distance_based_cycle_avoidance = false,
            enable_optimize = false,
            enable_write = true;
    public String write_dir = "",
            write_type = ".lp";

    @Override
    public String toString() {
        return "MatchMakerGurobiConfig{" +
                "enable_constr_iface_capacity=" + enable_constr_iface_capacity +
                ", enable_constr_link_total_capacity=" + enable_constr_link_total_capacity +
                ", enable_constr_single_stream_link_capacity=" + enable_constr_single_stream_link_capacity +
                ", enable_constr_forbid_direct_links=" + enable_constr_forbid_direct_links +
                ", enable_paper_links_number=" + enable_constr_links_number +
                ", enable_constr_consumer_single_inlink=" + enable_constr_consumer_single_inlink +
                ", enable_constr_who_may_receive=" + enable_constr_who_may_receive +
                ", enable_constr_producer_single_outlink=" + enable_constr_producer_single_outlink +
                ", enable_constr_who_may_send=" + enable_constr_who_may_send +
                ", enable_constr_distributor_single_inlink=" + enable_constr_distributor_single_inlink +
                ", enable_constr_distributor_sanity=" + enable_constr_distributor_sanity +
                ", enable_objective_func=" + enable_objective_func +
                ", enable_elim_link_capacity=" + enable_elim_link_capacity +
                ", enable_elim_intrasite=" + enable_elim_intrasite +
                ", enable_elim_noapp=" + enable_elim_noapp +
                ", enable_distributor_cycle_avoidance=" + enable_distributor_cycle_avoidance +
                ", enable_optimize=" + enable_optimize +
                ", enable_write=" + enable_write +
                ", write_dir=" + write_dir +
                ", write_type=" + write_type +
                '}';
    }
}
