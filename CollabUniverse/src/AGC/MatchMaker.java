package AGC;

import NetworkRepresentation.MapVisualizer;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * MatchMakerUtils class is responsible for matching of available network topology with available MediaApplications in the network.
 * It uses constranit-based approach for scheduling MediaStreams onto specific links.
 * <p/>
 * BEWARE OF OGRES! This class is not thread safe and even worse, it requires that results are obtained while the
 * network topology hasn't changed in meantime! This will improve later as this is just a fast prototype.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 17:55:44
 */
public abstract class MatchMaker {

    static Logger logger = Logger.getLogger(MatchMaker.class);

    /**
     * Do the actual matching and create the master plan.
     * <p/>
     *
     * @return true if match was found, otherwise false
     */
    public abstract boolean doMatch();

    /**
     * Get result of scheduling in form of plan.
     * <p/>
     *
     * @return plan expressed as array of PlanElements
     */
    public abstract ArrayList<PlanElement> getPlan();

    /**
     * Get vizualization of resulting plan.
     * <p/>
     * @return MapVisualizer object with scheduled streams included
     */
    public abstract MapVisualizer getMapVizualization();

}
