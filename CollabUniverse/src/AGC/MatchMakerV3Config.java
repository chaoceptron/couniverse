package AGC;

/**
 * Configuration specific to MatchMakerV3
 * 
 * User: hopet
 * Date: 26.11.2009
 * Time: 8:05:40
 * To change this template use File | Settings | File Templates.
 */
public class MatchMakerV3Config {
    // for evaluation
    public boolean enable_paper_eq_1 = true,
            enable_paper_eq_2 = true,
            enable_paper_eq_3 = true,
            enable_paper_eq_4 = true,
            enable_paper_eq_5 = true,
            enable_paper_eq_6 = true,
            enable_paper_eq_7 = true,
            enable_paper_eq_8 = true,
            enable_paper_eq_9 = true,
            enable_paper_eq_10 = true,
            enable_paper_eq_11 = true,
            enable_paper_eq_12a = false,
            enable_paper_eq_12b = true,
            enable_paper_eq_13 = true, // minimization criteria
            enable_paper_eq_14 = true, // unused
            enable_paper_eq_15 = true,
            enable_paper_eq_16 = true,                                                             
            enable_paper_eq_17 = true,
            enable_paper_eq_18 = true,
            enable_distributor_cycle_avoidance = true,
            enable_distance_based_cycle_avoidance = false,
            tuple_check = false,
            tuple_check_coverity_only = false,
            find_all_solutions = true,
            custom_selector = false,
            custom_selector_2 = false,
            custom_selector_3 = false,
            custom_selector_4 = false,
            custom_selector_5 = false,
            custom_selector_6 = false,
            custom_selector_7 = true,
            custom_selector_8 = false,
            decreasing_order = true;


    @Override
    public String toString() {
        return "MatchMakerV3Config{" +
                "enable_paper_eq_1=" + enable_paper_eq_1 +
                ", enable_paper_eq_2=" + enable_paper_eq_2 +
                ", enable_paper_eq_3=" + enable_paper_eq_3 +
                ", enable_paper_eq_4=" + enable_paper_eq_4 +
                ", enable_paper_eq_5=" + enable_paper_eq_5 +
                ", enable_paper_eq_6=" + enable_paper_eq_6 +
                ", enable_paper_eq_7=" + enable_paper_eq_7 +
                ", enable_paper_eq_8=" + enable_paper_eq_8 +
                ", enable_paper_eq_9=" + enable_paper_eq_9 +
                ", enable_paper_eq_10=" + enable_paper_eq_10 +
                ", enable_paper_eq_11=" + enable_paper_eq_11 +
                ", enable_paper_eq_12a=" + enable_paper_eq_12a +
                ", enable_paper_eq_12b=" + enable_paper_eq_12b +
                ", enable_paper_eq_13=" + enable_paper_eq_13 +
                ", enable_paper_eq_14=" + enable_paper_eq_14 +
                ", enable_paper_eq_15=" + enable_paper_eq_15 +
                ", enable_paper_eq_16=" + enable_paper_eq_16 +
                ", enable_paper_eq_17=" + enable_paper_eq_17 +
                ", enable_paper_eq_18=" + enable_paper_eq_18 +
                ", enable_distributor_cycle_avoidance=" + enable_distributor_cycle_avoidance +
                ", enable_distance_based_cycle_avoidance=" + enable_distance_based_cycle_avoidance +
                ", tuple_check=" + tuple_check +
                ", tuple_check_coverity_only=" + tuple_check_coverity_only +
                ", find_all_solutions=" + find_all_solutions +
                ", custom_selector=" + custom_selector +
                ", custom_selector_2=" + custom_selector_2 +
                ", custom_selector_3=" + custom_selector_3 +
                ", custom_selector_4=" + custom_selector_4 +
                ", custom_selector_5=" + custom_selector_5 +
                ", custom_selector_6=" + custom_selector_6 +
                ", custom_selector_7=" + custom_selector_7 +
                ", custom_selector_8=" + custom_selector_8 +
                ", decreasing_order=" + decreasing_order +
                '}';
    }
}