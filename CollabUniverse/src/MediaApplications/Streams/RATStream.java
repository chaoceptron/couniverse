package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * Description of Robust Audio Tools (RAT) streams
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 12:21:28
 */
public class RATStream {

    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("RAT audio stream", 8000L, 1900000L, 80, 1900000L, 8.0f));
            }
        };
    }

}
