package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * VIdeo Conferencing (VIC) stream description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 12:21:49
 */
public class VICStream {
    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("VIC video stream", 200000L, 10000000L, 80, 10000000L, 5.0f));
            }
        };
    }

}
