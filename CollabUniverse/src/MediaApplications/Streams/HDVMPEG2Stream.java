package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * HDV MPEG-2 TS stream description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 13:30:14
 */
public final class HDVMPEG2Stream {

    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("HDV MPEG-2 TS stream", 25000000L, 25000000L, 1000, 30000000L, 7.0f));
            }
        };
    }

}
