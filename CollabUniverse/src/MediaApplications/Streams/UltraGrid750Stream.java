package MediaApplications.Streams;

import MediaAppFactory.MediaStream;

import java.util.HashSet;

/**
 * UltraGrid 1080i 750Mbps stream description
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 12:19:04
 */
public final class UltraGrid750Stream {

    public static HashSet<MediaStream> getMyStreams() {
        return new HashSet<MediaStream>() {
            {
                add(new MediaStream("UltraGrid 750 stream", 750000000L, 750000000L, 80, 900000000L, 7.5f));
            }
        };
    }

}
