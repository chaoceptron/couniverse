package MediaApplications;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationProducer;
import MediaAppFactory.MediaApplicationVideo;
import MediaApplications.Streams.HDVMPEG2Stream;
import NetworkRepresentation.CoNMLInterface;
import NetworkRepresentation.CoNMLDecodeParameters;
import NetworkRepresentation.CoNMLDecodeResult;
import NetworkRepresentation.CoNMLEncodeParameters;
import NetworkRepresentation.CoNMLEncodeResult;
import NetworkRepresentation.CoNMLExtensionNodes;
import NetworkRepresentation.EndpointNetworkNode;
import nu.xom.Element;

/**
 * Producer of HDV MPEG-2 TS streams
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 11:35:56
 */
public class HDVMPEG2Producer extends MediaApplication implements MediaApplicationVideo, MediaApplicationProducer, CoNMLInterface {
    private String targetIP;
    private EndpointNetworkNode targetNetworkNode;

    public HDVMPEG2Producer() {
        super("HDV MPEG-2 TS producer, 25Mbps", HDVMPEG2Stream.getMyStreams());
    }

    public HDVMPEG2Producer(String applicationName) {
        super(applicationName, HDVMPEG2Stream.getMyStreams());
    }

    @Override
    public void setTargetIP(String targetIP) {
        this.targetIP = targetIP;
    }

    @Override
    public String getTargetIP() {
        return this.targetIP;
    }

    public void setTargetNetworkNode(EndpointNetworkNode networkNode) {
        this.targetNetworkNode = networkNode;
    }

    public EndpointNetworkNode getTargetNetworkNode() {
        return this.targetNetworkNode;
    }
    
    @Override
    public CoNMLExtensionNodes encodeToCoNML(CoNMLEncodeResult result, CoNMLEncodeParameters parameters) {        
        CoNMLExtensionNodes extNodes = super.encodeToCoNML(result, parameters);        
        if(extNodes == null){
            return null;
        }
        
        Element baseElementNode = new Element("cou:HDVMPEG2Producer", CoNMLInterface.CONML_NS);
        ((Element) extNodes.getSubclassExtensionNode()).appendChild(baseElementNode); 
        
        if(this.targetNetworkNode != null){
            Element targetNodeElem = new Element("cou:target", CoNMLInterface.CONML_NS);                
            targetNodeElem.appendChild(this.targetNetworkNode.getUuid()); 
            baseElementNode.appendChild(targetNodeElem);
        }
        
        if(this.targetIP != null){
            Element targetIPElem = new Element("cou:targetIP", CoNMLInterface.CONML_NS);                
            targetIPElem.appendChild(this.targetIP); 
            baseElementNode.appendChild(targetIPElem);
        }
        
        return new CoNMLExtensionNodes(baseElementNode, baseElementNode, null);
    }
    
    @Override
    public void decodeFromCoNML(CoNMLDecodeResult result, CoNMLDecodeParameters parameters){
        if(result == null){
            throw new NullPointerException("CoNMLDecodeResult cannot be null!"); 
        }        
        if(parameters == null){
            throw new NullPointerException("CoNMLDecodeParameters cannot be null!"); 
        }
        
        super.decodeFromCoNML(result, new CoNMLDecodeParameters(parameters.getCurrentNode().getParent()));
        
        Element targetElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("target", CoNMLInterface.CONML_NS);
        if(targetElem != null){
            EndpointNetworkNode decoded = (EndpointNetworkNode) result.getDecodedNodes().get(targetElem.getValue());//the presence of target node is assumed!!! (dummy uninitialized instance will do)
            this.targetNetworkNode = decoded;
        }
        
        Element targetIPElem = ((Element) parameters.getCurrentNode()).getFirstChildElement("targetIP", CoNMLInterface.CONML_NS);
        if(targetIPElem != null){
            this.targetIP = targetIPElem.getValue();
        }   
    }
}
