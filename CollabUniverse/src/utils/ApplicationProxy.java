package utils;

import myGUI.ControllerFrame;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

/**
 * This is an application proxy class to facilitate running and terminating external applications,
 * e.g. by local peer agents as initiated by Application Group Controller.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 27.7.2007
 * Time: 11:54:56
 */

public class ApplicationProxy {
    static Logger logger = Logger.getLogger("utils");

    /**
     * This is an auxiliary exception class to support typed exceptions from utils.ApplicationProxy
     */
    @SuppressWarnings({"NonStaticInnerClassInSecureContext"})
    public class ApplicationProxyException extends Throwable {
        /**
         * Exception constructor.
         *
         * @param message giving the reason of exception being raised
         */
        public ApplicationProxyException(String message) {
            super(message);
        }
    }

    private String path;  // file to exec
    private String opts;  // command line options
    private boolean monitored;  // flag, whether the application is monitored
    private Process proc;  // Process object used to run the application
    private AtomicBoolean terminated = new AtomicBoolean(true);  // marks that the process has terminated
    private boolean killed;  // flag, whether the application was forcibly killed on its termination
    private int retvalue;  // exit code of the application
    private ControllerFrame localControllerFrame = null;
    private Thread stdOutReader;
    private Thread stdErrReader;


    /**
     * A private method checking whether the application is still running.
     * Intended for use in checkRunning and kill methods.
     *
     * @return true if the application is running, otherwise false
     */

    synchronized private boolean isRunning() {
        try {
            this.retvalue = this.proc.exitValue();
        } catch (NullPointerException e) {
            // Process was never invoked
            terminated.set(true);
        } catch (IllegalThreadStateException e) {
            // TODO: is this a correct way to check?
            terminated.set(false);
        }
        return !terminated.get();
    }

    /**
     * A constructor that creates an instance of application proxy.
     *
     * @param path      file to exec including the path
     * @param opts      command line options passed on to the file
     * @param monitored specifies whether the application will be monitored
     * @throws ApplicationProxyException when file to exec doesn't exist or is not accesible
     */

    public ApplicationProxy(String path, String opts, boolean monitored) throws ApplicationProxyException {
        this.path = path;
        this.opts = opts;
        this.monitored = monitored;
        this.proc = null;

        File f = new File(path);
        if (!f.canRead()) {
            throw new ApplicationProxyException("The requested file " + path + " doesn't exist or is not readable!");
        }
        // TODO: test the path is executable
    }


    /**
     * This runs (starts) the application.
     *
     * @throws ApplicationProxyException when starting the application fails for some reason
     */

    synchronized public void run() throws ApplicationProxyException {
        Runtime runtime = Runtime.getRuntime();
        // final String cmdLine = "./appwrapper " + this.path + " " + this.opts;
        final String cmdLine = this.path + " " + this.opts;
        try {
            this.proc = runtime.exec(cmdLine);
            ApplicationProxy.logger.debug("Started process " + this.proc);
            terminated.set(false);
        } catch (IOException e) {
            throw new ApplicationProxyException("Failed to exec the application: " + e.getMessage());
        }

        if (localControllerFrame != null) {
            // we have GUI
            final ApplicationProxy ap = this;
            //noinspection EmptyCatchBlock
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        localControllerFrame.writeCmd(ap, cmdLine);
                    }
                });
            } catch (InterruptedException e) {
            } catch (InvocationTargetException e) {
            }
            stdErrReader = new Thread(new Runnable() {
                public void run() {
                    BufferedReader b = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                    String str;
                    //noinspection EmptyCatchBlock
                    try {
                        while (!terminated.get()) {
                            str = b.readLine();
                            if (str == null) {
                                break;
                            }
                            final String str1 = str;
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    localControllerFrame.writeStdErr(ap, str1);
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            stdOutReader = new Thread(new Runnable() {
                public void run() {
                    BufferedReader b = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                    String str;
                    //noinspection EmptyCatchBlock
                    try {
                        while (!terminated.get()) {
                            str = b.readLine();
                            if (str == null) {
                                break;
                            }
                            final String str1 = str;
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    localControllerFrame.writeStdOut(ap, str1);
                                }
                            });
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            // we don't have GUI, thus using console
            System.out.println(this.toString() + " cmd: " + cmdLine);
            stdErrReader = new Thread(new Runnable() {
                public void run() {
                    BufferedReader b = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
                    String str;
                    //noinspection EmptyCatchBlock
                    try {
                        while (!terminated.get()) {
                            str = b.readLine();
                            if (str == null) {
                                break;
                            }
                            System.err.println(this.toString() + " stderr: " + str);
                            System.err.flush();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.err.flush();
                    //noinspection EmptyCatchBlock
                    try {
                        b.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            stdOutReader = new Thread(new Runnable() {
                public void run() {
                    BufferedReader b = new BufferedReader(new InputStreamReader(proc.getInputStream()));
                    String str;
                    //noinspection EmptyCatchBlock
                    try {
                        while (!terminated.get()) {
                            str = b.readLine();
                            if (str == null) {
                                break;
                            }
                            System.out.println(this.toString() + " stdout: " + str);
                            System.out.flush();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.flush();
                    //noinspection EmptyCatchBlock
                    try {
                        b.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        stdErrReader.start();
        stdOutReader.start();

        this.killed = false;
    }

    /**
     * This forcibly terminates the application.
     *
     * @return true if the application had to be terminated forcibly and false if it already terminated spontaneously
     *         prior to calling this method
     */
    @SuppressWarnings({"EmptyCatchBlock"})
    synchronized public boolean kill() {
        // try to terminate stdout and stderr readers first
        try {
            ApplicationProxy.logger.trace("Closing input stream");
            proc.getInputStream().close();
            ApplicationProxy.logger.trace("Closing error stream");
            proc.getErrorStream().close();
            ApplicationProxy.logger.trace("Waiting for STDOUT reading thread");
            stdOutReader.join(500);
            ApplicationProxy.logger.trace("Waiting for STDERR reading thread");
            stdErrReader.join(500);
        } catch (NullPointerException e) {
        } catch (InterruptedException e) {
            if (ApplicationProxy.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            ApplicationProxy.logger.error("Application Proxy: Failed to join stdOutReader or stdErrReader thread!");
        } catch (IOException e) {
            if (ApplicationProxy.logger.isDebugEnabled()) {
                e.printStackTrace();
            }
            ApplicationProxy.logger.error("Application Proxy: Failed to join stdOutReader or stdErrReader thread!");
        }

        // now we will take care of the process itself
        if (this.isRunning()) {
            try {
                ApplicationProxy.logger.debug("Destroying process " + this.proc);
                this.proc.destroy();
                int retval = this.proc.waitFor();
                ApplicationProxy.logger.debug("Process " + this.proc + " destroyed with retrun value " + retval + ".");
            } catch (NullPointerException e) {
            } catch (InterruptedException e) {
            }
            this.killed = true;
            this.terminated.set(true);
        }
        return this.killed;
    }

    /**
     * Checks whether the application is still running. It is only supported if the proxy has been initialized
     * with monitoring flag enabled.
     *
     * @return true if the application is running, otherwise false
     * @throws ApplicationProxyException when attempting to monitor an application with monitoring flag disabled
     */
    synchronized public boolean checkRunning() throws ApplicationProxyException {
        if (!this.monitored) {
            throw new ApplicationProxyException("This application proxy doesn't support monitoring.");
        }
        return this.isRunning();
    }

    synchronized public int getRetvalue() {
        return retvalue;
    }

    synchronized public String getOpts() {
        return opts;
    }

    synchronized public void setOpts(String opts) {
        this.opts = opts;
    }

    synchronized public String getPath() {
        return path;
    }

    synchronized public void setPath(String path) {
        this.path = path;
    }

    synchronized public ControllerFrame getLocalControllerFrame() {
        return localControllerFrame;
    }

    synchronized public void setLocalControllerFrame(ControllerFrame localControllerFrame) {
        this.localControllerFrame = localControllerFrame;
    }

    public boolean isMonitored() {
        return monitored;
    }
}
