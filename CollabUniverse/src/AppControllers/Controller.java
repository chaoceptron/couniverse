package AppControllers;

import MediaAppFactory.MediaApplication;
import utils.ApplicationProxy;
import org.apache.log4j.Logger;
import myGUI.ControllerFrame;

/**
 * Controller for steering applications on network nodes. This is an abstract class which
 * futher needs to be imlpemented by classes like LocalController (or RemoteController or whatever)
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 31.7.2007
 * Time: 16:39:08
 */
public abstract class Controller {

    MediaApplication application;  // reference to the media application to work with
    ControllerFrame controllerFrame = null;  // GUI interface if needed
    boolean isRunning = false;   // flag whether the application is running
    static Logger logger = Logger.getLogger(Controller.class);

    protected Controller() {
    }

    protected Controller(MediaApplication application) {
        this.application = application;
    }

    /**
     * Starts the application. setupApplication needs to be called prior to this
     *
     * @throws ApplicationProxy.ApplicationProxyException is thrown if application lauch in ApplicationProxy fails for any reason
     */
    public abstract void runApplication() throws ApplicationProxy.ApplicationProxyException;

    /**
     * Terminates the application
     */
    public abstract void stopApplication();
    
    public MediaApplication getApplication() {
        return application;
    }

    public void setApplication(MediaApplication application) {
        this.application = application;
    }

    public boolean isRunning() {
        return isRunning;
    }

    static void infoOnException(Throwable e, String s) {
        if (logger.isDebugEnabled()) {
            e.printStackTrace();
        }
        logger.error(s);
    }

    public ControllerFrame getControllerFrame() {
        return controllerFrame;
    }

    public void setControllerFrame(ControllerFrame controllerFrame) {
        this.controllerFrame = controllerFrame;
    }

    public abstract String toString();

}
