package AppControllers;

import MediaAppFactory.MediaApplication;
import MediaAppFactory.MediaApplicationProxy;
import NetworkRepresentation.EndpointNetworkNode;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import utils.ApplicationProxy;
import utils.ProxyNodeConnection;

/**
 * Application controller for steering applications running on a proxied node.
 * <p/>
 * User: Petr Holub (hopet@ics.muni.cz)
 * Date: 10.4.2009
 * Time: 15:25:02
 * To change this template use File | Settings | File Templates.
 */
public class ProxyController extends Controller {

    ProxyNodeConnection proxyNodeConnection;  // connection to the proxied host

    public ProxyController(MediaApplication application) {
        super(application);
        assert application instanceof MediaApplicationProxy;

        EndpointNetworkNode localNode = this.application.getParentNode();
        assert localNode.isProxyNode();
        proxyNodeConnection = localNode.getProxyNodeConnection();
    }

    @Override
    public void runApplication() throws ApplicationProxy.ApplicationProxyException {
        try {
            if (controllerFrame != null) {
                final ProxyController thisController = this;
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            controllerFrame.writeCmd(thisController, ((MediaApplicationProxy) application).getStartCommand() + " " + application.getApplicationCmdOptions());
                        }
                    });
                } catch (InterruptedException e) {
                    infoOnException(e, "Failed to write to a controller window!");
                } catch (InvocationTargetException e) {
                    infoOnException(e, "Failed to write to a controller window!");
                }
            }
            else {
                logger.info(this.toString() + ": " + ((MediaApplicationProxy) application).getStartCommand() + " " + application.getApplicationCmdOptions());
            }
            proxyNodeConnection.send(((MediaApplicationProxy) application).getStartCommand() + " " + application.getApplicationCmdOptions());
            isRunning = true;
        } catch (ProxyNodeConnection.ProxyNodeCommandFailed proxyNodeCommandFailed) {
            infoOnException(proxyNodeCommandFailed, "Failed to run proxied application!");
        }
    }

    @Override
    public void stopApplication() {
        if (!(application instanceof MediaApplicationProxy)) {
            logger.error("Application " + application + " is not a remote application, thus I don't know how to stop it!");
        }
        else {
            try {
                if (controllerFrame != null) {
                    final ProxyController thisController = this;
                    try {
                        SwingUtilities.invokeAndWait(new Runnable() {
                            @Override
                            public void run() {
                                controllerFrame.writeCmd(thisController, ((MediaApplicationProxy) application).getStopCommand());
                            }
                        });
                    } catch (InterruptedException e) {
                        infoOnException(e, "Failed to write to a controller window!");
                    } catch (InvocationTargetException e) {
                        infoOnException(e, "Failed to write to a controller window!");
                    }
                }
                else {
                    logger.info(this.toString() + ": " + ((MediaApplicationProxy) application).getStopCommand());
                }
            
                proxyNodeConnection.send(((MediaApplicationProxy) application).getStopCommand());
                isRunning = false;                
            } catch (ProxyNodeConnection.ProxyNodeCommandFailed proxyNodeCommandFailed) {
                infoOnException(proxyNodeCommandFailed, "Failed to stop proxied application!");
            }
        }
    }


    /**
     * Returns a string representation of the object. In general, the
     * <code>toString</code> method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * <p/>
     * The <code>toString</code> method for class <code>Object</code>
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `<code>@</code>', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        return super.application.getApplicationName() + " (" + super.application.getApplicationCmdOptions() + ") (from ProxyController)";
    }

}
